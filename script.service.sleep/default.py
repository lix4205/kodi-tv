#!/usr/bin/env python
# -*- coding: utf-8 -*-

import socket
#from resources.lib import websocket 
#from resources.lib import kodiKump 
import json
import os 
import xbmc
import xbmcgui
import xbmcaddon
#from select import select
#import time
#import hashlib

# Import modules from koditvm
import koditvm
from koditvm.QueriesSocket import QuerySocket
#from koditvm.ClientSocket import ClientSocket
from koditvm.KodiMove import KodiMove
from koditvm.kodiUtils import KodiTools
from koditvm.kodiUtils import KodiJson

try:
    from urllib.parse import parse_qs, urlencode
except ImportError:
    from urlparse import parse_qs
    from urllib import urlencode

#from resources.lib.ClientSocket import ClientSocket
#from resources.lib.JumpTimeLine import Jump
#from resources.lib.KodiMove import KodiMove
#from resources.lib import kodiUtils

# BEGIN Main class TODO : Lui donner un vrai nom !
class MemoryPlayer(xbmc.Player) :
    
#   BEGIN Init
    """
    Lance xbmc.Monitor() pour capter les évênements 
    """
    def Monitor(self, host, port, sleep_mode = True) :
        monitor = xbmc.Monitor()
        logXbmc( '### Init ###' )
        xbmc.Player.__init__( self )
        self.PORT_SOCKET = port
        self.HOST_SOCKET = host
        self.socket = QuerySocket(self.HOST_SOCKET, self.PORT_SOCKET)

        self.sleep_active = False
        self.sleep_mode = sleep_mode

        while not monitor.abortRequested():
            if monitor.waitForAbort(10):
                break
#   END Init

#   BEGIN Main functions
    """
    Vérifie si on est dans le dossier de lecture, redirige et synchronise au besoin
    """
    def setEncours(self, path_file, path2remove, l_socket, protocol) :
        # Requete sur le socket
        # Et genere les liens en cours
        elem = self.CallGenerateLinks(path_file.replace(path2remove, ""), l_socket, protocol)
        if "error" in elem :
            logXbmc("[ERR] : " + str(elem) )
            return False
        
        if "list" in elem :
            return elem["list"][KEEP_FILES_NUMBER:]

        return []
        
#   END Main

#   BEGIN kodi events
    def onPlayBackStarted(self):
        #self.STOPPED = False
        path_video = xbmc.getInfoLabel('Player.FileNameAndPath')
        self.LAST_PLAYED = os.path.basename(path_video)
        
        # On récupère les infos du fichiers joué
        info_filesrv = KodiTools.GetPathToRemove(path_video)
        path2remove = info_filesrv[0]
        SERVEUR = info_filesrv[1]

        self.RUNNING = path_video.replace(path2remove, "")
        # On détermine le socket
        #logXbmc("ADDR1 : " + str(self.socket.ADDR) + "|" + SERVEUR + "==" + BACKUP_SOURCE_IP + " EPS :: " + self.RUNNING)
        if SERVEUR != self.socket.ADDR[0] and BACKUP_SOURCE_IP.split(":")[0] == SERVEUR :
            a_host = BACKUP_SOURCE_IP.split(":")
            l_socket = QuerySocket(a_host[0], int(a_host[1]))
        else :
            l_socket = self.socket

        #logXbmc("ADDR2 : " + str(info_filesrv[2]))

        ##if not self.MOVED :
        truc = self.setEncours(path_video, path2remove, l_socket, info_filesrv[2])
        DEFAULT_TIMES_SLEEP = []
        if len(truc) > 0 :
            #logXbmc("SET EN COURS :" + path_video + " from " + SERVEUR + ":" + str( self.PORT_SOCKET)) # + " :: " + str(self.INDEX_PLAYLIST))
            #self.INDEX_PLAYLIST += 1
            #logXbmc("Synchro socket done !")
            #logXbmc(path2remove)
            idx = 1
            for eps in truc :
                #logXbmc(path2remove + eps[0])
                #logXbmc(json.loads(xbmc.executeJSONRPC('{"jsonrpc":"2.0", "method":"Files.GetFileDetails", "params":{"file":"nfs://192.168.0.30/nfs/data/videos/ANIMES/Les Simpson/S15 2003/01 - Simpson Horror Show 14.avi", "media": "video",  "properties" : ["runtime"]}, "id":1}')))
                strJson = path2remove + eps[0]
                #logXbmc("STR :: " + strJson)
                resultTime = json.loads(xbmc.executeJSONRPC('{"jsonrpc":"2.0", "method":"Files.GetFileDetails", "params":{"file":"'+strJson+'", "media": "video",  "properties" : ["runtime"]}, "id":'+str(idx)+'}'))
                #logXbmc(resultTime)
                if "runtime" in resultTime["result"]["filedetails"] :
                    if len(DEFAULT_TIMES_SLEEP) > 0 :
                        #logXbmc(DEFAULT_TIMES_SLEEP[idx - 2])
                        DEFAULT_TIMES_SLEEP.append(str(int(int(int(resultTime["result"]["filedetails"]["runtime"]) + 60) / 60) + int(DEFAULT_TIMES_SLEEP[idx - 2])))
                    else :
                        l_cur = 0
                        if xbmc.Player().isPlaying() :
                            cur_time = 0
                            while cur_time < 0.5 :
                                cur_time = xbmc.Player().getTime()

                            l_cur = int(cur_time / 60)
                            logXbmc(l_cur)
                        DEFAULT_TIMES_SLEEP.append(str(int(int(int(resultTime["result"]["filedetails"]["runtime"]) + 90) / 60) - l_cur))

                        #logXbmc(DEFAULT_TIMES_SLEEP[0])
                idx += 1
                if idx > 4 :
                    break

        logXbmc("DEFAULT_TIMES_SLEEP :" + str(DEFAULT_TIMES_SLEEP))
        if self.sleep_mode :
            if xbmc.Player().isPlaying() :
                l_tot = 0
                while l_tot == 0 :
                    l_tot = int(xbmc.Player().getTotalTime() / 60)

                if not len(DEFAULT_TIMES_SLEEP) > 0 :
                    DEFAULT_TIMES_SLEEP = [ str(l_tot + 5), str(l_tot * 2 + 10), str(l_tot * 3 + 15), str(l_tot * 4 + 20), "Now", "Custom" ]
                else :
                    DEFAULT_TIMES_SLEEP.append("Now")
                    DEFAULT_TIMES_SLEEP.append("Custom")

                if (self.sleep_active and xbmcgui.Dialog().yesno("Modifier le temps avant extinction ?", "Etes vous sur ?", "Non", "Oui", 5000)) or not self.sleep_active :
                    dialog = xbmcgui.Dialog()
                    ret = dialog.contextmenu(DEFAULT_TIMES_SLEEP)
                    #logXbmc(ret)
                    #l_time2sleep = DEFAULT_TIMES_SLEEP[ret]
                    if ret > -1 :
                        # Now
                        # xbmc.executebuiltin('XBMC.CECStandby()')
                        if ret == len(DEFAULT_TIMES_SLEEP) - 2 :
                            xbmc.executebuiltin('AlarmClock("quit", PlayerControl("stop"), "00:05")')
                            logXbmc("END")
                        elif ret == len(DEFAULT_TIMES_SLEEP) - 1 :
                            xbmc.executebuiltin('AlarmClock("quit", PlayerControl("stop"))')
                        else :
                            xbmc.executebuiltin('AlarmClock("quit", PlayerControl("stop"), "'+DEFAULT_TIMES_SLEEP[ret]+':00")')
                            if int(DEFAULT_TIMES_SLEEP[ret]) > 30 :
                                cmd = 'AlarmClock("quitMsg", Notification("Extinction dans : ", "30:00"), "' + str(int(DEFAULT_TIMES_SLEEP[ret])-30) + ':00")'
                                xbmc.executebuiltin(cmd)
                        self.sleep_active = True
                    else :
                        if xbmcgui.Dialog().yesno("Annuler l'extinction planifiée ?", "Etes vous sur ?",  "Non", "Oui", 5000) :
                            self.sleep_mode = False
                            xbmc.executebuiltin('CancelAlarm("quit")')
                            xbmc.executebuiltin('CancelAlarm("quitMsg")')
        #else :
            #self.MOVED = False

    def onPlayBackStopped(self):
        self.sleep_mode = True
                
#   END kodi events

    """
    Call socket to get video list
    """
    def CallGenerateLinks(self, path_video, l_socket, protocol) :
        try :
            return l_socket.CallGenerateLinks(path_video, protocol, MAIN_SOURCE_DIR, KEEP_FILES_NUMBER, SKIP_FIRSTS_DIR) #.decode()
        except Exception as e:
            #logXbmc("WWFFTT" + str(e))
            return {"error": str(e) }

def logXbmc(log_str) :
    xbmc.log(REAL_ADDON + " " + str(log_str), 1)


DIR_ENCOURS_NAME = "ENCOURS"

IGNORE_FILE = [ ".directory", "Thumb.db", ".log", ".srt" ]

#ADDON_ID_FK = 'service.koditv'

#kodi_source = 
MAIN_SOURCE_PROTOCOL = "nfs"

#MAIN_SOURCE_DIR = "/nfs/data/videos"
FILENAME_AUTOREAD = DIR_ENCOURS_NAME
FILENAME_AUTOREAD_EXT = ".mp4"
#MAIN_SOURCE_PORT = "2049"
#ADDON = KodiTools.GetAddon()

ADDON = "plugin.video.kodi2tv"
ADDON_ID_SYNC = 'plugin.video.tvSkip'
REAL_ADDON = xbmcaddon.Addon("script.service.sleep").getAddonInfo('name')


MAIN_SOURCE_IP = xbmcaddon.Addon(ADDON).getSetting("ip_host")
#MAIN_SOURCE_ADDR = MAIN_SOURCE_PROTOCOL + "://" + MAIN_SOURCE_IP


BACKUP_SOURCE_IP = xbmcaddon.Addon(ADDON).getSetting("ip_back")
# BACKUP_SOURCE_IP = KodiTools.GetSetting(ADDON, "ip_back")
##LOCAL_SOURCE_PATH = os.path.expanduser('~user') + ""

MAIN_SOURCE_PORT = xbmcaddon.Addon(ADDON).getSetting("kodi_port")
MAIN_SOURCE_DIR = xbmcaddon.Addon(ADDON).getSetting("main_path_video")
KEEP_FILES_NUMBER = int(xbmcaddon.Addon(ADDON).getSetting("keep_lasts"))

##logXbmc("FFF" + str(KEEP_FILES_NUMBER))
SKIP_FIRSTS_DIR = int(xbmcaddon.Addon(ADDON).getSetting("keep_dirs"))


logXbmc(json.loads(xbmc.executeJSONRPC('{"jsonrpc":"2.0", "method":"Files.GetFileDetails", "params":{"file":"nfs://192.168.0.30/nfs/data/videos/ANIMES/Les Simpson/S15 2003/01 - Simpson Horror Show 14.avi", "media": "video",  "properties" : ["runtime"]}, "id":1}')))

MemoryPlayer().Monitor(MAIN_SOURCE_IP, int(MAIN_SOURCE_PORT), True)


logXbmc('END')
