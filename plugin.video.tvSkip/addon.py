# -*- coding: utf-8 -*-
import sys
#import json
import xbmc
import xbmcplugin
import xbmcaddon
#import xbmcvfs
import xbmcgui
from xbmcplugin import addDirectoryItem, endOfDirectory
#from koditvm.ClientSocket import ClientSocket
from koditvm.Constants import CmdSocket
from koditvm.QueriesSocket import QuerySocket
from koditvm.KodiMove import KodiMove
import os
import time

try:
    from urllib.parse import parse_qs, urlencode
except ImportError:
    from urlparse import parse_qs
    from urllib import urlencode

#addon = xbmcaddon.Addon()

# Parameters comes from service.koditv
#param_addon_id = addon.getSetting("id")
param_addon = xbmcaddon.Addon("plugin.video.kodi2tv")

MAIN_SOURCE_IP = param_addon.getSetting('ip_host')
KEEP_FILES_NUMBER = int(param_addon.getSetting('keep_lasts')) + 1
SKIP_FIRSTS_DIR = int(param_addon.getSetting('keep_dirs'))
MAIN_SOURCE_DIR = param_addon.getSetting('main_path_video')
MAIN_SOURCE_PORT = int(param_addon.getSetting("kodi_port"))

OTHER_SOURCE = str(param_addon.getSetting("ip_back")).split(":")
OTHER_SOURCE_PORT = MAIN_SOURCE_PORT
OTHER_SOURCE_IP = OTHER_SOURCE[0]
if OTHER_SOURCE != "" and len(OTHER_SOURCE) > 1 :
    OTHER_SOURCE_PORT = OTHER_SOURCE[1]
#xbmc.log(OTHER_SOURCE_PORT, 1)

MAIN_SOURCE_PROTOCOL = "nfs"
DIR_ENCOURS_NAME = "ENCOURS"
MAIN_SOURCE_ADDR = MAIN_SOURCE_PROTOCOL + "://" + MAIN_SOURCE_IP

#DIR_ENCOURS_PATH = os.path.join(MAIN_SOURCE_DIR, DIR_ENCOURS_NAME)

# Ignore IP local/ array
IGNORE_IP_LOCAL = [ "127.0.0.1", "localhost" ]

#KEEP_FILES_NUMBER = 1
#SKIP_FIRSTS_DIR   = 0
#MAIN_SOURCE_DIR   = "/nfs/data/videos"
#MAIN_SOURCE_IP = "192.168.0.5"
#MAIN_SOURCE_DIR = kodiUtils.KodiTools.GetSetting(ADDON, "main_path_video")
#KEEP_FILES_NUMBER = int(kodiUtils.KodiTools.GetSetting(ADDON, "keep_lasts")) + 1


xbmc.log(str(sys.argv), 1)
addon_handle = int(sys.argv[1])
args = parse_qs(sys.argv[2][1:])
#xbmc.log(str(args), 1)
menu = args.get('menu', None)
saga = args.get('saga', None)
episode = args.get('episode', None)
play = args.get('play', False)
folder = args.get('folder', "")
addr = args.get('addr', None)
index = args.get('index', None)
count = args.get('count', 0)

if folder != "" :
    MAIN_SOURCE_DIR = folder[0]

DIR_ENCOURS_PATH = os.path.join(MAIN_SOURCE_DIR, DIR_ENCOURS_NAME)
#try: list_size = int(addon.getSetting('list_size'))
#except Exception: list_size=0
#try: top_size = int(addon.getSetting('top_size'))
#except Exception: top_size=0
#single_list = addon.getSetting('single_list')
#show_date = addon.getSetting('show_date')
#show_time = addon.getSetting('show_time')
#enable_debug = addon.getSetting('enable_debug')
#lang = addon.getLocalizedString
#if addon.getSetting('custom_path_enable') == "true" and addon.getSetting('custom_path') != "":
    #txtpath = addon.getSetting('custom_path')
#else:
    #txtpath = xbmc.translatePath(addon.getAddonInfo('profile'))
#txtfile = txtpath + "lastPlayed.json"
#imgPath=xbmcvfs.translatePath(xbmcaddon.Addon().getAddonInfo('path'))
#group_by_type=lang(30018)

def url(pQuery):
    #xbmc.log("URL:" + sys.argv[0] + '?' + urlencode(pQuery), 3)
    return sys.argv[0] + '?' + urlencode(pQuery)

def Right(s, amount):
    return s[-amount:]

"""
List files from a directory on server
"""
def list_items(selGroup, episode = "", index = -1):
    if selGroup == "*" :
        return
    last_played = CallPlaylist(SKIP_FIRSTS_DIR, selGroup)
    MAIN_SOURCE_PATH = MAIN_SOURCE_PROTOCOL + "://" + socket.ADDR[0]
    xbmc.log( "ADDR : " + str(last_played) , 1)


    xbmcplugin.setContent(addon_handle, "files")
    #if xbmcvfs.exists(txtfile):
        #f = xbmcvfs.File(txtfile)
        #nbr=0 # nbr of line on screen (selected)
    #idx=int(last_played["index"]) # idx of line on the json file
        #try: lines = json.load(f)
        #except: lines = []
    #serie_name = last_played.keys()
    #if "list" in last_played :
    for line in last_played :
        #allo = last_played.get("list")
        #xbmc.log(str(allo), 1)
        #if nbr>=nbrLines: break
        #if group_by == group_by_type: group = line["type"]
        #else: group = line["source"]
        #if len(line)>3 and (group==selGroup or selGroup=='*'):
        #nbr=nbr+1
        desc = os.path.basename(line)
        #desc = Right("0000" + str(idx), 4) + "_" + os.path.basename(line[0])

        #path = MAIN_SOURCE_DIR + "/ENCOURS/" + last_played["name"] + "/" + desc
        #if show_date == "true": desc = desc + line["date"].strip() + ' '
        #if show_time == "true": desc = desc + line["time"].strip() + ' '
        show = ''
        #if 'show' in line: show=line["show"] + " "
        #if 'season' in line and line["season"]!='' and str(line["season"])!="-1":
            #if 'episode' in line and line["episode"]!='' and str(line["episode"])!="-1":
                #show = show + str(line["season"])+"x"+str(line["episode"]) + " "
        #desc=desc + show + line["title"]  + " file"
        #if 'artist' in line and line["artist"]!='': desc=desc+" - "+str(line["artist"])
        #xpath=""
        infolabels={'title': line } #, 'year': line['year'], "mediatype": line['type'], 'Top250': line['id']}
        li = xbmcgui.ListItem(label=desc)
        li.setInfo('video', infolabels)
        #li.setArt({ "poster" : line["thumbnail"].strip() })
        #li.setArt({ "thumbnail" : line["thumbnail"].strip() })
        #li.setArt({ "fanart" : line["fanart"].strip() })
        li.setProperty('IsPlayable', 'true')
        #command = []
        #command.append((lang(30008), "RunPlugin(plugin://plugin.video.last_played?menu=remove&id="+str(idx)+")"))
        #if line["file"][:6]=="plugin":
            #command.append((lang(30031)+line["source"], "PlayMedia(" + line["file"] + ")"))
        #li.addContextMenuItems(command)
        if socket.ADDR[0] in IGNORE_IP_LOCAL :
            xurl = line
        else :
            xurl = MAIN_SOURCE_PATH + line
        #xbmc.log(str(line), 3)
        #if "video" in line and line["video"]!="": xurl=line["video"]
        addDirectoryItem(addon_handle, xurl, li)
            #idx = idx + 1
        #f.close()
        #if single_list == "true" and nbr == 0:
            #li = ListItem(lang(30030))
            #li.setProperty('IsPlayable', 'false')
            #addDirectoryItem(addon_handle, "", li, isFolder = True)

        #l_path_saga = os.path.dirname(line[0])
        #l_saga = os.path.basename(os.path.dirname(os.path.dirname(line[0])))
        #l_saison = os.path.basename(os.path.dirname(line[0]))
        #l_url = Right("0000" + str(int(last_played["index"])), 4) + "_" + os.path.basename(line[0])

        ##xbmc.log("ALLO :: " + l_path_saga, 1)
        ##xbmc.log("ALLO :: " + l_saga, 1)
        ##xbmc.log("ALLO :: " + l_saison, 1)
        #xbmc.log("ALLO :: " + url({ "addr": socket.GetAddr(), "saga" : l_path_saga, "index" : int(last_played["index"]),  "episode" : l_url, "folder" : os.path.dirname(xurl), "count" : int(last_played["count"])}), 1)
        ##l_url = last_played["list"][0][0]
##"episode" : last_played[folder][line]
        #li = xbmcgui.ListItem(label=os.path.basename(l_saga))
        #li.setProperty('IsPlayable', 'false')
        #addDirectoryItem(addon_handle, url({ "addr": socket.GetAddr(), "saga" : l_saga+"|"+l_saison, "index" : int(last_played["index"]),  "episode" : l_url, "count" : int(last_played["count"])}), li, isFolder = True)

        #l_path_saga = os.path.dirname(xurl)
        #l_saga = os.path.dirname(os.path.dirname(xurl))
        #l_saison = os.path.basename(os.path.dirname(xurl))
        #li = xbmcgui.ListItem(label=os.path.basename(l_saga) + " | " + l_saison)
        #li.setProperty('IsPlayable', 'false')
        #addDirectoryItem(addon_handle, url({ "saga" : l_path_saga}), li)

        #l_saga = os.path.dirname(os.path.dirname(xurl))
        #li = xbmcgui.ListItem(label=os.path.basename(l_saga))
        #li.setProperty('IsPlayable', 'false')
        #addDirectoryItem(addon_handle, url({ "saga" : l_saga}), li)
    #else :
        #l_saga = selGroup
        #li = xbmcgui.ListItem(label=selGroup)
        #li.setProperty('IsPlayable', 'false')
        #addDirectoryItem(addon_handle, url({ "saga" : l_saga}), li)

    return last_played #["list"][0][0]
        #if episode != "" :

            #if SearchInDir(episode) :
                #time.sleep(1)
                #xbmc.executebuiltin('Action(Select)')
                #time.sleep(3)
                #if not xbmc.Player().isPlaying() :
                    #xbmc.executebuiltin('Action(Select)')

"""
List directories on server
"""
def list_groups():
    #window = xbmcgui.Window(xbmcgui.getCurrentWindowId())
    #window.setProperty('windowLabel', '')
    #window.setProperty('menu1', 'chiquita')


    last_played = CallLastPlayed()

    xbmc.log("WINID: " + str(last_played) + " " + str(socket.GetAddr()) ,3)
    if "error" in last_played :
        xbmc.executebuiltin('Notification("ERROR !", "' + str(last_played["error"]) + '" , 3000)')


    #if xbmcvfs.exists(txtfile):
        #groups = []
        #f = xbmcvfs.File(txtfile)
        #try:
            #lines = json.load(f);
        #except Exception:
            #lines = []
        ##xbmc.log(str(lines), 3)
    for line in sorted(last_played) :
        #if len(line)>5:
            #if group_by == group_by_type:
                #group = line["type"]
            #else:
                #group = line["source"]
        #if group not in groups:
        #groups.append(group)
        #group = line
        #if group_by == group_by_type:
            #nm = group + " srcv"
            #ic = imgPath+'/resources/' + group + '.png'
        #else:
        #xbmc.log("OKAYU" + str(last_played), 1)
    #for line in sorted(last_played[folder].keys()):
        nm = line
        #ads = group.split("/")
        #if len(ads) > 2: nm = ads[2]
        #if group==lang(30002): ic = imgPath+'/resources/movie.png'
        #elif group==lang(30003): ic = imgPath+'/resources/episode.png'
        #elif group==lang(30004): ic = imgPath+'/resources/musicvideo.png'
        #else:
            #try:
                #la = xbmcaddon.Addon(nm)
                #nm = la.getAddonInfo('name')
                #ic = la.getAddonInfo('icon')
            #except Exception:
                #ic = imgPath+'/resources/addons.png'
        li = xbmcgui.ListItem(nm)

        delete_cmd_url = url({'menu': "remove", 'id' : str(line), "addr" : socket.GetAddr()})
        delete_cmd = "RunPlugin("+ delete_cmd_url + ")" #url({'menu': "remove", 'id' : str(line), "addr" : SOCKET_ADDR})
        #delete_cmd = "plugin://plugin.video.koditv?menu=remove&id="+str(line)+"&addr=" + SOCKET_ADDR  #url({'menu': "remove", 'id' : str(line), "addr" : SOCKET_ADDR})

        #xbmc.log(delete_cmd, 1)
        #command = []
        #command.append(("Delete", "RunPlugin(" + delete_cmd + ")"))

        command = []
        command.append(("Delete", delete_cmd)) #"RunPlugin(plugin://plugin.video.koditv?menu=remove&id="+str(line)+")"))
        #if line["file"][:6]=="plugin":
            #command.append((lang(30031)+line["source"], "PlayMedia(" + line["file"] + ")"))
        li.addContextMenuItems(command)
        #li.setArt({'thumbnail':ic})
        #xbmc.log("ADN" + str(addon_handle), 3)
        xbmc.log(url({'menu': nm, "addr" : socket.GetAddr()}), 1)
        addDirectoryItem(addon_handle, url({'episode' : line, 'menu' : line, "addr" : socket.GetAddr()}), li, isFolder = True)



    ## On liste les dossiers accessibles du socket
    #for folder in last_played.keys():
        #if socket.ADDR[0] in IGNORE_IP_LOCAL :
            #xurl = folder
        #else :
            #xurl = "nfs://" + socket.ADDR[0] + folder
        ##l_saga = os.path.dirname(os.path.dirname(xurl))
        #li = xbmcgui.ListItem(label=folder)
        ##li.setProperty('IsPlayable', 'false')
        #addDirectoryItem(addon_handle, url({ "saga" : xurl}), li)

    ## On lance la synchro depuis le kodi distant avec le module synKodi
    #dir_path = "script://plugin.video.synkodi/?"
    #l_saga = "Stream from : " +str(socket.GetAddr())
    #li = xbmcgui.ListItem(label=l_saga)
    #li.setProperty('IsPlayable', 'false')
    #addDirectoryItem(addon_handle, dir_path, li)

    ## On envoi la video courante sur le kodi distant avec le module sendTo
    #dir_path = "script://plugin.video.sendTo/"
    #l_saga = "Run : " + dir_path
    #li = xbmcgui.ListItem(label=l_saga)
    #li.setProperty('IsPlayable', 'false')
    #addDirectoryItem(addon_handle, dir_path, li)


#"""
#Recupere le chemin de la derniere vidéo jouée depuis le socket
#"""
#def RunLastVideo(path_video) :
    #try :
        #data = socket.RunLastVideo(path_video, MAIN_SOURCE_DIR, KEEP_FILES_NUMBER, SKIP_FIRSTS_DIR)
    #except Exception as e:
        #xbmc.log(str(e), 3)
        #return {"error": str(e) }

    ## On va traiter la réponse qui contient le fichier a jouer
    #if not "error" in data :
        #if socket.HOST_SOCKET == "localhost" or socket.HOST_SOCKET == "127.0.0.1" :
            ## On lance le chemin retourné
            #logXbmc(data)
            ##self.moves.RunVideo(data["last"])
            #self.moves.PlayFromAddon(*data.values())
        #else :
            #self.moves.PlayFromAddon(*data.values())
    #else :
        #self.moves.SendNotificationToKodi("Erreur !", str(data["error"]))

"""
List socket on network
"""
def list_sockets(list_sockets):
    #window = xbmcgui.Window(xbmcgui.getCurrentWindowId())
    #window.setProperty('windowLabel', '')
    #window.setProperty('menu1', 'chiquita')

    #sources = [ MAIN_SOURCE_IP + ':' + str(MAIN_SOURCE_PORT), OTHER_SOURCE_IP + ":" + str(OTHER_SOURCE_PORT) ]
    #last_played = CallLastPlayed()
    l_a_socket = []
    for l_s_socket in list_sockets :
        l_a_socket.append(l_s_socket.GetAddr())

    #xbmc.log("WINID: " + str(list_sockets) ,3)
    if len(list_sockets) == 1 :
        return list_sockets[0]

    if len(list_sockets) > 1 :
        #sources = list_sockets #[ MAIN_SOURCE_IP + ':' + str(MAIN_SOURCE_PORT), OTHER_SOURCE_IP + ":" + str(OTHER_SOURCE_PORT) ]
        param = xbmcgui.Dialog().contextmenu(l_a_socket)
        #xbmc.log("GOGGOGO" + str(list_sockets[param]), 1)
        if param > -1 :
            socket = list_sockets[param]
            MAIN_SOURCE_IP = l_a_socket[param].split(":")[0]
            MAIN_SOURCE_PORT = l_a_socket[param].split(":")[1]
            return socket
    return None

"""
Recupere la liste des derniers dossiers joués.
On traite l'erreur dans la fonction du socket
"""
def CallLastPlayed() :
    return socket.CallGenerixMiss(SKIP_FIRSTS_DIR)

"""
Recupere la liste des derniers fichiers joués
"""
def CallPlaylist(skip_dirs, saga) :
    xbmc.log("CallPlaylist :: " + str(skip_dirs) + str(socket.ADDR), 1)
    return socket.CallGenerixMiss(SKIP_FIRSTS_DIR, saga)

def CallCleanLinks(saga) :
    try :
        data = socket.CallCleanLinks(saga, MAIN_SOURCE_DIR)
        if "error" in data :
            KodiMove().SendNotificationToKodi("[DEL] - Erreur:", socket.GetAddr() + "/" + os.path.basename(data["deleted"]) + " " + data["error"])
        else :
            KodiMove().SendNotificationToKodi("Supprimé :", socket.GetAddr() + "/" + os.path.basename(data["deleted"]))
            #return data
        #else :
            #return {}
    except Exception as e:
        xbmc.log(str(e), 3)
        return {"error": str(e) }

#FICHIERS = []
#xbmc.log("SAGA : " + str(saga), 1)

a_sockets = []
# Open directory
try :
    #if play :
    if index is not None :
        list_goto = [ "Début", "Go to " + str(int(index[0]) - 10), "Go to " + str(int(index[0]) + 10), "Fin" ]
        param = xbmcgui.Dialog().contextmenu(list_goto) #[ str(index[0]), str(int(index[0]) + 1)])
        if param >= 0 :
            if param == 0 :
                l_idx = 0
            elif param == 1 :
                l_idx = int(index[0]) - 10
            elif param == 2 :
                l_idx = int(index[0]) + 10
            elif param == 3 :
                l_idx = int(count[0]) - 9
                #l_idx = int(index[0]) + 20
            #elif param == 4 :
            elif param == 4 :
                endOfDirectory(addon_handle)
                xbmc.executebuiltin("ReplaceWindow(Videos, " + folder[0] + ")")
                #l_idx = int(index[0]) + 30
            socket = QuerySocket(addr[0].split(":")[0], int(addr[0].split(":")[1]))
            #xbmcplugin.setContent(addon_handle, "menu")
            xbmc.log(str(saga[0]) + ' ' +str(episode[0]) + "WTF11 :: !!!!!!", 1)
            episode = list_items(saga[0], episode[0], l_idx)
            #dir_path = "script://plugin.video.sendTo/"
            #l_saga = "Run : " + dir_path
            #li = xbmcgui.ListItem(label=l_saga)
            #li.setProperty('IsPlayable', 'false')
            #addDirectoryItem(addon_handle, dir_path, li)
            #last_played = CallPlaylist(os.path.join(DIR_ENCOURS_PATH, saga[0]), index)
            xbmc.log(saga[0], 1)
            #xbmcplugin.setContent(addon_handle, "menu")
            ##xbmc.log(str(episode) + " " + str("lmjklk"), 1)
            #episode = list_items(menu[0], episode[0])
            ##xbmc.log( str(episode) , 1)
            endOfDirectory(addon_handle)

    else :
        #xbmc.log(sys.argv[0], 1)
        if saga is not None and index is None :
            xbmc.log(str(saga), 1)
            xbmc.executebuiltin("ReplaceWindow(Videos, " + saga[0] + ")")
            #sys.exit(0)
        else :
            if addr is not None :
                # On créer l'objet qui va lancer les requetes aupres du socket
                MAIN_SOURCE_IP = addr[0].split(":")[0]
                MAIN_SOURCE_PORT = int(addr[0].split(":")[1])

                #a_sockets.append(addr)
                #socket = QuerySocket(addr[0], MAIN_SOURCE_PORT)
            #else :
            SOCKET_ADDR = MAIN_SOURCE_IP + ":" + str(MAIN_SOURCE_PORT)
            socket = QuerySocket(MAIN_SOURCE_IP, MAIN_SOURCE_PORT)
            socketReachable = socket.TestSocket()
            if socketReachable != "" :
                if socket.ADDR not in a_sockets :
                    a_sockets.append(socket)


            if OTHER_SOURCE_IP != "" :
                if socketReachable == "" :
                    xbmc.executebuiltin('Notification("ERROR !", "' + str(socket.ADDR) + ' is unreachable" , 3000)')
                SOCKET_ADDR = OTHER_SOURCE_IP + ":" + str(OTHER_SOURCE_PORT)
                socket = QuerySocket(OTHER_SOURCE_IP, int(OTHER_SOURCE_PORT))
                #socketReachable = socket.TestSocket()
                if socket.TestSocket() != "" :
                    OTHER_SOURCE_IP = MAIN_SOURCE_IP
                    OTHER_SOURCE_PORT = MAIN_SOURCE_PORT
                    MAIN_SOURCE_IP = socket.ADDR[0]
                    MAIN_SOURCE_PORT = int(socket.ADDR[1])
                    if socket.ADDR not in a_sockets :
                        a_sockets.append(socket)


            if len(a_sockets) > 0 :
                if play :
                    if addr is not None :
                        # On créer l'objet qui va lancer les requetes aupres du socket
                        MAIN_SOURCE_IP = addr[0].split(":")[0]
                        MAIN_SOURCE_PORT = int(addr[0].split(":")[1])

                        #a_sockets.append(addr)
                        #socket = QuerySocket(addr[0], MAIN_SOURCE_PORT)
                    #else :
                    SOCKET_ADDR = MAIN_SOURCE_IP + ":" + str(MAIN_SOURCE_PORT)
                    socket = QuerySocket(MAIN_SOURCE_IP, MAIN_SOURCE_PORT)
                    #xbmcplugin.setContent(addon_handle, "menu")
                    #xbmc.log(str(episode) + " " + str("lmjklk"), 1)
                    episode = list_items("1|2","1")
                    #xbmc.log( str(episode) , 1)
                    endOfDirectory(addon_handle)
                    #if KodiMove().SearchInDir(os.path.basename(episode)) :
                        #KodiMove().SelectFile()

                else :
                    if menu is not None :
                        xbmc.log(menu[0], 1)
                    # Menu
                    if menu is None or menu[0]=="top":
                        if OTHER_SOURCE_IP != "" and addr is None :

                            socket = list_sockets(a_sockets)
                            if socket != None :
                                xbmcplugin.setContent(addon_handle, "menu")
                                #xbmc.log(str(socket.ADDR) + "WTF :: !!!!!!" + str(a_sockets), 1)
                                list_groups()
                                endOfDirectory(addon_handle)
                            #else :
                        #elif addr is Not None
                            ## On créer l'objet qui va lancer les requetes aupres du socket
                            #socket = QuerySocket(addr, MAIN_SOURCE_PORT)
                        else :
                            #if single_list == "true":
                                #list_items("*", list_size)
                            #else:
                                #list_items("*", top_size)
                                #if top_size>0: addDirectoryItem(addon_handle, "", xbmcgui.ListItem(label=""), False)

                            if addr is not None :
                                # On créer l'objet qui va lancer les requetes aupres du socket
                                MAIN_SOURCE_IP = addr[0].split(":")[0]
                                MAIN_SOURCE_PORT = int(addr[0].split(":")[1])
                                socket = QuerySocket(MAIN_SOURCE_IP, MAIN_SOURCE_PORT)

                            xbmc.log(str(socket.ADDR) + "WTF2 :: !!!!!!" + str(a_sockets), 1)
                            xbmcplugin.setContent(addon_handle, "menu")
                            list_groups()
                            #if enable_debug	== "true":
                                #addDirectoryItem(addon_handle, url({'menu': 'showlist'}), xbmcgui.ListItem(lang(30014)), True)
                                #if xbmcvfs.exists(txtfile):
                                    #addDirectoryItem(addon_handle, url({'menu': 'deletelist'}), xbmcgui.ListItem(lang(30015)), True)
                                #else:
                                    #addDirectoryItem(addon_handle, url({'menu': 'deletelist'}), xbmcgui.ListItem(lang(30016)), True)
                            #endOfDirectory(addon_handle)
                            endOfDirectory(addon_handle)
                    elif menu[0] == 'remove':
                        lid = args.get('id', None)
                        xbmc.log("REMOVE : " + str(lid), 1)
                        if addr is not None :
                            # On créer l'objet qui va lancer les requetes aupres du socket
                            MAIN_SOURCE_IP = addr[0].split(":")[0]
                            MAIN_SOURCE_PORT = int(addr[0].split(":")[1])
                            socket = QuerySocket(MAIN_SOURCE_IP, MAIN_SOURCE_PORT)
                        CallCleanLinks(str(lid[0]))
                        delete_cmd_url = url({'menu': "top", 'addr' : str(socket.GetAddr())})

                        xbmc.executebuiltin("ReplaceWindow(Videos, " + delete_cmd_url + ")") #plugin://plugin.video.koditv?menu=top&addr = " + SOCKET_ADDR + ")")
                            #lid = args.get('id', None)
                            #if xbmcvfs.exists(txtfile) and lid is not None:
                                #f = xbmcvfs.File(txtfile)
                                #lines = json.load(f)
                                #f.close()
                                #osz = len(lines)
                                #lines.remove(lines[int(lid[0])])
                                ## to avoid accidental cleaning, update empty file only it had only one line before
                                #if len(lines)>0 or osz==1:
                                    #f = xbmcvfs.File(txtfile, 'w')
                                    #json.dump(lines, f)
                                    #f.close()
                                #xbmc.executebuiltin("ActivateWindow(Videos,plugin://plugin.video.last_played?menu=top)")
                        #elif menu[0] == 'showlist':
                            #if xbmcvfs.exists(txtfile):
                                #f = xbmcvfs.File(txtfile)
                                #lines = json.load(f)
                                #f.close()
                                #xbmc.log(lines,3)
                                #for line in lines:
                                    #addDirectoryItem(addon_handle, url({}), ListItem(str(line)), False)
                                #endOfDirectory(addon_handle)
                        #elif menu[0] == 'deletelist':
                            #if xbmcvfs.exists(txtfile):
                                #lines = []
                                #f = xbmcvfs.File(txtfile, 'w')
                                #json.dump(lines, f)
                                #f.close()
                    else:
                        #xbmcplugin.setContent(addon_handle, "menu")
                        xbmc.log(str(menu[0]) + ' ' +str(episode[0]) + "WTF :: !!!!!!", 1)
                        if addr is not None :
                            # On créer l'objet qui va lancer les requetes aupres du socket
                            MAIN_SOURCE_IP = addr[0].split(":")[0]
                            MAIN_SOURCE_PORT = int(addr[0].split(":")[1])

                            #a_sockets.append(addr)
                            #socket = QuerySocket(addr[0], MAIN_SOURCE_PORT)
                        #else :
                        SOCKET_ADDR = MAIN_SOURCE_IP + ":" + str(MAIN_SOURCE_PORT)
                        socket = QuerySocket(MAIN_SOURCE_IP, MAIN_SOURCE_PORT)
                        #xbmc.log(str(episode) + " " + str("lmjklk"), 1)
                        episode = list_items(menu[0], episode[0])
                        #xbmc.log( str(episode) , 1)
                        endOfDirectory(addon_handle)
                        #xbmc.executebuiltin('Notification("OK !", "' + str(addon_handle) + ' is unreachable" , 3000)')
            else:
                xbmc.executebuiltin('Notification("ERROR !", "' + str(socket.ADDR) + ' is unreachable" , 3000)')

                                #return True
                            #return False


                            #xbmc.log(str(SearchInDir(episode[0])), 3)
                            #if SearchInDir(episode[0]) :
                                #time.sleep(1)
                                #xbmc.executebuiltin('Action(Select)')
                                #time.sleep(3)
                                #if not xbmc.Player().isPlaying() :
                                    #xbmc.executebuiltin('Action(Select)')

                #if play :
                    ##time.sleep(2)
except Exception as e:
    #self.moves.SendNotificationToKodi("ERROR [DEL]:", data["deleted"])
    xbmc.log(str(e), 3)
        #return {"error": str(e) }
