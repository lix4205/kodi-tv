#!/usr/bin/env python
# -*- coding: utf-8 -*-

import socket
import json
import os
import xbmc
import xbmcgui
import xbmcaddon
import time

# Import modules from koditvm
import koditvm
from koditvm.Constants import CmdSocket, KodiAddonId, Path
from koditvm.QueriesSocket import QuerySocket
from koditvm.KodiMove import KodiMove, Log
from koditvm.FilesMng import FilesMng
from koditvm.JumpTimeLine import Jump
from koditvm import kodiUtils
from koditvm.generix_addon import SearchGeneric

from threading import Thread
import atexit


try:
    from urllib.parse import parse_qs, urlencode
except ImportError:
    from urlparse import parse_qs
    from urllib import urlencode

# BEGIN Main class TODO : Lui donner un vrai nom !
class SkipGeneric(xbmc.Player) :

#   BEGIN Init
    """
    Lance xbmc.Monitor() pour capter les évênements
    """
    def Monitor(self, host, port, auto_skip = False) :
        #monitor = xbmc.Monitor()
        #Log.Show( '### Init ###' )
        #ADDON_ID_FK = 'plugin.video.tvSkip'
        #RunScript(script[,args]*)

        monitor = xbmc.Monitor()
        #xbmc.Player.__init__( self )
        #self.generic = {}

        self.skip_auto = auto_skip
        self.SKIP_NEXT = True
        self.PORT_SOCKET = port
        self.HOST_SOCKET = host
        self.RUNNING = ""
        self.WINTOP = False
        self.generic = {}
        #self.moves = KodiMove()
        self.jump = None
        self.socket = QuerySocket(self.HOST_SOCKET, self.PORT_SOCKET)
        self.sgc = []
        self.DUREE_GENERIC = 0

        #self.moves.SendNotificationToKodi("Jump manager started !", "", 3000)
        #if autorun :
            #FILE_PLAY = MAIN_SOURCE_DIR + "/" + DIR_ENCOURS_NAME + "/" + FILENAME_AUTOREAD + FILENAME_AUTOREAD_EXT
            #self.RunLastVideo(FILE_PLAY)
            #self.moves.RunLastVideo(self.socket, FILE_PLAY, MAIN_SOURCE_DIR, KEEP_FILES_NUMBER, SKIP_FIRSTS_DIR)


        #self.RUNNING = path_video
        if xbmc.Player().isPlaying() :
            path_video = xbmc.getInfoLabel('Player.FileNameAndPath')
            self.RUNNING = path_video
            self.LAST_PLAYED = os.path.basename(path_video)
            # On récupère les infos du fichiers joué
            info_filesrv = kodiUtils.KodiTools.GetPathToRemove(path_video)
            path2remove = info_filesrv[0]
            SERVEUR = info_filesrv[1]
            # Log.Show("ADDR3 : " + str(self.socket.ADDR) + "|" + SERVEUR + "==" + BACKUP_SOURCE_IP + " EPS :: " + self.RUNNING)
            if SERVEUR != self.socket.ADDR[0] and BACKUP_SOURCE_IP.split(":")[0] == SERVEUR :
                l_socket = QuerySocket(*BACKUP_SOURCE_IP.split(":"))
            else :
                l_socket = self.socket

            #path2remove = kodiUtils.KodiTools.GetPathToRemove(path_video)[0]
            self.EPISODE_NAME = os.path.basename(path_video)
            self.EPISODE_PATH = os.path.dirname(path_video.replace(path2remove, ""))
            #Log.Show
            self.SkipGeneric(path_video, path2remove, l_socket)
        #xbmc.Player().play('nfs://192.168.0.5/nfs/data/videos/ANIMES/Family guy/S06/F.G.607 - La corneille de la mariée.avi')
        while not monitor.abortRequested():
            #if xbmc.Player().isPlaying() :
                #self.RUNTIME_VIDEO = xbmc.Player().getTime()
                #Log.Show("Nouvelle prise de temps : " + str(self.RUNTIME_VIDEO))
            if monitor.waitForAbort(10):
                break
#   END Init

    def exit_handler():
        Log.Show('My application is ending!')


#   BEGIN kodi events
    # """
    # Evenement pause de la lecture
    # Sur lequel on affiche une fenetre d'options
    # """
    # def onPlayBackPaused(self) :
    #     l_smooth = True
    #     l_duree = 0
    #     l_socket = None
    #
    #     # On recharge la liste des génériques pour l'interface
    #     #path2remove = kodiUtils.KodiTools.GetPathToRemove(self.RUNNING)
    #     #xbmc.executebuiltin("RunScript(" + ADDON_ID_FK + ", " + str(urlencode(self.generic))+ ")")
    #     # On récupère les infos du fichiers joué
    #     info_filesrv = kodiUtils.KodiTools.GetPathToRemove(self.RUNNING)
    #     path2remove = info_filesrv[0]
    #     SERVEUR = info_filesrv[1]
    #
    #     Log.Show("onPlayBackPaused : " + str(self.socket.ADDR) + "|" + SERVEUR + "==" + BACKUP_SOURCE_IP + " EPS :: " + str(xbmcgui.getCurrentWindowDialogId() ))
    #     if SERVEUR != self.socket.ADDR[0] and BACKUP_SOURCE_IP.split(":")[0] == SERVEUR :
    #         if BACKUP_SOURCE_IP != "" :
    #             l_socket = QuerySocket(*BACKUP_SOURCE_IP.split(":"))
    #     else :
    #         l_socket = self.socket
    #
    #     # id - xbmcgui.getCurrentWindowId()
    #     # name -
    #     xbmc.executebuiltin('Dialog.Close(9999)')
    #     # Dialog.Close(9999)
    #     Log.Show(self.sgc)
    #     if not l_socket == None :
    #         Log.Show("ADDR2 : " + str(l_socket.ADDR))
    #         if self.sgc == [] :
    #             self.sgc.append(Thread(target=SearchGeneric, args=([self.generic, l_socket, SKIP_GENERIC_AUTO, self.sgc])))
    #         # self.main = Thread(target=self.RunJob, args=([l_times]))
    #         # if self.sgc.Show() :
    #         #     self.SkipGeneric(self.RUNNING, path2remove, l_socket)
    #             self.sgc[0].start()
    #         else :
    #             # xbmc.executebuiltin('Action(Back)')
    #             # self.sgc[0].join(1)
    #             self.sgc = []
    #     else :
    #         Log.Show("ADDR2 : " + str(SERVEUR != self.socket.ADDR[0] and BACKUP_SOURCE_IP.split(":")[0] == SERVEUR))
    #
    # def onPlayBackResumed(self) :
    #
    #     xbmc.executebuiltin("Dialog.Close(all)")
    #     Log.Show("onPlayBackResume : " + str(self.sgc))
    #     if self.sgc != [] :
    #         # self.sgc[0].join(1)
    #         # xbmc.executebuiltin('Action(Back)')
    #         # self.sgc.WINTOP = False
    #         #self.sgc.wGeneric.close()
    #         self.sgc = []
    """
    Evenement pause de la lecture
    Sur lequel on affiche une fenetre d'options
    """
    def onPlayBackPaused(self) :
        l_smooth = True
        l_duree = 0
        l_socket = None

        # On recharge la liste des génériques pour l'interface
        #path2remove = kodiUtils.KodiTools.GetPathToRemove(self.RUNNING)
        #xbmc.executebuiltin("RunScript(" + ADDON_ID_FK + ", " + str(urlencode(self.generic))+ ")")
        # On récupère les infos du fichiers joué
        info_filesrv = kodiUtils.KodiTools.GetPathToRemove(self.RUNNING)
        path2remove = info_filesrv[0]
        SERVEUR = info_filesrv[1]

        # Log.Show("ADDR1 : " + str(self.socket.ADDR) + "|" + SERVEUR + "==" + BACKUP_SOURCE_IP + " EPS :: " + self.RUNNING)
        if SERVEUR != self.socket.ADDR[0] and BACKUP_SOURCE_IP.split(":")[0] == SERVEUR :
            if BACKUP_SOURCE_IP != "" :
                l_socket = QuerySocket(*BACKUP_SOURCE_IP.split(":"))
        else :
            l_socket = self.socket

        if not l_socket == None :
            # Log.Show("ADDR2 : " + str(l_socket.ADDR))
            if self.sgc == [] :
                self.sgc.append(Thread(target=SearchGeneric(self.RUNNING, self.DUREE_GENERIC, self.generic, l_socket, SKIP_GENERIC_AUTO).Show))
                self.sgc[0].start()
                # if self.sgc.Show() :
                #     self.SkipGeneric(self.RUNNING, path2remove, l_socket)
        # else :
        #     Log.Show("ADDR2 : " + str(SERVEUR != self.socket.ADDR[0] and BACKUP_SOURCE_IP.split(":")[0] == SERVEUR))
        #
    def onPlayBackResumed(self) :

        # xbmc.executebuiltin("Dialog.Close(all)")
        if self.sgc is not None :
            # self.sgc.WINTOP = False
            self.sgc = []
            xbmc.executebuiltin("Dialog.Close(all)")
            #xbmc.executebuiltin('Action(Back)')
            #self.sgc.wGeneric.close()



    def onPlayBackStarted(self) :
        try :
            path_video = xbmc.getInfoLabel('Player.FileNameAndPath')
            self.RUNNING = path_video
            self.LAST_PLAYED = os.path.basename(path_video)


            # Log.Show("onPlayBackResume : " + str(self.sgc))
            #self.RUNNING = path_video
            if xbmc.Player().isPlaying() :
                # On récupère les infos du fichiers joué
                info_filesrv = kodiUtils.KodiTools.GetPathToRemove(path_video)
                path2remove = info_filesrv[0]
                SERVEUR = info_filesrv[1]
                # Log.Show("ADDR3 : " + str(self.socket.ADDR) + "|" + SERVEUR + "==" + BACKUP_SOURCE_IP + " EPS :: " + self.RUNNING)
                if SERVEUR != self.socket.ADDR[0] and BACKUP_SOURCE_IP.split(":")[0] == SERVEUR :
                    l_socket = QuerySocket(*BACKUP_SOURCE_IP.split(":"))
                else :
                    l_socket = self.socket
                # self.socket = l_socket
                Log.Show("ADDR3 : " + str(l_socket.ADDR))

                #path2remove = kodiUtils.KodiTools.GetPathToRemove(path_video)[0]
                self.EPISODE_NAME = os.path.basename(path_video)
                self.EPISODE_PATH = os.path.dirname(path_video.replace(path2remove, ""))
                #Log.Show
                self.SkipGeneric(path_video, path2remove, l_socket)
        except Exception as e:
            Log.Error("[END PLAYED] " + str(e))

    def onPlayBackStopped(self):
        path_video = self.RUNNING
        self.RUNNING = ""
        #self.stopServer()
        #if self.REDIRECT_MAIN :

        # Log.Show('#onPlaybackStopped() : ' + str(""))
        self.KillGenericThread()

#   END kodi events

#   BEGIN Main functions
    """
    Tue les threads pour sauter les génériques lancé en arriere plan et vide le tableau de threads
    """
    def KillGenericThread(self) :
        if self.jump != None :
            self.jump.KillGenericThread()
    """
    Creer l'objet qui va lancer les thread pour sauter les génériques
    """
    def SkipGeneric(self, path_video, path2remove, back_host) :
        l_s_logInfo = ""
        l_realpath = str(path_video).replace(path2remove, "")
        #Log.Show("ADDR4 : " + os.path.realpath(l_realpath) + " || " + path2remove) # + " => " + str(back_host.TestSocket()) )

        dirSocket = back_host.Init()

        realSocketDir = ""
        for dirVid in dirSocket :
            if l_realpath.startswith(os.path.realpath(dirVid)) :
                realSocketDir = os.path.realpath(dirVid)
                break

        if realSocketDir != "" :
            # info_files = FilesMng(realSocketDir, Path.DIR_ENCOURS_NAME)
            # l_saga = FilesMng.GetTypeVideo(l_realpath, realSocketDir)["name"]
            # # Log.Show("ADDR5 : " + l_realpath)

            Log.Show("Search : {} in {}".format(path_video, realSocketDir))
            # if l_saga != "" :
            # Si le dico de generiques est vide, ou si le fichier n'est pas dans le dico ...
            if self.generic == {} or (self.generic != {} and os.path.basename(path_video) not in self.generic) :
                # On recharge la liste des génériques a sauter
                self.generic = self.GetFileGeneric(path_video.replace(path2remove, ""), back_host, realSocketDir)
            else :
                # On attend que la video se lance avant d'essayer de sauter, sinon,
                # le script considère que la vidéo ne joue pas, et s'arrete...
                time.sleep(0.5)

            # Si le dico comporte des entrées
            if len(self.generic) > 0 :
                # On tue les processus existant...
                self.KillGenericThread()
                # if "duration" in self.generic :
                    # On affecte la durée du générique global
                if len(self.generic) > 1 :
                    self.DUREE_GENERIC = self.generic[0][0]
                    self.generic = self.generic[1]
                else :
                    self.DUREE_GENERIC = 0
                    # self.generic = self.generic[0]

                Log.Show("PATH : " + str(self.generic))
                if len(self.generic) > 0 :
                    # Si on trouve des sauts pour le fichier
                    if os.path.basename(path_video) in self.generic :
                        # On récupère les sauts pour la video en cours de lecture
                        l_times = self.generic[os.path.basename(path_video)]

                        Log.Show("Will try to skip : " + str(l_times))
                        # On lance les sauts
                        self.jump = Jump(path_video, l_times, self.skip_auto and self.SKIP_NEXT)
                        # Log.Show(self.jump.time_begin)

    """
    # Recupere et lis le fichier de générique.
    Si il n'existe pas en local, il va chercher le fichier sur le serveur.
    """
    def GetFileGeneric(self, path_video, l_backup, source_dir) :
        try :
            data = {}
            l_message = " from " + str(l_backup.ADDR)

            # Log.Show("GFG" + l_message + " :: " + str(l_backup))
            param_json = { "path" : path_video, "source_dir" : source_dir, "skip_firsts_dir" : SKIP_FIRSTS_DIR }
            data = l_backup.Query(CmdSocket.KEEPENTRY, param_json)
        except Exception as e:
            Log.Error("ERREUR : " + str(e) + " " + l_message)
        finally :
            return data

#   END Main

# END Main class !

ADDON = xbmcaddon.Addon(KodiAddonId.KodiTV)
MAIN_SOURCE_IP = kodiUtils.KodiTools.GetSetting("ip_host", ADDON)

BACKUP_SOURCE_IP = kodiUtils.KodiTools.GetSetting("ip_back", ADDON)
MAIN_SOURCE_PORT = kodiUtils.KodiTools.GetSetting("kodi_port", ADDON)
MAIN_SOURCE_DIR = kodiUtils.KodiTools.GetSetting("main_path_video", ADDON)

SKIP_FIRSTS_DIR = int(kodiUtils.KodiTools.GetSetting("keep_dirs", ADDON))

SKIP_GENERIC_AUTO = kodiUtils.KodiTools.GetBoolParam("skip_generic_auto", ADDON)

# END

#if SKIP_GENERIC :
    #xbmc.executebuiltin("StopScript(" + ADDON_ID_FK + ")")
    ##xbmc.executebuiltin("StopScript(" + addonid + ")")
    #xbmc.executebuiltin("RunAddon(" + ADDON_ID_FK + ")")
    #Log.Show("Lancé !")
    #SKIP_GENERIC = False
Log.Show("# BEGIN ")
SkipGeneric().Monitor(MAIN_SOURCE_IP, int(MAIN_SOURCE_PORT), SKIP_GENERIC_AUTO)


# Log.Show('END')
