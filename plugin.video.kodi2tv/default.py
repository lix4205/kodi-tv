#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import socket
#from resources.lib import websocket 
#from resources.lib import kodiKump 
# import json
import os 
import xbmc
import xbmcgui
#from select import select
import time
# import calendar
#import hashlib

# Import modules from koditvm
import koditvm
from koditvm.Constants import CmdSocket, Path, KodiAddonId
from koditvm.QueriesSocket import QuerySocket
#from koditvm.ClientSocket import ClientSocket
from koditvm.KodiMove import KodiMove, SocketMove, Log
from koditvm.kodiUtils import KodiTools
# from koditvm.kodiUtils import KodiJson

# BEGIN Main class TODO : Lui donner un vrai nom !
class MemoryPlayer(KodiMove) :
    
#   BEGIN Init
    def Monitor(self, host, port, backup_source_ip, autorun) :
        """ automatically run video, then run xbmc.Monitor() to get events """
        try :
            monitor = xbmc.Monitor()
            super(xbmc.Player, self).__init__()

            self.socketMove = SocketMove(host, port)
            self.socket = self.socketMove.socket
            self.RUNNING = ""
            self.LIST_EPS = []

            if autorun :
                res = self.socketMove.New()
                if not(len(res) > 0 and self.socketMove.RunFromSocket(KEEP_FILES_NUMBER, SKIP_FIRSTS_DIR)) :
                    if backup_source_ip != "" :
                        self.socketMove = SocketMove(*backup_source_ip.split(":"))
                        res = self.socketMove.New()
                        if len(res) > 0 and not xbmcgui.Dialog().yesno("Socket " + str(host) + " indisponible !", "Essayer " + backup_source_ip + " ?",  "Oui", "Non", 5000) :
                            self.socket = self.socketMove.socket
                            self.DIR_SOCKET = self.socket.DIRS[0]
                            self.socketMove.ReloadList(KEEP_FILES_NUMBER)
                            l_eps = KodiTools.Right("0000", str(self.socketMove.index + 1)) + "_" + os.path.basename(self.socketMove.LIST[KEEP_FILES_NUMBER])
                            self.socketMove.ShowAnRun(l_eps, True)
                        else :
                            raise Exception(self.socket.GetAddr() + " unreachable !")
                    else :
                        raise Exception(self.socket.GetAddr() + " unreachable !")

            while not monitor.abortRequested() :
                if monitor.waitForAbort(10):
                    break
        except Exception as e :
            self.SendNotificationToKodi(str(e), "ERROR !")
            Log.Error(e)
            raise e

#   END Init

#   BEGIN Main functions
    def setEncours(self, path_file, l_socket, protocol) :
        """ Update link in ENCOURS folder """
        try :
            elem = l_socket.UpdateLink(path_file, KEEP_FILES_NUMBER, SKIP_FIRSTS_DIR)
            self.LIST_EPS = l_socket.LIST
            l_new = bool(elem["new"])
            if l_new :
                # If it is a new playlist ...
                # ..., we stop player
                if self.isPlaying() :
                    self.stop()
                # Then we run the video from the video from socket list
                xbmc.executebuiltin("Dialog.Close(all)")
                l_socket.RunFromSocket(KEEP_FILES_NUMBER, SKIP_FIRSTS_DIR, elem["list"][0][0])
                self.MOVED = True
            return True
        except Exception as e:
            Log.Error(e)
            return False

#   END Main

#   BEGIN kodi events
    def onPlayBackStarted(self):
        path_file = xbmc.getInfoLabel('Player.FileNameAndPath')

        # On récupère les infos du fichiers joué
        info_filesrv = KodiTools.GetPathToRemove(path_file)
        path2remove = info_filesrv[0]
        SERVEUR = info_filesrv[1]

        self.RUNNING = path_file.replace(path2remove, "")

        # On détermine le socket
        if SERVEUR != self.socket.ADDR[0] and BACKUP_SOURCE_IP.split(":")[0] == SERVEUR :
            a_host = BACKUP_SOURCE_IP.split(":")
            l_socketMove = SocketMove(a_host[0], int(a_host[1]))
            l_socket = l_socketMove
        else :
            l_socket = self.socketMove

        if path_file not in l_socket.LIST :
            l_socket.index = -1
        if self.setEncours(self.RUNNING, l_socket, info_filesrv[2]) :
            Log.Show("SET EN COURS :" + self.RUNNING + " from " + SERVEUR + ":" + str( l_socket.PORT_SOCKET) + " :: " + str(l_socket.index))

    # def onPlayBackStopped(self):
    #     Log.Show("Stopped")

    def onPlayBackEnded(self):
        # time.sleep(10)
        if not xbmc.Player().isPlaying() :

            Log.LOG_FILE = ""
            # try_count = 0
            # while xbmc.getInfoLabel('Container.CurrentItem') == "" or xbmc.getInfoLabel('Container.CurrentItem') != "0":
            #     # xbmc.executebuiltin('Action(PageUp)')
            #     try_count += 1
            #     if try_count > 50 : # Try 50 times (12.5s)
            #         self.SendNotificationToKodi("Unable to find !", "Rien", 3 )
            #         return False
            #     time.sleep(0.25)

            # Log.Show("MOVED :: " + str(self.socketMove.LIST))
            # l_eps = KodiTools.Right("0000" + str(self.socketMove.index + 1), 4) + "_" + os.path.basename(self.socketMove.LIST[9])
            # self.socketMove.ReloadList(0, l_eps, True)
            if self.socketMove.index >= len(self.socketMove.LIST) :
                elem = self.socketMove.LIST[1]
                self.socketMove.UpdateLink(elem, KEEP_FILES_NUMBER, SKIP_FIRSTS_DIR)
                # print("LAST EPS")
                # l_eps = KodiTools.Right("0000", str(self.socketMove.index + 2)) + "_" + os.path.basename(self.socketMove.LIST[1])
                # Log.Show("LAST EPS {}/{} :: {}".format(self.socketMove.index, len(self.socketMove.LIST), elem))
                # Log.Show("WaitForEntry 3")
                # time.sleep(3)
                # xbmc.log("ShowAnRun start", 1)
                time_sleep = 5
                self.SendNotificationToKodi("Restarting in {}s".format(time_sleep), "kodi2TV")
                time.sleep(time_sleep)
                while not KodiMove.WaitForEntry() :
                    time.sleep(.5)
                self.socketMove.RunFromSocket(KEEP_FILES_NUMBER, SKIP_FIRSTS_DIR)
                # dir_path = self.socketMove.ShowAnRun()
                # # dir_path = self.socketMove.ShowAnRun()
                # # xbmc.log("ShowAnRun wait", 1)
                #
                # # xbmc.executebuiltin("Dialog.Close(all)")
                # xbmc.executebuiltin("ReplaceWindow(Videos, {})".format(dir_path))
                # xbmc.log("ShowAnRun end", 1)
                Log.Show("LAST EPS {} :: {}".format(self.RUNNING, elem))
                # # KodiMove.WaitForEntry()
                # Log.Show("WaitForEntry 4")
                # while not KodiMove.WaitForEntry() :
                #     time.sleep(.1)
                # Log.Show("... ok")
                # KodiMove.SearchInDir(elem["list"][0][0])
                # Log.Show("WaitForEntry 5")
                # while not KodiMove.WaitForEntry() :
                #     time.sleep(.1)
                # Log.Show("... ok")
                # return KodiMove.SelectFile()

#   BEGIN Socket server calls
    def SetTime(self, path_video, l_time) :
        """ Synchronise les serveurs et/ou les clients """
        try :
            # On construit les parametre a envoyer : la saga, et le chemin du fichier
            param_json = {"path" : path_video, 
                          "time" : l_time}
            # On envoie la requete et on recupere le resultat...
            data = self.socket.ClientSend("setTime", param_json).decode()
            # ... En Json
            # Log.Show("Nouvelle prise de temps : " + str(data) + " " + str(self.CURTIME_VIDEO))
            # if data != None :
            #     res_srv = json.loads(data)
            # else :
            return data
            
            # return res_srv
        except Exception as e:
            Log.Error(e)
            return {"error": str(e) }
    
    def SynchroServers(self, host2sync, host2from, port, list_path) :
        """ Synchronise les serveurs et/ou les clients """
        try :
            # On construit les parametre a envoyer : la saga, et le chemin du fichier
            param_json = {"list" : list_path, 
                          "from" : host2from , 
                          "to" : host2sync, 
                          "source_dir" : MAIN_SOURCE_DIR}
            # On envoie la requete et on recupere le resultat...
            data = self.socket.ClientSend(CmdSocket.SYNCPLAY, param_json).decode()
            # ... En Json
            res_srv = json.loads(data)
            
            return res_srv 
        except Exception as e:
            Log.Show(e)
            return {"error": str(e) }
        
# END Main class !



MAIN_SOURCE_IP = KodiTools.GetSetting("ip_host")
MAIN_SOURCE_ADDR = Path.MAIN_SOURCE_PROTOCOL + "://" + MAIN_SOURCE_IP

BACKUP_SOURCE_IP = KodiTools.GetSetting("ip_back")

MAIN_SOURCE_PORT = KodiTools.GetSetting("kodi_port")
MAIN_SOURCE_DIR = KodiTools.GetSetting("main_path_video")
KEEP_FILES_NUMBER = int(KodiTools.GetSetting("keep_lasts"))

SKIP_FIRSTS_DIR = int(KodiTools.GetSetting("keep_dirs"))

AUTORUN = KodiTools.GetBoolParam("run_at_boot")

MemoryPlayer().Monitor(MAIN_SOURCE_IP, int(MAIN_SOURCE_PORT), BACKUP_SOURCE_IP, AUTORUN)
xbmc.log(KodiAddonId.KodiTV +  " END", 1)
