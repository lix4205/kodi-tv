# -*- coding: utf-8 -*-
import sys
import os
import time
import json

import xbmc
import xbmcplugin
import xbmcaddon
import xbmcgui
from xbmcplugin import addDirectoryItem, endOfDirectory

from koditvm.QueriesSocket import QuerySocket
from koditvm.KodiMove import SocketMove, KodiMove, KodiTools, Log
from koditvm.KodiList import KodiList
from koditvm.kodiUtils import KodiTools, KodiConn
from koditvm.Constants import Path, KodiAddonId

try:
    from urllib.parse import parse_qs, urlencode
except ImportError:
    from urlparse import parse_qs
    from urllib import urlencode

class KodiList(KodiList) :
    SOCKETS = []

    def GetSockets(socketList) :
        # On checke si les sockets sont bien disponibles
        for s in socketList :
            socketMove = SocketMove(*s[0:2])
            socket = socketMove.socket
            socketReachable = socket.Init()
            if len(socketReachable) > 0 and socket not in KodiList.SOCKETS :
                KodiList.SOCKETS.append(socket)

    def __init__(self, socket) :
        self.socket = socket

    def GetSaga(self, serie, saison, episode, index = -1) :
        # Log.Show(Path.DIR_ENCOURS_PATH)
        if os.path.exists(os.path.join(Path.DIR_ENCOURS_PATH, serie, str(episode))) :
            protocol = "local"
        else :
            protocol = MAIN_SOURCE_PROTOCOL

        # Log.Show("GetSaga 1 :: {} || {} {} {} ".format(os.path.join(Path.DIR_ENCOURS_PATH, serie), saison, episode, index))
        if index > 0 :
            saga = serie
            # Searching saga
            if not os.path.exists(os.path.join(Path.DIR_ENCOURS_PATH, saga)) :
                saga = saison
                if not os.path.exists(os.path.join(Path.DIR_ENCOURS_PATH, saga)) :
                    saga = serie
                    try :
                        return self.socket.CallPlaylistIndex(saga, index, protocol, Path.MAIN_SOURCE_DIR, KEEP_FILES_NUMBER,    SKIP_FIRSTS_DIR)

                    except Exception as e:
                        Log.Show("Do Better next time !")
            # Log.Show("GetSaga 2 :: {} || {} {} {} ".format(saga, saison, episode, index))

            try :
                return self.socket.CallPlaylistIndex(serie, index, protocol, Path.MAIN_SOURCE_DIR, KEEP_FILES_NUMBER, SKIP_FIRSTS_DIR)
            except Exception as e:
                Log.Show("Do Better next time !")
        else :
            path_video = os.path.join(serie + "|" + saison, str(episode))

        # Log.Show("CallPlaylist :: " + path_video + ' || ' + str(self.socket.ADDR) + ' '  + str(episode) + " " +str(index))
        return self.socket.CallPlaylist(path_video, protocol, Path.MAIN_SOURCE_DIR, KEEP_FILES_NUMBER, SKIP_FIRSTS_DIR, index)

    def getSetting(param) :
        return json.loads(xbmc.executeJSONRPC('{"jsonrpc":"2.0", "method":"Settings.GetSettingValue", "params":{"setting":"' + param + '"}, "id":1}'))["result"]["value"]

    def list_items(self, serie, saison, episode = "", index = -1, tag = ""):
        """ List files from a directory on server """
        last_played = self.GetSaga(serie, saison, episode, index)

        if "error" in last_played :
            Log.Error("ADDR : " + str(last_played))
            KodiMove.Notification(str(self.socket.ADDR) + " is not valid !", "ERROR !", 5)
            return False

        if "error" not in last_played and "list" in last_played :
            MAIN_SOURCE_PATH = MAIN_SOURCE_PROTOCOL + "://" + self.socket.ADDR[0]
            # Log.Show( "LISTITEM begin 2")
            KodiList.items(addon_handle, MAIN_SOURCE_PATH, last_played, self.socket, MAIN_SOURCE_DIR, sys.argv[0], IGNORE_IP_LOCAL)

            return last_played

        else :
            l_saga = serie + "|" + saison
            li = xbmcgui.ListItem(label=l_saga)
            li.setProperty('IsPlayable', 'false')
            addDirectoryItem(addon_handle, KodiList.url({"saga" : l_saga}), li)

    def list_groups(self, socks = []):
        """ List directories on server """
        last_played = self.socket.CallLastPlayed(MAIN_SOURCE_DIR, KEEP_FILES_NUMBER, SKIP_FIRSTS_DIR)

        # Log.Show(sys.argv[0])
        KodiList.groups(addon_handle, last_played, self.socket, sys.argv[0], socks, IGNORE_IP_LOCAL)

    def list_index(self, serie, saison, count = "", index = -1, play = False):
        """ List index files from a directory on server """
        # Log.Show( "IDX {} | {} | {} | {}".format(serie, saison, count, index))

        KodiList.index(addon_handle, count, self.socket, MAIN_SOURCE_DIR, sys.argv[0], serie, saison, episode, index, play)

    def CallCleanLinks(saga) :
        """ Delete a saga in group """
        try :
            data = socket.CallCleanLinks(saga, MAIN_SOURCE_DIR)
            if "error" in data :
                # "[DEL] : ", socket.GetAddr() + "/" + os.path.basename(data["deleted"]) + " " + data["error"]
                KodiMove.Notification(socket.GetAddr() + "/" + os.path.basename(data["deleted"]) + " " + data["error"], "[DEL] ERROR :", 5)
            else :
                KodiMove.Notification(os.path.join(socket.GetAddr(), os.path.basename(data["deleted"])), "[DEL] OK :", 5)
        except Exception as e:
            Log.Error(e)
            # xbmc.log(str(e), 3)
            return {"error": str(e) }

param_addon = xbmcaddon.Addon()

MAIN_SOURCE_IP = param_addon.getSetting('ip_host')
KEEP_FILES_NUMBER = int(param_addon.getSetting('keep_lasts'))
SKIP_FIRSTS_DIR = int(param_addon.getSetting('keep_dirs'))
MAIN_SOURCE_DIR = param_addon.getSetting('main_path_video')
MAIN_SOURCE_PORT = int(param_addon.getSetting("kodi_port"))
OTHER_SOURCE = str(param_addon.getSetting("ip_back"))

#Log.Show(OTHER_SOURCE_PORT)

# Ignore IP local/ array
MAIN_SOURCE_PROTOCOL = Path.MAIN_SOURCE_PROTOCOL
IGNORE_IP_LOCAL = [ "127.0.0.1", "localhost" ]

addon_handle = int(sys.argv[1])
# Log.Show("ARGV" + str(sys.argv))
args = parse_qs(sys.argv[2][1:])

# Log.SetFile("/tmp/kodi2tv.log")
Log.PREFIX = " addon"
# Log.Show(str(sys.argv), " ARGS ")

# Menu type (classic, index, ...)
menu = args.get('menu', None)
# Saga|season
saga = args.get('saga', [""])
# Episode
episode = args.get('episode', [""])
# Episode index
index = args.get('index', [-1])
#
play = args.get('play', [False])
# Main source folder to display
folder = args.get('folder', [""])
# Specific socket to show
addr = args.get('addr', None)

count = args.get('count', [-1])

tag = args.get('tag', [""])

# BEGIN Get the sockets
# First socket from configuration
socketList = [ [MAIN_SOURCE_IP, str(MAIN_SOURCE_PORT) ] ]
# Second socket
if OTHER_SOURCE != "" :
    OTHER_SOURCE = OTHER_SOURCE.split(":")
    OTHER_SOURCE_PORT = MAIN_SOURCE_PORT
    if len(OTHER_SOURCE) > 1 :
        OTHER_SOURCE_PORT = OTHER_SOURCE[1]

    # TODO : Ajouter une option pour le répertoire optionnel...
    socketList.append([ OTHER_SOURCE[0], OTHER_SOURCE_PORT ])

# Main socket
if addr is not None :
    source = addr[0].split(":")
    new_sock = [ source[0], source[1] ]
    if new_sock in socketList :
        socketList.remove(new_sock)
        Log.Show("{} removed".format(new_sock))
    socketList.insert(0, new_sock)
else :
    Log.Show("No socket specified, using : " + MAIN_SOURCE_IP)

KodiList.GetSockets(socketList)

Log.Show("Socket list : {}".format(KodiList.SOCKETS))

if not len(KodiList.SOCKETS) > 0 :
    Log.Error("Socket list : {}".format(socketList))
    KodiMove.Notification("Socket(s) unreachable")
else :
    socket = KodiList(KodiList.SOCKETS[0])

# Log.Show(socketList)
# Define ENCOURS path
# DIR_ENCOURS_PATH = os.path.join(folder[0], DIR_ENCOURS_NAME)
if addr is not None :
    if folder[0] in socket.socket.DIRS :
        # Log.Show("ADDR :: " + socket.socket.GetAddr() + " " + str(socket.socket.DIRS))
        Path.MAIN_SOURCE_DIR = folder[0]
        Path.DIR_ENCOURS_PATH = os.path.join(folder[0], Path.DIR_ENCOURS_NAME)
    else :
        Path.MAIN_SOURCE_DIR = socket.socket.DIRS[0]
        Path.DIR_ENCOURS_PATH = os.path.join(socket.socket.DIRS[0], Path.DIR_ENCOURS_NAME)
else :

    if len(folder) > 0 and folder[0] in KodiList.SOCKETS[0].DIRS :
        Path.MAIN_SOURCE_DIR = folder[0]
        Path.DIR_ENCOURS_PATH = os.path.join(folder[0], Path.DIR_ENCOURS_NAME)
    else :
        Path.MAIN_SOURCE_DIR = KodiList.SOCKETS[0].DIRS[0]
        Path.DIR_ENCOURS_PATH = os.path.join(KodiList.SOCKETS[0].DIRS[0], Path.DIR_ENCOURS_NAME)

# Log.Show("PATH :: " + Path.DIR_ENCOURS_PATH)
# END Get the sockets
if play[0] != "False" :
    play = bool(play[0])

kodi_user = args.get('kodi_user', "")
kodi_pass = args.get('kodi_pass', "")
kodi_port = args.get('kodi_port', -1)

# Use to show current playlist items
serieSaison = [ "1", "2" ]
try :
    # xbmc.executebuiltin("Dialog.Close(all)")
    moves = KodiMove()
    moves.addon_handle = addon_handle

    if int(index[0]) >= 0 :
        # Log.Show("1 " + str(socket.socket.ADDR) + "  " + str(index))
        episode = socket.list_items(*saga[0].split("|"), menu[0], int(index[0]), tag)
    else :

        if len(KodiList.SOCKETS) > 0 :
            if play :
                if kodi_user != "":
                    myKodi = KodiJson(socket.socket.ADDR[0], kodi_user[0], kodi_pass[0], kodi_port[0])
                    xbmc.executeJSONRPC('{"jsonrpc":"2.0","method":"Addons.SetAddonEnabled","id":7,"params":{"addonid": "%s","enabled":false}}' % KodiAddonId.Generix)
                    moves.RunSynchro(myKodi, MAIN_SOURCE_PORT, KEEP_FILES_NUMBER, SKIP_FIRSTS_DIR)
                else :
                    # Log.Show( "list_items121" + saga[0] + "|" + saga[0] + ":" + episode[0])
                    if menu != None :
                        # Log.Show(str(menu))
                        serieSaison = menu[0].split("|")
                    last_played = socket.list_items(*serieSaison, episode[0], index[0], tag)
            else :
                # Menu
                if menu is None or menu[0]=="top":
                    # Log.Show("2 " + str(socket.socket.ADDR) + "  " + str(menu))
                    if int(count[0]) > 0 :
                        xbmcplugin.setContent(addon_handle, "menu")
                        episode = socket.list_index(*saga[0].split("|"), int(count[0]), int(index[0]))
                        xbmcplugin.endOfDirectory(addon_handle)
                    elif len(socketList) > 1 :
                        xbmcplugin.setContent(addon_handle, "menu")
                        socket.list_groups([":".join(socketList[1][0:2])])
                    else :
                        # TODO : DO not use list_sockets, and socket list? at the end of menu
                        # Log.Show( "list_items1222" + saga[0] + "|" + saga[0] + ":" + episode[0])
                        last_played = socket.list_items(*serieSaison, episode[0], index[0], tag)
                elif menu[0] == 'list':
                    # Log.Show("HERE !!! " + str(socketList))
                    xbmcplugin.setContent(addon_handle, "menu")
                    if len(socketList) > 1 :
                        socket.list_groups([":".join(socketList[1][0:2])])
                    else :
                        socket.list_groups()
                elif menu[0] == 'remove':
                    lid = args.get('id', None)
                    Log.Show("REMOVE : " + str(lid))
                    CallCleanLinks(str(lid[0]))
                    delete_cmd_url = KodiList.url({'menu': "top", 'addr' : str(socket.GetAddr())})

                    xbmc.executebuiltin("ReplaceWindow(Videos, " + delete_cmd_url + ")")
                else:
                    episode = socket.list_items(*menu[0].split("|"), episode[0], -1, tag)
        else:
            KodiMove.Notification(str(socket.socket.ADDR) + " is unreachable", "ERROR !")

except Exception as e:
    Log.Error(e)
    # xbmc.log(str(e), 3)
