# -*- coding: latin-1 -*-
from resources.lib import jsonUtils 

# On indique le header JSON
HEADER =    {'Content-Type': 'application/json'}

JSON_VERS = {'jsonrpc': '2.0'}

class KodiJson :
    
    # Get video time.
    def GetVideoTime(self, in_second = True) :
        # On execute la requete et on recoit un JSON
        video_time = self.myKodi.execQry('Player.GetProperties', {'properties': ['time',], 'playerid': self.PlayerID}, 'VideoGetItem')
        
        # Les temps en heures, minutes, secondes, millisecondes
        video_time_h = video_time["result"]["time"]["hours"]
        video_time_m = video_time["result"]["time"]["minutes"]
        video_time_s = video_time["result"]["time"]["seconds"] 
        # On ajoute 900ms pour amoindrir la latence...
        video_time_ms = video_time["result"]["time"]["milliseconds"]
        
        kodi_sec = [video_time_h, video_time_m, video_time_s, video_time_ms]
        if in_second :
            # On calcule le temps en seconde
            return GetTimeInSeconds(kodi_sec)
            
        # On retourne le temps en hh:mm:ss:mls
        return kodi_sec

    def GetTimeInSeconds(self, p_a_time) :
        kodi_sec = (p_a_time[0] *3600) + (p_a_time[1] * 60) + p_a_time[2] + (float(p_a_time[3]) / 1000)
        return kodi_sec
    
    # Recupere le chemin du fichier qui tourne sur kodi.
    def GetVideoPath(self, t_path = "dynpath") :
        # On recupere le chemin de la video qui tourne sur kodi.
        pathVideo = self.GetItemProperty(t_path) 
        # Et on retourne le chemin
        return pathVideo
    
    # Recupere la propriété de l'item qui tourne sur le kodi distant.
    def GetItemProperty(self, t_path ) :
        # On recupere le chemin de la video qui tourne sur kodi.
        currentItemProperty = self.myKodi.execQry('Player.GetItem', {'properties': [ t_path ], 'playerid': self.PlayerID}, 'VideoGetItem')["result"]["item"][ t_path ]
        # Et on retourne la propriété
        return currentItemProperty
    
    # Envoie une notification...   
    def SendNotification(self, p_s_title, p_s_msg, p_i_timeNotif = 5000) :
        resQry = self.myKodi.execQry( 'GUI.ShowNotification', {'title' : p_s_title , 'message': p_s_msg, "displaytime" : p_i_timeNotif }, 'NotifSent')
        # Et on retourne le résultat renvoyé par la requete
        return resQry 
    
    # Execute addon
    def ExecuteAddon(self, p_s_addon_id, p_o_params = {}, p_b_wait = False):
        resQry = self.myKodi.execQry( 'Addons.ExecuteAddon', {'addonid' : p_s_addon_id , 'params': p_o_params, "wait" : p_b_wait }, 'ExecAddon')
        # Et on retourne le résultat renvoyé par la requete
        return resQry 
    
    # Launch video specified by its path
    def Run_video(self, pathVideo) :
        resQry = self.myKodi.execQry( 'Player.Open', {'item': {'file' : pathVideo }}, 'VideoPlay')
        # Et on retourne le résultat renvoyé par la requete
        return resQry

    # Play/pause video
    def play_pause(self) :
        # Mis en pause du kodi distant.
        resQry = self.myKodi.execQry('Player.PlayPause', { 'playerid': self.PlayerID}, 'VideoPlayPause') 
        # Et on retourne le résultat renvoyé par la requete
        return resQry
    
    # Seek video at the time specified in hh:mm:ss:mill
    def SeekTo(self, time_video, offset = 0) :
        resQry = self.myKodi.execQry( 'Player.Seek', {'playerid' : self.PlayerID, 'value': {'time' : { "hours" : int(time_video[0]), "minutes" : (time_video[1]), "seconds" : int(time_video[2] + offset), "milliseconds" : int(time_video[3]) }}}, 'VideoSync')
        # Et on retourne le résultat renvoyé par la requete
        return resQry

    # Seek video to the time specified in seconds
    def SeekTime(self, time_seek, offset = 0, time_local = []) :

        if int(offset) > 0:
            
            #video_time_h = int(time_seek / 3600)
            #video_time_m = int(time_seek / 60)
            #video_time_s = int(xbmc.getInfoLabel('Player.Time(ss)')) + 1
            #video_time_ms = 600
            #time_local[2] = time_local[2] + int(offset / 1000) + 1 
            self.SendNotification("offset : " , str(offset / 1000 + 1))
            #self.SeekTo(time_local, offset)
            resQry = self.myKodi.execQry( 'Player.Seek', {'playerid' : self.PlayerID, 'value': {'seconds' : "+" + str(int(offset / 1000 + 1))}}, 'VideoSeekWTF')
        else:
            resQry = self.myKodi.execQry( 'Player.Seek', {'playerid' : self.PlayerID, 'value': {'seconds' : int(offset / 1000 + 1)}}, 'VideoSeek')

        return resQry

    # Change directory to value in parameters
    def ChangeDirectory(self, cd_path) :
        resQry = self.myKodi.execQry( 'GUI.ActivateWindow', {'window' : "videos", 'parameters': [ cd_path ] }, 'ChangeDirectory')
        return resQry

    # Initialise un nouvel objet kodi-json
    def __init__(self, p_s_host = "localhost", p_s_usr = "kodi", p_s_pass = "", p_s_port = "8080"):
        self.PlayerID = 1
        self.myKodi = jsonUtils.KodiConn(p_s_host, p_s_usr, p_s_pass, p_s_port)
