# -*- coding: latin-1 -*-
import time
import xbmc
import urllib2
from contextlib import closing
import base64
import json

# On indique le header JSON
HEADER =    {'Content-Type': 'application/json'}

JSON_VERS = {'jsonrpc': '2.0'}

class KodiConn :
    # Creation de requete JSON avec :
    # la methode a appeler sur le serveur, 
    # les parametres(propriétés) a recuperer ( un tableau JSON? )
    # Et un id (utilité?)
    def create_query(self, id_qry, method, params) :
        if params == {} :
            return {
            'jsonrpc': '2.0',
            'method': method,
            'id': id_qry
            }
        else :
            return {
                'jsonrpc': '2.0',
                'method': method,
                'params': params,
                'id': id_qry
            }
        
    # Fonction qui a partir des informations passées en paramètres
    # Envoie la requete JSon kodi_request sur le serveur 
    def json_request(self, kodi_request, username, password):
        l_url = self.URL
        
        request = urllib2.Request(l_url, json.dumps(kodi_request), HEADER)
        if username and password:
            base64string = base64.encodestring('%s:%s' % (username, password)).replace('\n', '')
            request.add_header("Authorization", "Basic %s" % base64string)
        with closing(urllib2.urlopen(request)) as response:
            return json.loads(response.read())

    # Créer et execute la requete puis renvoie le resultat.
    def execQry(self, method, params, id_qry) :
        set_qry = self.create_query( id_qry, method, params)
        resQry = self.json_request(set_qry, self.USER, self.PASS) 
        return resQry

    # Initialise la classe 
    def __init__(self, p_s_host = "localhost", p_s_usr = "kodi", p_s_pass = "", p_s_port = "8080"):
        self.URL = "http://" + p_s_host + ":" + p_s_port + "/jsonrpc"
        self.USER = p_s_usr
        self.PASS = p_s_pass
        print(self.URL)
        print(self.USER)
        print(self.PASS)
        print(self.PASS)
