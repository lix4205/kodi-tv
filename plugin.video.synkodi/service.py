#!/usr/bin/python
# -*- coding: latin-1 -*-

import xbmc
import xbmcgui
import xbmcaddon
import json
import urllib2
from contextlib import closing
import base64
import time
    
class LastPlayed( xbmc.Player ) :

    def __init__ ( self ):
        monitor = xbmc.Monitor()
        xbmc.log( '### Init...' , level=xbmc.LOGNOTICE )
        xbmc.Player.__init__( self )

        while not monitor.abortRequested():
            xbmc.log( '### Waiting...' , level=xbmc.LOGNOTICE )
            if monitor.waitForAbort(10):
                break
            
    # Fonctions kodi callback events 
    # Dommage pour onAvStarted ! => python 3 ?!
    # TODO Savoir pourquoi OAV started ne fonctionne pas
    def onPlayBackStarted( self ):
        # Will be called when xbmc starts playing a file
        run_video()
        
    def onPlayBackResumed( self ):
        # Will be called when xbmc starts playing a file
        xbmc.log( '###!! playback resumed' , level=xbmc.LOGNOTICE )
        play_pause()
        #play_pause()

    def onPlayBackPaused( self ) :
        # Will be called when xbmc starts playing a file
        xbmc.log( '###!! playback paused', level=xbmc.LOGNOTICE )
        play_pause()
        
    def onPlayBackSeek( self, time, offset ):
        # Will be called when xbmc starts playing a file
        xbmc.log( '###!! playback changed ' + str(time) + " " + str(offset) , level=xbmc.LOGNOTICE )
        resQry = execQry( 'Player.Seek', {'playerid' : 1, 'value': {'seconds' : (offset / 1000 + 1)}}, 'VideoSync')
        
        
    
# Creation de requete JSON avec :
# la methode a appeler sur le serveur, 
# les parametres(propriétés) a recuperer ( un tableau JSON? )
# Et un id (utilité?)
def create_query(method, params, id_qry):
    
    return {
        'jsonrpc': '2.0',
        'id': id_qry,
        'method': method,
        'params': params
    }

def execQry(method, params, id_qry) :
    set_qry = create_query( method, params, id_qry)
    resQry = json_request(set_qry, USER, PASS, URL) 
    return resQry
    
# Fonction qui a partir des informations passées en paramètres
# Envoie la requete JSon kodi_request sur le serveur 
def json_request(kodi_request, username, password, url_def = ""):
    l_url = URL
    if url_def != "":
        l_url = url_def        
    
    request = urllib2.Request(l_url, json.dumps(kodi_request), HEADER)
    if username and password:
        base64string = base64.encodestring('%s:%s' % (username, password)).replace('\n', '')
        request.add_header("Authorization", "Basic %s" % base64string)
    with closing(urllib2.urlopen(request)) as response:
        return json.loads(response.read())

def play_pause() :
    ## Récupere le chemin de la video diffusée sur kodi.
    get_path = create_query( 'Player.PlayPause', { 'playerid': 1}, 'VideoPlay')
    # On recupere le chemin de la video qui tourne sur kodi.
    resQry = json_request(get_path, USER, PASS, URL) #["result"]["speed"]
    xbmc.log( "Play : " + str(resQry["result"]["speed"]), level=xbmc.LOGNOTICE )
    
    ##xbmc.GUI.ActivateWindow("Video", "sources://video/DLs/")
    ## Récupere le chemin de la video diffusée sur kodi.
    #get_path = create_query( 'GUI.ActivateWindow', { "window":"video", "parameters": [ "sources://DLs/" ]}, 'ActiveDir')
    ## On recupere le chemin de la video qui tourne sur kodi.
    #resQry = json_request(get_path, USER, PASS, URL) #["result"]["speed"]
    #xbmc.log( str(resQry), level=xbmc.LOGNOTICE )
    
    #run_video()
  # Et on retourne le chemin
    return resQry


#{ "jsonrpc": "2.0", "method": "GUI.ActivateWindow", "params": { "window":"videos", "parameters": [ "movietitles" ] }, "id": "1"} }
     #"params": {"item": {"file":"/home/pi/images/my_file.MOV"}, "options": {"repeat": "all", "shuffled": False }}

def run_video() :
    # On recupere le chemin de la video qui tourne sur kodi.
    pathVideo = xbmc.getInfoLabel('Player.Filenameandpath') 
    #pathVideo = '' + xbmc.getInfoLabel('Player.Filenameandpath') + ''
    # On creer l'entete de la requete
    resQry = execQry( 'Player.Open', {'item': {'file' : pathVideo }}, 'VideoPlay')
    # On recupere le retour de la requete 
    #resQry = json_request(set_qry, USER, PASS, URL) #["result"]["speed"]
    xbmc.log( "Play : " + str(resQry) + " " + pathVideo, level=xbmc.LOGNOTICE )

    
    
    
    time.sleep(5)
    video_time_h = int(xbmc.getInfoLabel('Player.Time(hh)'))
    video_time_m = int(xbmc.getInfoLabel('Player.Time(mm)'))
    video_time_s = int(xbmc.getInfoLabel('Player.Time(ss)')) + 1
    video_time_ms = 600
    
    #video_time_h = video_time["result"]["time"]["hours"]
    #video_time_m = video_time["result"]["time"]["minutes"]
    #video_time_s = video_time["result"]["time"]["seconds"] 
    ## On ajoute 900ms pour amoindrir la latence...
    #video_time_ms = video_time["result"]["time"]["milliseconds"] + 900
    #bool_playing = execQry( 'Player.Open', {'item': {'file' : pathVideo }}, 'VideoPlay')
    #sys.sleep(2)
    resQry = execQry( 'Player.Seek', {'playerid' : 1, 'value': {'time' : { "hours" : video_time_h, "minutes" : video_time_m, "seconds" : video_time_s, "milliseconds" : video_time_ms }}}, 'VideoSync')
    xbmc.log( '###!! playback started' + str(video_time_h) + ":" + str(video_time_m) + ":" + str(video_time_s), level=xbmc.LOGNOTICE )
    #resQry = execQry( 'Player.Seek', {'playerid' : 1, 'value': 'smallforward' }, 'VideoSync')
    #resQry = execQryjson_request(set_qry, USER, PASS, URL) #["result"]["speed"]

    kodi_sec = (video_time_h * 3600) + (video_time_m * 60) + video_time_s + (video_time_ms / 1000)
    # On calcule le temps en seconde pour vlc
    xbmc.log( pathVideo + " " + str(resQry) + " " + str(kodi_sec), level=xbmc.LOGNOTICE )
    # On retourne le temps en seconde
    return pathVideo

#def sync_video() :
    
    
    # Et on retourne le chemin
    #return pathVideo

HOST = "192.168.0.32"
USER = "kodi"
PASS = "b0ndqg3"
PORT = "8080"
HEADER =    {'Content-Type': 'application/json'}

URL = "http://" + HOST + ":" + PORT + "/jsonrpc"           


LastPlayed()
xbmc.log( '#######END############' , level=xbmc.LOGNOTICE )

