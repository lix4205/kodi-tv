# -*- coding: latin-1 -*-
# import os
# import time
import xbmc
import xbmcaddon
from koditvm import kodiUtils
from koditvm.KodiMove import KodiMove, Log
import xbmcgui
from koditvm.Constants import KodiAddonId
import json

#   Launch player with file in parameters,
#   or search for remote playing video in another kodi defined in addon parameters
#
#   Then synchronise time with remote playing video.

# BEGIN SynchVideo class

class SynchFromKodi(KodiMove) :

    def __init__ (self, myKodi, MAIN_SOURCE_PORT, KEEP_FILES_NUMBER, SKIP_FIRSTS_DIR, l_b_synchro = True, kodi_master = False) :
        try :
            self.m_synchro = False
            self.m_synchroDone = False
            self.master = kodi_master
            #self.m_playing = l_vid2launch
            self.kodiMoves = KodiMove()
            self.abort = False
            self.searching = True

            is_enabled = bool(json.loads(xbmc.executeJSONRPC('{"jsonrpc": "2.0", "id": 1, "method": "Addons.GetAddonDetails","params":{"addonid":"%s","properties":["enabled"]}}' % KodiAddonId.Generix))["result"]["addon"]["enabled"])
            xbmc.Player.__init__( self )

            # Si on est en mode synchro, on va tourner...
            if l_b_synchro == True :
                monitor = xbmc.Monitor()

                # On désactive la recherche de générique...
                Log.Show("DEACTff :: " + str(KodiAddonId.Generix) + " " + str(self.abort))
                xbmc.executeJSONRPC('{"jsonrpc":"2.0","method":"Addons.SetAddonEnabled","id":7,"params":{"addonid": "%s","enabled":false}}' % KodiAddonId.Generix)
                while not monitor.abortRequested() and not self.abort:
                    self.kodiMoves.RunSynchro(myKodi, MAIN_SOURCE_PORT, KEEP_FILES_NUMBER, SKIP_FIRSTS_DIR)
                    if monitor.waitForAbort(4):
                        break
            else :
                self.kodiMoves.RunSynchro(myKodi, MAIN_SOURCE_PORT, KEEP_FILES_NUMBER, SKIP_FIRSTS_DIR)
        except Exception as ex :
            Log.Error(e)
        finally :
            #if test :
            Log.Show("ACTf :: " + str(KodiAddonId.Generix) + " "+ str(is_enabled) + "!")
            if is_enabled :
                # On relance l'addon de gestion des génériques
                reslu = xbmc.executeJSONRPC('{"jsonrpc":"2.0","method":"Addons.SetAddonEnabled","id":7,"params":{"addonid": "%s","enabled":true}}' % KodiAddonId.Generix)
            Log.Show("ACTf :: " + str(KodiAddonId.Generix) + " "+ str(is_enabled) + "!")

        Log.Show('###!! RUN ()') # + str(l_b_synchro) + ')' + str(l_vid2launch), 1 )
    
    def onPlayBackStopped(self):
        dialog = xbmcgui.Dialog()
        ret = dialog.yesno(addonid, 'Continue auto read ?', "Oui", "Non", 10000)
        if ret :
            self.abort = True
            Log.Show("Script should have stop now...")

    # def onPlayBackResumed( self ):
    #     # Will be called when xbmc starts playing a file
    #     if not self.searching and self.master :
    #         Log.Show( '###!! playback resumed')
    #     # play_pause()
    #     #play_pause()
    #
    # def onPlayBackPaused( self ) :
    #     # Will be called when xbmc paused a file
    #     if not self.searching and self.master :
    #         Log.Show( '###!! playback paused')
    #     # play_pause()
    #
    # def onPlayBackSeek( self, time, offset ):
    #     # Will be called when xbmc starts playing a file
    #     if not self.searching and self.master :
    #         Log.Show( '###!! playback changed ' + str(time) + " " + str(offset))
    #     # resQry = execQry( 'Player.Seek', {'playerid' : 1, 'value': {'seconds' : (offset / 1000 + 1)}}, 'VideoSync')

# END SynchVideo class

# BEGIN Main script
try:
    addonid = xbmcaddon.Addon(KodiAddonId.Synchro).getAddonInfo('id')
except RuntimeError:
    addonid = KodiAddonId.Synchro
else:
    if addonid == '':
        addonid = KodiAddonId.Synchro

if __name__ == '__main__':
    # Host kodi infos
    addon = xbmcaddon.Addon(addonid)
    kodi_host = kodiUtils.KodiTools.GetSetting("ip_host", addon)
    kodi_user = kodiUtils.KodiTools.GetSetting("kodi_user", addon)
    kodi_pass = kodiUtils.KodiTools.GetSetting("kodi_pass", addon)
    kodi_port = kodiUtils.KodiTools.GetSetting("kodi_port", addon)

    kodi_master = kodiUtils.KodiTools.GetBoolParam("kodi_master")
    kodi_synchro = kodiUtils.KodiTools.GetBoolParam("kodi_synchro")

    ADDON_SOCKET = xbmcaddon.Addon(KodiAddonId.KodiTV)
    # MAIN_SOURCE_IP = kodiUtils.KodiTools.GetSetting("ip_host", ADDON_SOCKET)
    MAIN_SOURCE_PORT = int( kodiUtils.KodiTools.GetSetting("kodi_port", ADDON_SOCKET))
    MAIN_SOURCE_DIR = kodiUtils.KodiTools.GetSetting("main_path_video", ADDON_SOCKET)
    KEEP_FILES_NUMBER = int(kodiUtils.KodiTools.GetSetting("keep_lasts", ADDON_SOCKET))
    SKIP_FIRSTS_DIR = int(kodiUtils.KodiTools.GetSetting('keep_dirs', ADDON_SOCKET))

    # Remote kodi
    myKodi = kodiUtils.KodiConn(kodi_host, kodi_user , kodi_pass, kodi_port)
    # Activate the TV by CEC.
    #xbmc.executebuiltin('XBMC.CECActivateSource')
    #xbmc.executebuiltin('XBMC.CECToggleState')

    # Arrete tout les services existants
    #xbmc.log("TRY TO STOP TVSKIP", 1)
    SynchFromKodi(myKodi, MAIN_SOURCE_PORT, KEEP_FILES_NUMBER, SKIP_FIRSTS_DIR, kodi_synchro, kodi_master)
    #xbmc.executebuiltin("RunScript(plugin.video.tvSkip)")
    # Send video move to the main kodi
    #if kodi_synchro == True :
        #xbmc.executebuiltin("EnableAddon(" + ADDON_ID_FK + ")")
        #xbmc.executebuiltin("RunAddon(" + ADDON_ID_FK + ")")

    Log.Show("End")
# END Main script
