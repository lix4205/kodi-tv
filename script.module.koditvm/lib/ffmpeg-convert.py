# Copyright (c) Elie Coutaud 2023
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!/usr/bin/python
# -*- coding: latin-1 -*-
import os
import json
import sys
import time
import shutil
from koditvm.dbus_mpris import Player
from koditvm.helper_help import usage
from koditvm.spinner import spinner
from koditvm.ffmpeg_tools import FfmpegTools
from koditvm.ffmpeg_tools import FfmpegSplit
from koditvm.ffmpeg_tools import FfmpegConcat
import getopt
from random import uniform
import subprocess
# from koditvm.TagsMng import TagsMng, Md5Mng, TagsList
from koditvm.play_tags import PlayTags
from koditvm.Constants import CmdSocket

AUDIO_FILE = ""

OUTPUT_CMD_FILE = "/tmp/test_fvm.log"
OUTPUT_CMD = "2>> " + OUTPUT_CMD_FILE
OUTPUT_CMD_SAMPLE = "2>> " + OUTPUT_CMD_FILE
OUTPUT_CMD_INTERMEDIATE = "2>> " + OUTPUT_CMD_FILE
TIME = -1
COMPILE = False
VLC_ARG = []
VLC_OPT = "--play-and-exit" #"--video-x 0 --video-y 1024 -f --crop 16:9  --play-and-exit --no-random"
# VLC_CMD = "cvlc"
VLC_CMD = "vlc"
SOCKET = 9876

W_DIR = "/media/xud/.dux/ffmpeg-sample/"
W_DIR = "/media/wd2t/.dux/"
# W_DIR = "/media/wd2t/data/"
FINAL_FILE = ""
TARGET = ""
TIME_BEGIN = 0
TIME_END = -1

# Deleting log file
if os.path.exists(OUTPUT_CMD_FILE) :
    if os.stat(OUTPUT_CMD_FILE).st_size > 5 * 1024 * 1024 :
        os.remove(OUTPUT_CMD_FILE)

# BEGIN Help
# "NhH:u:p:P:S:", ["help", "no-synchro", "host=", "user=", "password=", "port=", "socket="]

# TODO : Recuperer usage depuis DZ /!\
# TODO : Add vlc options !!!
# --video-x 0 --video-y 1024 -f --crop 16:9  --play-and-exit --no-random
usage.OPTIONS_ALIAS = {
    # "h" : "host",
    # "N" : "no-synchro",
    # "u" : "user",
    # "p" : "port",
    # "P" : "password",
    # "S" : "socket",
    "v" : "verbose",
    "t" : "time",
    "Z" : "random",
    "a" : "audio",
    "w" : "workdir",
}
usage.OPTIONS_VALUES = {
    # "h" : "host",
    # "u" : "kodi user",
    # "p" : "kodi port",
    # "P" : "kodi password",
    # "S" : "socket port",
    "begin": "time",
    "end" : "time",
    "w" : "directory",
    "split" : "time",
    "ts" : ".ts directory to compile",
    "target" : "Target directory",
    "final" : "final ts file",
    "a" : "audio file",
    "video-x" : "width",
    "video-y" : "height",
    "crop" : "(4:3,16:9,...)",
}
# usage.OPTION_LOCAL = [
#     "N" , "no-random", "f", "cvlc", "play-and-exit", "video-x", "video-y" #: "run without synchro",
# ]
usage.OPTIONS_HELP = {
    # "h" : "host",
    # "u" : "kodi user",
    # "p" : "kodi port",
    # "P" : "kodi password",
    # "N" : "run without synchro",
    # "S" : "socket port",
    # "a" : "Audio file",
    # "f": "Fullscreen",
    # "t" : "time to wait before switching",
    # "ts" : "use directory containing .ts files",
    # "target" : "Target directory",
    # "final" : "final ts file to compile with music",
    "split" : "Sample mode (play 10s then next video) ",
    "compile": "Compile a directory into a new video",
    "begin": "Start time (s or hh:mm:ss.ms)",
    "end" : "End time (s or hh:mm:ss.ms)",
    "fps" : "Frames per second (default : 30)",
    # "cvlc" : "Do not use vlc Interface",
    "video-x" : "Window width",
    "video-y" : "Window height",
    "crop" : "crop image",
    # "play-and-exit" : "Exit when playlist has finish",
    "Z": "Random mode",
    "w" : "Working directory",
    "v" : "Verbose mode (ffmpeg final video creation)",
    "vv" : "Verbose mode (ffmpeg intermediate video creation)",
    "vvv" : "Verbose mode (ffmpeg all video creation)",
    "path" : "local files to add to vlc playlist"
}

usage.OPTIONS_MANDATORY = ["path"]
usage.OPTIONS_ARGS = ["path"]
# usage.OPTIONS_MANDATORY = []
# TODO : Use Text as first line and use help.CMD instead
usage.TEXT = "python " + sys.argv[0]
usage.DESC = "Convert video(s) in path with libx265"
# END Help

LOADING = True

class FindThrow() :
    def __init__(self, f, dicoFrame = {}) :
        # f =  f.replace("\\$", "\\\\$")
        self.FILE_SIZE = os.stat(f).st_size
        self.VIDEO_FPS = FfmpegTools.GetFPS(f)
        VIDEO_TIME = FfmpegTools.GetTime(f)
        self.VIDEO_FRAME_T = self.VIDEO_FPS * VIDEO_TIME
        self.FRAMES_TEST = [] # [ VIDEO_TIME * 1 / 4, (VIDEO_TIME * 1 / 4) + 1, VIDEO_TIME * 1 / 3, (VIDEO_TIME * 1 / 3) + 1, VIDEO_TIME * 1 / 2, (VIDEO_TIME * 1 / 2) + 1, VIDEO_TIME * 2 / 3, (VIDEO_TIME * 2 / 3) + 1, VIDEO_TIME * 3 / 4, (VIDEO_TIME * 3 / 4) + 1, ]
        self.VIDEO_TIME = VIDEO_TIME
        self.path_file = f.replace("`","\`").replace("$", "\$")

        self.dicoFrame = dicoFrame

        for i in range(int(VIDEO_TIME * 1 / 5), int(VIDEO_TIME * 1 / 3) + 1) :
            self.FRAMES_TEST.append(i)
        for i in range(int(VIDEO_TIME * 2 / 3), int((VIDEO_TIME * 4 / 5)) + 1) :
            self.FRAMES_TEST.append(i)
        print()
        print("### {} ###".format(self.path_file))
        # print(self.dicoFrame)
        # print("Size : {}".format(self.FILE_SIZE))
        # print("Fps : {}".format(self.VIDEO_FPS))
        # print("Time : {}".format(self.VIDEO_TIME))
        # print("Frames (t) : {}".format(self.VIDEO_FRAME_T))
        # print("Taille frame moyen : {}".format(self.FILE_SIZE / self.VIDEO_FRAME_T))

        # W_DIR = "/media/xud/.dux/ffmpeg-sample/TEST_FONDS/{}/new".format(os.path.basename(self.path_file))
        self.W_DIR = "/tmp/test/{}/new".format(os.path.basename(self.path_file))
        if not os.path.exists(os.path.dirname(self.W_DIR)) :
            os.mkdir(os.path.dirname(self.W_DIR))

        self.MAX_FALSE_POS = int(self.VIDEO_FPS / 10) + 1
        self.FALSE_POS = 0
        self.FIRST_TIME = 0
        self.LAST_TIME_SKIP = 0

    def Find(self, last_time_skip = 0) :
        try :
            self.LAST_TIME_SKIP = last_time_skip
            frm_size = self.GetFrameSize()
            # print("END ", end="")
            self.GetFirstFrameToKeep()
            # ({}ko) {}".format(FIRST_TIME, round(obj.FRAME_SIZE/1024, 2), f))
            self.LAST_FRAME = self.GetTimeBackward(0.5, keep = True)
            # print("END : {} ({}ko) {}".format(self.FIRST_TIME, round(self.FRAME_SIZE/1024, 2), self.path_file))

            # return self.FIRST_TIME

            self.FRAME_SIZE = frm_size
            # print("BEGIN ", end="")
            # self.GetFirstFrameToKeep(True)
            self.FIRST_TIME = int(self.LAST_FRAME) - 1
            # ({}ko) {}".format(FIRST_TIME, round(obj.FRAME_SIZE/1024, 2), f))
            self.FIRST_TIME = self.GetTimeBackward(0.95)
            # print("BEGIN : {} ({}ko) {}".format(self.FIRST_TIME, round(self.FRAME_SIZE/1024, 2), self.path_file))


            # Clean dir
            for l_file in os.listdir(os.path.dirname(self.W_DIR)) :
                os.remove(os.path.join(os.path.dirname(self.W_DIR), l_file))

            # print(str(self.FIRST_TIME) + " " + str(self.LAST_FRAME))

            MIN_DUR_WAITED = 30
            MAX_DUR_WAITED = 40
            if self.LAST_FRAME > 0 and self.FIRST_TIME <= 0 :
                if self.LAST_TIME_SKIP > 0 :
                    if self.LAST_FRAME - self.LAST_TIME_SKIP > 0 :
                        print("Gonna try with : {}".format(self.LAST_TIME_SKIP))
                        self.FIRST_TIME = self.LAST_FRAME - self.LAST_TIME_SKIP
                        # self.FIRST_TIME = self.GetTimeBackward(0.95)
            else :
            # print(self.LAST_TIME_SKIP)
                self.LAST_TIME_SKIP = self.LAST_FRAME - self.FIRST_TIME

            print("CUT :: {} ({}, {})".format(self.LAST_FRAME - self.FIRST_TIME, self.FIRST_TIME, self.LAST_FRAME))
            if self.LAST_FRAME - self.FIRST_TIME >= MIN_DUR_WAITED and self.LAST_FRAME - self.FIRST_TIME <= MAX_DUR_WAITED  :
                self.DeleteTestFrame()
            else :
                print("{} does not match minimal duration : {}".format(self.path_file, MIN_DUR_WAITED))
            return self.FIRST_TIME
        except KeyboardInterrupt as e :
            raise e
        # self.GetTimeBackward()

    def FindForward(self, last_time_skip = 0) :
        try :
            self.LAST_TIME_SKIP = last_time_skip
            frm_size = self.GetFrameSize()
            # print("END ", end="")
            self.GetFirstFrameToKeep(True)
            print("({}ko) {}".format(frm_size, round(self.FRAME_SIZE/1024, 2)))
            self.FIRST_TIME = self.GetTimeForward(0.95)
            print("BEGIN : {} ({}ko) {}".format(self.FIRST_TIME, round(self.FRAME_SIZE/1024, 2), self.path_file))

            # return self.FIRST_TIME

            self.FRAME_SIZE = frm_size
            # print("BEGIN ", end="")
            # self.GetFirstFrameToKeep()
            self.LAST_FRAME  = int(self.FIRST_TIME) + 1
            # ({}ko) {}".format(FIRST_TIME, round(obj.FRAME_SIZE/1024, 2), f))
            self.LAST_FRAME  = self.GetTimeForward(0.5, keep = True)
            print("END : {} ({}ko) {}".format(self.FIRST_TIME, round(self.FRAME_SIZE/1024, 2), self.path_file))


            # Clean dir
            for l_file in os.listdir(os.path.dirname(self.W_DIR)) :
                os.remove(os.path.join(os.path.dirname(self.W_DIR), l_file))

            # print(str(self.FIRST_TIME) + " " + str(self.LAST_FRAME))

            MIN_DUR_WAITED = 30
            MAX_DUR_WAITED = 40
            if self.LAST_FRAME > 0 and self.FIRST_TIME <= 0 :
                if self.LAST_TIME_SKIP > 0 :
                    if self.LAST_FRAME - self.LAST_TIME_SKIP > 0 :
                        print("Gonna try with : {}".format(self.LAST_TIME_SKIP))
                        self.FIRST_TIME = self.LAST_FRAME - self.LAST_TIME_SKIP
                        # self.FIRST_TIME = self.GetTimeBackward(0.95)
            else :
            # print(self.LAST_TIME_SKIP)
                self.LAST_TIME_SKIP = self.LAST_FRAME - self.FIRST_TIME

            print("CUT :: {} ({}, {})".format(self.LAST_FRAME - self.FIRST_TIME, self.FIRST_TIME, self.LAST_FRAME))
            if self.LAST_FRAME - self.FIRST_TIME >= MIN_DUR_WAITED and self.LAST_FRAME - self.FIRST_TIME <= MAX_DUR_WAITED  :
                self.DeleteTestFrame()
            else :
                print("{} does not match minimal duration : {}".format(self.path_file, MIN_DUR_WAITED))
            return self.FIRST_TIME
        except KeyboardInterrupt as e :
            raise e
        # self.GetTimeBackward()

    def GetFrameSize(self) :
        size = 0
        print(self.FRAMES_TEST)
        for i in self.FRAMES_TEST :
            # cmd = "ffmpeg  -y -i \"{}\" \"{}-%03d.png\"".format(f, W_DIR)
            file_path = "{}-{}.png".format(self.W_DIR + "_init", i)

            cmd = "ffmpeg  -y -ss {} -i \"{}\" -frames:v {} \"{}\" {}".format(i, self.path_file, 1, file_path, " 2> /dev/null")
            # print(cmd)
            FfmpegTools.check_call(cmd)

            size +=  os.stat(file_path).st_size
            os.remove(file_path)

        self.FRAME_SIZE = size / len(self.FRAMES_TEST)
        self.TOTAL_SIZE = size
        return self.FRAME_SIZE

    def GetFirstFrameToKeep(self, keep = False, rapport = 1 / 5) :
        if keep :
            i = 0
            while i <= int(self.VIDEO_TIME * rapport) :
                file_path = "{}-{}.png".format(self.W_DIR + "_1st", i)
                cmd = "ffmpeg  -y -ss {} -i \"{}\" -frames:v {} \"{}\" {}".format(i, self.path_file, 1, file_path, " 2> /dev/null")

                #### BEGIN TEST !!!
                self.CreateTestFrame(i)
                #### END TEST !!!
                print(cmd)
                FfmpegTools.check_call(cmd)
                if os.stat(file_path).st_size > self.FRAME_SIZE :
                    if self.FALSE_POS == 0 :
                        self.FIRST_TIME = i
                    self.FALSE_POS += 1
                    if self.FALSE_POS > self.MAX_FALSE_POS :
                        os.remove(file_path)
                        break
                else :
                    if self.FIRST_TIME > 0 :
                        self.FIRST_TIME = i
                os.remove(file_path)

                i += 1
        else :
            i = int(self.VIDEO_TIME * rapport)
            while i >=  0:
                file_path = "{}-{}.png".format(self.W_DIR + "_1st", i)
                cmd = "ffmpeg  -y -ss {} -i \"{}\" -frames:v {} \"{}\" {}".format(i, self.path_file, 1, file_path, " 2> /dev/null")

                # print(cmd)
                FfmpegTools.check_call(cmd)
                if os.stat(file_path).st_size > self.FRAME_SIZE :
                    if self.FALSE_POS == 0 :
                        self.FIRST_TIME = i
                    self.FALSE_POS += 1
                    if self.FALSE_POS > self.MAX_FALSE_POS :
                        os.remove(file_path)
                        break
                else :
                    if self.FIRST_TIME > 0 :
                        self.FIRST_TIME = i
                os.remove(file_path)

                i -= 1
        return self.FIRST_TIME

    #### BEGIN TEST !!!
    def CreateTestFrame(self, i) :
        W_DIR = "/media/xud/.dux/ffmpeg-sample/TEST_FONDS/{}/new".format(os.path.basename(self.path_file))
        file_path = "{}-{}.png".format(W_DIR + "-" + str(i), "%003d")
        cmd = "ffmpeg  -y -ss {} -i \"{}\" -frames:v {} \"{}\" {}".format(i, self.path_file, int(self.VIDEO_FPS + 1), file_path, " 2> /dev/null")
        # if not os.path.exists(os.path.dirname(file_path)) :
        if not os.path.exists(os.path.dirname(W_DIR)) :
            os.mkdir(os.path.dirname(W_DIR))
        FfmpegTools.check_call(cmd)

    def DeleteTestFrame(self) :
        W_DIR = "/media/xud/.dux/ffmpeg-sample/TEST_FONDS/{}/".format(os.path.basename(self.path_file))
        # if not os.path.exists(os.path.dirname(file_path)) :
        if os.path.exists(W_DIR) :
            # os.remove(W_DIR)
            shutil.rmtree(W_DIR)

    #### END TEST !!!

    def GetTimeBackward(self, percent = 0.6, keep = False, l_last = 0, l_pair_size = 0) :
        try :
            # print("TB:{} ({}ko)".format(self.FIRST_TIME, round(self.FRAME_SIZE/1024, 2)), end=" | ")
            real_frame = 0
            ecart_max = self.FRAME_SIZE * 1.75/1024
            if self.FIRST_TIME >= 0 :
                i = self.FIRST_TIME
                #### BEGIN TEST !!!
                self.CreateTestFrame(i)
                #### END TEST !!!

                file_path = "{}-{}.png".format(self.W_DIR + "-" + str(i), "%003d")

                # No frames dico, we're gonna "split" video
                # else :

                files_dir = os.path.dirname(file_path)
                self.FALSE_POS = 0

                idx_frame = 0

                if not len(self.dicoFrame) > 0 or str(i) not in self.dicoFrame or ( str(i) in self.dicoFrame and len(self.dicoFrame[str(i)]) < self.VIDEO_FPS ) :
                    cmd = "ffmpeg  -y -ss {} -i \"{}\" -frames:v {} \"{}\" {}".format(i, self.path_file, int(self.VIDEO_FPS + 1), file_path, " 2> /dev/null")
                    FfmpegTools.check_call(cmd)
                    dir_list = list(reversed(sorted(os.listdir(files_dir))))
                    self.dicoFrame[str(i)] = {}
                    # print("FROM file {}".format(i))
                else :
                    dir_list = self.dicoFrame[str(i)].keys()
                    # print("FROM dico {}".format(i))

                # print("{} < {}".format(len(self.dicoFrame[str(i)]), self.VIDEO_FPS))
                # print(self.dicoFrame[str(i)])
                frame_size = self.FRAME_SIZE * percent

                # l_last = 0
                l_cur = 0

                for l_file in dir_list :
                    path = os.path.join(files_dir, l_file)
                    if not len(self.dicoFrame[str(i)]) > 0 or l_file not in self.dicoFrame[str(i)] :
                        size_file = os.stat(path).st_size
                        self.dicoFrame[str(i)][l_file] = size_file
                        # print("kkk {} {}".format(self.dicoFrame[str(i)][l_file], l_file))
                    else :
                        # print("ooo {} {}".format(self.dicoFrame[str(i)][l_file], l_file))
                        # if l_file in self.dicoFrame :
                        size_file = self.dicoFrame[str(i)][l_file]
                    if os.path.exists(path) :
                        os.remove(path)
                    l_cur = size_file
                    if l_last != 0 or l_pair_size != 0 :
                        if keep :
                            condition_next = l_cur * 10 < l_last
                            condition_next_pair = l_cur * 10 < l_pair_size
                            # print("COND {}:{} | {} < {}".format(l_file, condition, size_file, frame_size) )
                            # if i < 180 and i > 178 :
                            #     print("{} -> {} * 10 < {} | {} * 10 < {}".format(i, l_cur, l_last, l_cur, l_pair_size))
                            # if i == 148 :
                            #     print("{} -> {} * 10 < {} | {} * 10 < {}".format(i, l_cur, l_last, l_cur, l_pair_size))
                            if condition_next : #or condition_next_pair:
                            # if condition_next_pair:
                                # print("ok")
                                # print("FOUND 10 {} | {} * 10 < {}".format(condition_next, l_cur, l_last) )
                                real_frame = 1 - round((idx_frame / self.VIDEO_FPS), 2)
                                self.FIRST_TIME += real_frame
                                # self.dicoFrame[str(i)][l_file] = size_file
                                return self.FIRST_TIME
                            elif l_cur * 5 < l_last and l_pair_size > 0 and condition_next_pair :
                                # print("FOUND 10a {} | {} * 10 < {}".format(condition_next_pair, l_cur, l_pair_size) )
                                real_frame = 1 - round((idx_frame / self.VIDEO_FPS), 2)
                                self.FIRST_TIME += real_frame
                                # self.dicoFrame[str(i)][l_file] = size_file
                                return self.FIRST_TIME
                            elif l_cur < 10 * 1024 :
                                # print("FOUND 10b {} | {} * 10 < {}".format(condition_next_pair, l_cur, l_pair_size) )
                                real_frame = 1 - round((idx_frame / self.VIDEO_FPS), 2)
                                self.FIRST_TIME += real_frame
                                # self.dicoFrame[str(i)][l_file] = size_file
                                return self.FIRST_TIME

                        else :
                            condition_next = l_last * 10 > l_cur
                            condition_next_pair = l_pair_size * 10 > l_cur
                            # if i < 135 and i > 133 :
                            # print("{} * 10 > {}".format(l_last, l_cur))
                            if not condition_next :# or not condition_next_pair:
                            # if not condition_next_pair:
                                # print("FOUND 11 {} | {} * 10 > {}".format(condition_next, l_cur, l_last) )
                                # print("ok")
                                real_frame = 1 - round((idx_frame / self.VIDEO_FPS), 2)
                                self.FIRST_TIME += real_frame
                                # self.dicoFrame[str(i)][l_file] = size_file
                                return self.FIRST_TIME
                            elif not l_last * 5 > l_cur and  l_pair_size > 0 and not condition_next_pair :
                                # print("FOUND 11a {} | {} * 10 > {}".format(condition_next_pair, l_cur, l_pair_size) )
                                # print("ok")
                                real_frame = 1 - round((idx_frame / self.VIDEO_FPS), 2)
                                self.FIRST_TIME += real_frame
                                # self.dicoFrame[str(i)][l_file] = size_file
                                return self.FIRST_TIME
                            # elif l_cur < 10 * 1024 :
                            #     print("FOUND 11b {} | {} * 10 < {}".format(condition_next_pair, l_cur, l_pair_size) )
                            #     real_frame = 1 - round((idx_frame / self.VIDEO_FPS), 2)
                            #     self.FIRST_TIME += real_frame
                            #     # self.dicoFrame[str(i)][l_file] = size_file
                            #     return self.FIRST_TIME

                    # print("h {}".format(i))
                        # if condition_next_pair :
                        #     print("FOUND 12 at {} {} | {} * 10 > {}".format(i, condition_next_pair, l_cur, l_last) )
                            # print("OK ::: {}".format(condition_next_pair))
                    # os.remove(path)
                    l_last = l_cur

                    if idx_frame % 5 == 0 :
                        l_pair_size = l_cur

                    # if keep :
                    #     condition = size_file < frame_size
                    #     # print("COND {}:{} | {} < {}".format(l_file, condition, size_file, frame_size) )
                    # else :
                    #     condition = size_file > frame_size
                    # #     print("COND {}:{} | {} > {}".format(l_file, condition, size_file, frame_size) )
                    # if condition :
                    #     self.FALSE_POS += 1
                    #     if self.FALSE_POS > self.MAX_FALSE_POS :
                    #         print("FOUND 2 {} | {} > {}".format(keep, self.FALSE_POS, self.MAX_FALSE_POS) )
                    #         if os.path.exists(path) :
                    #             os.remove(path)
                    #         break

                    idx_frame += 1
                    # if l_file not in self.dicoFrame :
                    #     print("h")
                    #     self.dicoFrame[str(i)][l_file] = size_file

                if (self.FALSE_POS > 0 and self.FALSE_POS > self.MAX_FALSE_POS and idx_frame < self.VIDEO_FPS) or percent <= 0 :
                    idx_frame -= self.FALSE_POS - 1
                    real_frame = 1 - round((idx_frame / self.VIDEO_FPS), 2)
                    self.FIRST_TIME += real_frame
                    if keep :
                        self.FRAME_SIZE = frame_size
                else :
                    if i >= 1 :
                        self.FIRST_TIME = i - 1
                        # print("TF:{} ({}ko){} {}".format(self.FIRST_TIME, round(frame_size/1024, 2), self.path_file, ecart_max))
                        # return self.GetTimeBackward(0.2)
                        if not keep :
                            return self.GetTimeBackward(0.95, keep, l_last, l_pair_size)

                        if percent - 0.2 >= 0 :
                            return self.GetTimeBackward(percent - 0.2, keep, l_last, l_pair_size)
                        else :
                            return self.GetTimeBackward(0.2, keep, l_last, l_pair_size)
            # if not keep :
            #     return self.GetTimeForward()
            # print("TF:{} ({}ko) {}".format(self.FIRST_TIME, round(frame_size/1024, 2), self.path_file))
            return self.FIRST_TIME
        except KeyboardInterrupt as e :
            raise e

    def GetTimeForward(self, percent = 0.6, keep = False, l_last = 0, l_pair_size = 0) :
        try :
            # print("TB:{} ({}ko)".format(self.FIRST_TIME, round(self.FRAME_SIZE/1024, 2)), end=" | ")
            real_frame = 0
            ecart_max = self.FRAME_SIZE * 1.75/1024
            if self.FIRST_TIME >= 0 :
                i = int(self.FIRST_TIME)
                #### BEGIN TEST !!!
                self.CreateTestFrame(i)
                #### END TEST !!!

                file_path = "{}-{}.png".format(self.W_DIR + "-" + str(i), "%003d")

                # No frames dico, we're gonna "split" video
                # else :

                files_dir = os.path.dirname(file_path)
                self.FALSE_POS = 0

                idx_frame = 0

                if not len(self.dicoFrame) > 0 or str(i) not in self.dicoFrame or ( str(i) in self.dicoFrame and len(self.dicoFrame[str(i)]) < self.VIDEO_FPS ) :
                    cmd = "ffmpeg  -y -ss {} -i \"{}\" -frames:v {} \"{}\" {}".format(i, self.path_file, int(self.VIDEO_FPS + 1), file_path, " 2> /dev/null")
                    FfmpegTools.check_call(cmd)
                    dir_list = list(sorted(os.listdir(files_dir)))
                    self.dicoFrame[str(i)] = {}
                    # print("FROM file {}".format(i))
                else :
                    dir_list = list(sorted(self.dicoFrame[str(i)].keys()))
                    # print("FROM dico {}".format(i))

                print("{} < {}".format(len(self.dicoFrame[str(i)]), self.VIDEO_FPS))
                # print(self.dicoFrame[str(i)])
                frame_size = self.FRAME_SIZE * percent

                # l_last = 0
                l_cur = 0

                for l_file in dir_list :
                    path = os.path.join(files_dir, l_file)
                    if not len(self.dicoFrame[str(i)]) > 0 or l_file not in self.dicoFrame[str(i)] :
                        size_file = os.stat(path).st_size
                        self.dicoFrame[str(i)][l_file] = size_file
                        # print("kkk {} {}".format(self.dicoFrame[str(i)][l_file], l_file))
                    else :
                        # print("ooo {} {}".format(self.dicoFrame[str(i)][l_file], l_file))
                        # if l_file in self.dicoFrame :
                        size_file = self.dicoFrame[str(i)][l_file]
                    if os.path.exists(path) :
                        os.remove(path)
                    # l_cur = size_file
                    # if l_last != 0 or l_pair_size != 0 :
                    #     if keep :
                    #         condition_next = l_cur * 10 < l_last
                    #         condition_next_pair = l_cur * 10 < l_pair_size
                    #         # print("COND {}:{} | {} < {}".format(l_file, condition, size_file, frame_size) )
                    #         # if i < 180 and i > 178 :
                    #         #     print("{} -> {} * 10 < {} | {} * 10 < {}".format(i, l_cur, l_last, l_cur, l_pair_size))
                    #         # if i == 148 :
                    #         #     print("{} -> {} * 10 < {} | {} * 10 < {}".format(i, l_cur, l_last, l_cur, l_pair_size))
                    #         if condition_next : #or condition_next_pair:
                    #         # if condition_next_pair:
                    #             # print("ok")
                    #             # print("FOUND 10 {} | {} * 10 < {}".format(condition_next, l_cur, l_last) )
                    #             real_frame = 1 - round((idx_frame / self.VIDEO_FPS), 2)
                    #             self.FIRST_TIME += real_frame
                    #             # self.dicoFrame[str(i)][l_file] = size_file
                    #             return self.FIRST_TIME
                    #         elif l_cur * 5 < l_last and l_pair_size > 0 and condition_next_pair :
                    #             # print("FOUND 10a {} | {} * 10 < {}".format(condition_next_pair, l_cur, l_pair_size) )
                    #             real_frame = 1 - round((idx_frame / self.VIDEO_FPS), 2)
                    #             self.FIRST_TIME += real_frame
                    #             # self.dicoFrame[str(i)][l_file] = size_file
                    #             return self.FIRST_TIME
                    #         elif l_cur < 10 * 1024 :
                    #             # print("FOUND 10b {} | {} * 10 < {}".format(condition_next_pair, l_cur, l_pair_size) )
                    #             real_frame = 1 - round((idx_frame / self.VIDEO_FPS), 2)
                    #             self.FIRST_TIME += real_frame
                    #             # self.dicoFrame[str(i)][l_file] = size_file
                    #             return self.FIRST_TIME
                    #
                    #     else :
                    #         condition_next = l_last * 10 > l_cur
                    #         condition_next_pair = l_pair_size * 10 > l_cur
                    #         # if i < 135 and i > 133 :
                    #         # print("{} * 10 > {}".format(l_last, l_cur))
                    #         if not condition_next :# or not condition_next_pair:
                    #         # if not condition_next_pair:
                    #             # print("FOUND 11 {} | {} * 10 > {}".format(condition_next, l_cur, l_last) )
                    #             # print("ok")
                    #             real_frame = 1 - round((idx_frame / self.VIDEO_FPS), 2)
                    #             self.FIRST_TIME += real_frame
                    #             # self.dicoFrame[str(i)][l_file] = size_file
                    #             return self.FIRST_TIME
                    #         elif not l_last * 5 > l_cur and  l_pair_size > 0 and not condition_next_pair :
                    #             # print("FOUND 11a {} | {} * 10 > {}".format(condition_next_pair, l_cur, l_pair_size) )
                    #             # print("ok")
                    #             real_frame = 1 - round((idx_frame / self.VIDEO_FPS), 2)
                    #             self.FIRST_TIME += real_frame
                    #             # self.dicoFrame[str(i)][l_file] = size_file
                    #             return self.FIRST_TIME
                    #         # elif l_cur < 10 * 1024 :
                    #         #     print("FOUND 11b {} | {} * 10 < {}".format(condition_next_pair, l_cur, l_pair_size) )
                    #         #     real_frame = 1 - round((idx_frame / self.VIDEO_FPS), 2)
                    #         #     self.FIRST_TIME += real_frame
                    #         #     # self.dicoFrame[str(i)][l_file] = size_file
                    #         #     return self.FIRST_TIME
                    #
                    # # print("h {}".format(i))
                    #     # if condition_next_pair :
                    #     #     print("FOUND 12 at {} {} | {} * 10 > {}".format(i, condition_next_pair, l_cur, l_last) )
                    #         # print("OK ::: {}".format(condition_next_pair))
                    # # os.remove(path)
                    # l_last = l_cur
                    print(i)

                    if idx_frame % 5 == 0 :
                        l_pair_size = l_cur

                    if keep :
                        condition = size_file < frame_size
                        print("COND {}:{} | {} < {}".format(l_file, condition, size_file, frame_size) )
                    else :
                        condition = size_file > frame_size
                        print("COND {}:{} | {} > {}".format(l_file, condition, size_file, frame_size) )
                    if condition :
                        self.FALSE_POS += 1
                        if not keep or self.FALSE_POS > self.MAX_FALSE_POS :
                            print("FOUND 2 {} | {} > {}".format(keep, self.FALSE_POS, self.MAX_FALSE_POS) )
                            if os.path.exists(path) :
                                os.remove(path)
                            break

                    idx_frame += 1
                    # if l_file not in self.dicoFrame :
                    #     print("h")
                    #     self.dicoFrame[str(i)][l_file] = size_file

                if (self.FALSE_POS > 0 and self.FALSE_POS > self.MAX_FALSE_POS and idx_frame < self.VIDEO_FPS) or percent <= 0 :
                    idx_frame -= self.FALSE_POS - 1
                    real_frame = 1 - round((idx_frame / self.VIDEO_FPS), 2)
                    self.FIRST_TIME += real_frame
                    if keep :
                        self.FRAME_SIZE = frame_size
                else :
                    if i >= 1 :
                        self.FIRST_TIME = i + 1
                        # print("TF:{} ({}ko){} {}".format(self.FIRST_TIME, round(frame_size/1024, 2), self.path_file, ecart_max))
                        # return self.GetTimeBackward(0.2)
                        if not keep :
                            return self.GetTimeForward(0.95, keep, l_last, l_pair_size)

                        if percent + 0.2 <= .95 :
                            return self.GetTimeForward(percent + 0.2, keep, l_last, l_pair_size)
                        else :
                            return self.GetTimeForward(0.95, keep, l_last, l_pair_size)
            # if not keep :
            #     return self.GetTimeForward()
            # print("TF:{} ({}ko) {}".format(self.FIRST_TIME, round(frame_size/1024, 2), self.path_file))
            return self.FIRST_TIME
        except KeyboardInterrupt as e :
            raise e

    # def GetTimeForward(self, percent = 0.05, keep = True) :
    #     try :
    #         print("FW:{} ({}ko)".format(self.FIRST_TIME, round(self.FRAME_SIZE/1024, 2)), end=" | ")
    #         real_frame = 0
    #         ecart_max = self.FRAME_SIZE * 1.75/1024
    #         if self.FIRST_TIME >= 0 :
    #             i = int(self.FIRST_TIME)
    #             #### BEGIN TEST !!!
    #             self.CreateTestFrame(i)
    #             #### END TEST !!!
    #
    #             file_path = "{}-{}.png".format(self.W_DIR + "-" + str(i), "%003d")
    #             cmd = "ffmpeg  -y -ss {} -i \"{}\" -frames:v {} \"{}\" {}".format(i, self.path_file, int(self.VIDEO_FPS + 1), file_path, " 2> /dev/null")
    #             FfmpegTools.check_call(cmd)
    #
    #             files_dir = os.path.dirname(file_path)
    #             self.FALSE_POS = 0
    #
    #             idx_frame = 0
    #             dir_list = list(sorted(os.listdir(files_dir)))
    #             frame_size = self.FRAME_SIZE * percent
    #
    #             for l_file in dir_list :
    #                 path = os.path.join(files_dir, l_file)
    #                 size_file = os.stat(path).st_size
    #                 if keep :
    #                     condition = size_file < frame_size
    #                     print("COND {}:{} | {} < {}".format(l_file, condition, size_file, frame_size) )
    #                 else :
    #                     condition = size_file > frame_size
    #                     print("COND {}:{} | {} > {}".format(l_file, condition, size_file, frame_size) )
    #
    #                 if condition :
    #                     print("FOUND {} | {} > {}".format(keep, self.FALSE_POS, self.MAX_FALSE_POS) )
    #                     self.FALSE_POS += 1
    #                     if self.FALSE_POS > self.MAX_FALSE_POS :
    #                         os.remove(path)
    #                         break
    #                 idx_frame += 1
    #                 os.remove(path)
    #
    #             if (self.FALSE_POS > 0 and self.FALSE_POS > self.MAX_FALSE_POS and idx_frame < self.VIDEO_FPS) or percent <= 0 :
    #                 idx_frame -= self.FALSE_POS - 1
    #                 real_frame = 1 - round((idx_frame / self.VIDEO_FPS), 2)
    #                 self.FIRST_TIME += real_frame
    #                 if keep :
    #                     self.FRAME_SIZE = frame_size
    #             else :
    #                 # if i >= 1 :
    #                 self.FIRST_TIME = i + 1
    #                 print("TF:{} ({}ko){} {}".format(self.FIRST_TIME, round(frame_size/1024, 2), self.path_file, ecart_max))
    #                 return self.GetTimeForward(percent, keep)
    #                 # # return self.GetTimeBackward(0.2)
    #                 # if not keep :
    #                 #     return self.GetTimeBackward(0.95, keep)
    #                 #
    #                 # if percent - 0.2 > 0 :
    #                 #     return self.GetTimeBackward(percent - 0.2, keep)
    #                 # else :
    #                 #     return self.GetTimeBackward(0.2, keep)
    #         # if not keep :
    #         #     self.GetTimeForward()
    #         # print("TF:{} ({}ko) {}".format(self.FIRST_TIME, round(frame_size/1024, 2), self.path_file))
    #         return self.FIRST_TIME
    #     except KeyboardInterrupt as e :
    #         raise e


class ConvertTo() :

    def ScanDir(self, path, idx = 1, tot = 0) :
        """
        Scan a directory recursivly or try to scan a file
        """
        tot += len(os.listdir(path))
        for l_dir in os.listdir(path) :
            new_dir = os.path.join(path, l_dir)

            if os.path.islink(new_dir) :
                tot -= 1
                continue
            elif os.path.isdir(new_dir) :
                # tot -= 1
                res = self.SearchFilesInDir([new_dir], idx, tot)
                idx = res[0]
                tot = res[1] - 1
            elif os.path.isfile(new_dir) :
                if new_dir.lower().endswith(".xspf")  or new_dir.lower().endswith("thumbs.db") or new_dir.lower().endswith(".directory") or new_dir.lower().endswith(".jpg") or new_dir.lower().endswith(".png") or new_dir.lower().endswith(".part") or new_dir.lower().endswith(".xcf") or new_dir.lower().endswith(".gif") or new_dir.lower().endswith(".jpeg") or new_dir.lower().endswith(".zip") or new_dir.lower().endswith(".rar") or new_dir.lower().endswith(".pdf")  :
                    tot -= 1
                    continue

                res = self.ScanFile(new_dir.replace("`", "\`"), idx, tot)
                idx = res[0]
                tot = res[1]

        return [ idx, tot ]

    def ScanFile(self, path, idx = 1, tot = 0) :
        """
        Scan a file and add it to dict (if file is valid video file)
        """
        if idx != 0 :
            sys.stdout.write('\033[2K\033[1G')
            print("\r[{}/{}] Scanning : '{}'".format(idx, tot, path), end="")
        if path in self.dicoTime :
            duration = self.dicoTime[path]
        else :
            duration = FfmpegTools.GetTime(path.strip())
        # self.dicoSample[path] = duration
        self.dicoTime[path] = duration
        # if duration > self.SAMPLE_TIME :
        #     while len(self.dicoSample[path]) < 50 :
        #         self.dicoSample[path].append(self.GetRandomTime(duration))
        idx += 1
        # tot += 1
        return [ idx, tot ]
        # else    :
        #     tot -= 1
        #     # idx -= 1
        #     return [ idx, tot ]

    def SearchFilesInDir(self, arg_path, idx = 1, tot = 0) :
        """
        Scanning for existing files
        """
        l_files = []
        proc_list = []
        # print(arg_path)
        for path in arg_path :
            # print(path)
            # print("\nSearching new files in : {}".format(l_dir), end="")
            # sys.stdout.write('\033[2K\033[1G')
            if os.path.exists(path) :
                if os.path.islink(path) :
                    tot -= 1
                    continue
                elif os.path.isdir(path) :
                    # if len(proc_list) <= self.PARALLEL_PROC_NUMBER :
                    #     durThread = runner.start(method=self.ScanDir, args_method=[path, idx, tot])
                    res = self.ScanDir(path, idx, tot)
                    idx = res[0]
                    tot = res[1]
                elif os.path.isfile(path) :
                    res = self.ScanFile(path.replace("`", "\`"), idx, tot)
                    idx = res[0]
                    tot = res[1]

        return [ idx, tot ]

    def CheckTimes(self, vid_time, time_begin = 0, time_end = -1) :

        begin = 0
        end = -1
        if time_begin != 0 :
            try :
                begin = float(time_begin)
            except :
                try :
                    a_begin = time_begin.split(":")
                    # print(a_begin)
                    if not len(a_begin) > 0 :
                        print("Invalid end time !")
                    else :
                        time_h = 0
                        if len(a_begin) > 2 :
                            time_h = int(a_begin[0]) * 3600
                            time_m = int(a_begin[1]) * 60
                            time_s = float(a_begin[2])
                        else :
                            time_m = int(a_begin[0]) * 60
                            time_s = float(a_begin[1])

                        begin = time_h + time_m + time_s
                except :
                    print("Invalid end time !")
            # finally :
            #     print("ERROR : begin time is FLOAT" + str(time_end))


        if not time_end == -1 :
            try :
                end = float(time_end)
            except :
                try :
                    a_end = time_end.split(":")
                    # print(a_end)
                    if not len(a_end) > 0 :
                        print("Invalid end time !")
                    else :
                        time_h = 0
                        if len(a_end) > 2 :
                            time_h = int(a_end[0]) * 3600
                            time_m = int(a_end[1]) * 60
                            time_s = float(a_end[2])
                        else :
                            time_m = int(a_end[0]) * 60
                            time_s = float(a_end[1])

                        end = time_h + time_m + time_s
                except :
                    print("Invalid end time !")
            # finally :
            #     print("ERROR : end time is FLOAT")


        else :
            end = vid_time
        # print("beg : " + str(begin) + " end : " + str(end))
        # if not TIME_END == -1 or not TIME_BEGIN == -1 :
        if end > vid_time :
            print("ERROR : end time is over video ! ")
            sys.exit(3)
        if end < begin :
            print("ERROR : end time is over begin time ! ({} > {})".format(end + " < " + begin))
            sys.exit(3)

        end = end - begin
        return [ begin, end ]

    def CheckTimeStamps(self, vidTimeStamps, end) :
        """
        Return video time stamps to extract
        """
        # print(vidTimeStamps)
        dicoConvert = { }
        listTimeStamp = []
        for t in vidTimeStamps :
            if not CmdSocket.MAIN_CAT in vidTimeStamps[t][1] :
                continue
            listTimeStamp.append(float(t))

        idx = 0
        listTimeStamp = sorted(listTimeStamp)
        # print(listTimeStamp)
        for t in listTimeStamp :
            begin_jump = float(t)
            end_jump = vidTimeStamps[str(t)][0]

            if idx == 0 :
                if len(listTimeStamp) > 1 :
                    dicoConvert[end_jump] = listTimeStamp[idx + 1]
                else :
                    dicoConvert[end_jump] = end
            else :
                if idx == len(listTimeStamp) - 1:
                    dicoConvert[end_jump] = end
                    # break
                else :
                    dicoConvert[end_jump] = listTimeStamp[idx + 1]
            idx += 1

        return dicoConvert



    def CreateVid(self, fps = 30) :
        """
        Create a music video with mixed video samples
        """
        try :
            # time.sleep(1)

            l_vid2launch = ""
            instance = None
            instance_name = ""
            l_i_secs = 0
            films = []
            CUR_PLS = []
            idx = 0
            self.SAMPLE_TIME = TIME
            SAMPLE_TIME = self.SAMPLE_TIME
            l_vid2localSample = ""
            new_time = 0
            set_done = False
            proc_list = []

            # kodi_sec = .3
            self.eps = ""
            self.duration = 0
            self.target = 0
            self.FPS = fps

            self.dicoTime = {}
            self.dicoSample = {}
            self.PARALLEL_PROC_NUMBER = 10       # Number of process in parallel to run to create samples
            self.runner = spinner()
            try :
                # Why the fuck do I have to add TIME_END as global here ?!!
                global W_DIR, TIME_BEGIN, TIME_END

                time_new = 0
                LIST = []
                input_cmd = ""
                filter_cmd = ""
                DURATION_DONE = 0
                # W_DIR += os.path.basename(AUDIO_FILE)
                work_dir = W_DIR

                if self.SAMPLE_TIME > 2 :
                    self.PARALLEL_PROC_NUMBER = 2
                elif self.SAMPLE_TIME > 0.9 :
                    self.PARALLEL_PROC_NUMBER = 5

                if COMPILE :

                    output_file = os.path.join(os.path.join(work_dir, os.path.basename(os.path.dirname(VLC_ARG[0])) + "-output-{}.ts"))

                    ffmpeg_compile = FfmpegConcat(VLC_ARG[0], self.FPS)
                    if ffmpeg_compile.ConcatFiles(output_cmd = OUTPUT_CMD) :
                        if ffmpeg_compile.CompilFromTS(OUTPUT_CMD) :
                            final_file = ffmpeg_compile.output_file_final + ".mp4"
                            print("Video available : {}".format(final_file))
                            os.system("vlc \"" + final_file + "\"")

                    sys.exit(0)

                fileThread = self.runner.start(method=self.SearchFilesInDir, args_method=[VLC_ARG], in_thread=False)

                # dirs = self

                if len(self.dicoTime) == 0 :
                    print("ERROR : No files available in : " + str(VLC_ARG))
                    return False

                # 1/ Read dico md5
                # 2/ Read index file

                # 3/ if file in index file,
                # 4/    Compute md5
                # 5/    Rewrite in index

                # print()
                # dirs = os.path.dirname(list(self.dicoTime.keys())[0])
                # # print(TagsList([dirs]))
                # self.tags = TagsList([dirs])
                # sys.exit(0)
                if not os.path.exists(work_dir) :
                    os.mkdir(work_dir)

                dicoTimeStamps = {}
                # if os.path.exists("FileJSON.json") :
                #     with open("FileJSON.json", 'rb') as f:
                #         dicoTimeStamps = json.load(f)

                FILE_FRAMES = "/tmp/frames.json"
                dicoFrames = {}
                if os.path.exists(FILE_FRAMES ) :
                    with open(FILE_FRAMES, 'rb') as f:
                        dicoFrames  = json.load(f)
                print()
                pt = PlayTags(VLC_ARG, CmdSocket.MAIN_CAT, "")

                # pt.UpdateIndex()
                #
                # sys.exit(0)
                last_time_skip = 0
                for vid in sorted(self.dicoTime.keys()) :
                    # sys.exit(0)
                    if "`" in vid or "$" in vid :
                        print("TODO : {}".format(vid))
                        continue
                    vid_time = FfmpegTools.GetTime(vid)

                    # Searching for throw automatically
                    if False :
                        obj = None
                        if vid in dicoFrames :
                            # dicoFrames[vid] = {}
                            # print("WTF1!!!!")
                            obj = FindThrow(vid, dicoFrames[vid])
                        else :
                            # print("WTF2!!!!")
                            obj = FindThrow(vid, {})
                            obj.dicoFrame = {}

                        FIRST_TIME = obj.Find(last_time_skip)
                        last_time_skip = obj.LAST_TIME_SKIP

                        if len(obj.dicoFrame) > 0 :
                            if not vid in dicoFrames :
                                dicoFrames[vid] = {}
                            dicoFrames[vid] = obj.dicoFrame

                        if FIRST_TIME != 0 :
                            # TIME_BEGIN = 0
                            # TIME_END = FIRST_TIME
                            TIME_BEGIN = obj.FIRST_TIME
                            TIME_END = obj.LAST_FRAME

                        else :
                            TIME_BEGIN = 0
                            TIME_END = 3
                            continue
                        continue
                        #     VLC_ARG.pop(idx_vid)
                        # idx_vid += 1


                    times = self.CheckTimes(vid_time, TIME_BEGIN, TIME_END)
                    begin = times[0]
                    end = times[1]
                    dicoConvert = {}

                    if len(dicoTimeStamps) > 0 and vid in dicoTimeStamps :
                        dicoConvert = self.CheckTimeStamps(dicoTimeStamps[vid], end)

                    if not len(dicoConvert) > 0 :
                        dicoConvert = { begin : begin + end }

                    # Liste des dossiers déjà traités
                    list_done = []
                    old_dir_create = ""

                    for begin in dicoConvert :
                        end = dicoConvert[begin]
                        # print("JUMP1 :: " + str(begin) + ":" + str(end))

                        # sys.exit(1)
                        if TIME > 0 :
                            # print("ok")
                            fftools = FfmpegSplit(vid, self.FPS, self.PARALLEL_PROC_NUMBER)
                            fftools.SetSample(self.SAMPLE_TIME)

                            dir_time = os.path.join(work_dir, str(TIME))
                            dir_vid = os.path.join(dir_time, os.path.basename(vid))
                            if not os.path.exists(dir_vid) :
                                if not os.path.exists(dir_time) :
                                    os.mkdir(dir_time)

                                os.mkdir(dir_vid)
                            fftools.LoopForSample(os.path.join(work_dir, dir_vid), begin, end)
                        else :
                            # Take same FPS of original video.
                            # Useless to increased for simple conversion
                            fps_vid = FfmpegTools.GetFPS(vid)
                            final_file = os.path.join(work_dir, os.path.basename(os.path.dirname(vid)), os.path.basename(str(begin) + "_" + vid))
                            # Get file extension
                            a_vid = final_file.split(".")
                            vid_ext = a_vid[len(a_vid) - 1]

                            # Remove old extension
                            final_file = ".".join(a_vid[0:-1])
                            if vid_ext == "mkv" :
                                final_file += ".mkv"
                                final_cmd = FfmpegTools.CMD["ffmpeg_mkv"].format(begin, vid, round(end - begin, 2), "libx265", "aac", fps_vid, final_file, OUTPUT_CMD)
                            else :
                                final_file += ".mp4"

                                final_cmd = FfmpegTools.CMD["ffmpeg_end"].format(begin, vid, round(end - begin, 2), "libx265", "aac", fps_vid, final_file, OUTPUT_CMD)
                            # print(fps_vid)
                            # final_file = os.path.join(work_dir, os.path.basename(os.path.dirname(vid)), os.path.basename(str(begin) + "_" + vid + ".mp4"))

                            dir_create = os.path.dirname(final_file)
                            # Si on a changé de dossier
                            if old_dir_create != dir_create :
                                # On va écrire un fichier qui nous permettra de ne pas retraiter le dossier
                                if old_dir_create != "" :
                                    f = open(os.path.join(old_dir_create, ".done"), "w")
                                    f.write(old_dir_create)
                                    f.close()

                                old_dir_create = dir_create
                                #

                            if not os.path.exists(dir_create) :
                                os.mkdir(dir_create)

                            # On ne lance le traitement que si le dossier n'a pas été traité
                            if not os.path.exists(os.path.join(old_dir_create, ".done")) :
                                final_res = self.runner.start("Creating : {} from : {}", [final_file, vid], FfmpegTools.check_call, [final_cmd], False)

                            res_cmd = final_res.returnMethod
                            if res_cmd == 0 :
                                md5 = pt.md5.GetMd5(vid, False, True)
                                # Rewrite entries in dictionnary
                                if md5 in pt.tags.dicoTimeStamps:
                                    md5_new = pt.md5.GetMd5(final_file, True, True)
                                    pt.UpdateEntry(md5, vid, md5_new)
                                else :

                                    if os.path.basename(vid) in pt.md5.dicoMd5 :
                                        print(md5 + " " + vid + " " + pt.md5.dicoMd5[os.path.basename(vid)])
                            else :
                                print("{} failed with code : {}".format(final_cmd, res_cmd))

                                # sys.exit(0)
                            # "ffmpeg -r 30 -y -ss 0 -i \"{}\" -ss:a 0 -i \"{}\" -t {} -t:a {} -c:v {} -map 0:v -map 1:a -vf crop=16:9 -vf scale=\"1920:1080\" \"{}\" {}".format(output_file.format("end"), AUDIO_FILE, time_v, time_v, "libx265", final_file, OUTPUT_CMD)
                            # print(final_cmd)

                sys.exit(0)

            except KeyboardInterrupt as e:
                print("Interruption")
                # if len(proc_list) > 0 :
                #     if proc_list[0].is_alive() :
                #         proc_list[0].join(1)
                #     proc_list.pop(0)
                # return False
        except Exception as e:
            print("Error : "  + str(e))
            return False
        finally :
            if len(dicoFrames) > 0 :
                with open(FILE_FRAMES, 'w') as fp:
                    json.dump(dicoFrames, fp, indent=4)
            print("End Compil")
            # if len(proc_list) > 0 :
            #     if proc_list[0].is_alive() :
            #         proc_list[0].join(1)
            #     proc_list.pop(0)
            # return True

    def runVlc(self, cmd) :
        """
        run command in a thread and add it to the main list
        """
        t = Player.RunPlayer(cmd)
        # print(cmd)
        proc_list.append(t[0])
        return t[1]

def Right(s, amount):
    return s[-amount:]

def Left(s, amount):
    return s[:amount]

def SetConfig(argv):
    """
    Args treatment
    """
    try:
        # print(argv)
        # print(usage.GetOpts())
        # print([*usage.GetOptsValue(), "help"])
        # opts = help.DicoArgs(argv)
        # opts, args = getopt.getopt(argv, "NhH:u:p:P:S:", ["help", "no-synchro", "host=", "user=", "password=", "port=", "socket="])
        opts, args = getopt.getopt(argv, usage.GetOpts(), [*usage.GetOptsValue(), "help"])
    except Exception as ex:
        # usage(argv)
        print(ex)
        usage.usage()
        sys.exit(2)
    try:
        global VLC_OPT, OUTPUT_CMD, TIME_BEGIN, TIME_END
        for opt, arg in opts:
            # print(opt)
            if opt == "--help" :
                usage.usage()
                sys.exit()
            if opt in ("-a", "--audio"):
                global AUDIO_FILE
                if os.path.exists(arg) and os.path.isfile(arg) :
                    AUDIO_FILE = arg
                else :
                    print(str(arg) + " does not exists !")
                    sys.exit(1)
            if opt in ("--final"):
                global FINAL_FILE
                if os.path.exists(arg) and os.path.isfile(arg) :
                    FINAL_FILE = arg
                else :
                    print(str(arg) + " does not exists !")
                    sys.exit(1)
            if opt in ("--target"):
                global TARGET
                if os.path.exists(arg) and os.path.isdir(arg) :
                    TARGET = arg
                else :
                    print(str(arg) + " does not exists !")
                    sys.exit(1)
            if opt in ("--split"):
                global TIME
                # if arg == "" :
                #     raise Exception ("No host defined")
                TIME = float(arg)
                if TIME == 0 :
                    print("-t must be superior to 0 !")
                    sys.exit(23) # ref : number 23...
            if opt == "--begin" :
                TIME_BEGIN = arg
            if opt == "--end" :
                TIME_END = arg
            elif opt in ("-v", "--verbose") :
                OUTPUT_CMD = ""
            elif opt in ("-vv") :
                OUTPUT_CMD_INTERMEDIATE = ""
                OUTPUT_CMD = ""
            elif opt in ("-vvv") :
                OUTPUT_CMD_SAMPLE = ""
                OUTPUT_CMD_INTERMEDIATE = ""
                OUTPUT_CMD = ""
            elif opt in ("--compile") :
                global COMPILE
                COMPILE = True
            elif opt == "--cvlc" :
                global VLC_CMD
                VLC_CMD = "cvlc"
            else :
                VLC_OPT += " " + opt + " " + arg
        global VLC_ARG
        for arg in args :
            # path =
            # if os.path.exists(arg) :
            #     print(os.path.realpath(arg))
            VLC_ARG.append(os.path.realpath(arg))
    except Exception as ex:
        # usage(argv)
        print(ex)
        usage.usage()
        sys.exit(2)

def test_loading(t, wft = "wft") :
    time.sleep(t)
    # print(wft)
    return t + 1

try :
    proc_list = []

    SetConfig(sys.argv[1:])
    VLC_OPT += " --no-osd"


    ConvertTo().CreateVid(60)

except Exception as e:
    print("FAIL !\n" + str(e))
    if DEBUG :
        if e.errno == os.errno.EsNOENT:
            sys.exit(e.errno)

finally :
    if len(proc_list) > 0 :
        if proc_list[0].is_alive() :
            proc_list[0].join(1)
        proc_list.pop(0)
