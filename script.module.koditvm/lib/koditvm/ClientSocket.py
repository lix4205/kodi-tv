#!/usr/bin/env python
# -*- coding: utf-8 -*-

import socket
import json

# BEGIN Socket client
class ClientSocket() :

    def ping_server(server: str, port: int, timeout=3):
        """ping server"""
        try:
            socket.setdefaulttimeout(timeout)
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((server, port))
        except OSError as error:
            return False
        else:
            s.close()
            return True

    def __init__(self, host, port, bufsize = 4096):
        self.ADDR = (host, port)
        self.BUFSIZE = bufsize

        self.HOST_SOCKET = host
        self.PORT_SOCKET = port

    def Log(funct = None, args = "") :
        """ Log : funct = print function, args = message to print """
        if funct != None :
            funct[0](args)

    def GetAddr(self) :
        """ Return socket address and port as addr:XXXX """
        return str(self.ADDR[0]) + ":" + str(self.ADDR[1])
    
    def TestSocket(self, log = None) :
        """ Test if socket is reachable by sending "getDirs" request
            log function is mainly `print` to debug...
        """
        try :
            # If server can't be reachable, then we return a empty dict
            if not ClientSocket.ping_server(*self.ADDR) :
                return json.loads(str({}).encode())

            isReachable = {}
            client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            isReachable = client.connect_ex(self.ADDR)
            client.close()

            if isReachable == 0 :
                client = self.Connect()
                data = json.dumps({"command": "getDirs"})
                client.send(data.encode())
                isReachable = client.recv(self.BUFSIZE)
                client.close()
            else :
                isReachable = str({}).encode()

            return json.loads(isReachable.decode())
        except KeyboardInterrupt as e:
            print("Interruption Test socket")
        # except Exception as e :
            raise KeyboardInterrupt(e)
        except Exception as e:
            ClientSocket.Log(log,"Exception {} ".format(str(e)))
            # log[0]("Exception {} ".format(str(e)))
            # isReachable = str({}).encode()
            # isReachable = "Except !"
            print("Test socket failed ! :\n" + str(e))
            raise Exception(e)
            # return json.loads(isReachable.decode())
        # finally :
        #     return json.loads(isReachable.decode())
        #     if isReachable == "Except !" :
        #         print("ok")
            # if len(log) > 0 :
            # ClientSocket.Log(log,"RETURNED {} ".format(self.ADDR) + " " + str(isReachable))
            # ClientSocket.Log(log,"RETURNED {} ".format(self.ADDR) + " " + str(isReachable.decode()))
            # ClientSocket.Log(log,"RETURNED JSON {} ".format(self.ADDR) + " " + str(json.loads(isReachable.decode())) + " " +str(len(json.loads(isReachable.decode()))))


    def Connect(self) :
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client.connect(self.ADDR)
        return client

    def Send(self, command = "list", args_command = {}) :
        """ Send query to socket """
        data = {}

        # Connecting...
        client = self.Connect()
        try :
            # Building params (command to do, args
            dataJ = json.dumps({"command": command, "args" : args_command})
            client.send(dataJ.encode())
            # with c, c.makefile('rb') as r: # read binary so .read(n) gets n bytes
            #     while True:
            #         header = r.readline() # read header up to a newline
            #         if not header: break  # if empty string, client closed connection
            #         size = int(header)
            #         data = json.loads(r.read(size)) # read exactly "size" bytes and decode JSON
            #         print(f'{a}: {data}')

            data = client.recv(self.BUFSIZE)
        except Exception as e :
            return e.Message
        finally :
            client.close()
            return data
    
    def Query(self, command, param_json = {}) :
        """ Send query to socket, then return answer as dict """
        data = {}
        try :
            data = self.Send(command, param_json)
            if data :
                return json.loads(data)
            else :
                return {"error": str(data) }
        except Exception as e:
            if data :
                return {"error": str(e) + " " + str(data)  }
            else :
                return {"error": str(e) }

    ### BEGIN Files transfer "TO REIMPLEMENT"...
    def ClientSendFile(self, path_file) :
        """ Send a file to the socket """
        client = socket.socket()
        client.connect(self.ADDR)
        with open(path_file, 'rb') as file:
            data = file.read(1024)
            client.send(data)
            while data != bytes(''.encode()):
                data = file.read(1024)
                client.send(data)

    def ClientSync(self, host_to, path_video) : #, addr_socket) :
        param_json = { "path" : path_video, "from" : self.ADDR[0], 'to' : host_to, "port" : self.ADDR[1] }
        data = self.Send("copyPlaying", param_json)
        return data
    # END "TO REIMPLEMENT"...

# END Client Socket
