#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import xbmc
import xbmcgui
import xbmcaddon
import time

from . import Constants
from . import ClientSocket
from . import KodiMove
# from . import Generics
from . import KodiMove
from . import kodiUtils

# BEGIN Main class
class SearchGeneric(xbmc.Player) :
    """
    Affiche une fenetre permettant l'ajout de sauts dans le fichier de générique.
    """
#   BEGIN Init

    """
    Lance xbmc.Monitor() pour capter les évênements
    """
    def __init__(self, eps, duration, list_tsp, socket, auto_skip = False, list_gen = []) :
        self.DUREE_GENERIC = 0
        self.DEBUT = -1
        self.FIN = -1
        self.NEW_ENTRY = []
        self.MULTIPLE = 0.9
        self.jump = None
        self.listeIndex = []
        self.SKIP_NEXT = False
        self.skip_auto = auto_skip
        self.FIRST_RUN = False
        self.SEARCH_MODE = True
        self.SMOOTH_MODE = True
        self.SEARCHING = False
        self.EXIT_ADDON = False
        self.SEARCH_SKIP_NEXT = False
        self.RUNNING = ""
        self.truc = KodiMove.KodiMove()
        self.WINTOP = False
        self.SET_GLOBAL = False
        self.socket = socket
        #xbmc.Player().play("nfs://192.168.0.5:2049/nfs/data/videos//ENCOURS/Gintama/../../MANGAS/Gintama/S03 2008-1/Gintama - 129 - Watch out for your pet eating off the floor.mp4")
        #xbmc.Player().play("nfs://192.168.0.5:2049/nfs/data/videos//ENCOURS/Gintama/../../MANGAS/Gintama/S03 2008-1/Gintama - 134 - Be Very Careful When Using Ghost Stories.mp4")
        #xbmc.Player().play("nfs://192.168.0.5:2049/nfs/data/videos//ENCOURS/Archer/../../ANIMES/Archer/S06/Archer.2009.S06E09.FRENCH.LD.WEB-DL.avi")
        #xbmc.Player().play("nfs://192.168.0.5:2049/nfs/data/videos//ENCOURS/Archer/../../ANIMES/Archer/S06/Archer.2009.S06E10.FRENCH.WEB-DL.avi")

        #xbmc.Player().play("nfs://192.168.0.5:2049/nfs/data/videos//ENCOURS/Gintama/../../MANGAS/Gintama/S03 2008-1/Gintama - 128 - There are things you can't understand even when you meet them.mp4")

        self.EPISODE_NAME = ""
        self.generic = list_tsp
        self.eps = xbmc.getInfoLabel('Player.FileNameAndPath')
        path2remove = kodiUtils.KodiTools.GetPathToRemove(self.eps)[0]
        # l_duration = 0
        # if duration > 0 :
        #     KodiMove.Log.Show(duration)
        #     # if "duration" in args :
        #     self.generic["duration"] = duration
        #     l_duration = self.generic["duration"]
        #     # KodiMove.Log.Show(str(args) + " __ " + self.eps)
        # if os.path.basename(self.eps) ==  eps :
        #     self.generic[os.path.basename(eps)] = list_tsp
        self.EPISODE_NAME = os.path.basename(self.eps)
        self.EPISODE_PATH = os.path.dirname(self.eps.replace(path2remove, ""))

        #xbmc.log("!" + str(self.generic[os.path.basename(self.eps)][0]) + "! ++ " + self.eps, 1)

        if xbmc.Player().isPlaying() :
            if len(self.generic) > 0 and "duration" in self.generic :
                l_duree = self.generic["duration"]
            l_time = xbmc.Player().getTime()
        self.duration = duration
        self.time = l_time

        # self.Show()
    #def test1(self, arg) :
        #KodiMove.Log.Show("SBOOO" + str(arg))

    #def test2(self) :
        #KodiMove.Log.Show("SBOOO2")

    def Show(self) :
        return self.ShowOptions(self.duration, self.time)

#   END Init

#   BEGIN kodi events

    def onPlayBackResumed(self) :
        if not self.MAIN_MENU :
            xbmc.executebuiltin("Dialog.Close(all)")
            KodiMove.Log.Show( "Dialog.Close(busydialog)" )
        # self.wGeneric.close()
        if self.WINTOP :
            self.WINTOP = False
    #
    # """
    # Evenement pause de la lecture
    # Sur lequel on affiche une fenetre d'options
    # """
    # def onPlayBackPaused(self) :
    #     l_smooth = True
    #     l_duree = 0
    #     if self.WINTOP :
    #         self.WINTOP = False
    #     else :
    #
    #         if xbmc.Player().isPlaying() :
    #             if len(self.generic) > 0 and "duration" in self.generic :
    #                 l_duree = self.generic["duration"]
    #             l_time = xbmc.Player().getTime()
    #
    #             self.ShowOptions(l_duree, l_time)

    """
    Evenement démarage de la lecture
    """
    def onPlayBackStarted(self):
        time.sleep(0.5)
        self.DEBUT = -1
        self.FIN = -1
        self.SEARCHING = False
        self.SEARCH_SKIP_NEXT = False

        path_video = xbmc.getInfoLabel('Player.FileNameAndPath')
        if self.RUNNING != path_video :
            if len(self.NEW_ENTRY) > 0 :
                dialog = xbmcgui.Dialog()
                ret = dialog.yesno("Gestion des sauts", 'Sauver : {}:{} ?'.format(os.path.basename(self.RUNNING), str(self.NEW_ENTRY)),  "Oui", "Non")
                if ret == 0 :
                    self.SetFileGeneric(os.path.join(self.EPISODE_PATH, self.EPISODE_NAME), self.NEW_ENTRY)

            self.NEW_ENTRY = []
        self.RUNNING = path_video

    """
    Evenement arret de la lecture
    """
    def onPlayBackStopped(self):
        if len(self.NEW_ENTRY) > 0 :
            dialog = xbmcgui.Dialog()
            ret = dialog.yesno("Gestion des sauts", 'Sauver : {}:{} ?'.format(os.path.basename(self.RUNNING), str(self.NEW_ENTRY)),  "Oui", "Non")
            if ret == 0 :
                self.SetFileGeneric(os.path.join(self.EPISODE_PATH, self.EPISODE_NAME), self.NEW_ENTRY)
        if self.WINTOP :
            self.wGeneric.close()
            self.WINTOP = False

#   END kodi events

#   BEGIN Interactions avec l'utilisateur
    """
    Affiche la fenetre d'option
    """
    def ShowOptions(self, l_duree, l_time) :
        self.MAIN_MENU = True
        xbmc.executebuiltin("Dialog.Close(all)")
        # KodiMove.Log.Show( "Dialog.Close(busydialog)" )
        # KodiMove.Log.Show(float(l_time) > 0)
        # KodiMove.Log.Show(kodiUtils.KodiTools.HRTime(l_time, False))
        l_list = ["Throw at {}+{}".format(kodiUtils.KodiTools.HRTime(l_time, False), l_duree),
                  "Tag at {}".format(kodiUtils.KodiTools.HRTime(l_time, False)),
                  ]
        l_reponse = int(self.ShowDialog(l_list, "Generix", "Gestion des sauts", self.EPISODE_NAME))
        if l_reponse == 2 :
            xbmc.Player().pause()
            return False
        elif l_reponse == -1 :
            KodiMove.Log.Show("ABORT !" + str(xbmc.getCondVisibility("Player.Paused")))
            xbmc.executebuiltin('XBMC.PlayerControl(Play)')
            if bool(xbmc.getCondVisibility("Player.Paused")) :
                xbmc.Player().pause()

            return False

        self.MAIN_MENU = False

        menu = []
        if len(self.generic) > 0 :
            if self.EPISODE_NAME in self.generic :
                for l_saut in self.generic[self.EPISODE_NAME] :
                    l_text_episode = l_saut.split("+")
                    KodiMove.Log.Show(":: :: " + '"' + str(l_text_episode) + '"')
                    menu.append(kodiUtils.KodiTools.HRTime(l_text_episode[0], False) + "+" + l_text_episode[1])

        if l_reponse == 0 :
            l_list = menu + [
                        "Sauter maintenant ({}+{}s)".format(kodiUtils.KodiTools.HRTime(l_time, False), l_duree),
                        "Sauter \"suivant\" maintenant ({})".format(kodiUtils.KodiTools.HRTime(l_time, False)),
                        "Sauter pour tout les épisodes",
                        "Modifier durée"
                    ]
            l_first_option = 0

            l_reponse = int(self.ShowDialog(l_list, "Generix", "Gestion des sauts", self.EPISODE_NAME))
            self.FIRST_RUN = True
            if self.EPISODE_NAME in self.generic :
                l_first_option = len(self.generic[self.EPISODE_NAME])

            if l_reponse > -1 and self.EPISODE_NAME in self.generic and l_reponse < l_first_option :
                return self.ConfirmJump(l_reponse)
            elif l_reponse == l_first_option :
                KodiMove.Log.Show("SearchEndBegining")
                self.SEARCH_MODE = False

                p_multiple = 0.5
                self.DEBUT = l_time
                if not self.DUREE_GENERIC > 0 :
                    if l_duree > 0 :
                        self.DUREE_GENERIC = l_duree
                    else :
                        self.DUREE_GENERIC = self.DefineNumber(0, "Please give me generic time (in second) for " + self.EPISODE_NAME)

                xbmc.Player().pause()
                while self.FIN < 0 :
                    res_edit = self.SearchEndGeneric(l_time, p_multiple)
                self.FIN = -1

                return res_edit
            elif l_reponse == l_first_option + 1 :
                p_multiple = 0.5
                xbmc.Player().pause()
                self.NEW_ENTRY.append(str(l_time) + "+0")
                #self.SearchGenericToSkipNext(l_time, -0.9)
                self.SEARCH_SKIP_NEXT = False
                KodiMove.Log.Show("Save array !")
                retourSocket = self.SetFileGeneric(os.path.join(self.EPISODE_PATH, self.EPISODE_NAME), self.NEW_ENTRY)
                return True
            elif l_reponse == l_first_option + 2 :
                KodiMove.Log.Show("SearchGlobalBegining")
                #self.SEARCH_MODE = False
                #l_time = xbmc.Player().getTime()
                p_multiple = 0.5
                xbmc.Player().pause()
                self.DEBUT = l_time
                if not self.DUREE_GENERIC > 0 :
                    if l_duree > 0 :
                        self.DUREE_GENERIC = l_duree
                    else :
                        self.DUREE_GENERIC = self.DefineNumber(0, "Please give me generic time (in second) for " + self.EPISODE_NAME)

                while self.FIN < 0 :
                    res_edit = self.SearchEndGeneric(l_time, p_multiple)
                self.FIN = -1
                if res_edit :
                    self.SetFileGeneric(os.path.join(self.EPISODE_PATH, self.EPISODE_NAME), self.NEW_ENTRY[0], False, True)
                return res_edit
            elif l_reponse == l_first_option + 3 :
                l_list = [  "Modifier ({}s) et sauter".format(l_duree),
                            "Modifier ({}s) pour tous".format(l_duree),
                        ]
                l_retour = self.handleMenu(l_list)

                if l_retour == 0 :
                    self.DUREE_GENERIC = self.DefineNumber(0, l_list[l_retour])
                    return self.ShowOptions(self.DUREE_GENERIC, l_time)
                elif l_retour == 1 :
                    self.DUREE_GENERIC = self.DefineNumber(0, l_list[l_retour])
                    self.SET_GLOBAL = True
                    if self.DUREE_GENERIC > 0 :
                        self.SetFileGeneric(os.path.join(self.EPISODE_PATH, self.EPISODE_NAME), [])
                        self.SET_GLOBAL = False
                        return True
                KodiMove.Log.Show("PARAM :: " + str(l_retour))
                #l_retour = param
            elif l_reponse == l_first_option + 4 :
                xbmc.Player().pause()
            elif l_reponse == -1 :
                KodiMove.Log.Show("ABORT !" + str(xbmc.getCondVisibility("Player.Paused")))
                xbmc.executebuiltin('XBMC.PlayerControl(Play)')
                if bool(xbmc.getCondVisibility("Player.Paused")) :
                    xbmc.Player().pause()
                # xbmc.Player().play()
                return False

        self.MAIN_MENU = True
        return False

    """
    Gestion des sauts existants,
    on propose de : modifier, supprimer, vérifier le saut
    """
    def ConfirmJump(self, l_reponse) :
        l_jump = self.generic[self.EPISODE_NAME][l_reponse]
        dialog = xbmcgui.Dialog()
        ret = dialog.yesnocustom("Confirmation du saut", 'Vérifier ' + str(l_jump) + ' ?', 'Vérifier', "Modifier", "Supprimer")

        if ret == 0 :
            # On modifie l'entrée
            new_time = self.DefineTextTime(l_jump, "Jump syntax : BEGIN_TIME+DURATION" + str(l_jump))
            a_new_time = new_time.split("+")

            if (isinstance(float(a_new_time[0]), float) and isinstance(float(a_new_time[1]), float)) :
                self.generic[self.EPISODE_NAME][l_reponse] = new_time # str(new_time) + "+" + str(l_jump.split("+")[1])
                self.SetFileGeneric(os.path.join(self.EPISODE_PATH, self.EPISODE_NAME), sorted(self.generic[self.EPISODE_NAME]), True)
                return True
            else :
                self.truc.SendNotificationToKodi("Erreur de format", new_time)
                return False
        if ret == 1 :
            # On supprime l'entrée
            self.generic[self.EPISODE_NAME].remove(l_jump)
            self.SetFileGeneric(os.path.join(self.EPISODE_PATH, self.EPISODE_NAME), sorted(self.generic[self.EPISODE_NAME]), True)
            return True
        elif ret == 2 :
            # On se déplace au temps prévu
            l_time = float(l_jump.split("+")[0])
            xbmc.Player().seekTime(l_time)
            return self.ConfirmJump(l_reponse)
        else :
            return False

    """
    Affiche la fenetre d'option, puis renvoie les infos
    En instanciant les objets dont on a besoin
    """
    def ShowDialog(self, menu, p_title = "Generix", p_label = "", p_text = "") :
        # l_generic_current = []
        l_text_episode = []
        # Durations globale et par épisode
        # l_durations = [0, 0]
        l_retour = -1
        try :
            param = self.handleMenu(menu)

            KodiMove.Log.Show("PARAM :: " + str(self.EPISODE_NAME) +" in " + str(self.generic))
            if param is None :
                param = -1
            l_retour = param
        except Exception as e :
            KodiMove.Log.Show(e)

        return l_retour

    def handleMenu(self, menu):
        list  = []
        for item in menu:
            list.append(str(item))

        self.WINTOP = True
        self.wGeneric = xbmcgui.Dialog()
        # param = self.wGeneric.select("test", list)
        param = self.wGeneric.contextmenu(list)
        # time.sleep(5)
        # KodiMove.Log.Show("TRY TO CLOSE : " + str(self.wGeneric.close()))
        self.WINTOP = False
        # if param < 0:
        #     return None

        return param

#   END Interactions

#   BEGIN Search for jump

    """
    Clavier numérique pour définir le temps.
    """
    def DefineNumber(self, p_val = "", p_text = "Please give me a number") :
        dialog = xbmcgui.Dialog()
        #path_video = xbmc.getInfoLabel('Player.FileNameAndPath')
        d = dialog.numeric(0, p_text, str(p_val))

        return int(d)

    """
    Clavier numérique pour définir le temps.
    """
    def DefineTextTime(self, p_val = "", p_text = "Please give me a text") :
        dialog = xbmcgui.Dialog()
        #path_video = xbmc.getInfoLabel('Player.FileNameAndPath')
        d = dialog.input(p_text, str(p_val))

        return d

    #"""
    #Cherche le temps de début du générique.
    #"""
    #def SearchBeginGeneric(self, l_time, p_multiple) :
        #l_new_time = l_time + (self.DUREE_GENERIC * p_multiple)
        ## On a aucune info jusqu'a maintenant,
        ##while not self.FIN > -1 :

        ## Smooth mode, on va attendre jusqu'au générique...
        #if self.SMOOTH_MODE :
            #time.sleep(self.DUREE_GENERIC * p_multiple)
            #l_new_time = xbmc.Player().getTime()
        #else :
            ## On va a un certain pourcentage de la durée du générique
            #xbmc.Player().seekTime(l_new_time)

        #l_time = xbmc.Player().getTime()
        ##self.MULTIPLE = p_multiple

        ##l_text = "Begining search (estimation)"
        ###if self.DEBUT  > -1 :
        ##dialog = xbmcgui.Dialog()
        ##ret = dialog.yesnocustom(l_text, 'Have you reach generic at ' + str(l_new_time) + " ?", 'Revenir', "Oui", "Non", 10000)

        #l_text = "Begining search (estimation)"


        #l_list = ["Oui", "Non", "Définir la durée (" + str(self.DUREE_GENERIC) + ")"]
        #ret = self.ShowDialog(l_list, l_text, 'Have you reach generic at ' + str(l_new_time) + " ?", "Episode : {}".format(self.EPISODE_NAME))


        #if ret == 0 :
            #self.DEBUT = l_time
        #else :
            #self.SearchBeginGeneric(l_time, 0.9)
        ##else :
        #return -1

    """
    Cherche le temps de début de la time line a skipper.
    """
    def SearchGenericToSkipNext(self, l_time, p_multiple) :
        l_new_time = round(l_time + (10 * p_multiple), 2)

        xbmc.Player().seekTime(l_new_time)

        l_time = xbmc.Player().getTime()
        if p_multiple < 0.2 :
            KodiMove.Log.Show("Trouvé ! " + str(l_time))
        else :
            p_multiple -= 0.1
        #self.MULTIPLE = p_multiple

        #l_text = "Begining search (estimation)"
        ##if self.DEBUT  > -1 :
        dialog = xbmcgui.Dialog()
        #ret = dialog.yesnocustom(l_text, 'Have you reach generic at ' + str(l_new_time) + " ?", 'Revenir', "Oui", "Non", 10000)

        l_text = "Search for skip next"
        ret = dialog.yesnocustom(l_text, 'Always on generic at ' + str(l_new_time) + " ?", 'Définir', "Oui", "Non")

        if ret == 0 :
            if p_multiple > 0 :
                p_multiple *= -1
            self.SearchGenericToSkipNext(l_time, p_multiple)
        elif ret == 1 :
            if p_multiple < 0 :
                p_multiple *= -1
            self.SearchGenericToSkipNext(l_time, p_multiple)
        elif ret == 2 :
            self.NEW_ENTRY.append(str(l_time) + "+0")
            #self.SEARCHING = True
            #self.FIN = l_time
            #self.DEBUT = l_time - self.DUREE_GENERIC
        else :
            return -1
        #else :
        return -1

    """
    Cherche le temps de fin du générique.
    """
    def SearchEndGeneric(self, p_time, p_multiple) :
        #if p_multiple_last - p_multiple < 0.
        l_new_time = p_time + (self.DUREE_GENERIC * p_multiple)
        l_last_time = p_time - (self.DUREE_GENERIC * (1-p_multiple))
        l_multiple = p_multiple
        l_text = "Ending search"
        l_diff = (p_time - l_last_time) / 2

        KodiMove.Log.Show("multiple : " + str(p_multiple))
        KodiMove.Log.Show("diff : " + str(p_time) + " > " + str(l_last_time))

        if p_multiple >= 0.99 :
            KodiMove.Log.Show("On a trouvé ! 0.99")
            #if p_multiple > 1 :
                #ret = dialog.yesnocustom("Last verification !", 'Always on generic at ' + str(l_new_time) + " ?", 'Définir', "Oui", "Non", 10000)


            KodiMove.Log.Show("DIFF : " + str(l_diff) + " " + str(p_time - l_last_time))

            self.FIN = round(l_last_time + self.DUREE_GENERIC, 2)
            self.DEBUT = round(l_last_time - l_diff, 2)

            self.NEW_ENTRY.append(str(self.DEBUT) + "+" + str(round(self.DUREE_GENERIC - l_diff, 2)))
            #self.SEARCHING = True

            # On switch a la fin du génerique
            xbmc.Player().seekTime(self.DEBUT + self.DUREE_GENERIC)
            # Et on met la pause pour l'enregistrement
            #xbmc.Player().pause()

            self.SetFileGeneric(os.path.join(self.EPISODE_PATH, self.EPISODE_NAME), self.NEW_ENTRY)
            self.truc.SendNotificationToKodi("Nouvelle entrée ajoutée !","{}+{}".format(kodiUtils.KodiTools.HRTime(self.DEBUT), self.DUREE_GENERIC - l_diff))
            return True

        # On a aucune info jusqu'a maintenant,
        #while not self.FIN > -1 :
            # On va a un certain pourcentage de la durée du générique
        xbmc.Player().seekTime(l_new_time)

        l_time = xbmc.Player().getTime()
        #self.MULTIPLE = p_multiple

        dialog = xbmcgui.Dialog()
        ret = dialog.yesnocustom(l_text, 'Always on generic at ' + str(kodiUtils.KodiTools.HRTime(l_new_time, False)) + " ?", 'Définir', "Oui", "Non")

        if ret == 0 :
            KodiMove.Log.Show("+oui " + str(p_multiple + 0.10) + " " + str(self.DEBUT))
        # On est encore sur le générique, on va reduire la fenetre de tir pour chercher la fin
        # Saut de 5% : l_new_time = l_time + (self.DUREE_GENERIC * 0.95)
            if p_multiple >= 0.95 or p_multiple + 0.10 > 1:
                #else :
                p_multiple += 0.02
                #if p_multiple > 1 :
                    #p_multiple = 0.97
                    #l_last_time = l_last_time - (self.DUREE_GENERIC * (1-p_multiple))
            elif p_multiple >= 0.85 :
                p_multiple +=  0.10
            else :
                p_multiple += 0.15
            self.SearchEndGeneric(p_time, p_multiple)
        elif ret == 2 :
            self.FIN = l_time
            self.DEBUT = round(l_time - self.DUREE_GENERIC, 2)
        else :
            KodiMove.Log.Show("-non : " + str(p_multiple + 0.10) + " " + str(self.DEBUT))
            if p_multiple > 0.95 or p_multiple + 0.10 > 1 :
                p_multiple -= 0.02
                #if p_multiple > 1 :
                    #p_multiple = 0.97
                    #l_last_time = l_last_time - (self.DUREE_GENERIC * (1-p_multiple))
            elif p_multiple >= 0.85 :
                p_multiple +=  0.10
            else :
                p_multiple += 0.15
            self.SearchEndGeneric(l_last_time, p_multiple)

        return False


        #if self.DEBUT

#   END Search

#   BEGIN Edition du fichier

    """
    # Envoie une requete pour insérer une nouvelle entrée dans le fichier
    """
    def SetFileGeneric(self, path_video, skip_info, p_reset = False, p_global = False) :
        try :
            l_s_fileName = os.path.basename(path_video)
            if l_s_fileName in self.generic :
                #self.generic[os.path.basename(path_video)]
                KodiMove.Log.Show(self.generic[l_s_fileName])

            # Adresse, port
            # On construit les parametre a envoyer
            param_json = {"path" : path_video,
                          #"source_dir" : MAIN_SOURCE_DIR,
                          "skip_info" : skip_info
                         }
            if p_global :
                param_json["as_global"] = p_global

            if p_reset :
                param_json["reset"] = True

            if self.SET_GLOBAL :
                param_json["global"] = [self.DUREE_GENERIC]

            data = self.socket.Query(Constants.CmdSocket.SETSKIPENTRY, param_json)
            KodiMove.Log.Show("DATA:" + str(data) + " from " + str(self.socket.ADDR))
            if "error" in data :
                self.truc.SendNotificationToKodi("Error !", str(data["error"]) + " from " + str(self.socket.ADDR), 5000)
                #return {"error": str(data["error"]) + " from " + str(self.socket.ADDR) }
            else :
                self.truc.SendNotificationToKodi(str(len(skip_info)) + " nouvelle(s) entrée(s) sauvée !", str(skip_info), 5000)
            self.NEW_ENTRY = []

            # On envoie la requete et on recupere le resultat...
            #data = ClientSocket().ClientQuery(addr, "setSkipEntry", param_json)
            self.generic = self.GetFileGeneric(path_video)
            KodiMove.Log.Show("GENERIX:" + str(self.generic))
        except Exception as e:
            return {"error": str(e) + " from " + str(self.socket.ADDR) }
        finally :
            return data

#   END Edition

# END Main class !

# def KodiMove.Log.Show(log_str) :
#     xbmc.log("Generix : " + str(log_str), 1)

