#!/usr/bin/env python
# -*- coding: utf-8 -*-

from . import ClientSocket
from . import Constants

# BEGIN Queries to send from clients
class QuerySocket(ClientSocket.ClientSocket) :

    def __init__(self, host, port, bufsize = 4096):
        super(QuerySocket, self).__init__(host, int(port), bufsize)

        self.LIST = {}
        self.SOURCES = []
        self.DIRS = []
        self.KEEP = 0
        self.SKIP = 0

    def GetListFiles(self, array) :
        files = []
        for f in array :
            files.append(f[0])
        return files

    def CallPlaylistIndex(self, saga, index, protocol, source_dir = "", keep = -1, skip_firsts_dir = -1) :
        """ Get last played elements (with path in param) """
        try :
            if source_dir == "" :
                if len(self.DIRS) > 0 :
                    source_dir = self.DIRS[0]
                else :
                    raise Exception("Source dir is needed !")
            if keep == -1 :
                keep = self.KEEP
            if skip_firsts_dir == -1 :
                skip_firsts_dir = self.SKIP

            param_json = { "saga" : saga,
                            "index" : index,
                            "keep" : keep,
                            "skip_firsts_dir" : skip_firsts_dir,
                            "source_dir" : source_dir,
                            "protocol" : protocol }
            data = self.Query(Constants.CmdSocket.LISTPLAYING, param_json) #.decode()
            # print("DATA :: " + str(data))
            if data :
                if "list" in data :
                    self.LIST = self.GetListFiles(data["list"])
                return data
            else :
                return {}
        except Exception as e:
            return {"error": str(e) }

    def CallPlaylist(self, path_video, protocol, source_dir = "", keep = -1, skip_firsts_dir = -1, index = -1, create = False) :
        """ Get last played elements (with path in param) """
        try :
            if source_dir == "" :
                if len(self.DIRS) > 0 :
                    source_dir = self.DIRS[0]
                else :
                    raise Exception("Source dir is needed !")
            if keep == -1 :
                keep = self.KEEP
            if skip_firsts_dir == -1 :
                skip_firsts_dir = self.SKIP

            param_json = {"path" : path_video,
                            "keep" : keep,
                            "skip_firsts_dir" : skip_firsts_dir,
                            "source_dir" : source_dir,
                            "create" : False,
                            "index" : index,
                            "protocol" : protocol }
            if create :
                param_json["create"] = True
            data = self.Query(Constants.CmdSocket.LISTPLAYING, param_json) #.decode()
            if data :
                # print("DATA :: " + str(data))
                if "error" in data :
                    raise Exception(str(e))

                if "list" in data :
                    self.LIST = self.GetListFiles(data["list"])
                return data
            else :
                return {}
        except Exception as e:
            return {"error": str(e) }

    def CallCleanLinks(self, saga, source_dir) :
        """ Delete saga links """
        try :
            param_json = { "saga" : saga, "source_dir" : source_dir }
            data = self.Query(Constants.CmdSocket.CLEANLINKS, param_json) #.decode()

            if data :
                return data
            else :
                return {}
        except Exception as e:
            return {"error": str(e) }

    def UpdateLink(self, path_video, keep = 0, skip_firsts_dir = 0) :
        """ Update played element """
        try :
            param_json = {  "path" : path_video,
                            "keep" : keep,
                            "skip_firsts_dir" : skip_firsts_dir}
            data = self.Query(Constants.CmdSocket.UPDATELINK, param_json)
            if data :
                return data
            else :
                return {}
        except Exception as e :
            return {"error": str(e) }

    def ReloadGenerix(self, source_dir) :
        """ Update played element """
        try :
            param_json = source_dir
            data = self.Query(Constants.CmdSocket.RELOADKEEPENTRY, param_json)
            if data :
                return data
            else :
                return {}
        except Exception as e :
            return {"error": str(e) }

    def CallGenerixMiss(self, skip_firsts_dir = 0, saga = "") :
        """ Get files list which does not have jump. """
        try :
            param_json = { "skip_firsts_dir" : skip_firsts_dir}
            if saga != "" :
                param_json["saga"] = saga
            data = self.Query(Constants.CmdSocket.MISSGENERIX, param_json)
            return data
        except Exception as e:
            return {"error": str(e) }

    ### TO checked if used
    def Init(self, l_keep = 0, l_skip = 0, l_dir = "") :
        self.DIRS = []
        self.KEEP = l_keep
        self.SKIP = l_skip

        res = ""
        try :
            # print("INI3")
            # print("ok '{}'".format("m"))
            res = self.TestSocket()
            if len(res) > 0 :
                self.DIRS = res
            # print(res)
            # print(self.DIRS)
            return res
            # super(QuerySocket, self).__init__(self.HOST_SOCKET, self.PORT_SOCKET)
            # # self.socket = QuerySocket(self.HOST_SOCKET, self.PORT_SOCKET)
            # return self.Init(l_keep, l_skip, l_dir)

        except KeyboardInterrupt as e:
            print("Interruption Init socket")
        # except Exception as e :
            raise KeyboardInterrupt(e)
        except Exception as e:
            raise Exception(e)
            # return {}
        # finally :
        #     return res

    def InitWebServer(self, user = "kodi", password = "", port = 8080) :
        # Init web server information #
        self.WEB_SRV = { 'user' : user, "port" : port, "pass" : password }

    def CallLastPlayed(self, source_dir = "", keep = -1, skip_firsts_dir = -1) :
        # Get last played elements from socket (no path in param) #
        try :
            if source_dir == "" :
                if len(self.DIRS) > 0 :
                    source_dir = self.DIRS[0]
                else :
                    raise Exception("Source dir is needed !")
            if keep == -1 :
                keep = self.KEEP
            if skip_firsts_dir == -1 :
                skip_firsts_dir = self.SKIP

            param_json = {  "keep" : keep,
                            "skip_firsts_dir" : skip_firsts_dir,
                            "source_dir" : source_dir}
            data = self.Query(Constants.CmdSocket.DIRSPLAYING, param_json) #.decode()
            if data :
                return data
            else :
                return {}
        except Exception as e:
            return {"error": str(e) }
    """ TODO :: To be deleted : Simplified but should be the same as update link

    # def GetDirsEncours(self, source_dir, keep = 0) :
    #     # Get folders monitored by socket #
    #
    #     # Building parameters : main path
    #     param_json = { "source_dir" : source_dir, 'keep' : int(keep) }
    #     # Sending query...
    #     data = self.Send(Constants.CmdSocket.DIRSPLAYING, param_json).decode()
    #     print(data)
    #     return data
    #
    def CallGenerateLinks(self, path_video, protocol, source_dir, keep = 0, skip_firsts_dir = 0) :

        try :
            param_json = {  "path" : path_video,
                            "keep" : keep,
                            "skip_firsts_dir" : skip_firsts_dir,
                            "source_dir" : source_dir,
                            "protocol" : protocol}
            data = self.Query(Constants.CmdSocket.UPDATELINK, param_json)
            if data :
                return data
            else :
                return {}
        except Exception as e:
            return {"error": str(e) }
    """
    ### BEGIN "TO REIMPLEMENT"...
    def SynchroServers(self, host2sync, host2from, port, list_path) :
        """ Synchronise les serveurs et/ou les clients """
        try :
            # On construit les parametre a envoyer : la saga, et le chemin du fichier
            param_json = {"list" : list_path,
                          "from" : host2from ,
                          "to" : host2sync,
                          "source_dir" : MAIN_SOURCE_DIR}
            # On envoie la requete et on recupere le resultat...
            data = self.Send(Constants.CmdSocket.SYNCPLAY, param_json).decode()
            # ... En Json
            res_srv = json.loads(data)

            return res_srv
        except Exception as e:
            #logXbmc(e)
            return {"error": str(e) }
    # END "TO REIMPLEMENT"...


# END Client Socket
   

