import os
import hashlib
from getpass import getpass
import json

PEPPER = "PEPPER_KEY_SHOULD_BE_CHANGED_WITHIN APP"
HASH_NAME = 'sha256'
ITER = 100_000
RDM_CHARS = 16

class PassManage() :
    # Default values

    # Will changed based parameters
    def __init__(self, pepper = PEPPER, hash_name = HASH_NAME, iterations = ITER, rdm_chars = RDM_CHARS) :
        PassManage.PEPPER = pepper
        PassManage.HASH_NAME = hash_name
        PassManage.ITER = iterations
        PassManage.RDM_CHARS = rdm_chars
        self.dico = {}

    def create(self, msg_username = "Username : ", msg_pass = "") :
        username = input(msg_username)
        if msg_pass != "" :
            password = getpass(msg_pass)
        else :
            password = getpass()
        hash_pass = PassManage.create_secure_password(password)
        self.dico[username] = hash_pass


    def get(self, username, password) :
        if not username in self.dico :
            print("Invalid credential")
            return False
        return PassManage.login(self.dico[username], password)

    # BEGIN Hash passwords
    # Pulled from https://www.askpython.com/python/examples/storing-retrieving-passwords-securely
    # CAUTION :: Why in login function, ALGO parameters is not sha256 ???
    def create_secure_password(password):
        """ Return list with """
        try :
            salt = os.urandom(PassManage.RDM_CHARS)
            iterations = PassManage.ITER
            hash_value = hashlib.pbkdf2_hmac(
                    PassManage.HASH_NAME,
                    password.encode('utf-8') + PassManage.PEPPER.encode('utf-8'),
                    salt,
                    PassManage.ITER
            )
            password_hash = salt + hash_value
            hash_algo = PassManage.HASH_NAME
            salt, key = password_hash[:PassManage.RDM_CHARS], password_hash[PassManage.RDM_CHARS:]

            return [ salt, key, hash_algo, iterations ]
        except Exception as ex :
            raise ex

    def login(pass_user_info, password, encrypted = False):
        """ Return true if passwords matches... """
        try :
            # print(dicoPass)
            salt, key, hash_algo, iterations = pass_user_info

            if encrypted :
                password_hash = password
            else :
                # print("PASS {}".format(password.encode('utf-8')))
                # print("SALT {}".format(salt))
                # print("KEY {}".format(key))
                # print("ALGO {}".format(hash_algo))
                # print("ITER {} == {}".format(iterations, PassManage.RDM_CHARS))
                # Recompute hash from user entered password
                password_hash = hashlib.pbkdf2_hmac(
                    hash_algo,
                    password.encode('utf-8') + PassManage.PEPPER.encode('utf-8'),
                    salt,
                    iterations
                )
                # salt, key = password_hash[:PassManage.RDM_CHARS], password_hash[PassManage.RDM_CHARS:]
                # print(key)
            # print("{}" .format(password))
            # print("{} == {}" .format(password_hash, key))
            # Compare hashes
            if password_hash == key:
                print("Login successful")
                return True
            else:
                print("Invalid credential")
                return False
        except Exception as ex :
            raise ex
    # END Hash passwords

class StorePass(PassManage) :
    def __init__ (self, file_pass, pepper = PEPPER, hash_name = HASH_NAME, iterations = ITER, rdm_chars = RDM_CHARS) :
        self.dicoPass = {}
        self.FILE_PASS = file_pass
        super().__init__(pepper, hash_name, iterations, rdm_chars)
        self.load()

    def load(self) :
        if os.path.exists(self.FILE_PASS) :
            with open(self.FILE_PASS, 'rb') as f:
                self.dicoPass = json.load(f)

    def save(self) :
        if len(self.dicoPass) > 0 :
            with open(self.FILE_PASS, 'w') as fp:
                json.dump(self.dicoPass, fp)

dicoPass = {}
new_user = StorePass("/tmp/.test_pass.json")
new_user.create()

print(new_user.dico)

username = input("Username : ")
password = getpass()
new_user.get(username, password)
# new_user.get(username, password)
# dicoPass[username] = PassManage.create_secure_password(password)
# dicoPass = create_secure_password(password)

# PassManage.login(username, "bon")
