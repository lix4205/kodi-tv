#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import json
from random import shuffle
import hashlib
from threading import Thread
from .Constants import Path, CmdSocket

class Md5Mng() :
    def __init__ (self, file_md5, reverse = False) :
        self.dicoMd5 = {}
        self.FILE_MD5 = file_md5
        self.Load(reverse)

    def Load(self, reverse = False) :
        if os.path.exists(self.FILE_MD5) :
            with open(self.FILE_MD5, 'rb') as f:
                self.dicoMd5 = json.load(f)
        if reverse :
            self.dicoMd5 = {v: k for k, v in self.dicoMd5.items()}

    def GetMd5(self, l_file, b_save = False, force_compute = False) :
        l_s_fileName = os.path.basename(l_file)
        if not force_compute and l_s_fileName in self.dicoMd5 :
            l_s_md5 = self.dicoMd5[l_s_fileName]
        else :
            l_s_md5 = Md5Mng.md5(l_file)
            self.dicoMd5[l_s_fileName] = l_s_md5
            if b_save :
                self.Save()
        return l_s_md5

    def Save(self) :
        if len(self.dicoMd5) > 0 :
            with open(self.FILE_MD5, 'w') as fp:
                json.dump(self.dicoMd5, fp)

    def md5(fname) :
        """ Md5 computing """
        hash_md5 = hashlib.md5()
        with open(fname, "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                hash_md5.update(chunk)

        return hash_md5.hexdigest()
#
class TagsList() :

    def GetCats(self, tags, obj_folder) :
        cats = []
        for tag in tags :
            if tag == "" :
                continue
            cat = obj_folder.SearchForCat(tag)
            if cat == "" :
                raise Exception("{} was not found !".format(tag))
            if cat not in cats :
                cats.append(cat)

        # l_linked_cat = self.tags.GetLinkedCat(cats)
        # print(l_linked_cat)
        return cats

    def GetList(self, folder, tags, random_sample = False, split_time = -1, quality = None) :
        dicoTagged = {}
        # cats = self.GetCats(tags, TagsMng.LIST_INDEX_FILES[folder])
        dicoFolder = self.dirs[os.path.realpath(folder)]
        for d in dicoFolder :
            if os.path.realpath(d) == os.path.realpath(folder) :
                filter_on = ""
            else :
                filter_on = os.path.realpath(d).replace(os.path.join(folder, ""), "")
            dicoTmp = TagsMng.LIST_INDEX_FILES[folder].SearchForMultipleTags(tags, filter_on, quality)
            # print(dicoTmp)
            if folder not in TagsMng.LIST_TAGGED :
                TagsMng.LIST_TAGGED[folder] = {}
            TagsMng.LIST_TAGGED[folder] = dicoTmp
            for d in dicoTmp :
                dicoTagged[d] = dicoTmp[d]
            # TagsMng.LIST_TAGGED[folder] += TagsMng.LIST_INDEX_FILES[folder].SearchForMultipleTags(cats, random_sample, filter_on, split_time)
            # print(TagsMng.LIST_TAGGED[folder])
            for tagged in TagsMng.LIST_TAGGED[folder] :
                if tagged not in self.vidTagged :
                    # print(TagsMng.LIST_TAGGED[folder][tagged])
                    self.vidTagged[tagged] = TagsMng.LIST_TAGGED[folder][tagged]

        self.root_file = folder
        self.md5 = TagsMng.LIST_MD5_FILES[self.root_file]
        self.tags = TagsMng.LIST_INDEX_FILES[self.root_file]
        # self.vidTagged = TagsMng.LIST_TAGGED[self.root_file]
        # print(len(self.vidTagged))
        self.cat = tags[0]
        # self.catRead = cats
        # self.catSearch = cat
        return dicoTagged

    def HasInfoFile(self, l_eps) :
        l_s_md5 = self.md5.GetMd5(l_eps)
        if l_s_md5 not in self.tags.dicoTimeStamps :
            return False
        else :
            if "infos" not in self.tags.dicoTimeStamps[l_s_md5] :
                return False
        return True

    def SetInfoFile(self, l_eps, qual, fps, duration = -1) :
        infos = { }
        l_s_md5 = self.md5.GetMd5(l_eps)
        a_qual = qual.split("x")

        if l_s_md5 not in self.tags.dicoTimeStamps :
            print("{} : {} does not exists in dico file".format(l_s_md5, l_eps))
            return False
        else :
            if "infos" not in self.tags.dicoTimeStamps[l_s_md5] :
                infos["w"] = int(a_qual[0])
                if a_qual[1].endswith(",") :
                    a_qual[1] = a_qual[1][0:-1]
                # print(a_qual)
                infos["h"] = int(a_qual[1])
                infos["fps"] = float(fps)
                self.tags.dicoTimeStamps[l_s_md5]["infos"] = infos

            if "duration" not in self.tags.dicoTimeStamps[l_s_md5] and duration > 0 :
                self.tags.dicoTimeStamps[l_s_md5]["duration"] = duration
            return True
            # print(self.tags.dicoTimeStamps[l_s_md5]["infos"])

    # def GetInfoFiles(self) :
    #     l_s_md5 = self.md5.GetMd5(self.eps)
    #
    #     if l_s_md5 not in self.tags.dicoTimeStamps :
    #         print("{} : {} does not exists in dico file".format(l_s_md5, self.eps))
    #     else :
    #         if "infos" in self.tags.dicoTimeStamps[l_s_md5] :
    #             if "fps" in self.tags.dicoTimeStamps[l_s_md5]["infos"] :
    #                 return self.tags.dicoTimeStamps[l_s_md5]["infos"]["fps"]
    #     return 30

    def NewSample(self, beg, end, duration) :
        """ Will save new sample from beg to end """
        try :
            new_tag = []
            new_time = [ beg, end, end - beg ]
            l_s_md5 = self.md5.GetMd5(self.eps)
            if not len(self.catList) > 0 :
                self.catList = [self.catSearch]

            if self.catSearch != self.catRead :
                self.catRead = self.catList

            # print(self.catList)
            l_linked_cat = self.tags.GetLinkedCat(self.catList)
            print(l_linked_cat)
            for cat in l_linked_cat :
                new_tag = self.tags.NewTag(l_s_md5, self.eps, cat, new_time, duration)
                if len(new_tag) > 0 :
                    self.vidTagged[os.path.realpath(self.eps)] = self.tags.GetTimeStamps(l_s_md5, self.catRead)
                # self.counter_save = [self.counter_save[0] + 1, self.counter_save[1] + new_tag[2]]
            return new_tag
        except Exception as e :
            print("NewSample error !")
            raise(e)

    def DelSample(self, end, smp) :
        try :
            tag_modified = 0
            if self.DEL_SAMPLE >= 0 :
                beg = self.DEL_SAMPLE
                new_time = [ beg, end, end - beg ]
                l_s_md5 = self.md5.GetMd5(self.eps)

                for cat_delete in self.catList :
                    dico = self.tags.dicoTimeStamps[l_s_md5]["tags"][cat_delete]
                    # print(dico)
                    for ts in self.dico_eps :
                        a_new = self.GetNewArray(beg, end, ts)
                        if len(a_new) > 0 :
                            dico.remove(ts)
                            tag_modified += 1
                            for new_ts in a_new :
                                if len(new_ts) > 0 :
                                    dico.append(new_ts)

                        self.tags.dicoTimeStamps[l_s_md5]["tags"][cat_delete] = dico

                if tag_modified > 0 :
                    self.DEL_SAMPLE = -1
                    self.tags.Save()
                    #
                    self.vidTagged[os.path.realpath(self.eps)] = self.tags.GetTimeStamps(l_s_md5, self.catList)

            return tag_modified
        except Exception as e :
            print("DelSample error !")
            raise(e)

    def GetNewArray(self, beg, end, smp) :
        # Out of bounds
        if end < smp[0] :
            return []
        if beg > smp[1] :
            return []
        # print(beg, end, smp)
        # if beg > smp[0] :

        # return []


        new_tag_1 = []
        new_tag_2 = []
        if beg > smp[0] :
            # print("{} > {}".format(beg, smp[0]))
            new_tag_1.append(smp[0])
            if end < smp[1] :
                # print("{} > {}".format(end, smp[1]))
                new_tag_1.append(beg)
                new_tag_2 = [ end, smp[1] ]
            else :
                # print("{} <= {}".format(end, smp[1]))
                new_tag_1.append(beg)
                # new_tag_2 = [ smp[0], beg ]
        else :
            # print("{} <= {}".format(beg, smp[0]))
            new_tag_1 = [end, smp[1]]
            if end >= smp[1] :
                new_tag_1 = []

        return [ new_tag_1, new_tag_2 ]

    def GoToSample(self, direction, pos, goToEnd = True) :
        tagged = []
        res = []
        dicoSmp = self.GetDicoEps()
        tagged = sorted(dicoSmp)
        if len(tagged) == 0 :
            print("No {} timestamps defined for '{}'.".format(self.cat, os.path.basename(self.eps)))
            return []
        # print("ok")

        cur_sample = self.FindIndexNextJump(tagged, pos)
        if not self.ADD_SAMPLE and self.DEL_SAMPLE > 0 :
            # print("Will de"
            self.DelSample(tagged[self.count_vid[self.eps]][1])
            # print("DEL : {} {}".format(self.DEL_SAMPLE, p))

        # elif not self.ADD_SAMPLE and self.NEW_SAMPLE < 0 :
        else :
            if self.eps not in self.count_vid :
                self.count_vid[self.eps] = -1

            END = False
            # Sample counter
            cur_cnt = self.count_vid[self.eps]
            if len(tagged) > 0 :
                if direction > 0 :
                    # Init to have the first sample :
                    if cur_cnt < 0 :
                        cur_cnt = 0
                    else :
                        if cur_sample + 1 == len(tagged) :
                            # begin = tagged[cur_cnt][1]
                            cur_cnt = 0
                            if goToEnd :
                                END = True
                        else :
                            cur_cnt = cur_sample + 1
                else :
                    if cur_sample < 0 :
                        cur_cnt = len(tagged) - 1
                    else :
                        cur_cnt = cur_sample
                        if pos > tagged[cur_cnt][0] and pos < tagged[cur_cnt][1] :
                            cur_cnt -= 1

                if END :
                    begin = tagged[cur_cnt][1]
                    cur_cnt = -1
                else :
                    begin = tagged[cur_cnt][0]
                # print(str(cur_sample) + " " + str(len(tagged)))
                res = [ begin, tagged[cur_cnt][1] ]
                self.count_vid[self.eps] = cur_cnt

        return res

    def GetDicoEps(self, random_sample = False, split_time = -1) :
        dicoEps = []
        l_eps = self.eps
        # # Check if we're on the same saga
        a_saga = self.tags.SearchSaison(self.eps, self.tags.dicoTimeStamps["__general__"]["__sagas__"])
        if self.saga != a_saga[1] :
            # print(a_saga)
            self.saga = a_saga[1]
            # print(self.tags.dicoTimeStamps["__general__"]["__sagas__"][self.saga]["jumps"])
            if self.saga in self.tags.dicoTimeStamps["__general__"]["__sagas__"] and "jumps" in self.tags.dicoTimeStamps["__general__"]["__sagas__"][self.saga] :
                if len(self.tags.dicoTimeStamps["__general__"]["__sagas__"][self.saga]["jumps"]) :
                    self.jump = self.tags.dicoTimeStamps["__general__"]["__sagas__"][self.saga]["jumps"][0]
        # self.pref_jump = self.dicoTimeStamps["__general__"]["__sagas__"][

        # print(self.tags.dicoTimeStamps)
        if l_eps in self.vidTagged :
            dicoEps = self.vidTagged[l_eps]

        if split_time > 0 :
            dicoEps = self.tags.SplitedTimeStamp(dicoEps, split_time)

        if random_sample :
            shuffle(dicoEps)
            return dicoEps

        return sorted(dicoEps)

    def TotalTimeList(self, dico) :
        l_count = 0
        for v in dico :
            for tsp in dico[v] :
                l_count += tsp[1] - tsp[0]
        return l_count

    def TotalTimeCat(self, folder, cat) :
        l_count = 0
        if folder in TagsMng.LIST_TAGGED :
            for f in TagsMng.LIST_TAGGED[folder] :
                for tsp in TagsMng.LIST_TAGGED[folder][f] :
                    l_count += tsp[1] - tsp[0]
        return l_count

    def TotalTime(self, folder) :
        l_count = 0
        if folder in TagsMng.LIST_TAGGED :
            for f in TagsMng.LIST_TAGGED[folder] :
                # print(TagsMng.LIST_TAGGED[folder][f])
                for tsp in TagsMng.LIST_TAGGED[folder][f] :
                    l_count += tsp[1] - tsp[0]
        # print(l_count)
        return l_count

    def CoverTime(self, duration, dico) :
        """ Return tag time duration (duration, percent, number of sample) """
        l_count = 0
        for tsp in dico :
            l_count += tsp[1] - tsp[0]

        percent_cover = round(l_count * 100 / duration, 2)
        nb_sample = len(dico)

        return [ l_count, percent_cover, nb_sample ]

    def FindIndexNextJump(self, dicoSmp, done_vid) : #, cat_keep = "") :
        idx = -1

        # if eps in self.vidTagged :
        for l_tsp in sorted(dicoSmp) :
            # print(l_tsp)
            # print("FND :: {} >= {} and {} <= {}".format(done_vid, l_tsp[0], done_vid,l_tsp[1]))
            if done_vid >= l_tsp[0] :
                # done_vid = l_tsp[1] #- (2 / 3)
                idx += 1
                if done_vid <= l_tsp[1] :
                    # print("TIDX FOUND :: " + str(idx) + " " + str(l_tsp))
                    return idx
                        # break

        # # print(self.dicoCat)
        # if idx == -1 :
        #     idx = 0
        return idx

    def FindIfJump(self, eps, done_vid, cat_keep = "", cat_avoid = "") :
        l_s_md5 = self.md5.GetMd5(eps)
        new_pos = self.tags.FindIfJump(l_s_md5, done_vid, cat_keep, cat_avoid)
        if new_pos != done_vid :
            return new_pos
        # print("{} => {}".format(done_vid, new_pos))
        return -1

    def __init__ (self, VLC_ARG) :
        # Sample management
        self.ADD_SAMPLE = False          # Indicate that we will edit a video
        self.NEW_SAMPLE = -1            # Position from which to add
        self.DEL_SAMPLE = -1            # Position from which to delete
        # self.SAMPLE_TIME = TIME         # Time to play then next

        self.counter_save = [0, 0]      # Sample counter saved
        self.catSearch = ""             # Category to save
        self.catThrow = CmdSocket.MAIN_CAT   # Category to jump
        self.catList = []               # Category list (to add with catSearch)
        self.vidTagged = {}             # Dict of sample for a category
        self.covered = 0                # Covered time of sample for a video
        self.count_vid= {}
        self.dirs = {}
        self.catList = []
        self.saga = ""
        self.jump = -1

        indexed_files = TagsMng.GetIndexFile(VLC_ARG, os.path.join(Path.DIR_ENCOURS_NAME, Path.INDEX_TAGGED))

        if not len(indexed_files) > 0 :
            raise Exception("No index file in {}".format(",".join(VLC_ARG)))

        for f in indexed_files :
            FILE_INDEX = f
            FILE_MD5 = os.path.join(FILE_INDEX, os.path.join(Path.DIR_ENCOURS_NAME, Path.INDEX_MD5))
            FILE_TIME = os.path.join(FILE_INDEX, ".tags_tv.json")

            TagsMng.LIST_INDEX_FILES[f] = TagsMng(os.path.join(FILE_INDEX, os.path.join(Path.DIR_ENCOURS_NAME, Path.INDEX_TAGGED)), FILE_INDEX)
            TagsMng.LIST_MD5_FILES[f] = Md5Mng(FILE_MD5)

            self.dirs[os.path.realpath(f)] = indexed_files[f]

class TagsMng() :
    LIST_INDEX_FILES = {}
    LIST_MD5_FILES = {}
    LIST_TAGGED = {}

    def GetPathSagas(self) :
        l = []
        dicoTmp = self.dicoTimeStamps["__general__"]["__sagas__"]
        for k in dicoTmp :
            if dicoTmp[k]["path"] not in l :
                l.append(dicoTmp[k]["path"])
        return l
    def GetIndexFile(paths, pattern) :
        # try :
        paths_index = {}
        for p in paths :
            paths = TagsMng.FindIndexFile(p, pattern)
            if len(paths) > 1 :
                if paths[0] not in paths_index :
                    paths_index[paths[0]] = []
                paths_index[paths[0]].append(paths[1])

        return paths_index

    def FindIndexFile(path, pattern) :
        # try :
        paths_index = []
        # for p in paths :
        p = path
        if os.path.isdir(os.path.realpath(p)) :
            folder = p
            new_path = os.path.join(folder, pattern)
            # print(new_path)
            while not os.path.exists(new_path) and folder != "/" :
                folder = os.path.dirname(folder)
                new_path = os.path.join(folder, pattern)

            if folder != "/" :
                if p not in paths_index :
                #     paths_index.append = []
                    paths_index.append(p)
            # else :
            #     print("MANAGE root !!!")
        else :
            # print("TODO : Manage files only " + p)
            if os.path.isfile(os.path.realpath(p)) :
                paths_index = TagsMng.FindIndexFile(os.path.dirname(p), pattern)
                # paths_index[]
                if len(paths_index) > 1 :
                    # print(paths_index)
                    folder = paths_index[0]
                    paths_index = [p]
                else :
                    return []
            else :
                return []
        return [ folder ] + paths_index


    def Save(self) :
        if len(self.dicoTimeStamps) > 0 :
            with open(self.FILE_TAG, 'w') as fp:
                json.dump(self.dicoTimeStamps, fp, indent=4)

    def Load(self) :
        if os.path.exists(self.FILE_TAG) :
            with open(self.FILE_TAG, 'rb') as f:
                self.dicoTimeStamps = json.load(f)

    def GetPath(self, l_md5) :
        l_path = self.dicoTimeStamp[l_md5]["fullpath"]

        return l_path

    def CoverTime(self, md5, tag) :
        l_count = 0
        if md5 in self.dicoTimeStamps :
            if "tags" in self.dicoTimeStamps[md5] :
                if tag in self.dicoTimeStamps[md5]["tags"] :
                    for tsp in self.dicoTimeStamps[md5]["tags"][tag] :
                        l_count += tsp[1] - tsp[0]

        return l_count

    def TotalTimeEps(dicoTags) :
        l_count = 0
        for tsp in dicoTags :
            l_count += tsp[1] - tsp[0]

        return l_count

    def TotalTime(self, tag, filter_on = "") :
        l_count = 0
        l_files = 0
        for md5 in self.dicoTimeStamps :
            if md5 == "__general__" :
                continue
            if filter_on != "" :
                p = self.GetPath(md5)
                if not p.startswith(os.path.join(filter_on, "")) :
                    # print("j" + p + " "  + os.path.join(filter_on, ""))
                    continue

            if "tags" in self.dicoTimeStamps[md5] :
                if tag in self.dicoTimeStamps[md5]["tags"] :
                    for tsp in self.dicoTimeStamps[md5]["tags"][tag] :
                        if tsp[1] <= 0 :
                            if "duration" in self.dicoTimeStamps[md5] :
                                tsp[1] = self.dicoTimeStamps[md5]["duration"]
                        # print(tsp)
                        l_count += tsp[1] - tsp[0]
                    l_files += 1
        # print(l_count)
        return [ l_count, l_files ]
        # return l_count

    def Get(self, cat_keep, l_md5, l_eps) :
        if l_eps not in self.dicoCat :
            self.dicoCat[l_eps] = []
        if cat_keep in self.dicoTimeStamps[l_md5]["tags"] :
            self.dicoCat[l_eps] = self.dicoTimeStamps[l_md5]["tags"][cat_keep]
        return self.dicoCat[l_eps]

    def DelTag(self, l_s_md5, eps, catSearch, new_time) :

        if l_s_md5 not in self.dicoTimeStamps :
            print("Episode {} not found !".format(eps))
            return []

        if "tags" not in self.dicoTimeStamps[l_s_md5] :
            print("Tags not found !")
            return []

        if catSearch not in self.dicoTimeStamps[l_s_md5]["tags"] :
            print("Tags for {} not found !".format(catSearch))
            return []

        # Find timestamps to modify
        tag_time = self.FindChevauche(l_s_md5, catSearch, [new_time[0], new_time[1]], True)
        if tag_time == new_time :
            print("Entry {} not found !".format(new_time))
            return []

        new_time_1 = [ tag_time[0], new_time[0] ]
        new_time_2 = [ new_time[1], tag_time[1] ]

        if new_time_1[0] != new_time_1[1] :
            # print("Will add : " + str(new_time_1))
            self.dicoTimeStamps[l_s_md5]["tags"][catSearch].append([*new_time_1[0:2]])

        if new_time_2[0] != new_time_2[1] :
            # print("Will add : " + str(new_time_2))
            self.dicoTimeStamps[l_s_md5]["tags"][catSearch].append([*new_time_2[0:2]])
        # print("Will del : " + str(tag_time))

        # self.Save()
            # print("[{}] : Saved {}@{} for {}".format(catSearch, " " + str(new_time[0]), str(round(new_time[2], 2)), os.path.basename(eps)))


        return new_time

    def NewTag(self, l_s_md5, eps, catSearch, new_time, duration = -1) :
        dico_gen = self.dicoTimeStamps["__general__"]["__sagas__"]
        l_infoEps = self.SearchSaison(eps, dico_gen)
        l_type = l_infoEps[0]
        l_saga = l_infoEps[1]
        l_saison = l_infoEps[2]
        l_fullpath = l_infoEps[3]
        dico_eps = {}

        if l_saga != "" :
            self.Load()
            if l_saga not in dico_gen :
                dicoSaga = {"path" : "", "list" : [], "seasons" : [] , "jumps": [] ,"count": 0}
                dico_gen[l_saga] = dicoSaga

            dico_gen[l_saga]["path"] = l_type

            if l_s_md5 not in dico_gen[l_saga]["list"] :
                dico_gen[l_saga]["list"].append(l_s_md5)

            if l_saison != "" :
                if os.path.isdir(os.path.join(self.PATH_MEDIA, *l_infoEps[0:3])) :
                    if l_saison not in dico_gen[l_saga]["seasons"] :
                        dico_gen[l_saga]["seasons"].append(l_saison)

            self.dicoTimeStamps["__general__"]["__sagas__"] = dico_gen

            if l_s_md5 not in self.dicoTimeStamps :
                self.dicoTimeStamps[l_s_md5] = {}
            dico_eps = self.dicoTimeStamps[l_s_md5]
            l_realpath = os.path.realpath(eps)
            dico_eps["name"] = os.path.basename(eps)
            dico_eps["fullpath"] = l_fullpath
            if duration > 0 :
                dico_eps["duration"] = duration
            if "tags" not in dico_eps :
                dico_eps["tags"] = {}

            if catSearch not in dico_eps["tags"] :
                dico_eps["tags"][catSearch] = []

            new_time = self.FindChevauche(l_s_md5, catSearch, [round(new_time[0], 3), round(new_time[1], 3)])
            dico_eps["tags"][catSearch].append([*new_time[0:2]])

            self.dicoTimeStamps[l_s_md5] = dico_eps
            self.Save()
            # print("[{}] : Saved {}@{} for {}".format(catSearch, " " + str(new_time[0]), str(round(new_time[2], 2)), os.path.basename(eps)))
        else :
            print("Saga {} not found !")
            return []

        return new_time

    def GetLinkedCat(self, cats, reverse = False) :
        # Add the linked categories to cats
        new_cats = cats
        if not reverse :
            if "__general__" in self.dicoTimeStamps and  "__links__" in self.dicoTimeStamps["__general__"] :
                for c in cats :
                    main_cat = c.split("/")[0]
                    sub_cat = c.split("/")[1]
                    # If main category is in links
                    # print(self.dicoTimeStamps["__general__"]["__links__"])
                    if main_cat in self.dicoTimeStamps["__general__"]["__links__"] :
                        # We check if a category is in description
                        if main_cat in self.dicoTimeStamps["__general__"]["__desc__"] :
                            if sub_cat in self.dicoTimeStamps["__general__"]["__desc__"][main_cat] :
                                # print(self.dicoTimeStamps["__general__"]["__desc__"][main_cat][sub_cat])
                                if self.dicoTimeStamps["__general__"]["__desc__"][main_cat][sub_cat] not in new_cats :
                                    new_cats = new_cats + self.dicoTimeStamps["__general__"]["__desc__"][main_cat][sub_cat]
        else :
            if "__general__" in self.dicoTimeStamps and  "__desc__" in self.dicoTimeStamps["__general__"] :
                for c in cats :
                    # main_cat = c.split("/")[0]
                    # sub_cat = c.split("/")[1]
                    # We gonna search if category is in desc entry
                    for main_cat in self.dicoTimeStamps["__general__"]["__desc__"] :
                        # Then for each sub category
                        for sub_cat in self.dicoTimeStamps["__general__"]["__desc__"][main_cat] :
                            # print(main_cat + "/" + sub_cat + " " + c)
                            if c in self.dicoTimeStamps["__general__"]["__desc__"][main_cat][sub_cat] :
                                new_cats = new_cats + [ main_cat + "/" + sub_cat ]
                                # break
                        # # We check if a category is in description
                        # if main_cat in self.dicoTimeStamps["__general__"]["__desc__"] :
                        #     if sub_cat in self.dicoTimeStamps["__general__"]["__desc__"][main_cat] :
                        #         # print(self.dicoTimeStamps["__general__"]["__desc__"][main_cat][sub_cat])
                        #         if self.dicoTimeStamps["__general__"]["__desc__"][main_cat][sub_cat] not in new_cats :
                        #             new_cats = new_cats + self.dicoTimeStamps["__general__"]["__desc__"][main_cat][sub_cat]




        return new_cats


    def GetMainCategories(self, cat = "") :
        l_mainCat = []
        if cat != "" :
            print("TBI")
            return []

        if "__general__" in self.dicoTimeStamps and  "__categories__" in self.dicoTimeStamps["__general__"] :
            for main_cat in self.dicoTimeStamps["__general__"]["__categories__"] :
                if len(self.dicoTimeStamps["__general__"]["__categories__"][main_cat]) > 0 :
                    l_mainCat.append(main_cat)

        return l_mainCat

    def GetSubCategories(self, main_cat, cat = "") :
        l_subCat = []
        if cat != "" :
            print("TBI")
            return []

        if "__general__" in self.dicoTimeStamps and  "__categories__" in self.dicoTimeStamps["__general__"] :
            if main_cat in self.dicoTimeStamps["__general__"]["__categories__"] :
                for sub_cat in self.dicoTimeStamps["__general__"]["__categories__"][main_cat] :
                    l_subCat.append(sub_cat)
                # l_subCat.append(main_cat)

        return l_subCat

    def SearchForCat(self, tag) :
        # TAG =
        TAG = ""
        if "__general__" in self.dicoTimeStamps and  "__categories__" in self.dicoTimeStamps["__general__"]:
            a_cat = tag.split("/")
            if len(a_cat) > 1 :
                # We can manage dictionnary instead of lists
                if len(a_cat) > 2 :
                    if a_cat[0] in self.dicoTimeStamps["__general__"]["__categories__"] :
                        if a_cat[1] in self.dicoTimeStamps["__general__"]["__categories__"][a_cat[0]] :
                            if a_cat[2] in self.dicoTimeStamps["__general__"]["__categories__"][a_cat[0]][a_cat[1]] :
                                # print(a_cat[2])
                                return tag
                            else :
                                print("Invalid category : " + a_cat[1])
                            # return tag
                        else :
                            print("Invalid category : " + a_cat[1])
                    else :
                        print("Invalid category" + a_cat[0])

                else :
                    if a_cat[0] in self.dicoTimeStamps["__general__"]["__categories__"] :
                        if a_cat[1] in self.dicoTimeStamps["__general__"]["__categories__"][a_cat[0]] :
                            return tag
                        else :
                            print("Invalid category : " + a_cat[1])
                    else :
                        print("Invalid category" + a_cat[0])
            # print("wtd")

            for cat in self.dicoTimeStamps["__general__"]["__categories__"] :
                # print(str(self.dicoTimeStamps["__general__"]["__categories__"][cat]) + " "+ TAG)
                # print(tag + " " + cat)
                cat2search = TagsMng.endsin_array(tag, self.dicoTimeStamps["__general__"]["__categories__"][cat])
                if cat2search != "" :
                    cat2search = os.path.join(cat, cat2search)
                    TAG = cat2search
                    break
        return TAG

    def SearchForPredefinedJumps(self) :
        dicoGen = {}
        if "__general__" in self.dicoTimeStamps and "__sagas__" in self.dicoTimeStamps["__general__"] :
            for l_saga in self.dicoTimeStamps["__general__"]["__sagas__"] :
                if "jumps" in self.dicoTimeStamps["__general__"]["__sagas__"][l_saga] :
                    dicoGen[l_saga] = self.dicoTimeStamps["__general__"]["__sagas__"][l_saga]["jumps"]

        return dicoGen

    def RevertSamples(self, v_tagged, duration = -1) :
        t_beg = 0
        new_vtagged = []
        # print("iii {}".format(v_tagged))
        return v_tagged
        # print(v_tagged)
        for t in v_tagged :
            if t[0] != 0 and t_beg != 0 :
                new_vtagged.append([t_beg, t[0]])
            t_beg = t[1]

            # else :
            #     new_vtagged.append([t_beg, t[0]])
            #     t_beg = t[1]

        if duration > 0 :
            new_vtagged.append([t_beg, duration])
        else :
            new_vtagged.append([t_beg, 0])

        print(new_vtagged)
        return new_vtagged


    def GetTimeStamps(self, vid, tags, dicoTS = []) :
        """ Return track timestamp """
        dicoTmp = []
        v_tagged = []
        duration = -1

        if "duration" in self.dicoTimeStamps[vid] :
            duration = self.dicoTimeStamps[vid]["duration"]

        if not len(dicoTS) > 0 :
            # if only one tag defined, we directly return timestamps array
            if len(tags) == 1 :
                if CmdSocket.MAIN_CAT in tags :
                    # print(self.dicoTimeStamps[vid])
                    return self.RevertSamples(self.dicoTimeStamps[vid]["tags"][tags[0]], duration)
                else :
                    return self.dicoTimeStamps[vid]["tags"][tags[0]]
            dicoTmp = self.dicoTimeStamps[vid]["tags"][tags[0]]
        else :
            if len(tags) == 1 :
                # If usage/throw is used, then we will reorganize sample to get only playable parts
                if CmdSocket.MAIN_CAT in tags :
                    dicoTS = self.RevertSamples(dicoTS, duration)
                    # t_beg = 0
                    # new_vtagged = []
                    # for t in dicoTS :
                    #     new_vtagged.append([t_beg, t[0]])
                    #     t_beg = t[1]
                    # dicoTS = new_vtagged
                # print(v_tagged)
                return dicoTS
            dicoTmp = dicoTS

        for t in tags[1:] :
            dicoTS = []
            for tag_cur in dicoTmp :
                new_tsp = []
                if t in self.dicoTimeStamps[vid]["tags"] :
                    for tag_main in self.dicoTimeStamps[vid]["tags"][t] :
                        # If current tag (begin) is between main scanned sample :
                        if tag_cur[0] >= tag_main[0] and tag_cur[0] <= tag_main[1] :
                            # We're gonna add current tag begining
                            new_tsp.append(tag_cur[0])
                            # Search for ending
                            # If current tag (end) is before main scanned sample :
                            if tag_cur[1] <= tag_main[1] :
                                # main sample finish after current tag
                                # End of sample is current
                                new_tsp.append(tag_cur[1])
                            else :
                                # main sample finish before current tag
                                # End of sample is main tag
                                new_tsp.append(tag_main[1])
                        # If current tag (end) is between main scanned sample :
                        elif tag_cur[1] >= tag_main[0] and tag_cur[1] <= tag_main[1] :
                            # End of sample is tag main
                            new_tsp = [ tag_main[0], tag_cur[1]]
                        # If main scanned sample is inside current
                        elif tag_main[0] >= tag_cur[0] and tag_main[1] <= tag_cur[1] :
                            new_tsp = [ tag_main[0], tag_main[1]]

                        if len(new_tsp) > 0 :
                            if new_tsp not in dicoTS :
                                dicoTS.append(new_tsp)

            if len(dicoTS) > 0 :
                if len(tags) > 1 :
                    v_tagged = self.GetTimeStamps(vid, tags[1:], dicoTS)
                else :
                    v_tagged = dicoTS
                break

        # print(tags)
        # If usage/throw is used, then we will reorganize sample to get only playable parts
        if CmdSocket.MAIN_CAT in tags :
            # t_beg = 0
            # new_vtagged = []
            # for t in v_tagged :
            #     new_vtagged.append([t_beg, t[0]])
            #     t_beg = t[1]
            # v_tagged = new_vtagged
            v_tagged = self.RevertSamples(v_tagged, duration)

        return v_tagged

    def SearchForMultipleTags(self, tags, dir_filter = "", quality = None) :
        try :
            """ Search for multiple spamples in tags (tags=[...]) """
            self.TIME_TAGGED = 0
            v_tagged = {}

            new_tsp = []
            dicoTmp = {}
            del_keys = []

            # linked_tags = self.GetLinkedCat(tags, True)[len(tags):]
            # print(str(linked_tags[len(tags):]) + " oooo")
            # print(str(linked_tags[1:]) + " oooo")
            # print(len(tags) - 1)
            # print(len(linked_tags))

            # We're gonna get all tagged videos
            for vid in self.dicoTimeStamps :
                if vid == "__general__" :
                    continue
                if dir_filter != "" and not self.dicoTimeStamps[vid]["fullpath"].startswith(dir_filter) :
                    continue
                if not "tags" in self.dicoTimeStamps[vid] :
                    continue
                # Filter on quality
                if quality is not None :
                    if "infos" in self.dicoTimeStamps[vid] :
                        if "w" in self.dicoTimeStamps[vid]["infos"] :
                            if self.dicoTimeStamps[vid]["infos"]["w"] < int(quality) :
                                # print(self.dicoTimeStamps[vid]["name"] + " removed from list " )
                                continue
                IN_TAG = True
                # dicoTmp[vid] = []
                for t in tags :
                    # if t == "actress/Jada Fire" :
                    #     if vid == "36840784c96bf6e841d4b4125292b5a1" :
                    #         print("FOUND )")
                    if t not in self.dicoTimeStamps[vid]["tags"] :
                        IN_TAG = False
                        break
                # IN_TAG = True
                # for t in linked_tags :
                #     if t == "actress/Jada Fire" :
                #         if vid == "36840784c96bf6e841d4b4125292b5a1" :
                #             print("FOUND )")
                #     if t not in self.dicoTimeStamps[vid]["tags"] :
                #         IN_TAG = False
                #         break
                #
                if not IN_TAG :
                    continue

                new_vid_tagged = self.GetTimeStamps(vid, tags) #+ self.GetTimeStamps(vid, linked_tags)
                # print(new_vid_tagged)
                if not len(new_vid_tagged) > 0 :
                    continue
                path_vid = self.GetPath(vid)
                if path_vid  not in v_tagged :
                    v_tagged[path_vid] = []
                v_tagged[path_vid] = sorted(new_vid_tagged)

                if len(v_tagged[path_vid]) > 0 :
                    for t in v_tagged[path_vid] :
                        self.TIME_TAGGED += t[1] - t[0]
            # # print("ONNN ")
            # linked_tags = self.GetLinkedCat(tags, True)
            # print(linked_tags)



            # print(v_tagged)
            return v_tagged
        except Exception as e :
            raise Exception("SearchForMultipleTags : " + str(e))

    def GetPath(self, vid) :
        """ Return video full path """
        path_vid = os.path.join(self.PATH_MEDIA, self.dicoTimeStamps[vid]["fullpath"], self.dicoTimeStamps[vid]["name"])

        if not os.path.exists(path_vid) :
            path_vid = os.path.join(os.path.dirname(self.PATH_MEDIA), self.dicoTimeStamps[vid]["fullpath"], self.dicoTimeStamps[vid]["name"])
        path_vid = os.path.realpath(path_vid.replace("`", "\`").replace("!", "\!"))

        return path_vid

    def SearchForTags(self, tag, random_sample = False, dir_filter = "", split_time = -1) :
        self.TIME_TAGGED = 0
        v_tagged = {}
        for vid in self.dicoTimeStamps :
            if vid == "__general__":
                continue
            if dir_filter != "" and not self.dicoTimeStamps[vid]["fullpath"].startswith(dir_filter) :
                continue
            cat = TagsMng.endsin_array(tag, self.dicoTimeStamps[vid]["tags"])
            if cat != "" :
                if cat in self.dicoTimeStamps[vid]["tags"] and len(self.dicoTimeStamps[vid]["tags"][cat]) > 0 :
                    for t in self.dicoTimeStamps[vid]["tags"][cat] :
                        path_vid = self.GetPath(vid)
                        if path_vid not in v_tagged :
                            v_tagged[path_vid] = []
                        v_tagged[path_vid].append([t[0], t[1]])
                        self.TIME_TAGGED += t[1] - t[0]
                    if split_time > 0 :
                        v_tagged[path_vid] = self.SplitedTimeStamp(v_tagged[path_vid], split_time)
                    if random_sample :
                        shuffle(v_tagged[path_vid])
                    else :
                        v_tagged[path_vid] = sorted(v_tagged[path_vid])

        return v_tagged

    def SplitedTimeStamp(self, a_tsp, sample_time) :
        a_res = []
        l_range = sample_time
        for t in a_tsp :
            idx = 0
            l_range = int(t[1] - sample_time)
            if int(t[1] - sample_time) - int(t[0]) == 0 :
                l_range += 1
            for i in range(int(t[0]), l_range) :
                a_res.append([t[0] + idx * sample_time, t[0] + idx * sample_time + sample_time])
                idx += 1

        return sorted(a_res)

    def endsin_array(val, arr) :
        for v in arr :
            if v.endswith(val) :
                return v
        return ""

    def SearchSaison(self, l_s_path, dicoSaga = {}) :
        """ Search saga, season, and return fullpath of episode """
        retour = ""
        FOUND = False
        dicoSaga = self.dicoTimeStamps["__general__"]["__sagas__"]
        # print(self.dicoTimeStamps["__general__"]["__sagas__"])
        for l_saga in dicoSaga :
            l_type = dicoSaga[l_saga]["path"]
            for l_saison in dicoSaga[l_saga]["seasons"] :
                path_saison = os.path.join(dicoSaga[l_saga]["path"], l_saga, l_saison)
                if path_saison in l_s_path :
                    del_path = os.path.dirname(l_s_path).split(path_saison)[0]
                    retour = os.path.join(os.path.dirname(l_s_path).replace(del_path, ""))
                    FOUND = True
                    break
            if FOUND :
                break

        if retour == "" :
            l_path = l_s_path.replace(os.path.join(os.path.realpath(self.PATH_MEDIA), ""), "")
            retour = os.path.dirname(l_path)

            a_path = l_path.split("/")
            l_saison = ""
            if len(a_path) == 0 :
                print("TODO :: {}".format(k_file))
                return [l_saga, l_saison, retour]

            l_type = a_path[0]
            if len(a_path) > 1 :
                l_saga = a_path[1]
            else :
                l_saga = l_type

            if len(a_path) > 2 :
                l_saison = a_path[2]

        if l_type == l_saga and l_saga == os.path.basename(l_s_path) :
            # Track is on root folder
            l_type = "."
            l_saga = "."
            retour = "."

        if l_type == retour and l_saga == os.path.basename(l_s_path) :
            # Track is on type folder
            l_type = "."
            l_saga = retour

        return [l_type, l_saga, l_saison, retour]

    def FindChevauche(self, eps, cat, tsp, in_sample = False) :
        """ find if tsp is already in track samples (eps = md5) """
        new_tsp = tsp
        dicoTmp = []
        del_keys = []

        a_tsp = self.dicoTimeStamps[eps]["tags"][cat]
        for l_tsp in a_tsp :
            beg = l_tsp[0]
            end = l_tsp[1]
            if in_sample :
                if (tsp[0] < beg and tsp[1] < end) or (tsp[0] > beg and tsp[1] > end) :
                    continue
            if tsp[0] >= beg and tsp[0] <= end :
                new_tsp[0] = round(beg, 3)
                if l_tsp not in dicoTmp :
                    dicoTmp.append(l_tsp)

            if tsp[1] >= beg and tsp[1] <= end :
                new_tsp[1] = round(end, 3)
                if l_tsp not in dicoTmp :
                    dicoTmp.append(l_tsp)
            if tsp[0] <= beg and tsp[1] >= end :
                if l_tsp not in dicoTmp :
                    dicoTmp.append(l_tsp)

        if len(dicoTmp) > 0 :
            for k in dicoTmp :
                # print(k)
                if k in self.dicoTimeStamps[eps]["tags"][cat] :
                    # print("Deleted key : " + str(k) + " " + str(a_tsp.index(k)))
                    del_keys.append(k)
                    self.dicoTimeStamps[eps]["tags"][cat].pop(a_tsp.index(k))

            return [ *self.FindChevauche(eps, cat, new_tsp), del_keys ]

        return [ *new_tsp, new_tsp[1] - new_tsp[0] ]


    def FindIndexNextJump(self, eps, done_vid, cat_keep = "") :
        idx = -1
        if eps in self.dicoTimeStamps and "tags" in self.dicoTimeStamps[eps] :
            # We're searching for tagged timestamps
            if cat_keep in self.dicoTimeStamps[eps]["tags"] :
                for l_tsp in sorted(self.dicoTimeStamps[eps]["tags"][cat_keep]) :
                    if done_vid >= l_tsp[0] :
                        idx += 1
                        if done_vid <= l_tsp[1] :
                            return idx

        return idx

    def FindIfJump(self, eps, done_vid, cat_keep = "") :
        if eps in self.dicoTimeStamps and "tags" in self.dicoTimeStamps[eps] :
            if CmdSocket.MAIN_CAT in self.dicoTimeStamps[eps]["tags"] :
                for l_tsp in self.dicoTimeStamps[eps]["tags"][CmdSocket.MAIN_CAT] :
                    if done_vid >= l_tsp[0] and done_vid <= l_tsp[1] :
                        done_vid = l_tsp[1]
                        # print("USAGE THROW FOUND :: " + str(done_vid))
                        break

            # We're searching for tagged timestamps
            if cat_keep != "" :
                if cat_keep in self.dicoTimeStamps[eps]["tags"] :
                    for l_tsp in sorted(self.dicoTimeStamps[eps]["tags"][cat_keep]) :
                        if done_vid >= l_tsp[0] and done_vid <= l_tsp[1] :
                            done_vid = l_tsp[1] #- (2 / 3)
                            # print("T FOUND :: " + str(done_vid))
                            break
        # print(self.dicoCat)
        return done_vid

    def __init__ (self, file_tag, path_media, tags = []) :
        self.cats = {}
        self.dicoTimeStamps = {}
        self.TIME_TAGGED = 0
        self.PATH_MEDIA = path_media
        self.FILE_TAG = file_tag
        self.Load()


# END File listing
