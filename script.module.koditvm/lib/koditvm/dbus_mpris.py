# Copyright (c) Elie Coutaud 2024
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!/usr/bin/python
# -*- coding: latin-1 -*-

import os, sys, time, dbus
from threading import Thread
from urllib.parse import unquote

SOFTS = {
    "vlc" : "org.mpris.MediaPlayer2.vlc",
    "clementine" : "org.mpris.MediaPlayer2.clementine",
    "firefox" : "org.mpris.MediaPlayer2.firefox",
    "kodi" : "org.mpris.MediaPlayer2.kodi",
    # "firefox" : "org.mozilla.firefox"
}

class VFile() :
    def __init__ (self, name, track_id, duration, cur_time = -1) :
        self.name = name
        self.track_id = track_id
        self.duration = duration
        self.cur_time = cur_time

class Player() :
    plist = {}
    plist_old = {}
    current = ""
    timeline_file = {}
    track_list = {}
    player_list = []
    RUNNING = True
    PAUSE_STATUS = [ "Paused", "Playing" ]
    LOOP_STATUS = [ "None", "Loop" ]
    MAX_OFFSET = 0.417

    def init() :
        Player.plist = {}
        bus = dbus.SessionBus()
        for service in bus.list_names():
            if service.startswith('org.mpris.MediaPlayer2.'):
                # print(service)
                # idx = 0
                for key in SOFTS.keys() :
                    if service.startswith(SOFTS[key]) :
                        player = Player(dbus.SessionBus().get_object(service, '/org/mpris/MediaPlayer2'), str(service))
                        Player.plist[str(service)] = player
                        # print(player.Get('Metadata'))
                        if player.GetStatus() == "Playing" :
                            Player.current = str(service)

    def __init__ (self, service = "", name = "") :
        self.track = ""
        self.track_id = ""
        self.track_duration = -1
        self.rate = 1

        self.m_paused = []
        self.m_paused_1 = []
        self.m_paused_2 = []
        self.m_paused_3 = []
        self.m_paused_4 = []
        self.paused = False
        self.playing = True

        self.m_fullscreen = []
        self.m_loopTrack = []
        self.m_rate = []
        self.m_shuffle = []
        self.m_before_loop = []
        self.m_beg_loop = []
        self.m_end_loop = []
        self.m_after_loop = []

        self.playerList = []

        self.player = service
        self.name = name

        self.loop = ""
        self.shuffle = False
        self.fullscreen = False

        # Player states
        self.on_pause = False

        # Threads
        self.t_paused = None
        self.t_pause = None
        self.t_shuffle = None
        self.t_rate = None
        self.t_stop = None
        self.t_loop = None
        self.t_fullscreen = None
        self.t_play = None

    def GetControl(self) :
        try :
            return self.Interface()
        except Exception as e:
            print("GetControl "  + self.name + " : OK ?" )
            return ""

    def Interface(self, dbus_interface = "Player") :
        if self.Exists() :
            return dbus.Interface(self.player, dbus_interface='org.mpris.MediaPlayer2.' + dbus_interface)
        return None

    def Prop(self, prop, val = "", mpris_interface = 'org.mpris.MediaPlayer2.Player') :
        """
        Get/Set a property,
        # prop : array[prop name, prop value] ( ex : ["Volume", 0.1] )
        # val : value
        """
        try :
            if self.Exists() :
                if val != "" :
                    return self.player.Set(mpris_interface, prop, val, dbus_interface='org.freedesktop.DBus.Properties')

                return self.player.Get(mpris_interface, prop, dbus_interface='org.freedesktop.DBus.Properties')
            return None
        except Exception as e:
            print("Prop : "  + prop + " | " + self.name + " | ERR : " + str(e))
            return ""

    def Get(self, prop) :
        return self.Prop(prop)

    def Set(self, prop, val) :
        return self.Prop(prop, val)

    def Exists(self) :
        bus = dbus.SessionBus()
        if self.name in bus.list_names() :
            return True
        return False

    def GetTracks(self) :
        """
        Send tracklist
        """
        try :
            return self.Prop("Tracks", mpris_interface = 'org.mpris.MediaPlayer2.TrackList')
        except Exception as e:
            print("GetTracks " + self.name + " : OK ?" )
            # print(self.name + " : OK? Reloading player list " + str(e))
            return ""

    def GetTrackId(self) :
        try :
            status = self.Get('Metadata')
            if status != None and status != "":
                return status.get('mpris:trackid')
            else :
                return ""
        except Exception as e:
            print("GetTrackId" + str(e))
            return ""

    def GetTrackPath(self, to_replace = "", replace = "") :
        try :
            # print("3/ ")
            track_meta = self.Get('Metadata')
            # print("3/ " + str(track_meta.get('xesam:url')))
            if track_meta != None and track_meta != "" and track_meta.get('xesam:url') != None:
                # print("trmeta :: " +str(track_meta))
                track = unquote(track_meta.get('xesam:url'))
            else :
                return ""
            if to_replace != "" :
                track = track.replace(to_replace, replace)
            return os.path.realpath(track)
        except Exception as e:
            print("GetTrackPath" + str(e))
            return ""

    def GetTrackDuration(self) :
        """ return video time total (in seconds) """
        status = self.Get('Metadata')
        try :
            if status != None :
                duration = status.get('mpris:length')
            else :
                duration = -1
            if duration != None and duration > -1 :
                duration = duration / 1000 / 1000
            else :
                duration = -1
            return duration
        except Exception as e:
            # print(e)
            print("GetTrackDuration" + str(e))
            return -1

    def GetStatus(self) :
        """ Get player status """
        try :
            return str(self.Get('PlaybackStatus'))
        except Exception as e:
            # print("GetPlaybackStatus " + self.name + " : OK ?" )
            # print(self.name + " : OK? Reloading player list " + str(e))
            return ""

    # BEGIN "Is" Status method
    def IsPaused(self) :
        return self.on_pause

    def IsPlaying(self) :
        try :
            return self.GetStatus() == "Playing"
        # return False
        except Exception as e:
            print("IsPlaying"  + self.name + " : OK ?" )
            return False

    def IsStopped(self) :
        try :
            return self.GetStatus() == "Stopped"
        # return False
        except Exception as e:
            print("IsStopped"  + self.name + " : OK ?" )
            return False

    # END

    def SetFullScreen(self) :
        fullscreen_active = not bool(self.player.Get('org.mpris.MediaPlayer2', "Fullscreen",dbus_interface='org.freedesktop.DBus.Properties'))
        # print(self.player.Get('org.mpris.MediaPlayer2', "Fullscreen",dbus_interface='org.freedesktop.DBus.Properties'))

        # fullscreen_active = self.player.Get('org.mpris.MediaPlayer2', "FullScreen",dbus_interface='org.freedesktop.DBus.Properties')
        # print(fullscreen_active)
        self.player.Set('org.mpris.MediaPlayer2', "Fullscreen", fullscreen_active, dbus_interface='org.freedesktop.DBus.Properties')

    def FullScreen(self, val = None) :
        if val != None :
            self.player.Set('org.mpris.MediaPlayer2', "Fullscreen", val, dbus_interface='org.freedesktop.DBus.Properties')
        return bool(self.player.Get('org.mpris.MediaPlayer2', "Fullscreen",dbus_interface='org.freedesktop.DBus.Properties'))
        # dbus.Interface(self.player, dbus_interface='org.mpris.MediaPlayer2').Quit()

    def Quit(self) :
        print("Try to quit")
        # print(self.player.Get('org.mpris.MediaPlayer2', "CanQuit",dbus_interface='org.freedesktop.DBus.Properties'))
        dbus.Interface(self.player, dbus_interface='org.mpris.MediaPlayer2').Quit()

    def Raise(self) :
        print("Try to raise")
        print(self.player.Get('org.mpris.MediaPlayer2', "CanRaise",dbus_interface='org.freedesktop.DBus.Properties'))
        dbus.Interface(self.player, dbus_interface='org.mpris.MediaPlayer2').Raise()

    def Next(self) :
        return self.Interface().Next()

    def Previous(self) :
        return self.Interface().Previous()

    def LoopStatus(self, val = "") :
        if val != "" :
            self.Set("LoopStatus", val)
        return self.Get("LoopStatus")

    def SetShuffle(self) :
        return self.Prop("Shuffle", not self.Shuffle())

    def Shuffle(self, val = "") :
        return self.Prop("Shuffle", val)

    def Rate(self, val = "") :
        """ Get/Set rate """
        if val != "" :
            self.rate = val
            # print("SET RATE to :: " + str(val))
            self.Set("Rate", float(val))
            if self.rate != self.Get("Rate") :
                print("RATE F :: " + str(self.Get("Rate")))
        return self.Get("Rate")

    def Volume(self, val = "") :
        """ Get/Set volume """
        if val != "" :
            self.Set("Volume", val)
        return self.Get("Volume")

    def Position(self, in_second = True) :
        """ Get position """
        try :
            pos = -1
            if self.IsPlaying() or self.IsPaused() :
                pos = self.Get("Position")
                while pos == None :
                    pos = self.Get("Position")

                if in_second :
                    pos = pos / 1000 / 1000

        except Exception as e:
            print("Position " + self.name + " : OK ?" )
        return pos

    def Seek(self, position, in_second = True) :
        """ Seek to position """
        seek_to_time = position
        if in_second :
            seek_to_time = seek_to_time * 1000 *1000
        # Deplacement vers le temps
        if self.Exists() :
            self.Interface().Seek(seek_to_time)

    def SeekTo(self, new_position, in_second = True) :
        """ Seek to position """
        seek_to_time = new_position - self.Position(in_second)
        # Deplacement vers le temps
        self.Seek(seek_to_time, in_second)

    def GoTo(self, new_position, eps, speed = 1.5, jump = 0.1) :
        """ Go to new_position at rate defined """
        # print("NEW_POS :: {}".format(new_position))
        if new_position < 0 :
            new_position = 0.1
        pos = self.Position()
        ecart = new_position - self.Position()
        # print("NEW_POS :: {} ECAT : {}".format(new_position, ecart))

        if ecart > 0 :
            # self.Play()
            # self.Rate(speed)
            while new_position > pos - jump :
                if eps != self.GetTrackPath() :
                    break
                pos = self.Position()
                self.Seek(jump)
                ecart = new_position - pos
                if ecart < .5 :
                    jump = 0.1
                else :
                    jump = ecart * 0.25
        else :
            if ecart < -300 :
                jump = 1
            # 20240904 : Old way does not work for sample begin at 0s
            # new_position < pos + jump
            while new_position < pos :
                # print("{} < {} + {}".format(new_position, pos, jump))
                if eps != self.GetTrackPath() :
                    break
                pos = self.Position()
                self.Seek(-jump)
                ecart = new_position - pos
                if ecart > -0.5 :
                    jump = 0.1
                else :
                    jump = -ecart * 0.25

    def SetPosition(self, track_id, position = 0) :
        # print(track_id)
        return self.Interface().SetPosition(track_id, position)

    def PlayPause(self) :
        if self.IsPaused() :
            self.Play()
        else :
            self.Pause()

    def Pause(self) :
        self.Interface().Pause()

    def Play(self) :
        self.Interface().Play()

    def Stop(self) :
        self.Interface().Stop()

    def Run(self, uri) :
        if uri == None :
            return False

        if self.GetTrackPath() == uri :
            return False
        player_iface = self.Interface()
        # Ajout du titre dans la playlist
        return player_iface.OpenUri(uri)

    def SetTrackInfo(self, l_eps) :
        l_track_id = self.GetTrackId()
        l_track_duration = -1
        while os.path.isdir(self.GetTrackPath("file://", "")) :
            print("DIR1 : " + self.GetTrackId())
            time.sleep(0.5)
            l_track_id = self.GetTrackId()
        while l_track_duration == -1 :
            l_track_duration = self.GetTrackDuration()

        if l_eps not in Player.track_list :
            Player.track_list[l_eps] = VFile(l_eps, l_track_id, l_track_duration)
        else :
            if Player.track_list[l_eps].duration == -1 :
                Player.track_list[l_eps].duration = l_track_duration = self.GetTrackDuration()

    def SetTrackList(self, l_tracks = []) :
        if len(l_tracks) == 0 :
            l_tracks = self.GetTracks()
        # Rempli notre tracklist d'objets video
        for f in self.Interface("TrackList").GetTracksMetadata(l_tracks) :
            l_eps = unquote(f.get('xesam:url')).replace("file://", "")
            # print(f.get('xesam:url'))
            l_track_id = f.get('mpris:trackid')
            l_track_duration = f.get('mpris:length')
            if l_track_duration is not None :
                l_track_duration = l_track_duration / (1000 * 1000)

                Player.track_list[l_eps] = VFile(l_eps, l_track_id, l_track_duration)
            # print("{} : {}".format(l_track_id, l_eps))

    def AddTracks(self, f_list, f_type = "file://", play = True, thread = True) :
        if thread :
            # print(f_list[0])
            self.AddTrack(f_type + f_list[0])
            t = Thread(target=self.AddTracks, args=([f_list[1:], f_type, True, False]))
            t.start()
        else :
            # We add files in list in a thread
            idx = 0
            # self.AddTrack(f_type + f_list[0])

            for f in f_list :
                if not self.Exists() :
                    return False
                # print("ADD {} :: {}".format(idx, f))
                idx += 1
                self.AddTrack(f_type + f)
                time.sleep(0.05)

        if not thread :
            self.SetTrackList()

        if play :
            time.sleep(0.3)
            # while len(self.GetTracks()) == 0 :
            #     time.sleep(0.3)
            #     print("sleep")
            self.Play()

        return True

    def AddTrack(self, f, track_id = "/org/mpris/MediaPlayer2/TrackList/NoTrack") :
        # Player.track_list[f] = VFile(f)
        self.Interface("TrackList").AddTrack(f, track_id, False)

    def RunPlayer(cmd = "", pos = 0):
        """
        Run "cmd" via os.system in thread and
        Search for new "cmd" instance

        Return "cmd" instance process
        """
        # Init service list
        Player.init()
        # We keep old playlist
        plist_old = Player.plist
        # print("OLD :: " + str(plist_old))
        # time.sleep(1)
        # We run "cmd" via os.system in a thread
        # print(cmd)
        # print(Player.plist)
        if cmd != "" :
            t = Thread(target=os.system, args=([cmd]))
            t.start()
        # if cmd != "" :

        # Wait for new instance
        while len(plist_old) == len(Player.plist) :
            time.sleep(0.1)
            # print(Player.plist)
            # print("CUR :: " + str(Player.plist))
            Player.init()
            if not Player.RUNNING :
                return [ ]

        if not cmd != "" :
            print("PLIST : " + str(Player.plist))
            # pl_name = len(Player.player_list)
            # t = str(len(Player.player_list))
            # If player has quit, we return index of  array
            if len(Player.plist) < len(plist_old) :
                for l_player in list(plist_old.keys()) :
                    if l_player not in list(Player.plist.keys()) :
                        print("deleting p :: " + str(l_player))
                        # Player.current = l_player
                        return [l_player, True]
            else :
                # Searching for new instance
                for l_player in list(Player.plist.keys()) :
                    if l_player not in list(plist_old.keys()) :
                        # print("sth" + str(plist_old.keys()))
                        # print("pl" + str(l_player))
                        print("new p :: " + str(l_player) + " " + Player.plist[l_player].GetTrackPath())
                        return [l_player]

        # Searching for new instance
        for l_player in list(Player.plist.keys()) :
            if l_player not in list(plist_old.keys()) :
                # print("pl" + str(l_player))
                Player.current = l_player
                break
        # print(len(Player.plist))
        # Seek to time
        if pos != 0 :
            l_player.SeekTo(pos)
        # Then return new instance
        return [ t, l_player ]

    def ExecMeth(self, condition, met_args) :
        if condition :
            return met_args()

    def ExecEvent(self, condition, met_args = []) :
        if condition :
            if len(met_args) > 0 :
                # print("ARG :: " + str(condition) + " " + str(met_args))
                arg = []
                if len(met_args) > 1 :
                    arg = met_args[1:]
                met_args[0](*arg)
                # t = Thread(target=met_args[0], args=([*arg]))
                # t.start()

    def FullScreened(self, beg = 1, max_click = 0, execEvent = True) :
        """
        Manage Fullscreen changed event
        """
        f = self.FullScreen()
        if not max_click > 0 :
            max_click = len(self.m_fullscreen)
        click = beg
        start = time.time()
        # print(self.m_paused)
        if execEvent and not len(self.m_fullscreen) > 0 :
            # print("nothin")
            return False
        else :
            if not execEvent or len(self.m_fullscreen) > 1 :
                while f == self.FullScreen() :
                    time.sleep(.05)
                    if f != self.FullScreen() :
                        start = time.time()
                        click += 1
                        # self.Interface().Pause()
                    end = time.time() - start
                    if end > 0.25 or click == max_click:
                        break
                    f = self.FullScreen()

        if self.Exists() :
            if not execEvent :
                return click + 1
            if click - beg < len(self.m_fullscreen) :
                # self.Interface().Play()
                return self.ExecMeth(True, self.m_fullscreen[click - beg])

    def Shuffled(self, beg = 1, max_click = 0, execEvent = True) :
        """ Manage shuffled changed event """
        # try :
        shuffle = self.Shuffle()
        if not max_click > 0 :
            max_click = len(self.m_shuffle)
        click = beg
        start = time.time()
        # print(self.m_paused)
        if execEvent and not len(self.m_shuffle) > 0 :
            # print("nothin")
            return False
        else :
            if not execEvent or len(self.m_shuffle) > 1 :
                while shuffle == self.Shuffle() :
                    time.sleep(.05)
                    if shuffle != self.Shuffle() :
                        start = time.time()
                        click += 1
                        # self.Interface().Pause()
                    end = time.time() - start
                    if end > 0.25 or click == max_click:
                        break
                    shuffle = self.Shuffle()

        if self.Exists() :
            if not execEvent :
                return click + 1
            if click - beg < len(self.m_shuffle) :
                # self.Interface().Play()
                return self.ExecMeth(True, self.m_shuffle[click - beg])
        else :
    # except :
            return 0

    def Looped(self, beg = 1, max_click = 0, execEvent = True) :
        """ Manage loop changed event """
        loop = self.LoopStatus()
        if not max_click > 0 :
            max_click = len(self.m_loopTrack)
        click = beg
        start = time.time()
        if execEvent and not len(self.m_loopTrack) > 0 :
            return False
        else :
            if not execEvent or len(self.m_loopTrack) > 1 :
                while loop == self.LoopStatus() :
                    time.sleep(.05)
                    if self.LoopStatus() != loop :
                        start = time.time()
                        click += 1
                        # self.Interface().Pause()
                    end = time.time() - start
                    if end > 0.25 or click == max_click:
                        break
                    loop = self.LoopStatus()

        if self.Exists() :
            if not execEvent :
                return click + 1
            if click - beg < len(self.m_loopTrack) :
                return self.ExecMeth(True, self.m_loopTrack[click - beg])

    def Rated(self, beg = 1, max_click = 0, execEvent = True) :
        """ Manage rate changed event """
        rate = float(self.Rate())
        click = beg
        if not rate == 1.0 :
            if rate > 1 :
                if not max_click > 0 :
                    max_click = len(self.m_rate)
                start = time.time()
                if execEvent and not len(self.m_rate) > 0 :
                    return False
                else :
                    # if not execEvent or len(self.m_rate) > 1 :
                    while float(self.Rate()) >= 1 :
                        time.sleep(.05)
                        if float(self.Rate()) > rate :
                            start = time.time()
                            click += 1
                        end = time.time() - start
                        if end > 0.25 or click == max_click:
                            break

                        # Reinit at normal speed
                        self.Rate(1)
                        rate = 1

                if execEvent :
                    if click - beg < len(self.m_rate) :
                        # print("{} > {} :: {}".format(float(self.Rate()), rate, execEvent))
                        return self.ExecMeth(True, self.m_rate[click - beg])
                click += 1
            else :
                if not max_click > 0 :
                    max_click = len(self.m_rate)

                start = time.time()
                if execEvent and not len(self.m_rate) > 0 :
                    return False
                else :
                    # if not execEvent or len(self.m_rate) > 1 :
                    while float(self.Rate()) <= 1 :
                        time.sleep(.05)
                        if float(self.Rate()) < rate :
                            start = time.time()
                            click += 1
                            # self.Interface().Pause()
                        end = time.time() - start
                        if end > 0.25 or click == max_click:
                            break
                        self.Rate(1)
                        rate = 1

                if execEvent :
                    if click - beg < len(self.m_rate) :
                        return self.ExecMeth(True, self.m_rate[click - beg])
                click = -1 - click
        if self.Exists() :
            return click

    def Paused(self, beg = 1, max_click = 0, execEvent = True, onPause = False, in_thread = False) :
        """ Manage pause event """
        paused_status = Player.PAUSE_STATUS
        pause_method = self.Interface().Pause
        play_method = self.Interface().Play
        if onPause :
            paused_status = list(reversed(paused_status))
            pause_method = self.Interface().Play
            play_method = self.Interface().Pause

        if not max_click > 0 :
            max_click = len(self.m_paused)
        click = beg
        start = time.time()

        if execEvent and not len(self.m_paused) > 0 :
            return False
        else :
            if not execEvent or len(self.m_paused) > 1 :
                while self.GetStatus() == paused_status[0] :
                    time.sleep(.05)
                    if self.GetStatus() == paused_status[1] :
                        start = time.time()
                        click += 1
                        pause_method()
                        # self.Interface().Pause()
                    end = time.time() - start
                    if end > 0.25 or click == max_click:
                        break
        # print("ENDED 1 : ", end = str(paused_status), flush=True)
            # else :
        if self.Exists() :
            if not execEvent :
                # print("RETURN")
                return click
            if click - beg < len(self.m_paused) :
                play_method()
                if not in_thread :
                    # print("EXEC")
                    return self.ExecMeth(True, self.m_paused[click - beg])
                else :
                    # print("In thread")
                    t_pause = Thread(target=self.ExecMeth, args=(True, self.m_paused[click - beg]))
                    t_pause.start()

    def FramePerfectMode(self) :
        # self.paused = True
        # print("RUN")
        self.t_play = Thread(target=self.CheckPlaying)
        self.t_play.start()

    def SearchPlayer() :
        try :
            print("waiting")
            while True :
                try :
                    pl = Player.RunPlayer()
                    if not Player.RUNNING :
                        break
                    if len(pl) > 1 :
                        k_todel = Player.player_list.index(pl[0])
                        Player.player_list.pop(k_todel)
                        print("Player has quit ! :" + str(k_todel))
                    else :
                        Player.player_list.append(pl[0])
                        print("Player added ! :" + str(pl[0]))
                except Exception as e:
                    print("EXP SP :: " + str(e))
                    # print(Player.player_list)
                except KeyboardInterrupt as e:
                    print("Interruption SearchPlayer 1")
                    return False
        except Exception as e:
            # print("EXP SP :: " + str(e))
            return False

        except KeyboardInterrupt as e:
            print("Interruption SearchPlayer 2")
            # if len(proc_list) > 0 :
            #     if proc_list[0].is_alive() :
            #         proc_list[0].join(1)
            #     proc_list.pop(0)
            return False
        finally :
            print("End monitor")

    def Monitor() :
        try :
            t = Thread(target=Player.SearchPlayer)
            # print("i")
            t.start()
            return t
        except Exception as e:
            print("EXP MON :: " + str(e))
            return False

        except KeyboardInterrupt as e:
            print("Interruption MON")
            # if len(proc_list) > 0 :
            #     if proc_list[0].is_alive() :
            #         proc_list[0].join(1)
            #     proc_list.pop(0)
            return False

    def LoopThread(self, l_eps = "") :
        try :
            t = Thread(target=self.LoopPlayer, args=([l_eps]))
            print("NEW THREAD : @{} ".format(l_eps))
            t.start()
            return t
        except Exception as e:
            print("EXP LOOP :: " + str(e))
            return None

        except KeyboardInterrupt as e:
            print("Interruption")
            # if len(proc_list) > 0 :
            #     if proc_list[0].is_alive() :
            #         proc_list[0].join(1)
            #     proc_list.pop(0)
            return None

    def Playing(self) :
        pass

    def Stopped(self) :
        pass

    def endLoop(self) :
        pass

    def afterLoop(self) :
        pass

    def Log(meth_log = []) :
        if len(meth_log) > 0 :
            arg_log = []
            if len(meth_log) > 1 :
                arg_log = meth_log[1:]
            meth_log[0](*arg_log)
# BEGIN Set player states
    def SetOnPause(self) :
        try :
            while True :
                self.on_pause = self.GetStatus() == "Paused"
        # return False
        except Exception as e:
            # print("IsPaused"  + self.name + " : OK ?" )
            return False

# END

# BEGIN Events : TODO : Factorize

    def CheckPlaying(self) :
        try :
            click = 0
            while self.Exists() :
                if click > 0 :
                    break
                click = self.Paused(0, 0, False)
                # Security if Paused has "failed" ...
                max_click = len(self.m_paused)
                while self.IsPlaying() :
                    print("1")
                    click = 1
                    time.sleep(0.05)
                    while self.IsPaused() :
                    # if self.IsPaused() :
                        print("2")
                        click = 2
                        time.sleep(0.05)
                        if self.IsPlaying() :
                            click = 3
                            print("3")
                        break
                    break


            self.t_play = None
            self.m_paused[click - 1]()
        except :
            pass

    def CheckPause(self) :
        try :
            while self.Exists() :
                # print(self.on_pause)
                if not self.paused :
                    self.ExecEvent(self.IsPaused() and self.t_play is None, [self.Paused])
                elif self.IsPlaying() and self.t_play is None :
                    self.paused = False

            print("End Pause checking")
        except Exception as e:
            # print(self.m_paused)
            print("CheckPause : " + str(e))
            pass

    def CheckShuffle(self) :
        try :
            while self.Exists() :
                self.ExecEvent(self.Shuffle() != self.shuffle, [self.Shuffled])
            print("End Shuffle checking")
        except :
            pass

    def CheckRate(self) :
        try :
            while self.Exists() :
                self.ExecEvent(self.GetTrackPath("file://", "") == self.track, [ self.Rated ])
            print("End Rate checking")
        except :
            pass

    def CheckStop(self) :
        try :
            while self.Exists() :
                self.ExecEvent(self.GetStatus() == "Stopped", [self.Stopped])
            print("End Stop checking")
        except :
            pass

    def CheckLoop(self) :
        try :
            while self.Exists() :
                self.ExecEvent(self.LoopStatus() != self.loop, [self.Looped])
            print("End loop checking")
        except :
            pass

    def CheckFullScreen(self) :
        try :
            while self.Exists() :
                self.ExecEvent(self.FullScreen() != self.fullscreen, [self.FullScreened])
            print("End fullscreen checking")
        except :
            pass
# END Events

# BEGIN Synchronize players
    def SynchPlayers(self, players, master = False) :
        """ Synchronize players """
        try :
            max_offset = Player.MAX_OFFSET
            m_seek = 1000
            l_i_secs = 0.5
            for p in players :
                # max_offset = 0.417
                if not p.Exists() :
                    return -1

                if p.IsPaused() :
                    time.sleep(1)
                    # self.m_synchroDone = False
                # print("2 " + str(p)+ " " + str(p.Position()))
                # if not self.m_synchroDone :
                while l_i_secs > max_offset :
                    if not master :
                        l_i_secs = Player.GetOffset(p, self, max_offset)
                    else :
                        l_i_secs = Player.GetOffset(self, p, max_offset)
                    # self.m_synchroDone = True
                # else :
                # Searching for 1.5 offset
                # l_i_secs = Player.GetOffset(p, self, max_offset)
                # if l_i_secs > max_offset or l_i_secs < -max_offset :
                #     p.Seek(-m_seek, False)
                #     l_i_secs = Player.GetOffset(p, self, max_offset)
            return l_i_secs
        except Exception as e :
            raise e

    def GetOffset(myKodi, myVlc, delay_max = 0.417):
        """ Get offset between both videos and synchronise them """
        # If vlc instance is not running, then nothing to do
        if not myVlc.Exists() :
            return -1

        l_time2local = -1
        # Get local playing kodi video time in second to compare with other one
        while l_time2local < 0 :
            l_time2local = myVlc.Position()
            # print("her")
            if not myVlc.Exists() :
                return -1
        # Get current time (to reduce offset process time)
        tstart = time.time()
        # Get remote playing kodi video time
        l_time2launch = myKodi.Position()
        if l_time2launch > myVlc.GetTrackDuration() or l_time2launch == 0 :
            # print(str(l_time2launch) + " > " + str(myVlc.GetTrackDuration()) + " or " + str(l_time2launch)  + " == 0" )
            return -1
        # Get current time
        tend = time.time()
        # Compute process time.
        offset = tend - tstart
        # Compute time to seek to for local video
        l_time2seek = l_time2local - l_time2launch - offset
        # print("{} __ {} :: {}".format(offset, delay_max, l_time2seek), end="", flush=True)
        if l_time2seek > delay_max:
            if l_time2seek > delay_max * 2 :
                # print( '### SUP : ' + str(l_time2seek) + " | " + str(l_time2local) + " " + str(l_time2launch) + " # " + str(offset))
                myVlc.Seek(-(l_time2seek + (delay_max/2)))
                # if l_time2launch + offset > 10 :
                # myVlc.GoTo(l_time2launch + offset, myVlc.GetTrackPath())
            return l_time2seek
        else :
            if l_time2seek < 0.0  and l_time2seek < -delay_max * 2 : #
                # Seek local video to the computed time
                myVlc.Seek(-(l_time2seek-(delay_max/2)))
                # print( '### INF : ' + str(l_time2seek) + " | " + str(l_time2local) + " " + str(l_time2launch) + " # " + str(offset) )
                # myVlc.GoTo(l_time2launch + offset, myVlc.GetTrackPath())
                # time.sleep(2)

                # l_time2seek = VlcMove.GetOffset(myKodi, myVlc, 0.417)
            else:
                return 0
        return l_time2seek

# END Synch

    def LoopPlayer(self, l_eps = "", meth_log = []) :
        # while True :
        try :
            res = True
            pos = -1
            l_track_id = ""
            l_track_duration = -1
            # print("1")
            if not self.Exists() :
                return False

            # Check for events : pause, stop, shuffle, rate, loop and fullscreen
            if self.t_pause == None :
                self.t_pause = Thread(target=self.CheckPause)
                self.t_pause.start()

            if self.t_paused == None :
                self.t_paused = Thread(target=self.SetOnPause)
                self.t_paused.start()

            if self.t_shuffle == None :
                self.t_shuffle = Thread(target=self.CheckShuffle)
                self.t_shuffle.start()

            if self.t_rate == None :
                self.t_rate = Thread(target=self.CheckRate)
                self.t_rate.start()

            if self.t_stop == None :
                self.t_stop = Thread(target=self.CheckStop)
                self.t_stop.start()

            if self.t_fullscreen == None :
                self.t_fullscreen = Thread(target=self.CheckFullScreen)
                self.t_fullscreen.start()

            if self.t_loop == None :
                self.t_loop = Thread(target=self.CheckLoop)
                self.t_loop.start()

            if l_eps != "" :
                l_track_id = self.GetTrackId()
                # print(self.on_pause)
                while os.path.isdir(self.GetTrackPath("file://", "")) :
                    print("DIR1 : " + self.GetTrackId())
                    time.sleep(0.5)
                    l_track_id = self.GetTrackId()
                while l_track_duration == -1 :
                    l_track_duration = self.GetTrackDuration()

                if l_eps not in Player.track_list :
                    Player.track_list[l_eps] = VFile(l_eps, l_track_id, l_track_duration)
                else :
                    if Player.track_list[l_eps].duration == -1 :
                        Player.track_list[l_eps].duration = l_track_duration = self.GetTrackDuration()

            Player.Log(meth_log)
            # idx = 0
            # print("RATE 0 :: " + str(self.Rate()))
            self.Rate(1)
            # print("RATE 1 :: " + str(self.Rate()))
            # print("3 :: " + l_eps + " == " + self.track)
            self.ExecEvent(True, self.m_before_loop)
            while l_eps == self.track :
                # print("4a")
                if not self.Exists() :
                    # print("break")
                    return False
                # print("4b")
                self.ExecEvent(l_eps == self.track, self.m_beg_loop)
                l_eps = self.GetTrackPath("file://", "")
                if l_eps != self.track :
                    l_eps = self.track
                    break
                # print("4c")
                # pos = self.Position()
                self.ExecMeth(l_eps == self.track, self.endLoop)

            # if pos > 0 :
            #     Player.track_list[l_eps].cur_time = pos

            # print("6")
            while self.track == "" :
                self.track = self.GetTrackPath("file://", "")
                time.sleep(0.2)
            self.track = self.GetTrackPath("file://", "")

            while os.path.isdir(self.track) :
                time.sleep(0.2)
                self.track = self.GetTrackPath("file://", "")

            self.track_id = self.GetTrackId()
            self.track_duration = self.GetTrackDuration() #[print, "TRK NEW " + str(self.track) + " " + str(self.track_id)]) # + " " + str(Player.track_list))

            if self.track != "" :
                if self.track not in Player.track_list :
                    Player.track_list[self.track] = VFile(self.GetTrackPath("file://", ""), self.GetTrackId(), self.track_duration)

            # self.afterLoop()
            self.ExecMeth(True, self.afterLoop)
            if self.track not in Player.timeline_file :
                Player.timeline_file[self.track] = 0

            # print("7 --" + self.track)
            res = True
        except Exception as e:
            print("EXP LOOP :: " + str(e))
            res = False

        except KeyboardInterrupt as e:
            print("Interruption")
            res = False
        finally :
            # print("ok !!!!!!!!!!!!!!!!!")
            return res
