# import dbus
import time
import os
# from threading import Thread
from urllib.parse import unquote
# from . import dbus_mpris
from . import kodiUtils
#
# class VFile() :
#     def __init__ (self, name, track_id, duration, cur_time = -1) :
#         self.name = name
#         self.track_id = track_id
#         self.duration = duration
#         self.cur_time = cur_time


class KodiPlayer(kodiUtils.KodiConn) :
    plist = {}
    plist_old = {}
    current = ""
    timeline_file = {}
    track_list = {}
    #
    # def init() :
    #     Player.plist = {}
    #     bus = dbus.SessionBus()
    #     for service in bus.list_names():
    #         if service.startswith('org.mpris.MediaPlayer2.'):
    #             for key in SOFTS.keys() :
    #                 if service.startswith(SOFTS[key]) :
    #                     player = Player(dbus.SessionBus().get_object(service, '/org/mpris/MediaPlayer2'), str(service))
    #                     Player.plist[str(service)] = player
    #
    #                     # print(player)
    #
    #                     if player.GetStatus() == "Playing" :
    #                         Player.current = str(service)

    def __init__ (self, p_s_host = "localhost", p_s_usr = "kodi", p_s_pass = "", p_s_port = "8080") :

        super().__init__(p_s_host, p_s_usr, p_s_pass, p_s_port)

        # self.player = service
        self.name = "kodi"
        self.track = ""
        self.track_id = ""
        self.track_duration = -1

        # self.m_paused = []
        # self.m_loopTrack = []
        # self.m_rate = []
        # self.m_shuffle = []
        # self.m_before_loop = []
        # self.m_beg_loop = []
        # self.m_end_loop = []
        # self.m_after_loop = []


    def Exists(self) :
        track = self.GetVideoPath()
        if "ERROR" in track :
            return False
        return True

    # def IsPlaying(self) :
    #     try :
    #         return self.GetStatus() == "Playing"
    #     except Exception as e:
    #         print(self.name + " : OK ?" )
    #         return False

    def Volume(self, val = "") :
        """ Get kodi volume """
        if val != "" :
            return self.SetVolume(int(val * 100))
        return self.GetVolume()

    def Position(self, in_second = True) :
        """ Get position """
        try :
            pos = -1
            return self.GetVideoTime()
        except Exception as e:
            print(self.GetAddr() + " : OK ?" )
        return pos

    # position in microseconds
    def Seek(self, position, in_second = True) :
        # return self.SeekTo()
        seek_to_time = position
        if not in_second :
            seek_to_time = seek_to_time / 1000 / 1000
        # Deplacement vers le temps
        seek_to_time = kodiUtils.KodiTools.HRTime(seek_to_time)
        # print(seek_to_time)
        # print("SEEKING {} {}".format(position, seek_to_time))
        self.SeekTo(seek_to_time)

    def Prop(self, prop, val = "") :
        """
        Get/Set a property,
        # prop : array[prop name, prop value] ( ex : ["Volume", 0.1] )
        # val : value
        """
        try :
            # if self.Exists() :
            if val != "" :
                return self.myKodi.execQry(prop, val, 'Video_' + prop)

            return self.myKodi.execQry(prop, {'playerid' : 1}, 'Video_' + prop)
            # return None
        except Exception as e:
            print(self.name + " : OK?\n Reloading player list " + str(e))
            return ""

    def Get(self, prop) :
        return self.Prop(prop)
    #
    def Set(self, prop) :
        return self.Prop(*prop)

    def GetTrackPath(self, to_replace = "", replace = "") :
        track = self.GetVideoPath()
        if "ERROR" in track :
            # print(track)
            return ""
        if to_replace != "" :
            return track.replace(to_replace, replace)
        return track
