##!/usr/bin/env python
## -*- coding: utf-8 -*-

import xbmc
import xbmcaddon
import xbmcplugin
import xbmcgui
# from xbmcplugin import addDirectoryItem, endOfDirectory
import os
import time
#from . import QueriesSocket
from koditvm.kodiUtils import KodiTools
from koditvm.QueriesSocket import QuerySocket
from koditvm.Constants import KodiAddonId, Path
import json
import sys

try:
    from urllib.parse import urlencode
except ImportError:
    # from urlparse import parse_qs
    from urllib import urlencode

class Log() :
    LOG_FILE = ""
    # LOG_FILE = "/tmp/kodi2tv.log"
    PREFIX = ""
    # LOG_FILE = ""

    def SetFile(path) :
        if os.path.exists(os.path.dirname(path)) :
            Log.LOG_FILE = path
            f = open(Log.LOG_FILE, "w")
            f.write("")
            f.close()

    def AppendFile(msg) :
        f = open(Log.LOG_FILE, "a")
        f.write(msg + "\n")
        f.close()

    def Show(msg, addon_title = "") :
        # xbmc.log(xbmcaddon.Addon().getAddonInfo('name') + addon_title + " WTF " + str(Log.LOG_FILE), 1)
        if Log.LOG_FILE != "" :
            l_msg = "{}{} {} {}".format(xbmcaddon.Addon().getAddonInfo('name') + Log.PREFIX + " ", "[INFO]", addon_title, msg)
            Log.AppendFile(l_msg)
        else :
            l_msg = "{}{} {}".format(xbmcaddon.Addon().getAddonInfo('name') + Log.PREFIX + " ", addon_title, msg)
            xbmc.log(l_msg, 1)

    def Warn(msg, addon_title = "") :
        if Log.LOG_FILE != "" :
            l_msg = "{}{} {} {}".format(xbmcaddon.Addon().getAddonInfo('name') + Log.PREFIX + " ", "[CAUTION]", addon_title, msg)
            Log.AppendFile(l_msg)
        else :
            l_msg = "{}{} {}".format(xbmcaddon.Addon().getAddonInfo('name') + Log.PREFIX + " ", addon_title, msg)
            xbmc.log(l_msg, 2)

    def Error(msg, addon_title = "") :
        if Log.LOG_FILE != "" :
            l_msg = "{}{} {} {}".format(xbmcaddon.Addon().getAddonInfo('name') + Log.PREFIX + " ", "[ERROR]", addon_title, msg)
            Log.AppendFile(l_msg)
        else :
            l_msg = "{}{} {}".format(xbmcaddon.Addon().getAddonInfo('name') + Log.PREFIX + " ", addon_title, msg)
            xbmc.log(l_msg, 3)

class SocketMove() :

    def __init__(self, host, port) :
        self.PORT_SOCKET = port
        self.HOST_SOCKET = host
        self.socket = QuerySocket(self.HOST_SOCKET, self.PORT_SOCKET)

        self.index = 0
        self.LIST = []

    def New(self, l_keep = 0, l_skip = 0, l_dir = 0) :
        """ Reinit socket """
        # self.socket = QuerySocket(self.HOST_SOCKET, self.PORT_SOCKET)
        res = self.socket.Init(l_keep, l_skip, l_dir)

        # self.PORT_SOCKET = port
        # self.HOST_SOCKET = host
        self.index = 0
        self.LIST = []
        if len(res) > 0 :
            self.current_dir = self.socket.DIRS[0]
        return res

    def SetPath(self, path) :
        self.current_dir = path

    def UpdateLink(self, l_eps, l_keep, skip_firsts_dir) :
        """ Will send query to socket to update links """
        Log.Show("UpdateLink START")
        main_dir = self.socket.UpdateLink(l_eps, l_keep, skip_firsts_dir)
        # Log.Show(main_dir)
        self.LIST = self.socket.GetListFiles(main_dir["list"])
        self.SAGA = main_dir["name"]
        self.index = main_dir["index"] - 1
        self.saison = main_dir["saison"]
        Log.Show("UpdateLink END")
        return main_dir

    def ReloadList(self, l_keep, vid = "", create = False) :
        """ Query socket to retrieve current playing files """
        # Log.Show("CREATE :: " + str(create))

        l_index = self.index
        if vid == "" :
            l_eps = "1|" + os.path.join("2", "")
        else :
            l_eps = os.path.join(Path.DIR_ENCOURS_NAME, self.SAGA) + "|" + os.path.join(str(self.saison), os.path.basename(vid))

        if vid == "" :
            main_dir = self.socket.CallPlaylist(l_eps, "", self.current_dir, l_keep, create=create)
        else :
            main_dir = self.socket.CallPlaylist(l_eps, "", self.current_dir, l_keep, index=l_index, create=create)

        if "error" in main_dir :
            raise Exception("Error : " + str(main_dir["error"]))
        self.LIST = self.socket.GetListFiles(main_dir["list"])
        self.SAGA = main_dir["name"]
        self.index = main_dir["index"] - 1
        self.saison = main_dir["saison"]


    def ShowAnRun(self, vid = "", play = True) :
        """ Show video list then run wished element (if play at true) """
        try :
            # Log.Show("ShowAnRun START")
            if vid != "" :
                url = KodiMove.url({
                    "addr" : self.socket.GetAddr(),
                    "saga" : str(self.SAGA) + "|" + str(self.saison),
                    "menu" : str(self.saison),
                    "episode" : vid,
                    "keep" : 1,
                    "index" : self.index,
                    "folder" : self.current_dir
                    })
                dir_path = "plugin://" + KodiAddonId.KodiTV + url
            else :
                dir_path = "plugin://" + KodiAddonId.KodiTV

            if vid != "" :
                xbmc.executebuiltin("ReplaceWindow(Videos, {})".format(dir_path))
                while not KodiMove.WaitForEntry() :
                    time.sleep(.5)
                if play :
                    KodiMove.SearchInDir(vid, dir_path)
                    KodiMove.SelectFile()
            # Log.Show("ShowAnRun END ({} | {})".format(vid, play))
            return True
        except Exception as e :
            raise e

    def RunFromSocket(self, l_keep = 0, l_skip = 0, l_eps = "") :
        """ Create socket, then run first video from socket response """
        try :
            res = self.socket.Init(l_keep, l_skip)

            if len(res) > 0 :
                # self.DIR_SOCKET = self.socket.DIRS[0]
                self.ReloadList(l_keep, l_eps)
                l_eps = "{}_{}".format(KodiTools.Right("0000", str(self.index + 1)), os.path.basename(self.LIST[l_keep]))
                res = self.ShowAnRun(l_eps, True)
                # Log.Show("Will find : {}".format(res))

                return res
            else :
                # KodiMove.Notification("Socket {} unreachable !".format(self.socket.ADDR), "", 3000)
                return False
        except Exception as e :
            raise e

class KodiMove(xbmc.Player) :
    addon_handle = -1

    def __init__ (self, l_s_file = "") :
        self.file2play = l_s_file

    def GetGlobalSetting(self, param) :
        return json.loads(xbmc.executeJSONRPC('{"jsonrpc":"2.0", "method":"Settings.GetSettingValue", "params":{"setting":"' + param + '"}, "id":1}'))["result"]["value"]

    def SendNotificationToKodi(self, p_s_message, p_s_title = "", p_i_time = 3):
        """ Send notification to kodi """
        KodiMove.Notification(p_s_message, p_s_title, p_i_time)
        # xbmc.executebuiltin('Notification("' + str(p_s_title) + '", "' + str(p_s_message) + '" , ' + str(p_i_time) + ')')

    def Notification(p_s_message, p_s_title = "", p_i_time = 3):
        """ Send a notification to kodi (with p_i_time in seconds)"""
        p_i_time = p_i_time * 1000
        xbmc.executebuiltin('Notification("' + str(p_s_title) + '", "' + str(p_s_message) + '" , ' + str(p_i_time) + ')')

    def Error(p_s_message, p_s_title = "", p_i_time = 5):
        """ Send a notification to kodi (with p_i_time in seconds)"""
        # p_i_time = p_i_time * 1000
        p_i_time = p_i_time * 1000
        # xbmc.executebuiltin('Notification(Error,Database is missing or has already been deleted.,5000,xbmcgui.NOTIFICATION_ERROR)')
        xbmc.executebuiltin('Notification("' + str(p_s_title) + '", "' + str(p_s_message) + '" , ' + str(p_i_time) + ', ' + xbmcgui.NOTIFICATION_ERROR + ')')

    def RunVideo(self, file2find) :
        """ Run one file """
        self.play(file2find)

    """ Play video from addon
    Recupere le chemin de la derniere vidéo jouée depuis le socket
    Lance l'addon pour jouer le ficher automatique
    le socket est passé en paramètre...
    """
    def RunLastVideo(self, socket, keep = 0, skip_firsts_dir = 0, sync = False, run = False) :
        try :
            return self.PlayFromAddon(socket.GetAddr(), keep, sync, run)
        except Exception as e:
            Log.Error(str(e))
            KodiMove.Notification("Erreur !", str(e), 5 )
            return False

    def url(pQuery, addon_addr = ""):
        """ Build url query """
        # Log.Show("URL:" + sys.argv[0] + '?' + urlencode(pQuery))
        return addon_addr + '?' + urlencode(pQuery)

    def GoToBegin() :
        """ Trying to reach first element in kodi list """
        try :
            try_count = 0
            # Log.Show("GoToBegin start")
            while xbmc.getInfoLabel('Container.CurrentItem') == "" or xbmc.getInfoLabel('Container.CurrentItem') != "0" :
                xbmc.executebuiltin('Action(PageUp)')
                try_count += 1
                if try_count > 50 : # Try 50 times (12.5s)
                    raise Exception("Unable to go to begin")
                time.sleep(0.25)
            return True
        except Exception as e :
            Log.Error(e)
            KodiMove.Notification(str(e), "ERROR !", 5)
            return False
        # finally :
        #     Log.Show("GoToBegin end")

    def WaitForEntry() :
        """ Wait for selectionnable entry """
        try :
            # Log.Show("WaitForEntry start")
            try_count = 0
            while xbmc.getInfoLabel('Container.CurrentItem') == "" :
                try_count += 1
                if try_count > 50 : # Try 50 times (12.5s)
                    raise Exception("Unable to go to begin")
                time.sleep(0.25)
            return True
        except Exception as e :
            Log.Error(e)
            return False
        # finally :
        #     Log.Show("WaitForEntry end")

    def SearchInDir(file2find, dir_path = "", run = True) :
        """ Move kodi to dir_path, then search and run file2find """
        try :
            # Log.Show("SearchInDir start :: " + file2find)
            while not KodiMove.WaitForEntry() :
                time.sleep(.1)
            # Log.Show("... ok")
            # Log.Show("Select File : {} in {}".format(file2find, dir_path))
            l_path = file2find
            if dir_path != "" :
                xbmc.executebuiltin("ReplaceWindow(Videos, " + dir_path + ")")
                time.sleep(0.5)
                # Log.Show("WaitForEntry 2")
                while not KodiMove.WaitForEntry() :
                    time.sleep(.1)
                # Log.Show("... ok")

            if l_path != "" and (xbmc.getInfoLabel('ListItem.Label') == l_path or xbmc.getInfoLabel('ListItem.Label').endswith(l_path)) :
                return True

            if not KodiMove.GoToBegin() :
                return False

            i = 0
            idx = 0
            i_max = 0
            while not (idx > 0 or (i_max > 0 and i_max < i)):
                # Will go down until file has been found
                path = xbmc.getInfoLabel('ListItem.Label')

                if path == l_path or path.endswith(l_path) :
                    # file2find has been found ...
                    idx = int(xbmc.getInfoLabel('Container.CurrentItem'))
                    if run :
                        return KodiMove.SelectFile()
                    break
                else :
                    xbmc.executebuiltin('Action(Down)')
                time.sleep(0.25)

                if i_max < int(xbmc.getInfoLabel('Container.CurrentItem')) :
                    i_max = int(xbmc.getInfoLabel('Container.CurrentItem'))

                i += 1
                if i > 40  or idx > 2 : # Trying 10 times (10s)
                    KodiMove.Notification("Unable to find !", l_path, 5 )
                    return False

            if idx == 0 :
                KodiMove.Notification("Non trouvé !", l_path, 5 )
                return False
            else :
                return True

        except Exception as e:
            Log.Error("SearchInDir : " + str(e))
            KodiMove.Notification("Erreur SearchInDir : ", str(e), 5 )
            return False
        # finally :
        #     Log.Show("SearchInDir End :: " + file2find)

  
    # def Right(s, amount):
    #     return s[-amount:]

    """
    Selectionne une premiere fois (pour lancer une lecture)
    Puis encore 3 secondes si kodi ne joue rien
    """
    def SelectFile() :
        time.sleep(0.25)
        xbmc.executebuiltin('Action(Select)')
        #time.sleep(3)
        time.sleep(0.5)
        l_count = 1
        # On essaie de lancer le fichier 3 fois sinon on arrete
        while not xbmc.Player().isPlaying() and l_count < 3 :
            # Log.Show('Container.CurrentItem'+xbmc.getInfoLabel('Container.CurrentItem'))
            xbmc.executebuiltin('Action(Select)')
            time.sleep(1)
            l_count += 1

        return xbmc.Player().isPlaying()

# BEGIN Synchronization between 2 kodi

    def RunSynchro(self, myKodi, MAIN_SOURCE_PORT, KEEP_FILES_NUMBER = 0, SKIP_FIRSTS_DIR = 0) :
        try :
            self.m_playing = False
            self.m_synchroDone = False
            self.searching = True
            #'{"id":1,"jsonrpc":"2.0","result":{"addon":{"addonid":"%s","enabled":false,"type":"xbmc.python.lyrics"}}}')
            self.m_playing = self.check_set_vid(myKodi, MAIN_SOURCE_PORT, KEEP_FILES_NUMBER, SKIP_FIRSTS_DIR)
            self.searching = False
        except Exception as e :
            Log.Error("ERROR : " + str(e))

        return self.m_playing

    # Run the remote kodi video on local kodi.
    def check_set_vid(self, myKodi, MAIN_SOURCE_PORT, KEEP_FILES_NUMBER = 0, SKIP_FIRSTS_DIR = 0) :
        l_vid2local = ""
        l_vid2launch = ""
        #FILE_PLAY = MAIN_SOURCE_DIR + "/" + DIR_ENCOURS_NAME + "/" + FILENAME_AUTOREAD + FILENAME_AUTOREAD_EXT
        try :
            l_i_secs = -1
            #Log.Show("hello addon! %s" % time.time() + " !!! ")
            if self.isPlaying() :
                # On recupere le chemin de la video qui tourne sur le lecteur local.
                l_vid2local = self.getPlayingFile()

            #l_vid2launch = myKodi.GetVideoPath()
            #if not l_vid2launch:
            l_vid2launch = myKodi.GetVideoPath("dynpath")


            if "ERROR" in l_vid2launch :
                #l_vid2launch = FILE_PLAY
                Log.Error("END : " + str([l_vid2launch]))

                return False

            # Si aucune video ne tourne, on va lancer la vidéo du socket...
            if l_vid2launch == "" and l_vid2local == "" :
                socket = SocketMove(myKodi.ADDR, MAIN_SOURCE_PORT)
                socket.New()
                socket.RunFromSocket(KEEP_FILES_NUMBER, SKIP_FIRSTS_DIR)
                # QuerySocket(myKodi.ADDR, MAIN_SOURCE_PORT)
                # socket.Init(KEEP_FILES_NUMBER, SKIP_FIRSTS_DIR)
                # self.RunLastVideo(socket, KEEP_FILES_NUMBER, SKIP_FIRSTS_DIR)
                return False
                # On recupere le chemin de la video distant

            #info_filesrv = kodiUtils.KodiTools.GetPathToRemove(l_vid2launch)
            #path2remove = info_filesrv[0]
            #SERVEUR = info_filesrv[1]


            # Si les 2 kodis jouent les memes videos, on va synchroniser
            if str(l_vid2launch).replace("/","") == str(l_vid2local).replace("/","") or l_vid2local.endswith(l_vid2launch) or (l_vid2local != "" and l_vid2launch.endswith(l_vid2local)) :
                # Log.Show("# LAUNCH:" + l_vid2local + " " + str(l_vid2launch))
                if l_vid2launch != "" :
                    # On lance la synchro
                    l_i_secs = self.SetSynchro(myKodi)
                    if l_i_secs != 0 :
                        Log.Show("# Seek to time..." + str(l_i_secs) + " " + str(l_vid2local) + " " + str(l_vid2launch))
            else :
                # On change de fichier
                # We set that synchro is not done
                self.m_synchroDone == False

                info_filesrv = KodiTools.GetPathToRemove(l_vid2launch)
                path2remove = info_filesrv[0]
                SERVEUR = info_filesrv[1]

                socket = SocketMove(myKodi.ADDR, MAIN_SOURCE_PORT)
                socket.New()
                # On lance le fichier via l'interface
                if l_vid2launch != "" :
                    if socket.RunFromSocket(KEEP_FILES_NUMBER, SKIP_FIRSTS_DIR) :
                        Log.Show("# LAUNCH2:" + socket.socket.GetAddr() + " " + str(l_vid2launch))
                        # On attend que le lecteur se lance
                        while not self.isPlaying() :
                            time.sleep(1)

                        # On attend que le lecteur se lance, encore...
                        while self.getTime() == 0 :
                            time.sleep(1)

                    # On lance la synchro
                    l_i_secs = self.SetSynchro(myKodi)
                    Log.Show("# LAUNCH:" + l_vid2local + " " + str(self.getTime()) + " " + str(l_vid2launch))

            return True
        except Exception as e:
            KodiMove.Notification("Error !", str(e), 5 )
            Log.Error("Error : " + str(e))
            return False

    def SetSynchro(self, myKodi):
        """ Synchronize remote kodi video time on local kodi video... """
        #xbmc.executebuiltin("StopScript(" + ADDON_ID_SOCKET + ")")
        if self.m_synchroDone == False :
            l_i_secs = 0.5
            while l_i_secs > 0.417 or l_i_secs < -0.417 :
                l_i_secs = self.GetOffset(myKodi, 0.417)
            #if int(l_i_secs) < 0.417 :
            self.m_synchroDone = True
        else :
            # On recherche pour une seconde de décalage...
            l_i_secs = self.GetOffset(myKodi, 1.500)
            if l_i_secs > 1.500 or l_i_secs < -1.500 :
                self.m_synchroDone = False

        return l_i_secs

    # Synchronize remote playing video with local kodi
    def GetOffset(self, myKodi, delay_max):
        # Get current time (to reduce offset process time)
        tstart = time.time()
        # Get local playing kodi video time
        l_time2local = self.getTime()
        # Get remote playing kodi video time
        # l_t_time2launch = myKodi.GetVideoTime(False)
        l_time2launch = myKodi.GetVideoTime()
        if l_time2launch == 0 :
            return -1
        # Get current time
        tend = time.time()
        # Compute process time.
        offset = tend - tstart
       # Log.Show( str(tstart) + " => " + str(tend) + " : " + str(offset) )
        # Compute time to seek to for local video
        l_time2seek = l_time2local - l_time2launch - offset

        if l_time2seek > delay_max:
            #Log.Show( '###!! SUP : ' + str(l_time2seek) + " | " + str(l_time2local) + " " + str(l_time2launch) + " # " + str(delay_max))
            if l_time2seek > delay_max + 2 :
                self.seekTime(int(l_time2launch) + 2 + delay_max)
                l_time2seek = 2 + delay_max
            self.pause()
            time.sleep(l_time2seek)
            self.pause()
            return l_time2seek
        else :

            if l_time2seek < 0.0  and l_time2seek <  - 0.417: #
                #Log.Show( '###!! INF : ' + str(l_time2seek) + " | " + str(l_time2local) + " " + str(l_time2launch) + " # " + str(delay_max))
                # Seek local video to the computed time
                self.seekTime(int(l_time2local) - int(l_time2seek) + 3.6)
                time.sleep(2)

                l_time2seek = self.GetOffset(myKodi, 0.417)
            else:
                return 0
        return l_time2seek

# ADDON_ID_FK = "plugin.video.kodi2tv"
# END Synchronization
