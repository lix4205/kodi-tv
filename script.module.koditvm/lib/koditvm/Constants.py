#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Liste des requetes a envoyer au socket
"""
class CmdSocket() :
    # BEGIN Queries name
    # Repertoires
    MAINDIRS = "getDirs" # Liste des répertoires a "observer" dans lesquels se trouve les dossiers encours
    UPDATELINK = "updateLink"
    DIRSPLAYING = "dirsPlaying" # Liste des répertoires du dossier ENCOURS
    # Fichiers
    LISTPLAYING = "getNextPlaying" # Liste des fichiers a jouer
    FILEPLAYING = "filePlaying" # Fichier en cours de lecture, on refait la liste des liens
    # Nettoyage
    CLEANLINKS = "cleanLinks" # Nettoie les liens
    # Generiques
    RELOADKEEPENTRY = "reloadKeepEntry"
    SETSKIPENTRY = "setSkipEntry " # Enregistrement d'une entrée de générique
    KEEPENTRY = "getKeepFile " # Cherche une entrée dans le fichier de générique
    MISSGENERIX = "missGenerix " # Liste des épisodes qui n'ont pas d'entrée de générique définie
    # Copie et autre (pas trop usefull...)
    SYNCPLAY = "syncPlaying" # Synchro des fichiers entre socket
    copyPlaying = "copyPlaying " # Copie de fichiers manquant vers le client
    setTime = "setTime " # Defini le temp de lecture de la video en cours
    # END Queries

    # BEGIN Others params
    DEFAULT_PORT = 9876
    DEFAULT_HOST = ""
    BUFSIZE = 8192

    MAIN_CAT = "usage/throw"
    # END Others

class Path() :
    MAIN_SOURCE_PROTOCOL = "nfs"
    DIR_ENCOURS_NAME = "ENCOURS"
    DIR_ENCOURS_PATH = ""
    MAIN_SOURCE_DIR = ""
    FILENAME_AUTOREAD = DIR_ENCOURS_NAME
    FILENAME_AUTOREAD_EXT = ".mp4"
    INDEX_TAGGED = ".index.json"
    INDEX_MD5 = ".md5.json"

class KodiAddonId() :
    KodiTV = "plugin.video.kodi2tv"
    Generix = "plugin.video.tvSkip"
    Synchro = "plugin.video.synkodi"
    Sleep = 'script.service.sleep'
# MAIN_SOURCE_ADDR = MAIN_SOURCE_PROTOCOL + "://" + MAIN_SOURCE_IP



