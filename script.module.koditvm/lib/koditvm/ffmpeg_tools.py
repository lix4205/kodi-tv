# Copyright (c) Elie Coutaud 2023
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!/usr/bin/python
# -*- coding: latin-1 -*-
import os
import sys
import time
from koditvm.pythcmd.spinner import spinner
from random import shuffle
import subprocess

W_DIR = "/media/xud/.dux/ffmpeg-sample/"

class FfmpegTools() :
    runner = spinner()

# ffmpeg -y -ss 0 -i "/media/pix/data/videos/ANIMES/Les Simpson/S06 1994/Les Simpson 06_01 Bart des ténèbres.mkv" -t 1321.13 -map 0:v:0 -c:v libx265 -map 0:a -c:a aac -map 0:s -c:s copy -vf fps=25.0 "/media/wd2t/data/S06 1994/Les Simpson 06_01 Bart des ténèbres.mkv"
    CMD = {
        "ffprobe_frames" : "ffprobe -v error -select_streams v:0 -count_frames -show_entries stream=nb_read_frames  -print_format default=nokey=1:noprint_wrappers=1 \"{}\"",
        "duration" : "ffprobe \"{}\" 2>/dev/stdout | grep Duration",
        "ffprobe_fps" : "ffprobe  \"{}\" 2> /dev/stdout | grep Stream.*Video",
        "ffmpeg_sample" : "ffmpeg -accurate_seek -y -ss {} -i \"{}\" -r {} -t {} -c:v {} -map 0:v -map 0:a -vf fps={} -vf scale=\"1920:1080\" -c:a aac -af \"apad\" \"{}\" {}",
        "ffmpeg_concat" : "ffmpeg -y {} -c:v {} -c:a aac -af \"volume=0.5\" \"{}\" {}",
        "ffmpeg_mkv" : "ffmpeg -y -ss {} -i \"{}\" -t {} -map 0:v:0 -c:v {} -map 0:a -c:a {} -map 0:s -c:s copy -vf fps={} \"{}\" {}",
        "ffmpeg_end" : "ffmpeg -y -ss {} -i \"{}\" -t {} -c:v {} -c:a {} -vf fps={} \"{}\" {}",
        # "ffmpeg_end" : "ffmpeg -y -ss {} -i \"{}\" -t {} -map 0:a -c:v {} -map 0:a -c:a {} -map 0:s -c:s copy -vf fps={} \"{}\" {}",
        "ffmpeg_end_keep" : "ffmpeg -r {} -y -i \"{}\" -r {} -c:v {} -c:a {} -vf fps={} \"{}\" {}",
        "ffmpeg_wav" : "ffmpeg -y -i \"{}\" -i \"{}\" -filter_complex amix=inputs=2:duration=first \"{}\" {}",
# Use this to get 30fps for video, here to transform 30 -> 60 fps
# -filter_complex "[0:v]setpts=0.50*PTS[v];[0:a]atempo=2.0[a]"
            # Old way
           # "ffmpeg_sample" : "ffmpeg -accurate_seek -y -ss {} -i \"{}\" -t {} -c:v {} -map 0:v -map 0:a -vf crop=16:9 -vf scale=\"1920:1080\" -c:a aac  \"{}\" {}",

   # "ffmpeg_wav" : "ffmpeg -y -i \"{}\" -i \"{}\" -filter_complex amix=inputs=2:duration=first \"{}\" {}",
# ffmpeg -ss 5.32 -i input.mp4 -c:v libx264 -c:a aac -frames:v 60 out.mp4


        # "ffmpeg_sample" : "ffmpeg -r 30 -y -ss {} -i \"{}\" -t {} -c:v {} -map 0:v -vf crop=16:9  -vf scale=\"1920:1080\" \"{}\" {}",
        # "ffmpeg_concat" : "ffmpeg -r 30 -y {} -c:v {} \"{}\" {}",
        # "ffmpeg_end" : "ffmpeg -r 30 -y -ss 0 -i \"{}\" -ss:a 0 -i \"{}\" -t {} -t:a {} -c:v {} -map 0:v -map 1:a -vf crop=16:9 -vf scale=\"1920:1080\" \"{}\" {}",
    }

    def check_call(cmd) :
        """
        return command code to be treated
        """
        try :
            return subprocess.check_call([cmd], shell=True)
        except KeyboardInterrupt as e:
            print("  ")
            print("  \nInterruption check_call")
            raise e
        except :
            print("  \nCommand failed : " + cmd, end="")
            return -1

    def check_output(cmd) :
        """ return output done by command line to be treated """
        try :
            return subprocess.check_output([cmd], shell=True)
        except KeyboardInterrupt as e:
            # print("  ")
            print("  \nInterruption check_output")
            raise e
        except :
            print("  \nCommand failed : " + cmd, end="")
            return ""

    def GetInfos(path_file) :
        """ Get fps, quality and duration of a video """
        qual = 0
        fps = 0
        # cmd_probe = "ffprobe \"" + path_file +"\" 2>/dev/stdout | grep Duration"
        if path_file != "" :

            cmd_frame = FfmpegTools.CMD["ffprobe_fps"].format(path_file.replace("$", "\$"))
            cmd_res = FfmpegTools.check_output(cmd_frame)

            # cmd_probe = ClipCreate.CMD["duration"].format(path_file)
            # hey = ClipCreate.check_output(cmd_probe)
            if cmd_res != "" : # and hey.decode() != "N/A":
                cmd_res = cmd_res.decode().split("\n")[0]
                if cmd_res == "N/A" :
                    # print("Not a video...")
                    return 0

                # Searching FPS, ...
                search_fps = cmd_res.split(", ")
                qual = search_fps[2].split(" ")[0]

                # Searching FPS, ...
                search_fps = cmd_res.split(" ")
                # search_fps = cmd_res.split(" ")
                if "fps," in search_fps :
                    idx_fps = search_fps.index("fps,") - 1
                    fps = search_fps[idx_fps]
                elif "tbs," in search_fps :
                    idx_fps = search_fps.index("tbs,") - 1
                    fps = search_fps[idx_fps] / 2

            if "x" not in qual :
                for k in search_fps[2:] :
                    if "x" in k :
                        qual = k.split(" ")[0]

            if qual == 0 :
                print("Quality not found in {}".format(cmd_res))



        return [ qual, float(fps) ]

    def GetFrames(path_file) :
        """ Get frames of a video """
        frame_v = 0
        # cmd_probe = "ffprobe \"" + path_file +"\" 2>/dev/stdout | grep Duration"
        if path_file != "" :

            cmd_frame = FfmpegTools.CMD["ffprobe_frames"].format(path_file.replace("$", "\$"))
            hey = FfmpegTools.check_output(cmd_frame)

            if hey != "" :
                frame_v = hey.decode().split("\n")[0]
                if frame_v == "N/A" :
                    # print("Not a video...")
                    return 0
            # print("E!" + str(hey)
        # print(path_file + " " + str(frame_v))
        return int(frame_v)

    def GetQuality(path_file) :
        """ Get FPS of a vidéo """
        qual = 0
        # cmd_probe = "ffprobe \"" + path_file +"\" 2>/dev/stdout | grep Duration"
        if path_file != "" :

            cmd_frame = FfmpegTools.CMD["ffprobe_fps"].format(path_file.replace("$", "\$"))
            cmd_res = FfmpegTools.check_output(cmd_frame)

            # cmd_probe = ClipCreate.CMD["duration"].format(path_file)
            # hey = ClipCreate.check_output(cmd_probe)
            if cmd_res != "" : # and hey.decode() != "N/A":
                cmd_res = cmd_res.decode().split("\n")[0]
                if cmd_res == "N/A" :
                    # print("Not a video...")
                    return 0

                # Searching FPS, ...
                search_fps = cmd_res.split(", ")
                qual = search_fps[2].split(" ")[0]

            if "x" not in qual :
                for k in search_fps[2:] :
                    if "x" in k :
                        qual = k.split(" ")[0]

            if qual == 0 :
                print("Quality not found in {}".format(cmd_res))

        return qual

    def GetFPS(path_file) :
        """
        Get FPS of a vidéo
        """
        fps = 0
        # cmd_probe = "ffprobe \"" + path_file +"\" 2>/dev/stdout | grep Duration"
        if path_file != "" :

            cmd_frame = FfmpegTools.CMD["ffprobe_fps"].format(path_file.replace("$", "\$"))
            hey = FfmpegTools.check_output(cmd_frame)

            # cmd_probe = ClipCreate.CMD["duration"].format(path_file)
            # hey = ClipCreate.check_output(cmd_probe)
            if hey != "" : # and hey.decode() != "N/A":
                hey = hey.decode().split("\n")[0]
                if hey == "N/A" :
                    # print("Not a video...")
                    return 0

                # Searching FPS, ...
                search_fps = hey.split(" ")
                if "fps," in search_fps :
                    idx_fps = search_fps.index("fps,") - 1
                    fps = search_fps[idx_fps]
                elif "tbs," in search_fps :
                    idx_fps = search_fps.index("tbs,") - 1
                    fps = search_fps[idx_fps] / 2

        return float(fps)

    def GetTime(path_file, in_seconds = True) :
        """
        Get time of a vidéo
        """
        time_v = -1
        # cmd_probe = "ffprobe \"" + path_file +"\" 2>/dev/stdout | grep Duration"
        if path_file != "" :
            cmd_probe = FfmpegTools.CMD["duration"].format(path_file.replace("$", "\$"))
            hey = FfmpegTools.check_output(cmd_probe)
            if not in_seconds :
                if hey != "" : # and hey.decode() != "N/A":
                    time_v = hey.decode().split(",")[0].split(": ")[1]
                return time_v

            if hey != "" : # and hey.decode() != "N/A":
                time_v = hey.decode().split(",")[0].split(": ")[1].split(":")

                if time_v[0] == "N/A" :
                    # print("Not a video...")
                    return -1

                hours = int(time_v[0]) * 3600
                minuts = int(time_v[1]) * 60
                secs = float(time_v[2])

                time_v = hours + minuts + secs

        return time_v

    def CreateFinalVid(input_cmd, output_file, fps = 30, codec = "copy", output_cmd = "2>> /dev/null") :
        """
        Concat video with mixed video samples
        """
        cmd_ffmpeg = FfmpegTools.CMD["ffmpeg_concat"].format(input_cmd, codec, output_file, output_cmd)
        # ffmpeg_cmd = "ffmpeg    {} -c:v {} \"{}\"".format(concat_cmd.format(str("|".join(final_cmd_list))), "copy", output_file.format("end"))
        # print(cmd_ffmpeg)

        return FfmpegTools.check_call(cmd_ffmpeg)

    def MixAudioFile(audio_file_1, audio_file_2, output_file_wave, output_cmd = "2>> /dev/null") :
        cmd_wav = FfmpegTools.CMD["ffmpeg_wav"].format(audio_file_1, audio_file_2, output_file_wave, output_cmd)
        res = FfmpegTools.check_call(cmd_wav)
        if res != 0 :
            # print(cmd_wav)
            return False
        return True


class FfmpegConcat() :
    def __init__(self, vid_path, fps = 30) :
        """ Get bases info video """
        self.runner = FfmpegTools.runner
        # self.SAMPLE_TIME = os.path.basename(os.path.dirname(vid_path))
        self.FPS = fps
        # output_file = os.path.join(os.path.join(W_DIR, os.path.basename(os.path.dirname(vid_path)) + "-output-{}.ts"))
        # output_file_final = os.path.join(os.path.join(W_DIR, os.path.basename(os.path.dirname(vid_path)), "output-{}.ts")).format(os.path.basename(vid_path))
        #
        # # output_file_final = output_file.format(os.path.basename(vid_path))
        # # self.SAMPLE_TIME
        self.input_dir = vid_path
        self.output_file = os.path.join(os.path.join(W_DIR, os.path.basename(os.path.dirname(vid_path)) + "-output-{}.ts"))
        self.output_file_final = self.output_file.format(os.path.basename(vid_path))

    def ConcatFiles(self, random = False, output_cmd = "2>> /dev/null") :

        concat_cmd = "-i \"concat:{}\""
        list_file_all = sorted(os.listdir(self.input_dir))
        if random :
            shuffle(list_file_all)

        dico_file = {}
        for l_dir in list_file_all :
            path_ss_dir = os.path.join(self.input_dir, l_dir)
            if not os.path.isdir(path_ss_dir) :
                iteration = int(len(list_file_all) / 1000)
                i = 0
                while i < iteration + 1 :
                    dico_file[self.input_dir + "_" + str(i)] = list_file_all[(i*1000):(i*1000)+1000]
                    i += 1
                os.chdir(self.input_dir)
                break
            dico_file[path_ss_dir] = sorted(os.listdir(path_ss_dir))

        i = 0
        final_cmd_list = []
        iteration = int(len(dico_file.keys()))
        for l_dir in dico_file.keys() :
            if os.path.exists(l_dir) :
                os.chdir(l_dir)
            input_cmd = concat_cmd.format(str("|".join(dico_file[l_dir])))
            # print(input_cmd)
            proc = self.runner.start("[{}/{}]  -> {}", [i + 1, iteration, self.output_file.format(i)], FfmpegTools.CreateFinalVid,[input_cmd, self.output_file.format(i), self.FPS, "copy"], False)
            final_cmd_list.append(self.output_file.format(i))

            self.runner.wait()

            print()
            i += 1
        proc = self.runner.start("[{}]  -> {}", [str("|".join(final_cmd_list)), self.output_file_final], FfmpegTools.CreateFinalVid, [concat_cmd.format(str("|".join(final_cmd_list))), self.output_file_final, self.FPS, "copy", output_cmd], False)
        self.runner.wait()
        # print()

        return True

    def CompilFromTS(self, output_cmd = "2>> /dev/null") :
        if os.path.exists(self.output_file_final) :

            final_file = os.path.join(self.output_file_final + ".mp4")
            final_cmd = FfmpegTools.CMD["ffmpeg_end_keep"].format(self.FPS, self.output_file_final, self.FPS, "libx265", "aac", self.FPS, final_file, output_cmd)

            # print(final_cmd)
            final_res = self.runner.start("Creating : {} from : {}", [final_file, self.output_file_final], FfmpegTools.check_call, [final_cmd], False)

            if final_res.returnMethod != 0 :
                print("Error on : " + final_cmd)
                return False
        return True

class FfmpegSplit() :
    def __init__(self, vid_path, fps, parallel_proc_number = 20, verbose = True) :
        """
        Get bases info video
        """
        # fps_vid = FfmpegTools.GetFPS(vid_path)
        # if fps_vid == 0 :
        #     print("Error while searching fps for: " + vid_path)
            # sys.exit(3)

        vid_time = FfmpegTools.GetTime(vid_path)
        if vid_time < 0 :
            print("Error while searching time for : " + vid_path)
            sys.exit(3)
        self.VIDEO_PATH = vid_path
        self.TIME_VID = float(vid_time)
        self.FPS_VID = float(FfmpegTools.GetFPS(vid_path))
        self.runner = FfmpegTools.runner
        self.PARALLEL_PROC_NUMBER = parallel_proc_number
        self.verbose = verbose
        self.proc_list = []
        self.FPS = fps
        self.LAST_CMD = ""

    def SetFileName(float_value) :
        adj_0 = ""
        for i in range(0, len(str(float_value))) :
            adj_0 += "0"
        return adj_0

    def SetSample(self, sample_time) :
        # print("1000 / {}".format(self.FPS_VID))
        if self.FPS_VID > 0 :
            time_frame = 1000 / self.FPS_VID
            # print(time_frame)
            # vid_time = self.GetTime(vid)
            # if self.FPS >= self.FPS_VID :
            self.RAPPORT_FPS = self.FPS / self.FPS_VID
            # else :
            #     rapport_fps =

            frames_in_sample = round(sample_time*1000/time_frame) # * rapport_fps
            # new_sample_time = frames_in_sample * time_frame / 1000

            self.SAMPLE_TIME = frames_in_sample * time_frame / 1000
            self.SAMPLE_FRAME = frames_in_sample
            if self.verbose :
        # print("PROC NUM :: {}".format(self.PARALLEL_PROC_NUMBER))
                print("""#############################
# File : """ + self.VIDEO_PATH + """ | duration : """ + str(self.TIME_VID) +"""
# FPS : """ + str(self.FPS_VID) + """ | Frame duration : """ + str(time_frame) + """
# Sample time : """ + str(sample_time) + """ | Nb Frame in sample: """ + str(frames_in_sample) + """
# New sample time : """ + str(self.SAMPLE_TIME) +"""
# File to be created : """ + str(self.TIME_VID / self.SAMPLE_TIME) +"""
#############################""")
            return True
        return False

    # def LoopInDict(l_dico, sample_time, , output_cmd = "2>> /dev/null") :
    #     # For each time in dict
    #     for new_time in l_dico :
    #         l_smp = fftools.LoopForSample(path_sample, round(new_time, 2), round(new_time + self.SAMPLE_TIME * 2, 2), OUTPUT_CMD_SAMPLE)
    #         if len(l_smp) > 0 :
    #         # print(str(round(new_time, 2)) + "/" + str(round(new_time+ self.SAMPLE_TIME * 2, 2) ), end = "")
    #             smp_list.append(l_smp[0])
    #             # fftools.LAST_CMD = ""
    #
    #         else :
    #             print("\nFailed {} : {} -> {}".format(os.path.basename(path_sample), round(new_time, 2), round(new_time + self.SAMPLE_TIME * 2, 2)))
    #             print(fftools.LAST_CMD)
    #             fftools.LAST_CMD = ""
    #

    def LoopForSample(self, dir_vid, start_time = 0, end_time = -1, output_cmd = "2>> /dev/null") :
        """
        Generate samples from dir_vid from start time (default 0) to end time (default 0)
        """
        smp_done = []

        idx_time = 0
        if end_time < 0 :
            end_time = self.TIME_VID
        nb_files = 0


        # Old way, we will rename if file already exists
        # adj_0_old = FfmpegSplit.SetFileName(self.TIME_VID * 1000)
        # New way, dispose of some 0...
        adj_0 = FfmpegSplit.SetFileName(int(self.TIME_VID) * 1000)
        # adj_0 = adj_0_old
        # for i in range(0, len(str(self.TIME_VID * 1000))) :
        #     adj_0 += "0"
        adj_1 = FfmpegSplit.SetFileName(int(self.TIME_VID / self.SAMPLE_TIME))
        # adj_1 = ""
        # for i in range(0, len(str(int(self.TIME_VID / self.SAMPLE_TIME)))) :
        #     adj_1 += "0"
        dir_files = Right(adj_1 + str(nb_files), len(adj_1))
        # print("NEW_DIR 1 : " + str(start_time) + "/" + str(end_time*1000))
        frames_done = 0 # start_time * self.FPS_VID
        # dir_files = end_time / self.SAMPLE_TIME % 1000
        frames_total = end_time * self.FPS_VID

        # print(str(self.TIME_VID * 1000))
        if not os.path.exists(dir_vid) :
            os.mkdir(dir_vid)

        while frames_done < frames_total :
            # print(idx_time)
            if start_time > 0 and idx_time < start_time :
                nb_files += 1
                frames_done += self.SAMPLE_FRAME + 1

                idx_time = frames_done / self.FPS_VID
                if nb_files % 1000 == 0 :
                    dir_files = Right(adj_1 + str(nb_files), len(adj_1))
                continue

            if nb_files % 1000 == 0 :
                dir_files = Right(adj_1 + str(nb_files), len(adj_1))

            time_target = frames_done / self.FPS_VID
            time_int = int(time_target * 1000)

            time_int = Right(adj_0 + str(time_int), len(adj_0))
            sample_file = os.path.join(dir_vid, dir_files, time_int + ".ts")

            if not os.path.exists(os.path.join(dir_vid, dir_files)) :
                os.mkdir(os.path.join(dir_vid, dir_files))

            # Old way to do, no directory
            if os.path.exists(os.path.join(dir_vid, time_int + ".ts")) :
                os.rename(os.path.join(dir_vid, time_int + ".ts"), sample_file)

            if os.path.exists(sample_file) and os.stat(sample_file).st_size == 0 :
                os.remove(sample_file)

            sample_cmd = FfmpegTools.CMD["ffmpeg_sample"].format(time_target, self.VIDEO_PATH, self.FPS, self.SAMPLE_TIME, "libx265", self.FPS, sample_file, output_cmd)
            self.LAST_CMD = sample_cmd
            # print(sample_cmd)
            if not os.path.exists(sample_file) :
                # print()
                if self.verbose :
                    proc_cmd  = self.runner.start("[{}/{}]  <- {}", [round(idx_time, 2), str(end_time), sample_file], FfmpegTools.check_call, [sample_cmd])
                else :
                    proc_cmd  = self.runner.start(method=FfmpegTools.check_call, args_method=[sample_cmd])

                # print()
                self.proc_list.append(proc_cmd)
            # if self.FPS_VID > 30 :
            #     print(sample_cmd)
            idx_time += self.SAMPLE_TIME
            nb_files += 1
            frames_done += self.SAMPLE_FRAME + 1
            smp_done.append(sample_file)


            # Waiting for at least one processe to finish
            while len(self.proc_list) == self.PARALLEL_PROC_NUMBER :
                for proc in self.proc_list :
                    if not proc.is_alive() :
                        self.proc_list.pop(self.proc_list.index(proc))

        for proc in self.proc_list :
            while proc.is_alive() :
                time.sleep(.1)
            self.proc_list.pop(self.proc_list.index(proc))
        if self.verbose :
            print("\n[{}/{}] Samples ready !".format(round(idx_time, 2), str(end_time)))

        return smp_done

def Right(s, amount):
    return s[-amount:]

def Left(s, amount):
    return s[:amount]

