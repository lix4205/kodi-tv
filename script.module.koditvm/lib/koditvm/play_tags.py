# Copyright (c) Elie Coutaud 2024
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!/usr/bin/python
# -*- coding: latin-1 -*-

import os, sys, time
from koditvm.pythcomm import txt_utils as txt
from koditvm.TagsMng import TagsMng, TagsList
from koditvm.dbus_mpris import Player
from koditvm.ffmpeg_tools import FfmpegTools

# import TagsMng
# import dbus_mpris

class PlayTags(TagsList) :
    PLAYER = "vlc"
    PLAYER_CMD = "vlc"
    PLAYER_OPT = "--play-and-exit"
    PLAYER_OUTPUT = "2> /dev/null"
    # PLAYER_OUTPUT = ""

    PREFIX_MSG_DEFAULT = "  SMP "
    PREFIX_MSG = PREFIX_MSG_DEFAULT

    def runInstance(self, files, player_opt = [], pos = 0, html = False) :
        try :
            instance = None
            opts = ""
            for opt in player_opt :
                opts += " " + opt
            # vlc = "VLC_VERBOSE=0; " + VLC_CMD + " --start-time " + str(time_to_seek) + " " + VLC_OPT + " \"" + files[0] +"\" "  + OUTPUT_CMD

            # html = True
            if html :
                vlc = "firefox file:///tmp/vid_test.html"
            else :
                vlc = "VLC_VERBOSE=0; " + PlayTags.PLAYER_CMD + opts + " " + PlayTags.PLAYER_OPT  + " \"" + files[0] + "\" " + PlayTags.PLAYER_OUTPUT
            # vlc = "VLC_VERBOSE=0; " + VLC_CMD + " --start-time " + str(time_to_seek) + " " + VLC_OPT + " \"" + "\" \"".join(files) + "\" "  + OUTPUT_CMD

            instance_id = self.runVlc(vlc)
            # print(instance.Get('Metadata'))
            # print(vlc)
            instance = Player.plist[instance_id]
            time.sleep(.3)

            if not html :
            # idx = 0
                new_list = list(reversed(files[10:]))
                while not len(instance.GetTracks()) > 0 :
                    time.sleep(0.1)
                    # print("sle")

                l_eps = files[0]
                # print(instance.Interface("TrackList").GetTracksMetadata(instance.GetTracks()))
                instance.SetTrackList()
                for f in files[1:9] :
                    instance.AddTrack("file://" + f.replace("%", "%25"), Player.track_list[l_eps].track_id)
                    instance.Next()

                    while f not in Player.track_list :
                        instance.SetTrackList()
                    l_eps = f
                instance.SetPosition(Player.track_list[files[0]].track_id)
                # time.sleep(1)
                #
                if len(new_list) > 0 :
                    instance.AddTracks(new_list, play = False)
                #
                # # print(instance.GetTracks()[0])
                # # print(Player.track_list[files[0]].track_id)
                # while f not in Player.track_list.keys() :
                #     time.sleep(.3)
                #     print("{}".format(Player.track_list))
                #
                # print(instance.GetTracks())
                # while Player.track_list[files[0]].track_id != instance.GetTrackId() :
                #     print(instance.SetPosition(Player.track_list[files[0]].track_id, pos))
                #     time.sleep(.3)
                #     print("{} != {}".format(Player.track_list[files[0]].track_id, instance.GetTrackId()))
                #
                # print("track :: " + files[0] )
                # if pos != 0 :
                #     # print("seeo {}".format(pos))
                #     instance.SeekTo(pos)
                # while instance.GetTrackPath() != files[0] :
                #     time.sleep(.3)

        except Exception as e:
            print("Player failure : "  + str(e))
            return None
            # sys.exit(2)
        finally :
            # print("Player launched !")
            # if len(proc_list) > 0 :
            #     if proc_list[0].is_alive() :
            #         proc_list[0].join(1)
            #     proc_list.pop(0)
            # # return True
            return instance

    def runVlc(self, cmd) :
        """ run command in a thread and add it to the main list """
        t = Player.RunPlayer(cmd)
        # print(cmd)
        # proc_list.append(t[0])
        self.proc = t[0]
        return t[1]

    def SaveSample(self, end) :
        """ Will save new sample from beg to end """
        try :
            new_tag = []
            if self.NEW_SAMPLE >= 0 :
                beg = self.NEW_SAMPLE
                if end - beg <= 0 :
                    print()
                    print("Begin is over the end ! ({} > {})".format(beg, end))
                    PlayTags.PREFIX_MSG = PlayTags.PREFIX_MSG_DEFAULT
                    return []
                duration = Player.track_list[self.eps].duration
                new_tag = self.NewSample(beg, end, duration)
                # print(new_tag)
                self.NEW_SAMPLE = -1
                self.overwrite = 0
                if len(new_tag) > 0 :
                    # print()
                    deleted_keys = ""
                    if len(new_tag) > 3 :
                        deleted_keys = " (Deleted : {})".format(len(new_tag[3]))
                    self.dico_eps = self.GetDicoEps()
                    a_cover = self.CoverTime(duration, self.dico_eps)
                    percent_cover = a_cover[1]
                    self.count_vid[self.eps] = self.FindIndexNextJump(self.dico_eps, end)
                    print("{}% {} {}".format(percent_cover, txt.HRTime(percent_cover * duration / 100), deleted_keys))
                    self.counter_save[0] += 1
                    self.counter_save[1] = new_tag[2]
                PlayTags.PREFIX_MSG = PlayTags.PREFIX_MSG_DEFAULT
                # return new_tag
        except Exception as ex :
            print("Fail save : " + str(ex))
        finally :
            return new_tag

    # def SavePredefinedJump(self) :
    #     self.

    def DeleteSample(self, end) :
        duration = Player.track_list[self.eps].duration
        new_tag = self.DelSample(end, self.vidTagged[self.eps][self.count_vid[self.eps]])
        if new_tag > 0 :

            # deleted_keys = ""
            # if len(new_tag) > 3 :
            deleted_keys = " (Deleted : {})".format(new_tag)

            self.dico_eps = self.GetDicoEps()
            a_cover = self.CoverTime(duration, self.dico_eps)
            percent_cover = a_cover[1]
            self.count_vid[self.eps] = self.FindIndexNextJump(self.dico_eps, end)
            print("{}% {} {}".format(percent_cover, txt.HRTime(percent_cover * duration / 100), deleted_keys))

            PlayTags.PREFIX_MSG = PlayTags.PREFIX_MSG_DEFAULT
            return new_tag
        else :
            print("No tags modified !")


    def ThreadRunTime(self) :
        try :
            # First played track is not the same in kodi
            while self.eps != self.player.track :
                time.sleep(.3)
            while self.player.Exists() :
                # pos = self.player.Position()
                self.SetCurrentSample(self.player.Position(), True)
                self.printRunTime()
            print("End print runtime thread")
            return True
        except Exception as e :
            print("Error ThreadRunTime : " + str(e))
            return False
        finally :
            self.t_runtime = None

    def ConfirmSearch(self) :
        self.player.Play()
        self.confirm_search = True
        # print("Confirm : {}".format(self.confirm_search))

    def AbortSearch(self) :
        self.player.Play()
        self.abort_search = True
        # print("Abort : {}".format(self.confirm_search))

    def SwitchDelete(self) :
        # self.abort_search = True
        # self.player.Pause()
        print("SwitchDelete : {}".format(self.confirm_search))
        # self.player.Pause()
        # self.NEW_SAMPLE = -1
        self.DEL_SAMPLE = self.player.Position()
        # We relaunch the thread to detect confirmation or abortion
        self.player.FramePerfectMode()

    def RateSelection(self, a_choices) :
        l_choice = 0
        sys.stdout.write('\033[2K\033[1G')
        print("Selected : {}".format(a_choices[l_choice]), end = " ", flush = True)
        while not self.confirm_search and not self.abort_search :
            nb_click = self.player.Rated(0, 99, False)
            if nb_click != 0 :
                sys.stdout.write('\033[2K\033[1G')
                l_choice = (l_choice + nb_click)  % len(a_choices)
                print("Selected : {}".format(a_choices[l_choice]), end = " ", flush = True)
                # print("Selected : {} | {} | {}".format(a_choices[l_choice], self.confirm_search, self.player.m_paused), end = "", flush = True)
        self.player.Pause()
        return l_choice

    def SelectCat(self, a_choices = []) :
        # if a_choices == [] :
        #     a_choices = self.tags.GetMainCategories()
        # print(a_choices)
        self.confirm_search = False
        self.player.FramePerfectMode()
        return self.RateSelection(a_choices)

    def SetNewCat(self, cat = "", pref_cat = []) :
        """ On pause mode, we can choose to edit categories """
        try :
            cat_list = self.catEdit
            multiple = False
            cats = []
            self.searching = True
            self.NODISPLAY = True
            # self.paused = True

            # On pause method are not allowed to be done in this context
            m_rated = self.player.m_rate
            self.player.m_rate = [ ]
            m_paused = self.player.m_paused
            self.player.m_paused = [ self.ConfirmSearch, self.AbortSearch ]

            self.player.FramePerfectMode()

            # In first, we choose an action
            sys.stdout.write('\033[2K\033[1G')
            print("### Tag categories selection (current : {}) : ".format(self.catEdit))
            print("Use +/- to select entry (Pause to confirm, Double pause to abort) : ")
            sys.stdout.write('\033[2K\033[1G')

            a_choice = [ "Add to current", "Delete in current", "Clear current" ]
            l_action = self.RateSelection(a_choice)

            if self.abort_search :
                print("ABORTED !")
                self.abort_search = False
                return cat_list

            all_cats = self.tags.GetMainCategories()
            b_end = False
            while not b_end :

                if l_action == 0 :
                    # Add a category to list :
                    # Select a main category
                    print(all_cats)
                    l_new_cat = self.SelectCat(all_cats)
                    l_main_cat = all_cats[l_new_cat]
                elif l_action == 1 :
                    # Delete a category in list :
                    l_new_cat = self.SelectCat(self.catEdit)
                elif l_action == 2 :
                    # Clear list :
                    self.catEdit = []
                    sys.stdout.write('\033[2K\033[1G')
                    print("List cleared")
                    # Then we're gonna add a new category
                    l_action = 0
                    continue

                if self.abort_search :
                    print("ABORTED !")
                    self.abort_search = False
                    return cat_list

                if l_action == 0 :
                    # Then select a sub category
                    a_sub_cat = self.tags.GetSubCategories(l_main_cat)
                    print(a_sub_cat)
                    l_new_cat = self.SelectCat(a_sub_cat)
                    final_cat = l_main_cat + "/" + a_sub_cat[l_new_cat]
                    self.catEdit.append(final_cat)
                elif l_action == 1 :
                    sys.stdout.write('\033[2K\033[1G')
                    print("{} removed.".format(self.catEdit[l_new_cat]))
                    self.catEdit.pop(l_new_cat)
                elif l_action == 2 :
                    l_new_cat = self.SelectCat(all_cats)

                # else :
                print("Continue ? {}".format(self.catEdit))
                l_confirm = self.SelectCat(["End", "Add", "Abort", "Back"])

                self.confirm_search = False
                if self.abort_search :
                    print("ABORTED !")
                    self.abort_search = False
                    return cat_list

                if l_confirm in [0, 2]:
                    b_end = True
                    if l_confirm == 2 :
                        self.catEdit = cat_list
                if l_confirm == 1 :
                    l_action = 0
                if l_confirm == 3 :
                    l_action = self.RateSelection(a_choice)

            return self.catEdit
        except Exception as e:
            raise Exception("SetNewCat : " + str(e))
        finally :
            self.player.m_paused = m_paused
            self.player.m_rate = m_rated
            # print("CATS : " + str(main_cat))
            self.searching = False
            self.NODISPLAY = False
            self.player.Play()


    def FramePerfectMode(self, smp = [], pos_dsp = -1, b_confirm = False, prefix = "EDIT") :
        """ Overloads rated methods to search for the perfect frame """
        try :
            # On pause method are not allowed to be done in this context
            m_rated = self.player.m_rate
            self.player.m_rate = [ ]
            m_paused = self.player.m_paused
            self.player.m_paused = [ self.ConfirmSearch, self.AbortSearch, self.SwitchDelete ]
            self.player.Pause()
            PlayTags.PREFIX_MSG = " %" + prefix + " "
            while not self.player.IsPaused() :
                time.sleep(.05)
            self.searching = True
            jump = 1/30
            if not self.HasInfoFile(self.eps) :
                infos = FfmpegTools.GetInfos(self.eps)
                # print(infos)
                if infos[0] != 0 and infos[1] > 0 :
                    if self.SetInfoFile(self.eps, *infos) :
                        # print("save")
                        self.tags.Save()
                    # jump = 1/self.GetInfoFiles()
                    # else :
                    jump = 1/infos[1]
            # print(jump)
            # jump = 1/self.GetInfoFiles()
            # return jump
            b_smp = len(smp) > 0
            pos = -1

            PlayTags.PREFIX_MSG = "  " + prefix + " "

            self.player.FramePerfectMode()

            while not self.confirm_search and not self.abort_search :
                # if self.player.IsPlaying() :
                    # print("CAUTION ! confirm or abort are not True !")
                pos = self.player.Position()
                while pos == -1 :
                    pos = self.player.Position()
                    if self.player.IsStopped() or not self.player.Exists() :
                        pos = -1
                        b_confirm = False
                        break
                    continue

                if b_smp and b_confirm :
                    print(" END", end = "", flush = True)
                rate = float(self.player.Rate())
                if not rate == 1.0 :
                    if rate > 1 :
                        self.player.Seek(jump + jump/10)
                    else :
                        self.player.Seek(-jump - jump/10)
                    self.player.Rate(1)
                if b_smp and b_confirm and pos > smp[1] :
                    pos = smp[1]
                    self.player.Play()
                    break


            # Switch methods to normal
            # self.player.m_paused = m_paused
            self.player.Rate(1)
            # self.searching = False
            self.confirm_search = False
            if self.abort_search :
                self.abort_search = False
                # self.player.Rate(1)
                # self.searching = False
                #
                # PlayTags.PREFIX_MSG = PlayTags.PREFIX_MSG_DEFAULT
                if self.NEW_SAMPLE > 0 or self.DEL_SAMPLE > 0:
                    print("CANCELLED")
                return -1

            if self.player.IsStopped() or not self.player.Exists() :
                return -1

            PlayTags.PREFIX_MSG = PlayTags.PREFIX_MSG_DEFAULT
            return pos
        except Exception as e:
            raise Exception("FramePerfectMode : " + str(e))
        finally :
            PlayTags.PREFIX_MSG = PlayTags.PREFIX_MSG_DEFAULT
            self.player.m_paused = m_paused
            self.player.m_rate = m_rated
            # print("CATS : " + str(main_cat))
            self.searching = False

    def ShowCover(self, duration) :
        # print("1 :: d=" + str(duration))
        msg = ""
        if not len(self.dico_eps) > 0 :
            return ""
        dicoEps = self.dico_eps

        if dicoEps[len(dicoEps) - 1][1] == -1 :
            dicoEps[len(dicoEps) - 1][1] = duration
        # print(dicoEps[len(dicoEps) - 1])
        infos = self.CoverTime(duration, dicoEps)
        # print("2 :: " + str(infos))
        d = txt.HRTime(infos[0])
        msg += "[{}] : {} {}% ({})".format(",".join(self.catRead), d, *infos[1:])

        if self.catList != self.catRead :
            dicoTmp = TagsMng.LIST_INDEX_FILES[self.root_file].SearchForMultipleTags(self.catList)
            # l_md5 = TagsMng.LIST_MD5_FILES[self.root_file].GetMd5(self.eps)
            if self.eps in dicoTmp :
                # print(dicoTmp)
                infos = self.CoverTime(duration, dicoTmp[self.eps])
                d = txt.HRTime(infos[0])
                msg += " | [{}] : {} {}% ({})".format(",".join(self.catList), d, *infos[1:])

        return msg

    def Stats(self) :
        print("stat mode ! " + str(self.dirs))
        for k in self.dirs :
            for d in self.dirs[k] :
                if d == k :
                    ld = os.listdir(d)
                    for l_type in TagsMng.LIST_INDEX_FILES[k].GetPathSagas() :
                        if l_type in ld :
                            for l_saga in os.listdir(os.path.join(d, l_type)) :
                                p = os.path.join(k, l_type, l_saga)
                                if os.path.isdir(p) :
                                    print(p.replace(os.path.join(k, ""), ""), end = " ", flush = True)
                                    print(len(TagsMng.LIST_INDEX_FILES[k].SearchForMultipleTags(self.catRead,  p.replace(os.path.join(k, ""), ""))))

                else :
                    p = os.path.join(k, d)
                    if os.path.isdir(p) :
                        print(p.replace(os.path.join(k, ""), ""), end = " ", flush = True)
                        print(len(TagsMng.LIST_INDEX_FILES[k].SearchForMultipleTags(self.catRead,  p.replace(os.path.join(k, ""), ""))))


        # print(VLC_ARG)

    def PrintRunTime(self, tv_mode = True) :
        try :
            if self.eps != "" :
                # Get track duration if not over 0...
                if Player.track_list[self.eps].duration == 0 :
                    Player.track_list[self.eps].duration = self.player.GetTrackDuration()
                    # print("ok")
                duration = txt.HRTime(Player.track_list[self.eps].duration)
                path = os.path.basename(self.root_file) + self.eps.replace(self.root_file, "") # + " " + self.player.track_id.replace("/org/videolan/vlc/playlist/","")
                concat_smp = self.ShowCover(Player.track_list[self.eps].duration)
                if concat_smp != "" :
                    concat_smp = "| " + concat_smp

                # Message will be printed before loop start
                sys.stdout.write('\033[2K\033[1G')
                print("RUN {} ({}) {}".format(path, duration, concat_smp))
                if not tv_mode and len(self.dico_eps) > 0:
                    self.printRunTime(tv_mode)
        except Exception as e :
            raise Exception("PrintRunTime : " + str(e))

    def printRunTime(self, tv_mode = True, edit_mode = False) :
        try :
            if self.NODISPLAY :
                return False
            msg = "{}% | {}/{}"

            short_time = True

            pos = self.player.Position()
            if self.eps not in self.dicoDone :
                self.dicoDone[self.eps] = 0
            self.dicoDone[self.eps] = pos
            # print(self.player.GetTrackPath("file://", ""))
            # Saving new sample with end time as duration
            if self.eps not in Player.track_list :
                self.player.SetTrackInfo(self.eps)
            duration = Player.track_list[self.eps].duration
            percent = txt.Percent(pos, duration)

            p = txt.HRTime(pos)
            duration_display = txt.HRTime(duration)
            # nb_sample = 0
            cur_smp = self.cur_smp
            dico_eps = sorted(self.dico_eps)
            if self.eps not in self.count_vid : # or self.count_vid[self.eps] >= len(self.dico_eps):
                self.count_vid[self.eps] = 0
            cnt_smp = self.count_vid[self.eps]

            nb_sample = len(dico_eps)
            adj_0 = txt.FillValue(nb_sample, 2, 1)
            counter_display = txt.Right(adj_0, "{}/{}".format(cnt_smp + 1, nb_sample))
            # print(str(cnt_smp) + " " + str(dico_eps))
            if len(dico_eps) > 0 :
                if cnt_smp + 1 == len(dico_eps) and pos > dico_eps[cnt_smp][1]:
                    counter_display = txt.Right(adj_0, "{}/{}".format(nb_sample + 1, nb_sample))
                elif cnt_smp == 0 and pos < dico_eps[0][0]:
                    counter_display = txt.Right(adj_0, "{}/{}".format(0, nb_sample))
            elif cnt_smp == -1 :
                counter_display = txt.Right(adj_0, "{}/{}".format(nb_sample, nb_sample))

            if not len(dico_eps) > 0 :
                counter_display = "\b"

            msg = PlayTags.PREFIX_MSG + msg
            msg = msg.format(txt.Right("     ", percent), p, duration_display )
            pos_tag = ""

            if len(cur_smp) > 0 :
                pos_tag += "[{}]  IN {} ".format(",".join(self.catRead), counter_display)
                pos_tag += txt.HRTime(cur_smp[1], short_time) + " (-" + txt.HRTime(cur_smp[1] - pos, short_time) + ")"
            else :
                pos_tag += "[{}] OUT {} ".format(",".join(self.catRead), counter_display)
                if len(dico_eps) > 0 and cnt_smp + 1 >= 0 and cnt_smp + 1 < len(dico_eps) :
                    # pos_tag += txt.HRTime(pos) + "/" + txt.HRTime(dico_eps[cnt_smp + 1][0])
                    if cnt_smp == -1 :
                        pos_tag += txt.HRTime(dico_eps[0][0]) + " (-" + txt.HRTime(dico_eps[0][0] - pos) + ")"
                    else :
                        pos_tag += txt.HRTime(dico_eps[cnt_smp + 1][0]) + " (-" + txt.HRTime(dico_eps[cnt_smp + 1][0] - pos) + ")"
                else :
                    # pos_tag += txt.HRTime(pos) + "/" + duration_display
                    pos_tag += " (-" + txt.HRTime(duration - pos) + ")"

            msg += " | {} ".format(pos_tag)
            if self.catList != self.catRead :
                msg += " | {}".format(",".join(self.catList))
            # print(len(self.dico_eps))
            if self.NEW_SAMPLE >= 0 :
                msg += " | [{}] {}/{} ({})".format(",".join(self.catList), txt.HRTime(self.NEW_SAMPLE), txt.HRTime(pos), txt.HRTime(pos - self.NEW_SAMPLE))
                # if self.overwrite > 0 :
                #     msg += " (Overwrite : {})".format(self.overwrite)

            msg += " (target : {} | {})".format(self.target, self.cur_smp)
            sys.stdout.write('\033[2K\033[1G')
            print(msg, end=" ", flush=True)
        except Exception as ex :
            # print("Print failed !\n{}".format(ex))
            raise Exception("Print failed : " + str(ex))

    def SetRoot(self) :
        if not self.eps.startswith(self.root_file) :
            for k in self.dirs :
                if self.eps.startswith(k) :
                    self.md5 = TagsMng.LIST_MD5_FILES[k]
                    self.tags = TagsMng.LIST_INDEX_FILES[k]
                    self.root_file = k
                    break

    def SetCurrentSample(self, pos, force = False) :
        """ Define current or next sample """
        try :
            cur_smp = []
            if self.eps not in self.count_vid :
                pass
            if len(self.dico_eps) > 0 :
                # print(self.count_vid[self.eps])
                if self.count_vid[self.eps] < 0 :
                    self.count_vid[self.eps] = 0
                # Get current or next sample
                cur_smp = self.dico_eps[self.count_vid[self.eps]]
                b_compute = False
                # print(cur_smp)
                # If target has been reached ...
                if pos >= self.target :
                    if pos >= cur_smp[0] :
                        # position is in sample
                        self.target = cur_smp[1]
                        # position over sample
                        if pos >= cur_smp[1] :
                            # # Get next sample, then define new target
                            # # print("{} {}".format(self.count_vid[self.eps], cur_smp))
                            # cur_idx_sample = self.FindIndexNextJump(self.dico_eps, pos)
                            # cur_smp = self.dico_eps[cur_idx_sample]
                            # self.count_vid[self.eps] = cur_idx_sample
                            # # cur_smp = self.GoToSample(1, pos, False)
                            # self.target = cur_smp[0]
                            b_compute = True
                    else :
                        # position is before begining of sample
                        # self.target = cur_smp[0]
                        b_compute = True
                    # if cur_smp == self.dico_eps[len(self.dico_eps) - 1] :
                    #     # print(cur_smp)
                    # print("HEIN !!! {} {} == {}".format(int(pos), self.target, self.count_vid[self.eps]), end = "", flush = True)
                    # Define current sample
                    # if self.target == cur_smp[0] :
                    #     self.cur_smp = []
                    # else :
                    #     self.cur_smp = cur_smp

                else :
                    # if len(self.cur_smp) > 0 :
                    if self.target >= cur_smp[1] and pos < cur_smp[0]:
                        b_compute = True
                        # else :
                        #     print("HEIN22 !!! {} {} == {}".format(int(pos), self.target, self.count_vid[self.eps]), end = "", flush = True)
                    else :
                        b_compute = True
                        # print(cur_smp)
                    #     self.cur_smp = []
                    # else :
                    #     self.cur_smp = cur_smp

                    # if not cur_smp == self.dico_eps[len(self.dico_eps) - 1] :
                    #     print("HEIN22 !!! {} {} == {}".format(int(pos), self.target, self.count_vid[self.eps]), end = "", flush = True)

                    # # print(cur_smp)
                    # # print(self.target)
                    # # if pos > cur_smp[0] :
                    # #     self.cur_smp = cur_smp
                    # #     self.target = cur_smp[1]
                    # # else :
                        # Get next sample index
                if b_compute :
                    # cur_idx_sample = self.FindIndexNextJump(self.dico_eps, pos)
                    # print("{} - {}".format(cur_idx_sample, self.count_vid[self.eps]))
                    # Then define next sample and his index if sample index is not the same.
                    # if cur_idx_sample != self.count_vid[self.eps] :
                    self.count_vid[self.eps] = self.FindIndexNextJump(self.dico_eps, pos)
                    self.target = cur_smp[0]
                    cur_smp = self.dico_eps[self.count_vid[self.eps]]
                    if pos >= cur_smp[0] :
                        if pos <= cur_smp[1] :
                            self.cur_smp = cur_smp
                            self.target = cur_smp[1]
                        else :
                            # self.target = cur_smp[1]
                            # self.SetCurrentSample(pos)
                            self.cur_smp = []
                    else :
                        self.cur_smp = []

                    if self.count_vid[self.eps] == -1 and pos < self.dico_eps[0][0] :
                        # print(self.cur_smp)
                        self.count_vid[self.eps] = 0
                    # else :
                    #     print(self.count_vid[self.eps])
            else :
                self.count_vid[self.eps] = -1
                self.target = self.player.GetTrackDuration()
                self.cur_smp = []
            # self.overwrite = str(int(pos)) + ' ' + str(self.target) + ' ' + str(cur_smp) + " " + str(self.cur_smp)
            # print(self.target, end = "", flush=True)
        except Exception as e :
            raise Exception("SetCurrentSample : " + str(e))

    def UpdateIndex(self) :
        """ Function to build and clean timestamp dictionnary """
        # Copy original dict to prevent : "Error : dictionary keys changed during iteration"
        dico_orig = self.tags.dicoTimeStamps
        for v in dico_orig :
            if v == "__general__" :
                continue
            f = ""
            for p in self.dirs.keys() :
                f = os.path.join(p, self.tags.dicoTimeStamps[v]["fullpath"], self.tags.dicoTimeStamps[v]["name"])
                if os.path.exists(f) :
                    md5 = self.md5.GetMd5(f, True, True)
                    break
                else :
                    print("File not found : {}".format(f))
                    f = ""

            if f == "" :
                continue

            if md5 != v :
                self.UpdateEntry(v, f, md5)
                # print(f + " " + v + " " + md5)

    def UpdateEntry(self, md5, v, md5_new = "") :
        if md5_new == "" :
            # Computing md5sum
            md5_new = self.md5.GetMd5(v, True, True)
        print(v + " " + md5 + " -> " + " " + md5_new)
        # print("gonna recalculate md5")
        season = self.tags.SearchSaison(v)
        # print(pt.tags.SearchSaison(vid))
        # print(pt.tags.dicoTimeStamps["__general__"]["__sagas__"][season[1]]["list"])

        # Write new dict for this video
        # dico_md5 = pt.tags.dicoTimeStamps[md5]
        self.tags.dicoTimeStamps[md5_new] = self.tags.dicoTimeStamps.pop(md5)
        # Remove older key
        # pt.tags.dicoTimeStamps.pop(md5)
        # Remove older entry
        if md5 in self.tags.dicoTimeStamps["__general__"]["__sagas__"][season[1]]["list"] :
            self.tags.dicoTimeStamps["__general__"]["__sagas__"][season[1]]["list"].remove(md5)
        # And add video in season list
        self.tags.dicoTimeStamps["__general__"]["__sagas__"][season[1]]["list"].append(md5_new)
        # Then save
        self.tags.Save()

    def GetStats(self, f, catList) :
        for d in self.dirs[f] :
            total_tagged = [0, 0]
            for cat in catList :
                total_tagged_cat = 0
                if d == f :
                    total_tagged = self.tags.TotalTime(cat)
                else :
                    total_tagged = self.tags.TotalTime(cat, d)
                # print(total_tagged)
                total_tagged_cat = self.tags.TotalTime(cat)[0]
                percent_tagged = txt.Percent(total_tagged[0], total_tagged_cat)
                print("[{}] {} files found in {} ({}/{} - {}%)".format(cat, total_tagged[1], d, txt.HRTime(total_tagged[0]), txt.HRTime(total_tagged_cat), percent_tagged))

    def __init__(self, dirs, tag_new, tag_read, quality = None) :
        """
        Init dictionary of categories
        dirs : folders to scan
        tag_new : categories to edit
        tag_read : categories to play
        """
        try :
            super().__init__(dirs)

            self.proc = None
            self.overwrite = 0
            # Boolean to determine if player is on frame search
            self.searching = False
            self.counter_save = [0, 0]
            self.confirm_search = False
            self.abort_search = False

            self.NODISPLAY = False
            # # We're gonna get categories first
            # # TODO : only one categories file !!!
            if tag_read == "" :
                tag_read = tag_new

            self.catEdit = tag_new.split(":")
            if len(self.catEdit) > 1 :
                idx = 0
                for cat in self.catEdit :
                    # print(cat)
                    self.catEdit[idx] = self.GetCats(cat.split(","), TagsMng.LIST_INDEX_FILES[list(TagsMng.LIST_INDEX_FILES.keys())[0]])
                    idx += 1
                # tag_read = self.catEdit
            else :
                # print(self.catEdit)
                self.catEdit = self.GetCats(self.catEdit[0].split(","), TagsMng.LIST_INDEX_FILES[list(TagsMng.LIST_INDEX_FILES.keys())[0]])

            self.catList = self.catEdit #self.GetCats(tag_new.split(","), TagsMng.LIST_INDEX_FILES[list(TagsMng.LIST_INDEX_FILES.keys())[0]])
            if tag_new != tag_read :
                # print("{} != {}".format(tag_new, tag_read))
                self.catRead = self.GetCats(tag_read.split(","), TagsMng.LIST_INDEX_FILES[list(TagsMng.LIST_INDEX_FILES.keys())[0]])
            else :
                self.catRead = self.catList

            # Check if ffmpeg is installed to retrieve fps, quality or duration
            if FfmpegTools.check_call("command -v ffprobe >> /dev/null") != 0 :
                print("ffmpeg has not been found on this system !")

            self.tags_add = {}
            # print(self.catEdit)
            # print(tag_read)

            # print(self.catList)

            # Then we will scan json file content for each category
            for f in self.dirs :
                dicoTaggedList = self.GetList(f, self.catList, quality = quality)
                if self.catList != self.catRead :
                    dicoTaggedRead = self.GetList(f, self.catRead, quality = quality)
                    # self.tags_add = dicoTaggedList
                else :
                    dicoTaggedRead = dicoTaggedList
                    # self.tags_add = dicoTaggedList
                # Fill edit timestamps list
                for k in dicoTaggedList :
                    self.tags_add[k] = dicoTaggedList[k]

                # # Rewrite eps missing infos
                # # print(dicoTaggedRead)
                # for v in dicoTaggedRead :
                #     # print(v)
                #
                #
                #     if not self.HasInfoFile(v) :
                #         infos = FfmpegTools.GetInfos(v.replace("!", "\!"))
                #         print(infos)
                #         if infos[0] != 0 and infos[1] > 0 :
                #             if self.SetInfoFile(v, *infos) :
                #                 print("save " + v + " " + str(infos))
                #                 self.tags.Save()

                # Display stats
                self.GetStats(f, self.catRead)
                if tag_new != tag_read :
                    self.GetStats(f, self.catList)

                # Printing
                if len(self.catList) > 1 :
                    print("[{}] {}".format(",".join(self.catList), txt.HRTime(self.TotalTimeList(dicoTaggedList))))

                if self.catList != self.catRead :
                    if len(self.catRead) > 1 :
                        print("[{}] {}".format(",".join(self.catRead), self.TotalTimeList(dicoTaggedRead), False))
                # print("LNK :: " + str(self.tags.GetLinkedCat(self.catList)))
        except Exception as e:
            raise Exception("PlayTags __init__ : " + str(e) )

