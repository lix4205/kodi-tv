#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import re

class FilesMng() :
    EPS_LIST = {}
    
    def __init__ (self, source_dir, encours_name) :
        # Using real path for MAIN_SOURCE_DIR 20240227
        self.MAIN_SOURCE_DIR = os.path.realpath(source_dir)
        self.DIR_ENCOURS_NAME = encours_name
        #self.MAIN_SOURCE_DIR = args["source_dir"]
        self.DIR_ENCOURS = source_dir + "/" + encours_name
        self.NEW = False
        self.serie = {}
        self.episodes = []
        self.dicoSeries = {}
        self.dicoSubSerie = {}

# BEGIN link management
    """
    Creer les liens dans le repertoire ENCOURS,
    Ou renvoi la liste des derniers épisodes joués
    l_l_idx : la liste des fichiers du répertoire de la série
    l_idx : l'index du fichier en cours de lecture
    l_relPath : Le chemin relatif (../)
    l_name : Le nom de la série
    """
    def CreateNextLinks(self, l_l_idx, l_idx, l_relPath, l_name, l_i_keepNumber = 0, p_b_save = True) :
        l_idx_eps = l_idx
        l_a_result = []
        l_b_done = False

        # On se déplace vers le répertoire DIR_ENCOURS
        os.chdir(self.DIR_ENCOURS)
        if l_name != "" :
            # On créer le répertoire
            if not os.path.exists(l_name) :
                self.NEW = True
                os.mkdir(l_name)
            else :
                self.NEW = False
            # Et on se déplace dedans
            os.chdir(l_name)

        # On parcourt les fichiers trouvés
        for l_s_val in l_l_idx :
            # On génère le chemins et l'index du lien
            paths_link = self.CreatePathLink(l_idx_eps, l_s_val, l_relPath) + [ l_name]
            # print(paths_link)
            # On créer le lien
            if not l_b_done and p_b_save :
                self.CreateLink(*paths_link)
                if l_idx_eps >= l_idx + l_i_keepNumber :
                    l_b_done = True
            #os.path.getsize(paths_link[0])
            l_idx_eps += 1
            
            l_a_result.append([l_s_val, os.path.getsize(l_s_val)])
        # print("Link done :: {}".format(l_l_idx))
        # On fait un nettoyage
        if p_b_save :
            self.CleanLink(l_l_idx, l_i_keepNumber, l_name)
        # TODO : Le retour est tronqué ?!
        return l_a_result[0:10]

    """
    Retourne un tableau contenant le chemin relatif du fichier et le nom indexé du lien a créer
    l_idx : l'index du fichier dans la totalité des fichiers du répertoire parent
    real_path : le chemin complet du fichier a lier
    l_relPath : le chemin relatif a insérer dans le chemin calculé (../)
    """
    def CreatePathLink(self, l_idx, real_path, l_relPath) :
        # print(real_path + " " + self.MAIN_SOURCE_DIR )
        # Remplace le chemin de base dans le chemin complet du fichier
        l_path = real_path.replace(self.MAIN_SOURCE_DIR + "/", "")
        # On ajoute le chemin relatif (../)
        l_s_relpath = l_relPath + l_path
        # On donne le nom du lien indexé
        l_nameLink = Right(FILE_PREFIX + str(l_idx + 1), 4) + "_" + os.path.basename(l_path)
        # Et on retourne le tout
        return [l_s_relpath, l_nameLink]
    
    """
    Création du lien
    source : source du lien
    target : cible du lien
    new_dir : Le répertoire a créer 
    force_creation : Si on veut forcer la création... (PAS UTILISÉ 20200220)
    """
    def CreateLink(self, source, target, new_dir = "", force_creation = False) :
        l_source = str(source)
        l_target = str(target)
        l_targetTmp = l_target + ".tmp"        
        
        # print(l_source)
        # Si la source est bien trouvé
        if os.path.exists(source) :
            # Si la cible existe déjà,
            if os.path.exists(target) :
                # Et qu'on force la création
                if force_creation :
                    # On link
                    os.symlink(l_source, l_targetTmp)
                    os.rename(l_targetTmp, l_target)
                print("ERA :   -> " + os.path.basename(l_source) + " " + target)
            else :
                # On créer le lien
                print("ADD :   -> " + os.path.dirname(l_source) + " " + target)            
                os.symlink(l_source, str(target + ".tmp"))
                os.rename(l_targetTmp, l_target) 
        else :
            # Pas de source, on affiche l'erreur dans la log
            print("ERR : " + l_source + " doesn't exists !")
    
    """
    Nettoie les liens dans le répertoire DIR_ENCOURS
    """
    def CleanLink(self, l_l_idx, l_idx, new_dir) :
        # On se déplace vers le répertoire DIR_ENCOURS
        os.chdir(self.DIR_ENCOURS)
        
        # On remplit la liste des fichiers dans le réperoire
        list_file_present = self.SearchFileInLinks(new_dir)

        # print("Begin Clean 1")
        # print(list_file_present)
        # print("Begin Clean 2")
        # print(l_l_idx)
        # print("Begin Clean 3")
        # print(l_idx)

        for f in os.listdir(os.path.join(self.DIR_ENCOURS, new_dir )) :
            # print(f, end="")
            if not f.endswith(os.path.basename(l_l_idx[l_idx])) :
                # print(" KEEP")
            # else :
                # print(" DEL")
                if os.path.islink(os.path.join(self.DIR_ENCOURS, new_dir, f)) :
                    os.remove(os.path.join(self.DIR_ENCOURS, new_dir, f))

#         # On cherche si on a des liens qui n'ont rien a foutre la...
#         for i in range(len(l_l_idx)) :
#             # On reconstruit le nom du fichier
#             l_nameLink = self.DIR_ENCOURS + "/" + new_dir + "/" + Right(FILE_PREFIX + str(l_idx+i), 4) + "_" + os.path.basename(l_l_idx[i])
#             #l_nameLink = l_nameLink.encode("utf8")
#             print(l_nameLink + " " + str(l_idx))
#             # Si le fichier est trouvé,
#             if os.path.basename(l_nameLink) in list_file_present :
#                 # On le supprime de la liste
#                 list_file_present.remove(os.path.basename(l_nameLink))
#
#         #print("PRESENT : " + str(list_file_present))
#         #print("COPIED : " + str(l_l_idx))
#         # Si on a encore des fichiers
#         if len(list_file_present) > 0 :
#             # On les supprime.
#             for file_present in sorted(list_file_present) :
#                 os.remove(self.DIR_ENCOURS + "/" + new_dir + "/" + file_present)
#                 print(str(file_present) + " removed" )

        print("End Clean")

    def CleanDir(self, l_l_idx, l_idx, new_dir) :
        # Dernier épisode de la série, on nettoie
        if l_idx == len(l_l_idx) - 1 :
            print("Cleaning directory")
            l_nameLink = self.DIR_ENCOURS + "/" + new_dir + "/" + Right(FILE_PREFIX + str(l_idx), 4) + "_" + os.path.basename(l_l_idx[l_idx])
            #os.remove(l_nameLink)
            if new_dir != "" :
                # Delete folder and sub. ...
                #shutil.rmtree('/folder_name')
                # Delete empty directory
                 os.rmdir(self.DIR_ENCOURS + "/" + new_dir)
                 #os.remove(DIR_ENCOURS + "/" + DIR_ENCOURS_NAME + "_" + new_dir + FILENAME_AUTOREAD_EXT)
        
# END link management


# BEGIN File listing/searching...
    def GetList(self, l_saga) :
        """
        Return episode list of a saga
        """
        l = []
        if not l_saga in FilesMng.EPS_LIST :
            FilesMng.EPS_LIST[l_saga] = []
            if l_saga in self.dicoSeries :
                for l_season in self.dicoSeries[l_saga]["seasons"] :
                    # Creating season path
                    path_season = os.path.join(self.MAIN_SOURCE_DIR, self.dicoSeries[l_saga]["path"], l_season)
                    for eps in self.dicoSeries[l_saga]["seasons"][l_season]["list"] :
                        # We will search for full episode path
                        new_path = os.path.realpath(os.path.join(path_season, eps))
                        # print("N " + new_path)
                        if os.path.exists(new_path) :
                            l.append(new_path)
                        elif l_saga in self.dicoSubSerie :
                            # Maybe episode not in regular folder,
                            # Search in subdirectory list
                            for k_serie in self.dicoSubSerie[l_saga] :
                                new_path = os.path.join(self.MAIN_SOURCE_DIR, k_serie, eps)
                                if os.path.exists(new_path) :
                                    l.append(new_path)
                        # l.append(os.path.join(self.MAIN_SOURCE_DIR, self.dicoSeries[l_saga]["seasons"][l_season]["path"], eps ))
                    # print(test.dicoSeries[l_saga]["seasons"][l_season]["list"])
                    # l = l + test.dicoSeries[l_saga]["seasons"][l_season]["list"]
            FilesMng.EPS_LIST[l_saga] = l
        if l_saga in FilesMng.EPS_LIST :
            l = FilesMng.EPS_LIST[l_saga]

        return l

    def GetIndex(self, path_file, p_saga = "") :
        if p_saga != "" :
            l_saga = p_saga
        else :
            l_infoFile = FilesMng.GetTypeVideo(path_file, self.MAIN_SOURCE_DIR)
            l_saga = l_infoFile["name"]
        l = self.GetList(l_saga)
        p = path_file
        # print(p + " " + l_saga  + " " + str(len(l)))
        if p in l :
            return l.index(p)
        # elif l_saga in self.dicoSubSerie :
        #     for k_serie in self.dicoSubSerie[l_saga] :
        #         new_path = os.path.join(self.MAIN_SOURCE_DIR, k_serie, os.path.basename(path_file))
        #         if path_file == new_path :
        #         # for k_sub in self.dicoSubSerie[l_saga][k_serie] :
        #             print(os.path.join(self.MAIN_SOURCE_DIR, k_serie))


        return -1

    def GetSeason(self, path_file) :
        l_infoFile = FilesMng.GetTypeVideo(path_file, self.MAIN_SOURCE_DIR)
        l_saga = l_infoFile["name"]
        l_season = -1
        if l_saga in self.dicoSeries :
            for k_season in self.dicoSeries[l_saga]["seasons"] :
                if path_file in self.dicoSeries[l_saga]["seasons"][k_season] :
                    l_season = k_season
                elif k_season in self.dicoSubSerie :
                    for k_serie in self.dicoSubSerie[l_saga] :
                        for k_sub in self.dicoSubSerie[l_saga][k_serie] :
                            print(os.path.join(self.MAIN_SOURCE_DIR, k_sub))

        # print(p + " " + l_saga  + " " + str(l))
        # if p in l :
        return [ l_saga, l_season ]
        # return -1

    def GetAll(self, path_file, root_only = False) :
        """
        Search whole episodes of a serie/saga
        """
        l_path = path_file
        if l_path.startswith(self.MAIN_SOURCE_DIR) :
            l_path = l_path.replace(os.path.join(self.MAIN_SOURCE_DIR, ""), "")
        else :
            print("{} not found in : {}".format(self.MAIN_SOURCE_DIR, l_path))

        l_a_path = l_path.split("/")
        # print(l_a_path)
        l_type = l_a_path[0]
        l_a_path = l_a_path[1:]
        # print(l_a_path)
        if os.path.isfile(path_file) :
            if len(l_a_path) >= 2 :
                l_saga = l_a_path[0]
                if l_saga not in self.dicoSeries :
                    self.dicoSeries[l_saga] = {}

                # if len(l_a_path) > 3 :
                #     self.dicoSeries[l_saga]["path"] = os.path.join(*l_a_path[0:len(l_a_path) - 3])
                # else :
                # print("On a serie...")
                if len(l_a_path) >= 3 :
                    path_saga = os.path.join(l_type, *l_a_path[0:1])
                    self.dicoSeries[l_saga]["path"] = path_saga
                    if "seasons" not in self.dicoSeries[l_saga] :
                        self.dicoSeries[l_saga]["seasons"] = {}

                else :
                    path_saga = os.path.join(l_type, l_saga)
                    self.dicoSeries[l_saga]["path"] = path_saga
                    if "seasons" not in self.dicoSeries[l_saga] :
                        self.dicoSeries[l_saga]["seasons"] = {}
                    if "." not in self.dicoSeries[l_saga]["seasons"] :
                        self.dicoSeries[l_saga]["seasons"]["."] = {}

                    self.dicoSeries[l_saga]["seasons"]["."]["path"] = "."
                    if "list" not in self.dicoSeries[l_saga]["seasons"]["."] :
                        self.dicoSeries[l_saga]["seasons"]["."]["list"] = []

                self.GetEpisodes(l_saga, path_saga, [ l_type, *l_a_path[0:]])
            else :
                l_saga = l_type
                path_saga = l_type
                if l_saga not in self.dicoSeries :
                    self.dicoSeries[l_saga] = {}
                self.dicoSeries[l_saga]["path"] = path_saga
                if "seasons" not in self.dicoSeries[l_saga] :
                    self.dicoSeries[l_saga]["seasons"] = {}
                if "." not in self.dicoSeries[l_saga]["seasons"] :
                    self.dicoSeries[l_saga]["seasons"]["."] = {}

                self.dicoSeries[l_saga]["seasons"]["."]["path"] = "."
                if "list" not in self.dicoSeries[l_saga]["seasons"]["."] :
                    self.dicoSeries[l_saga]["seasons"]["."]["list"] = []
                # if
                # self.dicoSeries["Films"]["seasons"]["."]["list"] = []
                # print(path_file)
                # print(l_saga)
                # print(path_saga)
                # print([ l_saga, *l_a_path[0:]])
                # print(os.path.basename(path_file))
                self.GetEpisodes(l_saga, path_saga, [ l_saga, *l_a_path[0:]], os.path.basename(path_file))
        else :
            print("On a dir")
            l_dir_name = path_file

    def GetEpisodes(self, l_saga, path_saga, l_a_path, season = ".") :
        # print(l_a_path)

        if l_saga == path_saga :
            b_onFilm = True
        else :
            b_onFilm = False

        full_path_saga = os.path.join(self.MAIN_SOURCE_DIR, path_saga)
        # print("DD " + full_path_saga)
        l_listFile = sorted(os.listdir(full_path_saga))
        count_eps = 0
        l_type = l_a_path[0]

        for l_file in l_listFile :
            if self.IgnoreFile(l_file) :
                continue
            full_path_season = os.path.join(full_path_saga, l_file)
            # print(full_path_season)
            if os.path.isdir(full_path_season) :
                if not b_onFilm :
                    path_season = os.path.join(path_saga, l_file)
                    l_a_path = path_season.split("/")
                    l_a_path[0] = l_type
                    l_a_path.append("eps")

                    path_season = self.GetSubSeason(l_a_path[0], l_saga, l_a_path[1:] )

                    if l_file not in self.dicoSeries[l_saga]["seasons"] :
                        self.dicoSeries[l_saga]["seasons"][l_file] = {"path" : path_season, "list" : []}
                    # print("SS :: " + path_season + " " + str(l_a_path))
                    # if path_season == os.path.join(*l_a_path) :
                    if l_saga in self.dicoSubSerie and path_season in self.dicoSubSerie[l_saga] :
                        # print("ICI 1 :: " + season + " dir : " + l_file)
                        l_file = season

                    print("Scanning :: " + full_path_season)
                    self.GetEpisodes(l_saga, path_season, l_a_path, l_file)
                else :
                    print("On Films :: " + full_path_season)

                # if len(self.dicoSeries[l_saga]["seasons"][l_file]) == 0 :
                #     print("to delete :: " + l_file)
                #     self.dicoSeries[l_saga]["seasons"][l_file].pop()

                # l_found = self.searchPattern(l_file, ["(\d\d\d\d)", "(\d\d\d)","(\d\d)","(\d)" ])
                # #if l_found and int(l_found[0]) == l_saison :
                # #print(str(l_found) + " " + l_file)
                #
                # print(l_found)
                # if l_found :
                #     # We found a season
                #
                #     print("F" + str(l_found))
                #
                #     # self.saison = self.GetNewIndex(float(l_found[0]), self.serie.keys())
                #     # #print("NEW S : " + str(self.saison) + " | " + l_file)
                #     # self.serie[self.saison] = []
                # else :
                #     print(l_file + " search failed !" )
            # elif os.path.islink(full_path_season) :
            #     print("{} is a link !".format(full_path_season))
            elif os.path.isfile(full_path_season) :
                if b_onFilm :
                    season = "."

                if season not in self.dicoSeries[l_saga]["seasons"] :
                    self.dicoSeries[l_saga]["seasons"][season] = {"path" : season, "list" : []}
                # print(season)
                # if l_saga in self.dicoSubSerie and season in self.dicoSubSerie[l_saga] :
                #     print("ICI 2 :: " + season + " dir : " + l_file)
                # print("{} is a file !".format(full_path_season))
                if l_file not in self.dicoSeries[l_saga]["seasons"][season]["list"] :
                    self.dicoSeries[l_saga]["seasons"][season]["list"].append(l_file)
                    count_eps += 1
                # else :
            # else :
            #     print("oush :: " + full_path_season)
        return count_eps

    def GetSubSeason(self, l_type, l_saga, l_a_path) :
        season = l_a_path[1]
        if len(l_a_path) >= 4 :

            path_season = os.path.join(l_type, l_saga, *l_a_path[1:len(l_a_path) - 1])
            if l_saga not in self.dicoSubSerie :
                self.dicoSubSerie[l_saga] = {}

            # tmp_season = os.path.join(l_type, l_saga, path_season)
            if path_season not in self.dicoSubSerie[l_saga] :
                self.dicoSubSerie[l_saga][path_season] = season

                # print(" SUB :: " + season + " " + path_season + " " + str(l_a_path))
        else :
            path_season = os.path.join(l_type, l_saga, *l_a_path[1:len(l_a_path) - 1])
            # path_season = l_a_path[1]
        # print("TYPE : " + l_type)
        # print("SAGA: " + l_saga)
        # print("A : " + str(l_a_path))
        # print("PATH : " + path_season)
        return path_season

    def searchPattern(self, p_s_chaine, p_s_pattern) :
        """
        Searching and return p_s_pattern(s) in p_s_chaine
        """
        # print(p_s_chaine + " " + str(p_s_pattern))
        for l_s_pattern in p_s_pattern :
            l_found = re.findall(l_s_pattern, p_s_chaine)
            if l_found :
                return l_found 
        return []


# BEGIN NEW FUNCTIONS
    """
    Get index of a file in directories list
    path_file : chemin d'un fichier,
    root_only = (False) Ne cherche pas dans les sous dossier,
    p_saison = Saison en cours
    """
    def SetSerie(self, path_file, root_only = False, p_saison = -1) :
        l_idx_file = 0
        l_path = path_file
        l_listNextFiles = {}
        # Deleting prefix file to get SERIE/SAGA dir
        if l_path.startswith(self.MAIN_SOURCE_DIR) :
            l_path = l_path.replace(self.MAIN_SOURCE_DIR + "/", "")
        l_a_path = l_path.split("/")
        if os.path.isfile(path_file) :
            if len(l_a_path) > 2 :
                if len(l_a_path) > 3 :
                    if not root_only :
                        l_dir_name = os.path.dirname(os.path.dirname(path_file))
                    else :
                        l_dir_name = os.path.dirname(path_file)
                else :
                    l_dir_name = os.path.dirname(path_file)
            else :
                l_dir_name = os.path.dirname(path_file)
        else :
            l_dir_name = path_file
        #l_listFile = sorted(os.walk(l_dir_name))
        # print(l_dir_name)
        l_listFile = sorted(os.listdir(str(l_dir_name)))
        #print(l_dir_name)
        #l_saison = 1

        #print("LIST : " + str(l_listFile))
        for l_file in l_listFile :
            self.saison = p_saison
            if self.IgnoreFile(l_file) :
                continue

            #print(l_file)
            if not root_only and os.path.isdir(l_dir_name + "/" + l_file) :
                l_found = self.searchPattern(l_file, ["(\d\d\d\d)", "(\d\d\d)","(\d\d)","(\d)" ])
                #if l_found and int(l_found[0]) == l_saison :
                #print(str(l_found) + " " + l_file)

                if l_found :
                    self.saison = self.GetNewIndex(float(l_found[0]), self.serie.keys())
                    #print("NEW S : " + str(self.saison) + " | " + l_file)
                    self.serie[self.saison] = []
                else :
                    self.saison = l_file
                    self.serie[l_file] = []
                self.SetSerie(l_dir_name + "/" + l_file, root_only, self.saison)

                #print(self.findIndexSaison(self.serie))

            elif os.path.isfile(l_dir_name + "/" + l_file)  :
                if os.path.exists(os.path.realpath(l_dir_name + "/" + l_file)) :
                    if not self.saison in self.serie :
                        self.serie[self.saison] = []
                    #print(str(l_file) + " " + str(self.saison))
                    self.serie[self.saison].append(os.path.join(l_dir_name, l_file))


    def FindPattern(self, p_chaine, patterns_search) :
        idx_pattern = -1
        for pattern in patterns_search :
            l_found = re.findall(pattern, p_chaine)
            if l_found :
                idx_pattern +=1
                break
            idx_pattern +=1
        if not len(l_found) > 0 :
            idx_pattern = -1
        return [ idx_pattern, l_found ]

    def GetSerie(self, try_order = True) :
        """
        Recherche le numéro de l'épisode et de la saisons de la série avec le nom de fichier (S03E01, 302, 03x03,...)
        """
        l_a_fileDir = {}
        l_a_epsDir = {}
        l_a_returnPattern = []
        #patterns_search = [ "(\d\d\d)", "(\d\d)","(\d)" ]
        #patterns_search = [ "(\D\d\d[A-Za-z]\d\d\D)", "(\D\d[A-Za-z]\d\d\D)", "(\D\d\d\d\D)", "(\D\d\d\D)", "(\D\d\D)", "(^\d\d\D)", "(^\d\D)", "(^\d\d\d\D)"  ]
        patterns_search = [ "(^\d\d[A-Za-z]\d\d\D)", "(\D\d\d[A-Za-z]\d\d\D)","(^\d[A-Za-z]\d\d\D)", "(\D\d[A-Za-z]\d\d\D)", "(^\d\D)", "(^\d\d\D)", "(^\d\d\d\D)", "(^\d\d\d\d\D)", "(\D\d\D)", "(\D\d\d\D)", "(\D\d\d\d\D)", "(^\d\d\d\d\d\D)" ]
        l_count = 0
        l_i_saison = -1
        #print(self.serie)
        #return True

        #if -1 in self.serie.keys() :
        if try_order :
            l_saison_prec = -1
            for l_saison in self.serie.keys() :
                #print(l_saison)

                for l_episode in self.serie[l_saison] :
                    #print(str('SAISON :') + ' ' + str(l_saison) + " " + str(l_episode))
                    l_count += 1
                    #l_found = []
                    #for pattern in patterns_search :
                        #l_found = re.findall(pattern, os.path.basename(l_episode))
                        #if l_found :
                            #break
                        #idx_pattern +=1
                    l_a_returnPattern = self.FindPattern(os.path.basename(l_episode), patterns_search)
                    #print(l_episode + " " + str(l_a_returnPattern))
                    if l_a_returnPattern[0] == 0 or l_a_returnPattern[0] == 1 :
                        # Pattern classique S01E02
                        #print(l_a_returnPattern[1][0][1:len(l_a_returnPattern[1][0])-1])
                        # On recupere la chaine a analyser
                        if l_a_returnPattern[0] == 0 :
                            l_a_returnPattern[1] = l_a_returnPattern[1][0][0:len(l_a_returnPattern[1][0])-1]
                        else :
                            l_a_returnPattern[1] = l_a_returnPattern[1][0][1:len(l_a_returnPattern[1][0])-1]
                        l_i_saison = l_saison # float(l_a_returnPattern[1][0:2])
                        l_i_eps = float(l_a_returnPattern[1][3:])

                        #l_a_epsDir[l_i_eps] = l_episode
                        if l_i_saison not in l_a_fileDir :
                            l_a_fileDir[l_i_saison] = {}
                        #if l_i_eps not in l_a_fileDir[l_i_saison] :
                            #l_a_fileDir[l_i_saison] = {}
                        #l_a_fileDir[l_i_saison] = []
                        #print("NEW : " + str(self.GetNewIndex(l_i_eps, l_a_fileDir[l_i_saison].keys())) + " " +l_episode)
                        l_a_fileDir[l_i_saison][self.GetNewIndex(l_i_eps, l_a_fileDir[l_i_saison].keys())] = l_episode #l_a_epsDir
                    elif l_a_returnPattern[0] == 2 or l_a_returnPattern[0] == 3 :
                        # Autre pattern classique 1x02
                        if l_a_returnPattern[0] == 2 :
                            l_a_returnPattern[1] = l_a_returnPattern[1][0][0:len(l_a_returnPattern[1][0])-1]
                        else :
                            l_a_returnPattern[1] = l_a_returnPattern[1][0][1:len(l_a_returnPattern[1][0])-1]

                        l_i_saison = float(l_a_returnPattern[1][0:1])
                        l_i_eps = float(l_a_returnPattern[1][2:])

                        if l_i_saison not in l_a_fileDir :
                            l_a_fileDir[l_i_saison] = {}
                        l_a_fileDir[l_i_saison][l_i_eps] = l_episode
                    elif l_a_returnPattern[0] == 4 or l_a_returnPattern[0] == 8 :
                        # Juste l'épisode 2
                        if l_a_returnPattern[0] == 4 :
                            l_a_returnPattern[1][0] = l_a_returnPattern[1][0][0:len(l_a_returnPattern[1][0])-1]
                        else :
                            l_a_returnPattern[1][0] = l_a_returnPattern[1][0][1:len(l_a_returnPattern[1][0])-1]

                        if isinstance(l_saison, str) :
                            l_i_saison = l_saison_prec
                        else :
                            l_i_saison = self.GetNewIndex(float(l_saison), l_a_fileDir.keys())

                        l_i_eps = float(l_a_returnPattern[1][0])
                        if l_i_saison not in l_a_fileDir :
                            l_a_fileDir[l_i_saison] = {}

                        if l_i_eps in l_a_fileDir[l_i_saison] :
                            l_i_eps_tmp = l_i_eps + 0.1
                            while l_i_eps_tmp in l_a_fileDir[l_i_saison] :
                                l_i_eps_tmp += + 0.1

                            #print(l_i_eps_tmp)
                            l_a_fileDir[l_i_saison][l_i_eps_tmp] = l_episode
                        else :
                            l_a_fileDir[l_i_saison][l_i_eps] = l_episode
                    elif l_a_returnPattern[0] == 5 or l_a_returnPattern[0] == 9 :
                        # Autre cas, 01[...]03
                        if len(l_a_returnPattern[1]) > 1 :
                            l_a_returnPattern[1][0] = l_a_returnPattern[1][0][1:len(l_a_returnPattern[1][0])-1]
                            l_i_saison = float(l_a_returnPattern[1][0][0:2])
                            l_a_returnPattern[1][1] = l_a_returnPattern[1][1][1:len(l_a_returnPattern[1][1])-1]
                            l_i_eps = float(l_a_returnPattern[1][1])
                            #print(l_i_eps)
                            if l_i_saison not in l_a_fileDir :
                                l_a_fileDir[l_i_saison] = {}
                            l_a_fileDir[l_i_saison][l_i_eps] = l_episode
                        else :
                            if l_a_returnPattern[0] == 5 :
                                # Juste l'épisode 02
                                l_a_returnPattern[1][0] = l_a_returnPattern[1][0][0:len(l_a_returnPattern[1][0])-1]
                            else :
                                l_a_returnPattern[1][0] = l_a_returnPattern[1][0][1:len(l_a_returnPattern[1][0])-1]
                            #print(l_a_returnPattern[0])

                            if isinstance(l_saison, str) :
                                l_i_saison = l_saison_prec
                            else :
                                l_i_saison = self.GetNewIndex(float(l_saison), l_a_fileDir.keys())
                            #l_a_returnPattern[1][1] = l_a_returnPattern[1][1][1:len(l_a_returnPattern[1][1])-1]
                            l_i_eps = float(l_a_returnPattern[1][0])
                            #print(l_i_eps)
                            if l_i_saison not in l_a_fileDir :
                                l_a_fileDir[l_i_saison] = {}
                            l_a_fileDir[l_i_saison][l_i_eps] = l_episode
                    elif l_a_returnPattern[0] == 6  or l_a_returnPattern[0] == 10 :
                        # En ligne : 102
                        if l_a_returnPattern[0] == 6 :
                            l_a_returnPattern[1] = l_a_returnPattern[1][0][0:len(l_a_returnPattern[1][0])-1]
                        else :
                            l_a_returnPattern[1] = l_a_returnPattern[1][0][1:len(l_a_returnPattern[1][0])-1]
                        l_i_saison = float(l_a_returnPattern[1][0:1])
                        l_i_eps = float(l_a_returnPattern[1][1:])

                        if l_i_saison not in l_a_fileDir :
                            l_a_fileDir[l_i_saison] = {}
                        l_a_fileDir[l_i_saison][self.GetNewIndex(l_i_eps, l_a_fileDir[l_i_saison].keys())] = l_episode

                    elif l_a_returnPattern[0] == 7  or l_a_returnPattern[0] == 11 :
                        # En ligne : 102
                        if l_a_returnPattern[0] == 7 :
                            l_a_returnPattern[1] = l_a_returnPattern[1][0][0:len(l_a_returnPattern[1][0])-1]
                        else :
                            l_a_returnPattern[1] = l_a_returnPattern[1][0][1:len(l_a_returnPattern[1][0])-1]
                        l_i_saison = float(l_a_returnPattern[1][0:2])
                        l_i_eps = float(l_a_returnPattern[1][2:])

                        if l_i_saison not in l_a_fileDir :
                            l_a_fileDir[l_i_saison] = {}
                        l_a_fileDir[l_i_saison][self.GetNewIndex(l_i_eps, l_a_fileDir[l_i_saison].keys())] = l_episode

                    else :
                        print("Pattern not found : " + str(l_saison) + " " + str(l_episode))
                        l_i_eps = float(-1)

                        if not isinstance(l_saison, str) and l_i_saison > 0 and l_i_saison not in l_a_fileDir :
                            l_a_fileDir[l_i_saison] = {}
                        else :
                            if isinstance(l_saison, str) :
                                #l_i_saison = l_saison

                                l_i_saison = l_saison_prec
                            else :
                                l_i_saison = self.GetNewIndex(float(l_saison), l_a_fileDir.keys())
                            if l_i_saison not in l_a_fileDir :
                                l_a_fileDir[l_i_saison] = {}
                            #print(self.GetNewIndex(l_i_eps, l_a_fileDir[l_i_saison].keys()))
                        l_a_fileDir[l_i_saison][l_i_eps] = l_episode
                l_saison_prec = l_i_saison
        else :
            list_episode = []
            for l_saison in self.serie.keys() :
                #print(str(l_saison) + " " + str(len(self.serie[l_saison])))
                for l_episode in self.serie[l_saison] :
                    #print(str(l_saison) + "E" + str(l_episode) + " " + str(l_a_fileDir[l_saison][l_episode]))
                    list_episode.append(l_episode)

            return list_episode

        # print("COUNT : " + str(l_count))
        # print("FILES : " + str(l_a_fileDir.keys()))
        #self.serie = {}
        #l_a_fileDir
        list_episode = []
        for l_saison in sorted(l_a_fileDir.keys()) :
            #print(str(l_saison) + " " + str(l_a_fileDir[l_saison].keys()))
            for l_episode in sorted(l_a_fileDir[l_saison].keys()) :
                #print(str(l_saison) + "E" + str(l_episode) + " " + str(l_a_fileDir[l_saison][l_episode]))
                list_episode.append(l_a_fileDir[l_saison][l_episode])
                #self.serie[l_saison] = l_a_fileDir[l_saison][l_episode]
                #print(l_a_fileDir[l_saison][l_episode])
        #list_episode = []
        #for saison in sorted(self.serie.keys()) :
            #for episode in self.serie[saison] :
                ##print(episode)
                #list_episode.append(episode)

        return list_episode

    def GetNewIndex(self, p_idx, p_keys, p_order = 1) :
        l_index = p_idx
        #if l_i_eps in l_a_fileDir[l_i_saison] :
        #l_i_eps_tmp = p_idx + 0.1
        while l_index in p_keys :
            l_index += 0.1 * p_order
        return l_index

# END FUNCTIONS

    def GetRealPath(self, original_path) :
        """
        Retourne le chemin du fichier sans les parametres du serveur
        """
        l_path = original_path
        
        for l_invalid_path in PATH2REMOVE :
            if l_path.startswith(l_invalid_path) :
                l_path = l_path.replace(l_invalid_path ,"")
                break
        return l_path

    def SearchFileInLinks(self, l_dirname = "") :
        """
        Return file list in links directory
        """
        files_in_dir = list()
        if l_dirname != "" :
            for l_file in os.listdir(self.DIR_ENCOURS + "/" + l_dirname):
                files_in_dir.append(os.path.basename(l_file))
            
        return files_in_dir
    
    def GetTypeVideo(original_path, path_dir, jump_dir = 0) :
        """
        Retourne un dico avec les infos de la video
        """
        l_vid = { "path" : original_path, "type" : "", "name" : "" } 
        l_path = original_path
        l_a_path = []
        l_type = ""
        l_name = ""
        
        if l_path.startswith(path_dir) :
            l_path = l_path.replace(path_dir + "/", "")
            l_a_path = l_path.split("/")
            l_type = l_a_path[jump_dir]
            l_name = l_a_path[jump_dir+1]
            if len(l_a_path) > 2 :
                l_type = "SERIE"
            else :
                l_type = "FILM"
                l_name = l_a_path[0]
            l_vid["type"] = l_type
            l_vid["name"] = l_name
        return l_vid

    def IgnoreFile(self, l_s_path) :
        """
        Retourne vrai si le fichier n'est pas dans les valeurs de IGNORE_FILE
        qui contient les pattern de nom de fichier a ignorer
        Les fichiers cachés sont aussi ignoré (20211130)
        """
        if l_s_path.startswith(".") :
            return True
        for l_s_ignore in IGNORE_FILE :
            if l_s_path.endswith(l_s_ignore) :
                return True

        # print(" ###### " + l_s_path)
        return False

# END File listing

def Right(s, amount):
    return s[-amount:]

IGNORE_FILE = [ ".directory", ".db", ".log", ".srt" , ".nfo", "jpg", ".json" ]

NB_TOLINK = 2
NB_TOSHOW = 12

FILE_PREFIX = "0000"
FILE_PREFIX_LEN = len(FILE_PREFIX)
#
# encours_name = "ENCOURS"
#
# source_dir = "/media/pix/data/videos"
# try :
#     test = FilesMng(source_dir, encours_name)
# #
#     file_test = "/media/pix/data/videos/SERIES/Community/S02_VOSTFR/Community.S02E21.VOSTFR.HDTV.XviD-ATeam.avi"
#     test.GetAll(file_test)
#
#     file_test = "/media/pix/data/videos/MANGAS/Naruto/1 - Naruto/Naruto 001.avi"
#     test.GetAll(file_test)
#
#     file_test = "/media/pix/data/videos/MANGAS/Blood/Blood+ 01.avi"
#     test.GetAll(file_test)
#
#     file_test = "/media/pix/data/videos/ANIMES/Archer/S04/Archr.S04E02.REAL.REPACK.FR.LD.BDRiP.x264-iWire.mkv"
#     test.GetAll(file_test)
#
#     file_test = "/media/pix/data/videos/SERIES/Day Break/01. Machination.avi"
#     test.GetAll(file_test)
#
#     file_test = "/media/pix/data/videos/ANIMES/Les Simpson/S03 1991/Les simpson 3X01 - Mon pote Michael Jackson.avi"
#     test.GetAll(file_test)
#
#     file_test = "/media/pix/data/videos/FILMS_720/Asterix.Le.Domaine.Des.dieux.2014.FRENCH.720p.BluRay.x264-Goatlove.mkv"
#     test.GetAll(file_test)
#
#     file_test = "/media/pix/data/videos/FILMS_720/Moi moche et mechant/Despicable.Me.3.2017.FRENCH.720p.BluRay.Light.x264.AC3.mkv"
#     test.GetAll(file_test)
#
#     source_dir = "/media/xud/.dux/DL"
#     # test = FilesMng(source_dir, encours_name)
#     test.MAIN_SOURCE_DIR = source_dir
#
#     file_test = "/media/xud/.dux/DL/_V/2018/20181216/ItA4/8729488_480p.mp4"
#     test.GetAll(file_test)
#
#     file_test = "/media/xud/.dux/DL/_V/2018/20180428/Amazing Babe in BDSM_720p.mp4"
#     test.GetAll(file_test)
#
#     file_test = "/media/xud/.dux/DL/_V/2018/20181104/ItA4/IntoTheAttic_Sammy_Synclair_720p.mp4"
#     test.GetAll(file_test)
#     # print(test.dicoSeries)
#
#     file_test = "/media/xud/.dux/DL/_V/2018/20181104/Armbinder and Fishnet_720p.mp4"
#     test.GetAll(file_test)
#
#     file_test = "/media/xud/.dux/DL/_V/2020/Jade the captive cunt.mp4"
#     test.GetAll(file_test)
#
#     file_test = "/media/xud/.dux/DL/_V/2020/20201220/Bound Cougars Deauxma & Ashley Renee Fuck Ball Gagged¡_stickydollars_1080p.mp4"
#     test.GetAll(file_test)
# #file:///media/xud/.dux/DL/_V/2020/20201220/Bound Cougars Deauxma & Ashley Renee Fuck Ball Gagged¡_stickydollars_1080p.mp4
# #file:///media/xud/.dux/DL/_V/2020/Jade the captive cunt.mp4
# #     # # /media/xud/.dux/DL/_V/2018/20181104/ItA4/IntoTheAttic_Sammy_Synclair_720p.mp4
# except Exception as e:
#     print(e)
# finally :
#     print(test.dicoSubSerie)
#     print(test.dicoSeries)
#     print()
#
#     for k in test.dicoSeries :
#         tstSerie = 0
#         print(k)
#         for kk in test.dicoSeries[k] :
#             if kk == "path" :
#                 continue
#             for s in test.dicoSeries[k][kk] :
#                 if len(test.dicoSeries[k][kk][s]["list"]) > 0 :
#                     print("S :: " + s + " " + str(len(test.dicoSeries[k][kk][s]["list"])))
#                     tstSerie += len(test.dicoSeries[k][kk][s]["list"])
#                 # print(len(test.dicoSeries[k][kk][s]["list"]))
#         print(tstSerie)
#     print()
#     #
#     print(test.GetIndex("/media/xud/.dux/DL/_V/2018/20180525/JEV-083_480p.mp4"))
#     print(test.GetIndex("/media/xud/.dux/DL/_V/2018/20181104/ItA4/IntoTheAttic_Sammy_Synclair_720p.mp4"))
#     print(test.GetIndex("/media/xud/.dux/DL/_V/2018/20181216/ItA4/8729488_480p.mp4"))
#     print(test.GetIndex("/media/xud/.dux/DL/_V/2018/20181104/Armbinder and Fishnet_720p.mp4"))
#     print(test.GetIndex("/media/xud/.dux/DL/_V/2020/20201220/Bound Cougars Deauxma & Ashley Renee Fuck Ball Gagged¡_stickydollars_1080p.mp4"))
#     print(test.GetIndex("/media/xud/.dux/DL/_V/2020/Muddy Cunt -  Catherine de Sade.wmv"))
#
#     source_dir = "/media/pix/data/videos"
#     test.MAIN_SOURCE_DIR = source_dir
#     print(test.GetIndex("/media/pix/data/videos/ANIMES/Les Simpson/S03 1991/Les simpson 3X01 - Mon pote Michael Jackson.avi"))
#     print(test.GetIndex("/media/pix/data/videos/FILMS_720/Asterix.Le.Domaine.Des.dieux.2014.FRENCH.720p.BluRay.x264-Goatlove.mkv"))
#     print(test.GetIndex("/media/pix/data/videos/FILMS_720/Moi moche et mechant/Despicable.Me.3.2017.FRENCH.720p.BluRay.Light.x264.AC3.mkv"))
#
#
# # # print(test.serie)
