# Copyright 2024 Elie COUTAUD

#!/usr/bin/env python
# -*- coding: utf-8 -*-

import socket
import json
from select import select

class Socket() :
    def __init__(self) :
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("130.89.148.77", 80))
        self.IP_LOCAL = s.getsockname()[0]
        self.method = []

    def Run(self, host, port, serverSocket = None) :
        """
        Run socket loop
        """
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        self.socket.bind((host, port))
        self.socket.listen(1)
        print('\n#=================#\n# Listen on ' + host + ":" + str(port) + ' #\n#=================#')

        # self.DIRS = dirs
        self.HOST = host
        self.PORT = port

        self.keepServing = True
        SERVER_TUPLE = (self.socket,)
        EMPTY_TUPLE = ( )

        if not len(self.method) > 0 :
            print("No method defined !")

        while self.keepServing:
            try:
                readSockets = select(SERVER_TUPLE, EMPTY_TUPLE, EMPTY_TUPLE)[0]
                if not readSockets:
                    print('SELECT: empty readSockets')
                    continue

                if not self.keepServing:
                    break

                # Blocking call.
                connection, sockname = self.socket.accept()
                print('CONNECTION: ' + str(sockname))

                # recv() can possibly raise an Exception when the other peer disconnects.
                input_recv = connection.recv(4096) # 4096 bytes to give enough room for Kodi's standard requests.

                try:
                    retour = self.method[0](input_recv, sockname, *self.method[1:])
                    retourJ = json.dumps(retour).encode()
                    # connection.sendall(f'{len(retourJ)}\n'.encode())
                    # print(retourJ.decode())
                    connection.sendall(str(json.dumps(retour)).encode())
                    print("SENT : " + str(retour))
                except KeyboardInterrupt as e:
                    print("Interrupt")
                    self.keepServing = False
                except Exception as e:
                    print('SENDALL:', e)
                    self.keepServing = False
            except KeyboardInterrupt as e:
                print("Interrupt")
                self.keepServing = False
            except Exception as e:
                print('EXCP :', e)
                self.keepServing = False
        try:
            self.socket.shutdown(socket.SHUT_RDWR)
            self.socket.close()
            print('CLOSE -----')
        except Exception as e:
            print('SHUTDOWN:', e)
