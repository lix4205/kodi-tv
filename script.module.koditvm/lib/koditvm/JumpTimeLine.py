#!/usr/bin/env python
# -*- coding: utf-8 -*-

import xbmc
import xbmcgui
import time

from threading import Thread
# BEGIN Skip 
"""
Jump on time line on kodi's reading
Skip duration on a video when time is over time_begin
"""
class Jump() :
    # Liste des processus
    #LIST_PROCESS = []
    
    def __init__ ( self, file_name, l_times, autoskip = False):
        self.file_name = file_name
        self.AUTOSKIP = autoskip
        self.dialog_time = 5 # delay in second for showing dialog
        # Forme 00:0X
        #logXbmc(time_begin)

        #xbmc.log("Will try to skip :" + str(l_times), level=xbmc.LOGINFO)

        self.sauts = []
        self.main = Thread(target=self.RunJob, args=([l_times]))
        self.main.start()


        #return self.sauts
    
    """
    Tue les processus...
    """
    def KillGenericThread(self) :

        #if self.t.is_alive() :
            #self.t.join(1)
        if self.main.is_alive() :
            self.main.join(1)

    """
    Va lancer successivement les sauts de générique
    Lancé dans un thread, un seul sous processus sera lancé et on attend la fin pour lancer le suivant...
    """
    def RunJob(self, l_times) :
        try :
            res_saut = []
            l_newTimes = []
            l_newDuration = []

            for saut in l_times :
                l_time = saut.split("+")
                if isinstance(l_time[0], str) :
                    # On gere les temps 00:00
                    if ":" in str(l_time[0]) :
                        l_newTime = l_time[0].split(":")
                        #self.time_begin = int(l_newTime[0]) * 60 + int(l_newTime[1])
                        l_newTimes.append(int(l_newTime[0]) * 60 + int(l_newTime[1]))
                        # l_newDuration.append(float(l_time[1]))
                        #logXbmc(self.time_begin)
                    else :
                        l_newTimes.append(float(l_time[0]) + 0.1)
                        # l_newDuration.append(float(l_time[1]))
                        #self.time_begin = float(l_time[0]) + 1
                else :
                    #self.time_begin = time_begin + 1
                    l_newTimes.append(time_begin + 0.1)
                l_newDuration.append(float(l_time[1]))


            for saut in sorted(l_newTimes) :
                l_idx = l_newTimes.index(saut)
                self.done = False
                self.time_begin = saut
                if self.AddJump(saut, l_newDuration[l_idx]) :
                    # on attend la fin pour lancer le suivant...
                    while self.t.is_alive() :
                        time.sleep(1)
        except Exception as e:
            xbmc.log("JumpTimeLine : " + str(e), 2)
        # finally :


    """
    Ajoute chaque saut a effectuer sur la piste dans le tableau
    """
    def AddJump(self, time_begin, duration) :
        try :
            new_proc = None
            self.duration = float(duration)
            current_time = xbmc.Player().getTime()
            self.time_begin  = float(time_begin) + 0.1

            # xbmc.log("0:" + str(current_time) + " >=  " + str(self.time_begin) + " " + str(self.TimeEnd()) + " " + str(current_time >= self.time_begin and current_time <= self.TimeEnd()), 1)
            # xbmc.log("1:" + str(float(current_time) >= float(self.time_begin)) + " and " + str(current_time <= self.TimeEnd()) + " " + str(self.TimeEnd()) + " " + str(current_time >= self.time_begin and current_time <= self.TimeEnd()), 1)

            if (self.time_begin <= 0.2 and current_time <= self.TimeEnd()) or (not current_time >= self.time_begin and current_time <= self.TimeEnd()) :
                new_proc = Thread(target=self.JumpPiste, args=())
                new_proc.start()
        except Exception as e:
            xbmc.log("JumpTimeLine : " + str(e), 2)
        finally :
            if new_proc == None :
                return False
                xbmc.log("2:" + str(current_time) + " >=  " + str(self.time_begin) + " " + str(self.TimeEnd()) + " " + str(current_time >= self.time_begin and current_time <= self.TimeEnd()), 2)
            else :
                self.t = new_proc
                return True
    """
    Retourne le timer de fin du saut (point initial du saut + durée du saut)
    """
    def TimeEnd(self) :
        return float(self.time_begin) + float(self.duration)
    
    """
    Boucle dans laquelle on attend un certain temps (max 30 sec) avant de tester si on est dans la section du saut.
    """
    def JumpPiste(self) :
        # Temps minimum a attendre (au début pour s'assurer que la video tourne encore et ne pas laisser des threads inutiles tourner)
        l_wait_min = 5
        # Temps a attendre entre chaque check (augmente jusqu'a atteindre l_wait_max )
        l_wait = l_wait_min
        # Temps maximum a attendre
        l_wait_max = 15
        # On s'assure que la vidéo est lancé avant d'effectuer le test
        time.sleep(0.5)
        #logXbmc("Searching for :" + str(self.time_begin) + " - " + str())
        #logXbmc( 'Searching for ' + str(self.time_begin) + " + " +  str(self.duration) + " = " + str(self.TimeEnd()) )
        # On récupère le chemin de la video joué
        path_video = xbmc.getInfoLabel('Player.FileNameAndPath')
        # Tant que le saut n'a pas été fait, et qu'on est toujours sur la meme piste,
        # On va "dormir" 30 seconde, jusqu'a atteindre la partie a couper...
        while not self.Skip() and not self.done and path_video == self.file_name :
            #logXbmc(path_video + " == " + self.file_name)
            # Si une vidéo est en cours de lecture,
            if xbmc.Player().isPlaying() :
                # On récupère le chemin du fichier
                path_video = xbmc.getInfoLabel('Player.FileNameAndPath')
                # On récupere le temps courant
                current_time = xbmc.Player().getTime()
            else :
                # La vidéo ne joue plus, on arrete
                #logXbmc( 'Not playing ' + str(self.time_begin) + " "  + str(self.TimeEnd()) )
                #self.NextProcess()
                return False

            # Si la piste joué est la meme...
            if path_video == self.file_name :
                # Si on a dépassé le créneau,
                if self.duration != 0 and current_time >= self.TimeEnd() :
                    # On sort
                    #logXbmc( 'Abort ' + str(self.time_begin) + " "  + str(self.TimeEnd()) )
                    #self.NextProcess()
                    return False
                # Si il reste moins de 30 secondes avant le saut
                if current_time + l_wait >= self.time_begin and current_time <= self.TimeEnd() :
                    # on attend le temps qu'il faut
                    time.sleep(self.time_begin - current_time)
                else :
                    # Sinon, on va attendre
                    time.sleep(l_wait)
                # Tant qu'on a pas atteind les 30s, on ajoute plus 5 au timer
                if l_wait < l_wait_max :
                    l_wait += l_wait_min
            else :
                # On a changé de piste, on sort...
                #logXbmc( 'Not playing ' + str(os.path.basename(self.file_name)) + " " + str(self.time_begin) + " "  + str(self.TimeEnd()))
                #self.NextProcess()
                return False
        
        
        #xbmc.log("# INFO # Will return : " + str(self.done) + " " + str(self.time_begin) + "+" + str(self.duration) + " " + str(self.t), level=xbmc.LOGINFO)

        #self.NextProcess()
        #LIST_PROCESS[index_saut].join()
        #del(Jump.LIST_PROCESS[index_saut])
        #if len(Jump.LIST_PROCESS) > 0 :
            #Jump.LIST_PROCESS[0].start()
        return self.done
    
    #"""
    #Termine le processus en cours et lance le suivant dans la liste
    #"""
    #def NextProcess(self) :
        ##l_process = Jump.LIST_PROCESS[0]
        #xbmc.log("# WARN # " + str(self.t.is_alive()) + " !" + str(self.LIST_JUMPS[0]), level=xbmc.LOGINFO)
        #if self.t.is_alive() :
            #self.t.join()
            
        #del(self.LIST_JUMPS[0])
        #if len(self.LIST_JUMPS[0]) > 0 :
            ##Jump.LIST_PROCESS[0].start()
            #xbmc.log("# WARN # " + str(self.LIST_JUMPS[0]) + " !", level=xbmc.LOGINFO)
    
    """
    Effectue un saut dans la video
    """
    def Skip(self) :
        # si la video joue,
        if xbmc.Player().isPlaying() :
            # On recupere le temps courant
            current_time = xbmc.Player().getTime()
            # Si la lecture atteind le temps initial, et que la durée du saut est 0,
            if current_time >= self.time_begin and self.duration == 0 :
                if self.AUTOSKIP or xbmcgui.Dialog().yesno("Passer a l'épisode suivant?", "Etes vous sur ?", "Continuer", "Passer", self.dialog_time * 1000) :
                    # On saute a la derniere seconde...
                    #xbmc.Player().
                    total_time = xbmc.Player().getTotalTime()
                    xbmc.Player().seekTime (total_time - 1)
                    # On passe a la piste suivante
                    #xbmc.Player().playnext()
                    #logXbmc( 'Next ' + str(self.time_begin) + " "  + str(self.TimeEnd()) )
                    self.done = True
                # Et on sort
                return True
            # Si la lecture atteind le temps initial, on va sauter le passage,
            if current_time >= self.time_begin and current_time <= self.TimeEnd():
                if self.AUTOSKIP or xbmcgui.Dialog().yesno("Passer le générique ?", "Etes vous sur ?",  "Continuer", "Passer", self.dialog_time * 1000) :
                    xbmc.Player().seekTime (self.TimeEnd() - 1)
                    #logXbmc( 'Skip ' + str(xbmc.Player().getTime()) + " "  + str(self.TimeEnd()) )
                        #logXbmc( '###!! Generic skipped\n"### ' + str(self.time_begin) + " + " +  str(self.duration) + " = " + str(self.TimeEnd()) + "\n ### " + "\n### " + self.file_name  )
                    self.done = True
                # Et on sort
                return True

            #elif current_time + 5 >= self.time_begin and current_time < self.TimeEnd() :
                ##xbmc.Player().seekTime (time_end - current_time)
                #time.sleep(5)
                #return self.Skip()
            else :
                return False

# END Skip

