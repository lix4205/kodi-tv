##!/usr/bin/env python
## -*- coding: utf-8 -*-

import xbmcplugin
import xbmcgui
import os
from . import KodiMove
from . import TagsMng
# from . import KodiLog

try:
    from urllib.parse import urlencode
except ImportError:
    # from urlparse import parse_qs
    from urllib import urlencode


class KodiList(KodiMove.KodiMove) :

    def url(pQuery, addon_addr = ""):
        """ Return formatted url """
        return addon_addr + '?' + urlencode(pQuery)

    # TODO: Create unique function to add in a list
    def addInList(addon_handle, xurl, li) :
        xbmcplugin.addDirectoryItem(addon_handle, xurl, li)

    def GetPrevNextPage(addon_handle, addon_addr, l_label, l_saga, episode, idx, addr, folder, menu = "", count = -1) :
        """ Create links to display index (all index or only 10 last or next episode """
        li = xbmcgui.ListItem(label=l_label)
        li.setProperty('IsPlayable', 'true')
        l_url_param = {
            "saga" : l_saga,
            "addr": addr,
            "index" : idx,
            "folder" : folder,
            "episode" : episode
        }
        #
        if menu != "" :
            l_url_param["menu"] = menu
        if count > 0:
            l_url_param["count"] = count
        l_url = KodiList.url(l_url_param, addon_addr)
        xbmcplugin.addDirectoryItem(addon_handle, l_url, li, isFolder = True)

    def items(addon_handle, main_source_path, last_played, socket, folder, addon_addr, IGNORE_IP_LOCAL = []):
        """ List files from last_played list """
        idx = int(last_played["index"]) # idx of line on the json file

        # KodiMove.Log.Show("SRE :: " +str(last_played["name"]))
        l_path_saga = os.path.dirname(last_played["list"][0][0])
        l_saga = last_played["name"]
        l_saison = os.path.basename(l_path_saga)

        # list all items
        for line in last_played["list"] :
            desc = os.path.basename(line[0])
            desc = KodiMove.KodiTools.Right("0000", str(idx)) + "_" + os.path.basename(line[0])

            li = xbmcgui.ListItem(label=desc)
            li.setProperty('IsPlayable', 'true')

            if socket.ADDR[0] in IGNORE_IP_LOCAL :
                xurl = line[0]
            else :
                if os.path.exists(line[0]) :
                    xurl = line[0]
                else :
                    xurl = main_source_path + line[0]

            xbmcplugin.addDirectoryItem(addon_handle, xurl, li)
            idx = idx + 1

        addr = socket.GetAddr()
        # Previous page
        prev_idx = last_played["index"] - 10
        if prev_idx < 0 :
            prev_idx = 0

        if idx > 8 and prev_idx > 0 :
            l_label = KodiMove.KodiTools.Right("0000", str(prev_idx)) + " - " + KodiMove.KodiTools.Right("0000", str(prev_idx + 9))
            KodiList.GetPrevNextPage(addon_handle, addon_addr, l_label, l_saga + "|" + l_saison, line, prev_idx - 1, addr, folder, os.path.basename(line[0]))

        # Next page
        if idx < int(last_played["count"]) :
            l_label = KodiMove.KodiTools.Right("0000", str(int(last_played["index"]) + 10)) + " - " + KodiMove.KodiTools.Right("0000", str(int(last_played["index"]) + 19))
            KodiList.GetPrevNextPage(addon_handle, addon_addr, l_label, l_saga + "|" + l_saison, line, idx - 1, addr, folder, os.path.basename(line[0]))

        # Index page
        l_label = "Index " + os.path.basename(l_saga)
        KodiList.GetPrevNextPage(addon_handle, addon_addr, l_label, l_saga + "|" + l_saison, line, -1, addr, folder, count=last_played["count"])

        # Folder page
        li = xbmcgui.ListItem(label=os.path.basename(l_saga) + " | " + l_saison)
        li.setProperty('IsPlayable', 'false')
        xbmcplugin.addDirectoryItem(addon_handle, os.path.join(l_path_saga, ".."), li, isFolder = True)

        li = xbmcgui.ListItem(label="Home")
        li.setProperty('IsPlayable', 'False')
        l_url_param = {
            "addr": addr,
            "menu" : "list"
        }

        l_url = KodiList.url(l_url_param, addon_addr)
        xbmcplugin.addDirectoryItem(addon_handle, l_url, li, isFolder = True)

        xbmcplugin.endOfDirectory(addon_handle)


    def groups(addon_handle, last_played, socket, addon_addr, socks = [], IGNORE_IP_LOCAL = []):
        """ List server directories """
        if "error" in last_played :
            KodiMove.Notification(last_played["error"], "ERROR !")
            # xbmc.executebuiltin('Notification("ERROR !", "' + str(last_played["error"]) + '" , 3000)')

        for folder in last_played.keys():
            if folder == "fromkodi" :
                continue
            for line in sorted(last_played[folder].keys()):
                nm = line
                li = xbmcgui.ListItem(nm)

                delete_cmd_url = KodiList.url({'menu': "remove", 'id' : str(line.split("|")[0]), "addr" : socket.GetAddr()}, addon_addr)
                delete_cmd = "RunPlugin("+ delete_cmd_url + ")"

                command = []
                command.append(("Delete", delete_cmd))
                li.addContextMenuItems(command)

                xbmcplugin.addDirectoryItem(addon_handle, KodiList.url({'folder' : folder, 'menu': nm, "addr" : socket.GetAddr(), "episode" : last_played[folder][line]}, addon_addr), li, isFolder = True)


        # On liste les dossiers accessibles du socket
        for folder in last_played.keys():
            if folder == "fromkodi" :
                continue
            if socket.ADDR[0] in IGNORE_IP_LOCAL :
                xurl = folder
            else :
                xurl = "nfs://" + socket.ADDR[0] + folder

            li = xbmcgui.ListItem(label=folder)
            xbmcplugin.addDirectoryItem(addon_handle, xurl, li, isFolder = True)
        li = xbmcgui.ListItem(label="Stream")
        li.setProperty('IsPlayable', 'false')
        xbmcplugin.addDirectoryItem(addon_handle, "plugin://plugin.video.synkodi", li, isFolder = True)

        if len(socks) > 0 :
            for addr in socks :
                xurl = addr
                li = xbmcgui.ListItem(label=addr)
                li.setProperty('IsPlayable', 'true')
                xbmcplugin.addDirectoryItem(addon_handle, KodiList.url({ "addr" : xurl, "menu" : "list" }, addon_addr), li, isFolder = True)
        xbmcplugin.endOfDirectory(addon_handle)

    def index(addon_handle, count, socket, folder, addon_addr, serie, saison, episode = "", index = -1, play = False):
        """
        List index files from a directory on server
        """
        for idx in range(int(count / 10)) :
            li = xbmcgui.ListItem(label=str(idx * 10 + 1) + " - " + str(idx * 10 + 10))
            li.setProperty('IsPlayable', 'false')
            l_url = {
                'saga' : serie + "|" + saison,
                'menu': episode,
                # 'episode': episode,
                "addr" : socket.GetAddr(),
                "index" : idx * 10,
                "folder" : folder
            }
            xbmcplugin.addDirectoryItem(addon_handle, KodiList.url(l_url, addon_addr), li, isFolder = True)
        KodiMove.Log.Show("WTF20 :: !!!!!!" + str(serie) + " " + str(saison) + " " + str(folder))

        if count % 10 > 0 :
            idx = int(count / 10)
            li = xbmcgui.ListItem(label=str(idx * 10 + 1) + " - " + str(count))
            li.setProperty('IsPlayable', 'false')
            KodiMove.Log.Show("WTF21 :: !!!!!!" + str(serie))
            xbmcplugin.addDirectoryItem(addon_handle, KodiList.url({'saga' : serie + "|" + saison, 'menu': episode, 'episode': episode, "addr" : socket.GetAddr(), "index" : idx * 10, "folder" : folder }, addon_addr), li, isFolder = True)

    def list_tags(addon_handle, main_source_path, last_played, socket, addon_addr,  IGNORE_IP_LOCAL = []):
        """ List tagged video """
        test = TagsMng.TagsMng("")

        # return True
        last_played = list(test.TAGGED_VID.keys())

        for line in last_played[0:10] :
            # for line in sorted(last_played[folder].keys()):
            nm = os.path.basename(line)
            li = xbmcgui.ListItem(nm)
            li.setProperty('IsPlayable', 'true')

            if socket.ADDR[0] in IGNORE_IP_LOCAL :
                xurl = line
            else :
                if os.path.exists(line) :
                    xurl = line
                else :
                    xurl = main_source_path + line

            xbmcplugin.addDirectoryItem(addon_handle, xurl, li)

            KodiMove.Log.Show(line)

        xbmcplugin.endOfDirectory(addon_handle)
