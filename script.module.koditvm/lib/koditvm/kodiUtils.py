# -*- coding: latin-1 -*-
# from . import jsonUtils
import xml.etree.ElementTree as ET
import base64
import json
from urllib.request import Request, urlopen
import koditvm.pythcomm.txt_utils as txt
try :
    # Try to use addon from xbmc for get settings
    import xbmcaddon
except :
    pass

class KodiTools :
    #"""
    #Return Plugin url script
    #"""
    #def url(plugin_id, pQuery):
        #delete_cmd_url = url({'menu': "remove", 'id' : str(line.split("|")[0]), "addr" : SOCKET_ADDR})
        #delete_cmd = "RunPlugin("+ delete_cmd_url + ")" #url({'menu': "remove", 'id' : str(line), "addr" : SOCKET_ADDR})

        ##xbmc.log("URL:" + sys.argv[0] + '?' + urlencode(pQuery), 3)
        #return sys.argv[0] + '?' + urlencode(pQuery)

    def Right(s, val) :
        return txt.Right(s, val)

    def GetPathToRemove(path_video) :
        """
        Renvoie un tableau contenant :
        0 = le chemin du fichier sans les infos sur le serveur
        1 = l'adresse IP du serveur sur laquelle la vidéo tourne
        """
        path2remove = ""
        if str(path_video).startswith("/") :
            SERVEUR = "127.0.0.1"
            PROTOCOL = "local"
        else :
            SERVEUR = str(path_video)

            l_found_beg = path_video.lower().find("://") + 3
            l_found = path_video[l_found_beg:]
            l_found_end = l_found.lower().find("/") + l_found_beg
            SERVEUR = path_video[l_found_beg:l_found_end]
            l_tmpSrv = SERVEUR.find(":")
            if l_tmpSrv >= 0 :
                SERVEUR = SERVEUR[0:l_tmpSrv]
            PROTOCOL = path_video[0:l_found_beg - 3]
        #if l_found :
            #try :
                #logXbmc(str(SERVEUR) + " " + os.path.basename(path_video).encode())
            #except :
                #logXbmc(str(SERVEUR))
            path2remove = path_video[0:l_found_end]
        return [path2remove, SERVEUR, PROTOCOL]

    def HRTime(time_begin, in_array = True, shortest = True) :
        """ Return Human readable time (hh:mm:ss.ms) """
        # TODO : Use txt.HRTime
        return txt.HRTime(time_begin, shortest, in_array)

    def GetSetting(setting_id, addon = None) :
        if addon == None :
            addon = xbmcaddon.Addon()
        return addon.getSetting(setting_id)

    def GetBoolParam(kodi_param, addon = None) :
        if addon == None :
            addon = xbmcaddon.Addon()

        l_b_param = True
        if addon.getSetting(kodi_param) == "false" :
            l_b_param = False
        return l_b_param

class KodiConn :
    """ Methods to query kodi """
    # Initialise la classe
    def __init__(self, p_s_host = "localhost", p_s_usr = "kodi", p_s_pass = "", p_s_port = "8080"):
        self.ADDR = p_s_host
        self.PORT = p_s_port
        self.USER = p_s_usr
        self.PASS = p_s_pass

    # Créer et execute la requete puis renvoie le resultat.
    def ExecQry(self, method, params, id_qry) :
        # if method == "Player.GetItem" :
        #     print('pl' + str(method) + " " +str(params) + " " + str(id_qry), end="", flush=True)
        return GetJsonRemote(self.ADDR, self.PORT, self.USER, self.PASS, method, params)

    def GetAddr(self) :
        return self.USER + "@" + self.ADDR + ":" + str(self.PORT)

    # Get video time.
    def GetVideoTime(self, in_second = True) :
        # On execute la requete et on recoit un JSON
        video_time = self.ExecQry('Player.GetProperties', {'properties': ['time',], 'playerid': 1}, 'VideoGetItem')

        if "time" in video_time :
            #pathVideo = pathVideo["item"][ t_path ]
        # Les temps en heures, minutes, secondes, millisecondes
            video_time_h = video_time["time"]["hours"]
            video_time_m = video_time["time"]["minutes"]
            video_time_s = video_time["time"]["seconds"]
            # On ajoute 900ms pour amoindrir la latence...
            video_time_ms = video_time["time"]["milliseconds"]
        else :
            return -1

        if in_second :
            # On calcule le temps en seconde pour vlc
            #kodi_sec = ((video_time_h *3600) + (video_time_m * 60) + video_time_s + video_time_ms)
            #kodi_sec =
            return self.GetTimeInSeconds([video_time_h, video_time_m, video_time_s, video_time_ms])

        # On retourne le temps en !seconde
        return [video_time_h, video_time_m, video_time_s, video_time_ms]

    def GetVolume(self) :
        res = self.ExecQry('Application.GetProperties', {'properties': ["volume"] }, 'GetVol')
        if not "ERROR" in res :
            return res["volume"]
        return 0
        # return self.ExecQry('Application.GetProperties', {'properties': ["volume"] }, 'GetVol')["volume"]

    def SetVolume(self, vol) :
        return self.ExecQry('Application.SetVolume', {'volume' : vol }, 'SetVol')

    def GetTrackDuration(self) :
        res = self.ExecQry('Player.GetProperties', {'properties': ["totaltime"], 'playerid': 1 }, 'GetTotalTime')
        if not "ERROR" in res :
            video_time = res["totaltime"]
            video_time_h = video_time["hours"]
            video_time_m = video_time["minutes"]
            video_time_s = video_time["seconds"]
            # On ajoute 900ms pour amoindrir la latence...
            video_time_ms = video_time["milliseconds"]
            # t = self.GetTimeInSeconds([video_time_h, video_time_m, video_time_s, video_time_ms])
            # print(t)
            return self.GetTimeInSeconds([video_time_h, video_time_m, video_time_s, video_time_ms])
            # return self.GetTimeInSeconds(res["totaltime"])
        return 0

    # Recupere le chemin du fichier qui tourne sur kodi.
    def GetVideoPath(self, t_path = "dynpath") :
        # print("TRACK :: ")
        # On recupere le chemin de la video qui tourne sur kodi.
        pathVideo = self.ExecQry('Player.GetItem', {'properties': [ t_path ], 'playerid': 1}, 'VideoGetItem')
        if "item" in pathVideo :
            pathVideo = pathVideo["item"][ t_path ]
        # print(pathVideo + " END.")
        # Et on retourne le chemin
        return pathVideo

    # Envoie une notification...
    def SendNotification(self, p_s_title, p_s_msg, p_i_timeNotif = 5000) :
        resQry = self.ExecQry( 'GUI.ShowNotification', {'title' : p_s_title , 'message': p_s_msg, "displaytime" : p_i_timeNotif }, 'NotifSent')
        return resQry

    def RunVideo(self, pathVideo) :
        resQry = self.ExecQry( 'Player.Open', {'item': {'file' : pathVideo }}, 'VideoPlay')

    def PlayPause(self) :
        resQry = self.ExecQry( 'Player.PlayPause', {'playerid' : 1}, 'VideoPlayPause')
        return resQry

    def Next(self) :
        resQry = self.ExecQry( 'Player.GoTo', {'playerid' : 1, "to" : "next"}, 'VideoNext')
        return resQry

    def Previous(self) :
        resQry = self.ExecQry( 'Player.GoTo', {'playerid' : 1, "to" : "previous"}, 'VideoNext')
        return resQry

    def SeekTo(self, time_video, offset = 0) :
        resQry = self.ExecQry( 'Player.Seek', {'playerid' : 1, 'value': {'time' : { "hours" : int(time_video[0]), "minutes" : int(time_video[1]), "seconds" : int(time_video[2]) + offset, "milliseconds" : int(time_video[3]) }}}, 'VideoSync')

   # Change directory to value in parameters
    def ChangeDirectory(self, cd_path) :
        resQry = self.ExecQry( 'GUI.ActivateWindow', {'window' : "videos", 'parameters': [ cd_path ] }, 'ChangeDirectory')
        return resQry

    # Execute addon
    def ExecuteAddon(self, p_s_addon_id, p_o_params = {}, p_b_wait = False):
        resQry = self.ExecQry( 'Addons.ExecuteAddon', {'addonid' : p_s_addon_id , 'params': p_o_params, "wait" : p_b_wait }, 'ExecAddon')
        return resQry

    def IsPaused(self) :
        resQry = self.ExecQry( 'Player.GetProperties', { 'playerid' : 1, "properties" : ["speed"]}, 'OnPause')
        if "speed" in resQry :
            return resQry["speed"] == 0
        return False

    def IsPlaying(self) :
        resQry = self.ExecQry( 'Player.GetProperties', { 'playerid' : 1, "properties" : ["speed"]}, 'OnPlay')
        # print(resQry)
        if "speed" in resQry :
            return resQry["speed"] == 1
        return False

    def Play(self) :
        if self.IsPaused() :
            self.PlayPause()

    def Pause(self) :
        if self.IsPlaying() :
            self.PlayPause()

    def GetTimeInSeconds(self, p_a_time) :
        kodi_sec = (p_a_time[0] *3600) + (p_a_time[1] * 60) + p_a_time[2] + (float(p_a_time[3]) / 1000)
        return kodi_sec


    def GetKodiCredential(self) :
        return [self.ADDR, self.PORT, self.USR, self.PASS ]

class kodiXmlConf :
    def GetSocketConf(self, file_path, nodeId = "", tagName = "setting"):
        tree = ET.parse(file_path)
        root = tree.getroot()
        if nodeId == "" :
            #nodeId = "id"
            return False;

        for setting in root.findall(tagName):
            idSetting = setting.get("id")

            if idSetting == nodeId :
                return setting.text

        return root.findall(tagName)

def GetJsonRemote(host,port,username,password,method,parameters):
    # First we build the URL we're going to talk to
    url = 'http://%s:%s/jsonrpc' %(host, port)
    # Next we'll build out the Data to be sent
    values ={}
    values["jsonrpc"] = "2.0"
    values["method"] = method
    # This fork handles instances where no parameters are specified
    if parameters:
        values["params"] = parameters
    values["id"] = "1"
    #headers = {"Content-Type":"application/json"}
    # Format the data
    data = json.dumps(values)
    data = data.encode('ascii')
    # Now we're just about ready to actually initiate the connection
    req = Request(url, data)
    req.add_header("Content-Type", "application/json")
    # This fork kicks in only if both a username & password are provided
    if username and password:
        # This properly formats the provided username & password and adds them to the request header
        base64string = '%s:%s' % (username, password)
        base64string = base64.b64encode( bytes(base64string, "utf-8") )
        authorization = b'Basic ' + base64string
        #base64string = base64.b64encode('%s:%s' % (bytes(username, "utf-8"), bytes(password, "utf-8"))) #.replace('\n', '')
        req.add_header("Authorization", authorization)
        #print(base64string)
    # Now we're ready to talk to XBMC
    # I wrapped this up in a try: statement to allow for graceful error handling
    try:
        #print(url)
        response = urlopen(req)
        response = response.read()
        response = json.loads(response)
        # A lot of the XBMC responses include the value "result", which lets you know how your call went
        # This logic fork grabs the value of "result" if one is present, and then returns that.
        # Note, if no "result" is included in the response from XBMC, the JSON response is returned instead.
        # You can then print out the whole thing, or pull info you want for further processing or additional calls.
        if 'result' in response:
            response = response['result']
        if "error" in response :
            if "code" in response["error"] :
                if response["error"]["code"] == -32601 :
                    raise Exception("Method not found ! : " + method)
    # This error handling is specifically to catch HTTP errors and connection errors
    except Exception as e:
        # In the event of an error, I am making the output begin with "ERROR " first, to allow for easy scripting.
        # You will get a couple different kinds of error messages in here, so I needed a consistent error condition to check for.
        response = 'ERROR : '+str(e)
    return response
