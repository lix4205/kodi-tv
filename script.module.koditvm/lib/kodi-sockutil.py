#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import getopt
import re
import json
import os 
from threading import Thread
import time
import shutil
from koditvm.run_socket import Socket
from koditvm.ClientSocket import ClientSocket
from koditvm.Constants import CmdSocket, Path
from koditvm.FilesMng import FilesMng
from koditvm import kodiUtils
from koditvm.TagsMng import TagsMng, Md5Mng
from koditvm.pythcomm.helper_help import usage
# # Observateur d'event pour detection nouveau fichier
# from watchdog.observers import Observer
# from watchdog.events import FileSystemEventHandler
# help = helper_help.

#from resources.lib import kodiUtils
# BEGIN Help

# Option courte : longue
usage.OPTIONS_ALIAS = {
    "p": "port",
    "d": "debug"
    #"h" : "help"
    #"h": "hostname",
    #"u": "username",
}

# Options necessitant une valeur
usage.OPTIONS_VALUES = {
    # "path" : "paths to monitor",
    "p": "port",
    #"h": "hostname",
    #"u": "username",
    }
# # Autres options --truc
# usage.OPTION_LOCAL = [
#             #"gentoo" : "Install gentoo",
# ]
# Descriptions des options
usage.OPTIONS_HELP = {
    "p" : "Port to listen",
    "d": "debug mode",
    "path": "path(s) to monitor",
            #"t" : "Test mode",
            #"q" : "Quiet mode"
}
usage.OPTIONS_MANDATORY = [
    "path"
    ]
usage.OPTIONS_ARGS = [
    "path",
    ]
usage.TEXT = "python " + sys.argv[0]
usage.DESC = "Run socket on port [port(default 9876)] to specify which media is playing to restitute for another media player."
# END Help

# class ExampleHandler(FileSystemEventHandler):
#     def on_created(self, event): # when file is created
#         # do something, eg. call your function to process the image
#         print("Got event for file %s" % event.src_path)
#
#         return "ok"

class VideoSocket() :

    def __init__(self, dirs_video, host, port) :
        try :
            self.socket = Socket()
            self.socket.method = [ self.Loop ]
            self.DIRMNG = dirs_video
            # print(dirs_video)
            self.ENCOURS = ""
            self.listeIndex = {}
            self.RUNTIME_VIDEO = {}
            self.dicoMd5 = {}
            self.dicoTag ={}
            self.dicoJump = {}
            self.MAIN_SOURCE_DIR = ""
            self.SKIP_DIR = 0
            self.tags = {}
            self.sagas = {}
            DIR_FILE_TAG = "/media/xud/.dux/ffmpeg-sample/"
            # FILE_TAG = DIR_FILE_TAG + ".tags.json"
            # FILE_MD5 = DIR_FILE_TAG + ".md5.json"
            FILE_TIME = DIR_FILE_TAG + ".times.json"
            # FILE_BDD = "/media/xud/.dux/ffmpeg-sample/.bdd.json"

            self.GetThrow(dirs_video)

            self.GetSagas()

            if os.path.exists(FILE_TIME) :
                with open(FILE_TIME, 'rb') as f:
                    self.dicoDone = json.load(f)
            # log(self.dicoJump)

            # self.tags[dirs_video[2]].SearchSaison("/media/pix/data/videos/ANIMES/Archer/S03/Archer.S03E11.VOSTFR.WEB-DL.XviD-SLT.avi", self.tags[dirs_video[2]].dicoTimeStamps["__general__"]["__sagas__"])
            # self.tags[dirs_video[2]].SearchSaison("/media/pix/data/videos/ANIMES/Futurama/Futurama S06/Futurama.S06E04.VOSTFR.HDTV.XviD-BaDH.avi", self.tags[dirs_video[2]].dicoTimeStamps["__general__"]["__sagas__"])
            # sys.exit(0)
            # truc = kodiUtils.kodiXmlConf().GetSocketConf("/home/dux/.kodi/userdata/guisettings.xml", "services.webserverpassword")

            USR_DIR = os.path.expanduser('~')
            KODI_CONF = os.path.join(USR_DIR, ".kodi", "userdata", "guisettings.xml")

            kodi_web = kodiUtils.kodiXmlConf().GetSocketConf(KODI_CONF, "services.webserver")
            self.WEB_KODI = []
            if bool(kodi_web) :
                passKodi = kodiUtils.kodiXmlConf().GetSocketConf(KODI_CONF, "services.webserverpassword")
                userKodi = kodiUtils.kodiXmlConf().GetSocketConf(KODI_CONF, "services.webserverusername")
                portKodi = kodiUtils.kodiXmlConf().GetSocketConf(KODI_CONF, "services.webserverport")

                self.WEB_KODI = [ userKodi, passKodi, portKodi ]
        # print(self.WEB_KODI)
            self.socket.Run(host, port)
        except Exception as e :
            log(e)
        finally :
            if len(self.dicoMd5) > 0 :
                for l_dir in self.dicoMd5 :
                    self.dicoMd5[l_dir].Save()

    def GetSagas(self) :
        """ Get a dict of all sagas """

        for main_dir in self.tags :
            # On type :
            for type_dir in os.listdir(main_dir) :
                d = os.path.join(main_dir, type_dir)
                # Do not scan link or hidden files
                if os.path.islink(d) or type_dir.startswith(".") or type_dir == Path.DIR_ENCOURS_NAME :
                    continue
                if os.path.isdir(d) :
                    for saga in os.listdir(d) :
                        saga_dir = os.path.join(d, saga)
                        if os.path.isdir(saga_dir) :
                            if saga not in self.sagas :
                                self.sagas[saga] = []
                            self.sagas[saga].append(os.path.join(type_dir, saga))

    def Loop(self, input_recv, sockname) :
        """
        Main loop to query socket
        """
        try :
            retour = "{}"
            log('INPUT:' + str(input_recv, encoding="utf-8") + '(%i)' % len(input_recv))
            if not input_recv :
                retour = self.DIRMNG
            else :
                # print(self.DIRMNG)
                input_socket = json.loads(str(input_recv, encoding="utf-8").encode())
                cmd_kodi = input_socket["command"]
                if "args" in input_socket :
                    args = input_socket["args"]
                if cmd_kodi == CmdSocket.MAINDIRS :
                    retour = self.DIRMNG
                if cmd_kodi == "webserver" :
                    retour = self.WEB_KODI
                elif cmd_kodi == CmdSocket.DIRSPLAYING :
                    retour = self.ListSeriesEnCours(args)
                elif cmd_kodi == CmdSocket.LISTPLAYING :
                    retour = self.ListSerie(args)
                elif cmd_kodi == CmdSocket.UPDATELINK :
                    retour = self.UpdateLink(args)
                elif cmd_kodi == CmdSocket.KEEPENTRY :
                    retour = self.TryToJump(args)
                elif cmd_kodi == CmdSocket.RELOADKEEPENTRY :
                    # Reload json dict
                    retour = self.GetThrow(args)
                elif cmd_kodi == CmdSocket.SETSKIPENTRY :
                    retour = self.SetGeneric(args)
                elif cmd_kodi == CmdSocket.MISSGENERIX :
                    retour = self.GetMissGenerix(args)
                elif cmd_kodi == CmdSocket.CLEANLINKS :
                    # Nettoyage des liens dans ENCOURS
                    retour = self.CleanEncours(args)

            return retour
        except KeyboardInterrupt as e:
            log("Interrupt")
        except Exception as e:
            raise Exception('LOOP:', e)

    # BEGIN Series & eps list
    def ListSeriesEnCours(self, args) :
        """
        List series/sagas directory
        """
        content_dir = {}
        self.SKIP_DIR = args["skip_firsts_dir"]
        # Conserve les x derniers fichiers (sleep)
        l_keep = int(args["keep"])
        for folder in self.DIRMNG :
            self.MAIN_SOURCE_DIR = folder
            self.DIR_ENCOURS = self.MAIN_SOURCE_DIR + "/" + Path.DIR_ENCOURS_NAME
            dir_list = {}
            last_elem = ""
            last_elem_key = ""
            if os.path.exists(self.DIR_ENCOURS) :
                for l_s_dir in os.listdir(self.DIR_ENCOURS) :
                    # Ignore hidden files
                    if l_s_dir.startswith(".") :
                        continue
                    if os.path.isdir(self.DIR_ENCOURS + "/" + l_s_dir) :
                        file_list = sorted(os.listdir(self.DIR_ENCOURS + "/" + l_s_dir))

                        if len(file_list) > 0 :
                            # log(l_s_dir + " " + str(file_list))
                            if l_keep >= len(file_list) :
                                l_keep = len(file_list) - 1
                            real_path = os.path.realpath(os.path.join(self.DIR_ENCOURS, l_s_dir, file_list[l_keep]))
                            b_exist = os.path.exists(real_path)
                            l_saison = os.path.basename(os.path.dirname(os.path.realpath(real_path)))
                            dir_list[l_s_dir + "|" + l_saison] = []
                            dir_list[l_s_dir + "|" + l_saison] = file_list[l_keep]
            content_dir[folder] = dir_list

        return content_dir

    def ListSerie(self, args) :
        """
        List a serie directory
        """
        # args = json.loads(input)["args"]
        l_file = ""
        b_create = False

        if "create" in args :
            b_create = bool(args["create"])

        if args["source_dir"] not in self.DIRMNG :
            log("WARN : using first folder : {} !".format(self.DIRMNG[0]))
            return self.GetPlaylist(args, b_create)

        self.SKIP_DIR = args["skip_firsts_dir"]
        self.MAIN_SOURCE_DIR = args["source_dir"]
        self.DIR_ENCOURS = os.path.join(self.MAIN_SOURCE_DIR, Path.DIR_ENCOURS_NAME)
        if "saga" in args :
            l_index = int(args["index"])
            l_saga = args["saga"]
            info_files = FilesMng(self.MAIN_SOURCE_DIR, Path.DIR_ENCOURS_NAME)
            self.listeIndex = info_files.GetList(l_saga)
            args["path"] = self.listeIndex[l_index]

            return self.GetPlaylist(args, b_create)

        # log(args)
        a_saga = args["path"].split("|")
        if len(a_saga) > 1 :
            a_current = a_saga[1].split("/")
        else :
            a_saga = [ "1" ]
            log(a_saga)
            a_current = [ "2" ]
            log(a_current)
        l_s_filePath = args["path"]
        # We found the saga name in our dictionary :
        if a_saga[0] in self.sagas :
            l_saga = a_saga[0]
            l_s_filePath = os.path.join(self.DIR_ENCOURS, a_saga[0], a_current[1])
            if a_current[0] == a_saga[0] :
                l_s_filePath = os.path.join(self.DIR_ENCOURS, a_saga[1])
            #
            if not os.path.exists(l_s_filePath) :
                log("{} does not exists ".format(l_s_filePath))
                # l_s_filePath = os.path.join(self.MAIN_SOURCE_DIR, a_saga[1])

            # l_s_filePath = os.path.join(main_folder, self.sagas[a_saga[0]][0], a_saga[1])
            # log(l_s_filePath)
            # if len(self.sagas[a_saga[0]]) > 1 :
            #     l_s_filePath = os.path.join(main_folder, a_saga[0], a_saga[1])
                # return self.ListSeriesEnCours(args)
            # args["path"] = l_s_filePath
        elif a_current[0] in self.sagas :

            l_s_filePath = os.path.join(self.DIR_ENCOURS, *a_current)
            if not os.path.exists(l_s_filePath) :
                log("{} does not exists ".format(l_s_filePath))
                # if args["index"] > 0 :
                #     l_file = args["index"]
            # if a_current[0] == a_saga[0] :
            #     l_s_filePath = os.path.join(self.DIR_ENCOURS, a_saga[1])

        else :

            if a_current[0] == a_saga[0] :
                l_s_filePath = os.path.realpath(os.path.join(self.DIR_ENCOURS, a_saga[1]))
            if l_s_filePath == "1|2/" :
                # On renvoi la liste des series en cours
                l_s_filePath = os.path.realpath(os.path.join(self.DIR_ENCOURS, Path.DIR_ENCOURS_NAME + ".mp4"))
        l_s_filePath = os.path.realpath(l_s_filePath)
        args["path"] = l_s_filePath
        log(l_s_filePath)

        return self.GetPlaylist(args, b_create)

    def GetPlaylist(self, args, b_create = False) :
        """
        #Créer les liens nécessaires (si besoin)
        #path_file_local : le chemin du complet du fichier joué
        #l_i_keep_number : Le nombre de fichier a conserver dans le dossier (pour ne pas supprimer en cas de dodo...)
        #skip_first_dir : (OBSOLETE?) Si on doit sauter un dossier par rapport a celui d'origine
        """
        retour = {}

        l_s_filePath = args["path"]
        l_b_onFilm = False
        l_index = -1
        l_countLinks = []
        if "index" in args :
            l_index = int(args["index"])

        log(args)

        l_i_keep_number = int(args["keep"]) # Conserve les x derniers fichiers (sleep)
        if not self.ENCOURS != "" :
            self.ENCOURS = l_s_filePath


        # print("FFFF"+ str(l_s_filePath))
        self.SKIP_DIR = args["skip_firsts_dir"]
        if not os.path.exists(l_s_filePath) and os.path.exists(self.ENCOURS) :
            l_s_filePath = self.ENCOURS
        if os.path.exists(l_s_filePath) :
            path_file_local = os.path.realpath(l_s_filePath)

            # On recupere la liste de tout les fichiers du répertoire parent
            info_files = FilesMng(self.MAIN_SOURCE_DIR, Path.DIR_ENCOURS_NAME)
            l_infoFile = info_files.GetSeason(path_file_local)
            l_saga = l_infoFile[0]
            l_saison = l_infoFile[1]
            if l_saga not in FilesMng.EPS_LIST :
                info_files.GetAll(path_file_local)

            self.listeIndex = info_files.GetList(l_saga)

            l_idx = info_files.GetIndex(path_file_local)
            l_relPath = "../../"

            if l_index > -1 :
                l_idx = l_index

            # On calcule le premier index a renvoyer
            l_firstIndex = l_idx - l_i_keep_number

            # Si l'index est inférieur a 0
            if l_firstIndex < 0 :
                # On le remet a 0
                l_firstIndex = 0
            l_curIndex = l_firstIndex + 1

            l_countLinks = self.listeIndex[l_firstIndex:l_firstIndex + 10 + l_i_keep_number]
            # print("{} | {} | {} | {}".format(l_countLinks, l_idx, l_relPath, l_saga))
            l_countLinks = info_files.CreateNextLinks(l_countLinks, l_idx, l_relPath, l_saga, l_i_keep_number, False)

            l_time = -1
            if path_file_local in self.RUNTIME_VIDEO.keys() :
                l_time = self.RUNTIME_VIDEO[path_file_local][0]

            retour = { "name" : l_saga, "list" : l_countLinks, 'saison' : l_saison, 'index' : l_curIndex, 'new' : b_create, "count" : len(self.listeIndex), "time" : l_time} #, "file" : path_file_local, 'LL' : self.RUNTIME_VIDEO }

        return retour

    def UpdateLink(self, args) :
        try :
            path_file_local = os.path.realpath(args["path"])
            # print(args["path"] + " == " + path_file_local)
            # print(str(not path_file_local in self.listeIndex) + " == " + str(self.DIRMNG))
            l_i_keep_number = args["keep"]
            l_countLinks = []
            # if not path_file_local in self.listeIndex or not len(self.listeIndex) > 0 or self.MAIN_SOURCE_DIR == "":
                # if self.MAIN_SOURCE_DIR == "" :
            for folder in self.DIRMNG :
                if path_file_local.startswith(folder) :
                    self.MAIN_SOURCE_DIR = folder
                    self.DIR_ENCOURS = self.MAIN_SOURCE_DIR + "/" + Path.DIR_ENCOURS_NAME
                    self.SKIP_DIR = args["skip_firsts_dir"]
                    break
            # On recupere la liste de tout les fichiers du répertoire parent
            info_files = FilesMng(self.MAIN_SOURCE_DIR, Path.DIR_ENCOURS_NAME)
            l_infoFile = info_files.GetSeason(path_file_local)
            l_saga = l_infoFile[0]
            l_saison = l_infoFile[1]
            if l_saga not in FilesMng.EPS_LIST :
                info_files.GetAll(path_file_local)

            # if os.path.realpath(path_file_local) not in self.listeIndex :
                # # info_files.SetSerie(os.path.realpath(path_file_local), l_b_onFilm)
            self.listeIndex = info_files.GetList(l_saga)

            l_idx = info_files.GetIndex(path_file_local)

            l_relPath = "../"
            l_relPathEnCours = l_relPath
            l_relPath += "../"

            # On calcule le premier index a renvoyer
            l_firstIndex = l_idx - l_i_keep_number

            # Si l'index est inférieur a 0
            if l_firstIndex < 0 :
                # On le remet a 0
                l_firstIndex = 0
            l_curIndex = l_firstIndex + 1

            l_countLinks = self.listeIndex[l_firstIndex:l_firstIndex + 10 + l_i_keep_number]
            l_countLinks = info_files.CreateNextLinks(l_countLinks, l_idx, l_relPath, l_saga, l_i_keep_number, True)

            # On créer un lien vers le fichier que kodi va lire en démarrant
            paths_link = info_files.CreatePathLink(0, self.listeIndex[l_idx], l_relPathEnCours) + ["", True]
            paths_link[1] = Path.FILENAME_AUTOREAD + Path.FILENAME_AUTOREAD_EXT
            # print("OK : {}".format(paths_link))
            info_files.CreateLink(*paths_link)
            args["new"] = info_files.NEW
            args["index"] = l_firstIndex
            # print(args)
            return self.GetPlaylist(args, info_files.NEW)
        except Exception as e :
            log(e)

    def CleanEncours(self, args) :
        """
        Suppression du dossier de la série
        """
        l_saga = args["saga"]
        l_dirEnCours = os.path.join(args["source_dir"], Path.DIR_ENCOURS_NAME)

        l_dirSaga = os.path.join(l_dirEnCours, l_saga)
        if os.path.exists(l_dirSaga) :
            log("Suppression de : " + l_dirSaga)
            shutil.rmtree(l_dirSaga)
            return json.dumps({"deleted" : l_dirSaga})
        else :
            return json.dumps({"error" : "Erreur lors de la suppression de " + l_dirSaga})

    def GetTime(self, args) :
        """
        Get current video time
        """
        l_file = args["path"]

        return self.RUNTIME_VIDEO[l_file][0] + time.time() - self.RUNTIME_VIDEO[l_file][1]

    def SetTime(self, args) :
        """
        Set current video time
        """
        l_file = args["path"]
        l_time = args["time"]
        self.RUNTIME_VIDEO[l_file] = [l_time + 1, time.time()]
        # print(str(args) + " " + str(self.RUNTIME_VIDEO))
        if l_file in self.RUNTIME_VIDEO.keys() :
            return self.RUNTIME_VIDEO[l_file] # = [l_time + 1, time.time()]
        else :
            return 0
    # END Series & eps list

    # BEGIN Generix
    def GetThrow(self, dirs_video = [], tag = "") :
        if tag == "" :
            tag = CmdSocket.MAIN_CAT
        for l_dir in dirs_video :
            FILE_INDEX = os.path.join(l_dir, Path.DIR_ENCOURS_NAME, Path.INDEX_TAGGED)
            FILE_MD5 = os.path.join(l_dir, Path.DIR_ENCOURS_NAME, Path.INDEX_MD5)
            self.dicoMd5[l_dir] = Md5Mng(FILE_MD5)
            self.tags[l_dir] = TagsMng(FILE_INDEX, l_dir)
        # if "__general__" not in self.dicoJump :
        #     self.dicoJump["__general__"] = {}

        # Searching all entries to "throw"
        for k_dir in self.tags :
            self.dicoJump[k_dir] = self.tags[k_dir].SearchForTags(tag, False)
            self.dicoJump[k_dir]["__general__"] = self.tags[k_dir].SearchForPredefinedJumps()

    def TryToJump(self, args) :
        # Entrée de générique pour un fichier donné
        # l_file = args["path"]
        path_file_local = os.path.realpath(args["path"])
        l_a_backup = {}
        l_backup = ""

        retour = {}
        for folder in self.DIRMNG :
            if path_file_local.startswith(folder) or path_file_local.startswith(os.path.realpath(folder)) :
                self.MAIN_SOURCE_DIR = folder
                self.DIR_ENCOURS = self.MAIN_SOURCE_DIR + "/" + Path.DIR_ENCOURS_NAME
                self.SKIP_DIR = args["skip_firsts_dir"]

                # On recupere la liste de tout les fichiers du répertoire parent
                info_files = FilesMng(self.MAIN_SOURCE_DIR, Path.DIR_ENCOURS_NAME)
                l_infoFile = info_files.GetSeason(path_file_local)
                l_saga = l_infoFile[0]
                l_saison = l_infoFile[1]
                if l_saga not in FilesMng.EPS_LIST :
                    info_files.GetAll(path_file_local)

                # print("CALC Gen idx :: " + path_file_local)
                # if os.path.realpath(path_file_local) not in self.listeIndex :
                    # # info_files.SetSerie(os.path.realpath(path_file_local), l_b_onFilm)
                self.listeIndex = info_files.GetList(l_saga)

                # print("CALC Gen idx :: " + l_saga)
                l_idx = info_files.GetIndex(path_file_local)

                # l_realpath = os.path.realpath(l_file)
                info_files = FilesMng(self.MAIN_SOURCE_DIR, Path.DIR_ENCOURS_NAME)

                retour = self.ReadJSON(l_saga, l_idx)
                if len(retour) > 0 :
                    break

                log("End search : " + self.DIR_ENCOURS + "/" + l_saga)
                if l_saga != "" :
                    retour["saga"] = l_saga

        return retour

    def ReadJSON(self, l_saga, l_idx) :
        """ Lit le fichier JSON et renvoie les saut de l'épisode """
        retour = {}
        l_global = []
        l_dico_cut = {}
        l_s_fileName = os.path.basename(self.listeIndex[l_idx])
        # Avoid md5 computing, to freed socket
        if l_s_fileName in self.dicoMd5[self.MAIN_SOURCE_DIR].dicoMd5 :
            # print("MD5 for {} found from {}".format(l_s_fileName, self.MAIN_SOURCE_DIR))
            # l_s_md5 = self.dicoMd5[self.MAIN_SOURCE_DIR].GetMd5(self.listeIndex[l_idx])
            jumps = []
            # print(self.listeIndex[l_idx] + " " + str(self.dicoJump)+ " " +str()
            if self.listeIndex[l_idx] in self.dicoJump[self.MAIN_SOURCE_DIR] :
                # l = {}
                for j in self.dicoJump[self.MAIN_SOURCE_DIR][self.listeIndex[l_idx]] :
                    if float(j[1]) == 0 :
                        jumps.append(str(j[0]) +"+0")
                    else :
                        # print(j)
                        jumps.append(str(j[0]) +"+" + str(round(float(j[1]) - float(j[0]), 2)))
                retour[l_s_fileName] = jumps
        else :
            print("Searching MD5 for" + l_s_fileName)
            md5_thread = Thread(target = self.dicoMd5[self.MAIN_SOURCE_DIR].GetMd5, args=([self.listeIndex[l_idx], True]))
            md5_thread.start()

        if l_saga in self.dicoJump[self.MAIN_SOURCE_DIR]["__general__"] :
            if len(self.dicoJump[self.MAIN_SOURCE_DIR]["__general__"][l_saga]) > 0 :
                retour = [ self.dicoJump[self.MAIN_SOURCE_DIR]["__general__"][l_saga], retour ]

        return retour

    def SearchSaga(self, l_s_path, dico) :
        retour = ""
        for l_saga in self.dicoTag["__general__"]["__sagas__"] :
            if l_saga not in self.dicoJump["__general__"] :
                self.dicoJump["__general__"][l_saga] = []

            if l_s_path in self.dicoTag :
                if os.path.join("", l_saga) in self.dicoTag[l_s_path]["fullpath"] :
                    retour = l_saga
                    break

        if retour == "" :
            print(l_s_path + " not found in list !")
        return retour

    def SetGeneric(self, args) :
        """
        Enregistre les infos du générique d'un épisode
        """
        l_file = args["path"]
        for folder in self.DIRMNG :
            if l_file.startswith(folder) :
                self.MAIN_SOURCE_DIR = folder
                self.DIR_ENCOURS = self.MAIN_SOURCE_DIR + "/" + Path.DIR_ENCOURS_NAME

                FILE_INDEX = os.path.join(self.DIR_ENCOURS, ".index.json")
                break

        l_file = args["path"]
        l_realpath = os.path.realpath(l_file)
        l_filename = os.path.basename(l_realpath)
        # l_md5 = md5(l_realpath)
        l_reset = False
        l_infoGeneric = args["skip_info"][0]

        if "reset" in args :
            l_reset = True

        l_s_fileName = os.path.basename(l_file)
        l_s_md5 = self.dicoMd5[self.MAIN_SOURCE_DIR].GetMd5(l_file)

        a_tsp = l_infoGeneric.split("+")
        a_tsp[0] = float(a_tsp[0])
        a_tsp[1] = float(a_tsp[1]) + float(a_tsp[0])

        if a_tsp[0] < 0 :
            a_tsp[0] = 0

        retour = self.tags[self.MAIN_SOURCE_DIR].NewTag(l_s_md5, l_file, CmdSocket.MAIN_CAT, a_tsp)

        if l_file not in self.dicoJump[self.MAIN_SOURCE_DIR] :
            self.dicoJump[self.MAIN_SOURCE_DIR][l_file] = []
        self.dicoJump[self.MAIN_SOURCE_DIR][l_file].append(retour[0:2])

        return retour

# BEGIN Main functions

# BEGIN Files transfer

    def sendPlaylist(self, path_file_local, host_from, host_to, port) :
        """ Send a file via socket """
        #HOST = host_from
        retour = ""
        #PORT = 9876 + 1
        ADDR = (host_to,port)
        # BUFSIZE = 4096
        videofile = str(os.path.realpath(path_file_local))
        #log("Sending file " + str(videofile) + " to " + host_to)

        try :
            
            ClientSocket().ClientSendFile(ADDR, videofile)
            
            retour = "Copy " + path_file_local + " from " + host_from + " to " + host_to + "."
        except Exception as e:
            log("error sendPlaylist" + str(e))
            retour = {"error" : str(e)}
        finally :
            return json.dumps(retour)

    def SynchPlaylist(self, paths_file_local, host_from, host_to, port) :
        """
        # TODO : Faire quelque chose pour les ports...
        Synchronize files between hosts
        """
        retour = ""
        l_idx = 0
        for l_s_fileInfo in paths_file_local :
            l_s_file = l_s_fileInfo[0]
            l_s_size = l_s_fileInfo[1]
            l_s_filePath = os.path.realpath(l_s_file)
            if not os.path.exists(l_s_filePath) :
                
                total, used, free = shutil.disk_usage(self.DIR_ENCOURS)
                # On vérifie qu'il reste de la place sur le disque
                if not l_s_size > free :
                    #print("Total: %d GiB" % (total // (2**30)))
                    #print("Used: %d GiB" % (used // (2**30)))
                    #print("Free: %d GiB" % (free))
                
                    log("Receive " + l_s_filePath + " ... from " + host_from + " to " + host_to)
                    
                    # TODO 
                    #port = 9876 + 1
                    
                    # On lance l'ecoute sur le socket
                    s = socket.socket()
                    s.bind(('',port))
                    s.listen(1)

                    # On envoie la requete dans un thread qui va nous envoyer un fichier via le socket
                    send_thread = Thread(target = ClientSocket().ClientSync, args=(host_from, host_to, l_s_file))
                    send_thread.start()

                    c, addr = s.accept()
                    data = c.recv(1024)

                    # On créer le répertoire
                    if not os.path.exists(os.path.dirname(l_s_filePath)) :
                        #log(os.path.dirname(l_s_filePath))
                        os.makedirs(os.path.dirname(l_s_filePath))
    
                    # On ouvre le fichier
                    myfile = open(l_s_filePath, 'wb')
                    while data != bytes(''.encode()):
                        # On écrit dans le fichier
                        myfile.write(data)
                        # On recoit les datas
                        data = c.recv(1024)
                    # On ferme le fichier
                    myfile.close()
                    # On ferme la connexion
                    c.close()
                    l_idx += 1
                else :
                    log("No space left on device... on " + host_to)
                    break
                    
        return json.dumps({"count" : l_idx})
# END Files transfer

        

# END MAIN

# BEGIN GENERIC

    """
    Envoie la liste des épisodes qui n'ont pas d'entrée de générique définie
    """
    def GetMissGenerix(self, args) :
        l_saga = ""
        if "saga" in args :
            l_saga = args["saga"]
        l_index = 0
        if "index" in args :
            l_index = args["index"]

        l_tag = arg["tag"]

        dir_list = []
        if l_saga != "" :
            with open(self.DIR_ENCOURS + "/." + l_saga + ".json", 'rb') as f:
                retour = json.load(f)

            if not len(self.listeIndex) > 0 or self.SAGA != l_saga :
                #self.listArray = {}
                l_file = ""
                l_s_path = os.path.join(self.DIR_ENCOURS, l_saga)
                if os.path.isdir(l_s_path) :
                    l_a_dir = os.listdir(os.path.join(self.DIR_ENCOURS, l_saga))
                    if len(l_a_dir) > 0 :
                        l_file = os.path.realpath(os.path.join(l_s_path, l_a_dir[0]))
                    # print(l_file + " " + l_saga)

                info_files = FilesMng(self.MAIN_SOURCE_DIR, Path.DIR_ENCOURS_NAME)
                info_files.SetSerie(l_file)
                self.listeIndex = info_files.GetSerie()
                self.SAGA = l_saga
                for l_eps in self.listeIndex :
                    self.listArray[os.path.basename(l_eps)] = os.path.dirname(l_eps)

                # print("FFFF : " + str(self.listArray))


            for l_custom in retour["custom"] :
                l_b_isEmpty = True
                # Verfie si on est sur les anciens modeles de fichiers
                if "jumps" in retour["custom"][l_custom] :
                    l_array = retour["custom"][l_custom]["jumps"]
                else :
                    l_array = retour["custom"][l_custom]

                if len(l_array) > 0 :
                    for l_jump in l_array :
                        #log(l_jump)
                        if l_jump["duration"] != 0 and l_jump["time"] != 0 :
                            l_b_isEmpty = False
                if l_b_isEmpty :
                    if l_custom in self.listArray :
                        dir_list.append(os.path.join(self.listArray[l_custom], l_custom))
                    else :
                        dir_list.append("NOT FOUND :" + l_custom)
            return dir_list[l_index:10]

        # Le chemin du dossier ENCOURS existe bien
        if os.path.exists(self.DIR_ENCOURS) :
            # On liste les dossiers présents
            for l_s_dir in os.listdir(self.DIR_ENCOURS) :
                # Si le fichier de générique existe
                if os.path.isfile(self.DIR_ENCOURS + "/." + l_s_dir + ".json") :
                    dir_list.append(l_s_dir)
                    #file_list = sorted(os.listdir(self.DIR_ENCOURS + "/" + l_s_dir))
                    #if len(file_list) > 0 :
                        #dir_list[l_s_dir] = []
                        #dir_list[l_s_dir] = file_list[0]
                        ##log("LLL:" + l_s_dir+ " : " ) # + "*"+ str(file_list))


        return dir_list


# END GENERIC

def log(msg) :
    if DEBUG :
        print(str(msg))

"""
Calcule la somme md5 d'un fichier
"""
def md5(fname) :
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
            
    return hash_md5.hexdigest()

def logXbmc(log_str) :
    log("### " + str(log_str))

# arguments de ligne de commande
def SetConfig(argv) :
    try:
         opts, args = getopt.getopt(argv, usage.GetOpts())
        # opts = help.DicoArgs(argv)
    except getopt.GetoptError as e:
        # print(e)
        log(e)
        usage.usage()
        # return e
    except Exception as ex:
        log(str(e))
        #print(e)
        usage.usage()
        sys.exit(2) 
    try:
        global PATH2MONITOR

        for opt, arg in opts :
            if opt ==  "--help" :
                #usage()
                usage.usage()
                sys.exit(0)
            elif opt in ("-p", "--port") :
                #print(str(arg))
                global PORT
                PORT = int(arg)
            elif opt in ("-d", "--debug") :
                global DEBUG
                DEBUG = True

        for arg in args :
            if os.path.exists(arg) and os.path.isdir(arg) :
                PATH2MONITOR.append(arg.rstrip("/"))
            else :
                print(arg.rstrip("/") + " does not esist !")

        if not len(PATH2MONITOR) > 0 :
            print('No folder to monitoring specified !')
            usage.usage()
            return False
        else :
            return True

        # else :
            # raise Exception('No folder to monitoring specified !')

                #conf_user.PATH_INSTALL = os.path.realpath(arg)
                #print(copymirrorlist)
            #elif opt in ("-G"):
                #global copykeyring 
                #copykeyring = False
            #elif opt in ("-a", "--arch"):
                #conf_user.NEW_ARCH = arg
    except Exception as e :
        print(str(e))
        return False


DEBUG = True
# host empty to listen on whole network...
HOST = CmdSocket.DEFAULT_HOST
PORT = CmdSocket.DEFAULT_PORT
#PORT = 16666
#ADDR = (HOST,PORT)
BUFSIZE = CmdSocket.BUFSIZE
PATH2MONITOR = []
#DIR_VID = "/nfs/data/videos"
#DIR_ENCOURS = DIR_VID + "/" + DIR_ENCOURS_NAME


#NB_TOLINK = 1

#tree = etree.parse("~/.kodi/userdata/addon_data/script.service.koditv/settings.xml")
#for user in tree.xpath("/settings/category"):
    #print(user.get("category"))

if not SetConfig(sys.argv[1:]) :
    # help.usage(sys.argv[1:])
    sys.exit(2)

ADDON_ID_FK = 'plugin.video.synkodi'
#MyKodi = kodiUtils.KodiJson("192.168.0.30", "kodi", "kodi4pix")
#MyKodi2 = kodiUtils.KodiJson("192.168.0.31", "kodi", "pi2kodi")
#log(MyKodi.GetVideoPath())
#log(MyKodi.GetVideoTime())
#MyKodi2.RunVideo(MyKodi.GetVideoPath())
#time.sleep(2)
#MyKodi2.SeekTo(MyKodi.GetVideoTime(False))
##log(PATH2MONITOR)
#hostname = socket.gethostname()

#local_ip = socket.gethostbyname("pi4.numericable.fr")
# print("ok" + os.path.realpath("~/.kodi/userdata/guisettings.xml"))
# truc = kodiUtils.kodiXmlConf().GetSocketConf("/home/dux/.kodi/userdata/guisettings.xml", "services.webserverpassword")
# print(truc)

VideoSocket(PATH2MONITOR, HOST, PORT)

#print(hostname + " " + local_ip)
#ADDON_ID_SYNCH = 'plugin.video.synkodi'
#myKodi = kodiUtils.KodiJson("192.168.0.35", "kodi", "kodi4pix", "8080")
#resQry = myKodi.ExecuteAddon(ADDON_ID_SYNCH)
#log(resQry)

#log(os.listdir("../../"))


print( '#######END############' )
