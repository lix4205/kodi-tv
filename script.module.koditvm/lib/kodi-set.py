# Copyright (c) Elie Coutaud 2024
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!/usr/bin/python
# -*- coding: latin-1 -*-
import os
import json
import sys
from koditvm import kodiUtils
from koditvm.QueriesSocket import QuerySocket
import time
from koditvm.dbus_mpris import Player
from koditvm.kodi_mpris import KodiPlayer
from threading import Thread
from urllib.parse import unquote
from koditvm.pythcomm.helper_help import usage
import koditvm.pythcomm.txt_utils as txt
import getopt
from random import uniform
from koditvm.play_tags import PlayTags
from koditvm.Constants import CmdSocket

HOSTS = "localhost"
USER = "kodi"
PASS = ""
PORT = "8080"
SYNCHRO = True
NOEXIT = False
OUTPUT_CMD = "2> /dev/null"
SAMPLE = False
DAEMON_MODE = False
TAG = CmdSocket.MAIN_CAT
VLC_ARG = ""
VLC_OPT = "--play-and-exit --no-random"
VLC_CMD = "vlc"
SOCKET = 9876
AUDIO_MASTER = False
PATH2REPLACE = "/media/pix"
PATH2REMOVEROOT = "/nfs"
PREFIX_SHARE = "nfs://"

PATH2REMOVE = PREFIX_SHARE + HOSTS + PATH2REMOVEROOT

# BEGIN Help
usage.OPTIONS_ALIAS = {
    "h" : "host",
    "N" : "no-synchro",
    "u" : "user",
    "p" : "port",
    "P" : "password",
    "S" : "socket",
    "v" : "verbose",
    "n" : "tag",
    "A" : "audio",
    "f" : "fullscreen",
    "r" : "replace"

}
usage.OPTIONS_VALUES = {
    "h" : "host",
    "u" : "kodi user",
    "p" : "kodi port",
    "P" : "kodi password",
    "S" : "socket port",
    "n" : "categorie/tag",
    "r" : "original,replacement[:prefix_share]",
    "video-x" : "width",
    "video-y" : "height",
    "crop" : "(4:3,16:9,...)",
    "--" : "vlc options"
}

usage.OPTIONS_HELP = {
    "h" : "host",
    "u" : "kodi user",
    "p" : "kodi port",
    "P" : "kodi password",
    "N" : "run without synchro",
    "S" : "socket port",
    "f": "Fullscreen",
    "n" : "Tag reccording",
    "A" : "Audio master",
    "r" : "Replace \"original\" by \"replacement\"",
    "sample" : "Sample mode (play 10s then next video) ",
    "no-exit": "Do not exit after end of playlist",
    "cvlc" : "Do not use vlc Interface",
    "video-x" : "Window width",
    "video-y" : "Window height",
    "--" : "vlc options",
    "crop" : "crop image",
    "v" : "Verbose mode (vlc)",
    "path" : "Add local files to playlist (sample mode)"

}

# usage.OPTIONS_MANDATORY = []
usage.OPTIONS_ARGS = ["path"]
usage.TEXT = "python " + sys.argv[0]
usage.DESC = "Run vlc instance with socket/kodi playing file on host [host(default : localhost)].\nCredentials are required if kodi is involved.\nDefault socket port is 9876.\nAudio master allow you to handle kodi sound.\n"
# END Help

class VlcMove(PlayTags) :
    JUMPS = [ 10, 30, 60, 180, 300, 600 ]
    MAX_OFFSET = 0.417

    def GetSocket(HOST, MAIN_SOURCE_PORT) :
        try :
            socket = QuerySocket(HOST, MAIN_SOURCE_PORT)
            # print("!!!" + str(socket) + " !!!")
            # if "error" in socket :
            #     return None
            if not len(socket.Init()) > 0 :
                return None
            return socket

        except KeyboardInterrupt as e:
            raise KeyboardInterrupt(e)

    def ThreadMainSocket(self) :
        if len(self.hosts) > 1 :
            while True :
                idx = 0
                s = self.socket
                if s is not None :
                    cur_socket = s.GetAddr()

                for h in self.hosts :

                    if not (s is not None and h == s.GetAddr()) :
                        socket = VlcMove.GetSocket(*h.split(":"))
                        if socket is not None :
                            # print("{} < {}".format(idx , self.hosts.index(cur_socket)))
                            if idx < self.hosts.index(cur_socket) :
                                print("Best socket found : {}".format(socket.GetAddr()))
                                # self.SetSocket(self.catSearch, self.catSearch, 0, self.player)
                                # self.socket = None
                                # while self.socket is None :
                                #     self.SetSocket(self.catSearch, self.catSearch, 1, self.player)
                                # Get authentication information from socket
                                ws = socket.Query("webserver")
                                # while "error" in ws :
                                #     ws = socket.Query("webserver")
                                    # print(str(ws) + " " + str(self.socket.ADDR))
                                if len(ws) > 0 :
                                    kodi = KodiPlayer(socket.HOST_SOCKET, *ws)

                                if kodi.IsPlaying() :
                                    self.socket = socket
                                    self.kodi = kodi

                                    sys.stdout.write('\033[2K\033[1G')
                                    print("Using kodi at : {}".format(self.kodi.GetAddr()))
                                    k_track = self.kodi.GetTrackPath(PATH2REMOVEROOT, PATH2REPLACE)
                                    while k_track == "" :
                                        time.sleep(.2)
                                    k_track = os.path.realpath(k_track)
                                    if k_track != os.path.realpath(self.eps) :
                                        while k_track != os.path.realpath(self.eps) :
                                            print('ThreadMainSocket 1' + k_track + " != " + self.eps)
                                            time.sleep(.2)
                                        # while self.t_synchro is not None :
                                        #     time.sleep(.2)
                                        self.t_synchro = None
                                        self.afterLoop()
                                # else :
                                    pos = self.kodi.Position()
                                    print('ThreadMainSocket 1 ' + k_track + " == " + self.eps + " " + str(pos))
                                    self.kodi.Seek(pos - 10)
                                    self.player.Seek(pos)
                            else :
                                if not self.kodi.Exists() and len(self.hosts) > 1 :
                                    print("SEARCH new player 1 !")
                                    self.socket = None
                                    while self.socket is None :
                                        self.SetSocket(self.catSearch, self.catSearch, 1, self.player)
                                    if self.kodi.IsPlaying() :
                                        k_track = self.kodi.GetTrackPath(PATH2REMOVEROOT, PATH2REPLACE)
                                        while k_track == "" :
                                            time.sleep(.2)
                                        k_track = os.path.realpath(k_track)
                                        if k_track != os.path.realpath(self.eps) :
                                            while k_track != os.path.realpath(self.eps) :
                                                print('ThreadMainSocket 2' + k_track + " != " + self.eps)
                                                time.sleep(.2)
                                            # while self.t_synchro is not None :
                                            #     time.sleep(.2)
                                            self.t_synchro = None
                                            self.afterLoop()
                                        else :
                                            self.kodi.Seek(self.player.Position())

                                        pos = self.kodi.Position()
                                        print('ThreadMainSocket 2' + k_track + " == " + self.eps + " " + str(pos))
                                        self.kodi.Seek(pos - 10)

                    else :
                        if not self.kodi.Exists() and len(self.hosts) > 1 :
                            print("SEARCH new player 2 !")
                            self.socket = None
                            while self.socket is None :
                                self.SetSocket(self.catSearch, self.catSearch, 0, self.player)
                            if self.kodi.IsPlaying() :
                                k_track = self.kodi.GetTrackPath(PATH2REMOVEROOT, PATH2REPLACE)
                                while k_track == "" :
                                    time.sleep(.2)
                                k_track = os.path.realpath(k_track)
                                if k_track != os.path.realpath(self.eps) :
                                    while k_track != os.path.realpath(self.eps) :
                                        print('ThreadMainSocket 3 ' + k_track + " != " + self.eps)
                                        time.sleep(.2)
                                    # while self.t_synchro is not None :
                                    #     time.sleep(.2)
                                    self.t_synchro = None
                                    self.afterLoop()
                                else :
                                    self.kodi.Seek(self.player.Position())

                                pos = self.kodi.Position()
                                print('ThreadMainSocket 3' + k_track + " == " + self.eps + " " + str(pos))
                                self.kodi.Seek(pos - 10)

                    idx += 1

    def ThreadVolume(self) :
        """ Synchronize volumes between player and kodi """
        try :
            while self.player.Exists() :
                volPlayer = self.player.Volume()
                volkodi = self.kodi.Volume()

                # Synchronize volumes between both instances
                if AUDIO_MASTER :
                    volvlc = int(volPlayer * 100)

                    if volvlc <= 100 :
                        if volkodi - volvlc != 0 :
                            # We sleep because if player has exited, volume is at max (eg : 1.0)s
                            time.sleep(.1)
                            if self.player.Exists() :
                                self.kodi.Volume(volvlc / 100)
                else :
                    self.player.Volume(volkodi / 100)

            print("End volume thread")
            return True
        except Exception as e :
            print("Error ThreadVolume : " + str(e))
            return False

    def ThreadSynchro(self) :
        try :
            b_wait = False
            # Waiting new player instance
            while self.kodi.Exists() and self.eps == "" :
                time.sleep(.3)

            k_track = os.path.realpath(self.kodi.GetTrackPath(PATH2REMOVEROOT, PATH2REPLACE))
            # First played track is not the same in kodi
            # while k_track != self.player.track :
            #     time.sleep(.3)
            #     if self.eps == k_track :
            #         break

            while self.eps == k_track and self.player.Exists() :
                # print(self.kodi.Exists())
                # pos = self.player.Position()
                # Only if kodi is playing
                if self.kodi.IsPaused() :
                    self.m_synchroDone = False
                    pos = self.player.Position()
                    time.sleep(.5)
                    if self.searching :
                        if pos != self.player.Position() and (self.kodi.Position() - self.player.Position() > .5 or self.kodi.Position() - self.player.Position() < .5) :
                            self.kodi.Seek(self.player.Position())
                        continue

                # if self.player.IsPaused() :
                #     new_pos = self.player.Position() - pos
                #     print(new_pos)
                #     if new_pos > 1 or new_pos < -1 :
                #         self.kodi.Seek(self.player.Position())
                #         time.sleep(1)

                if SYNCHRO :
                    self.SetSynchro(self.kodi, self.player)


                    # print('ThreadSynchro ' + k_track + " == " + self.eps)
            return True
        except Exception as e :
            print('ThreadSynchro' + str(e))
            return False
        finally :
            # print("End synch thread")
            self.t_synchro = None

    def SetSynchro(self, myKodi, myVlc):
        """ Synchronize remote kodi video time on local kodi video... """
        try :
            # max_offset = Player.MAX_OFFSET
            # m_seek = 1000
            # # if not myKodi.Exists() :
            # #     return -1
            # # if myKodi.Paused() :
            # #     time.sleep(1)
            # #     self.m_synchroDone = False
            # #
            # l_i_secs = 0.5
            # if not self.m_synchroDone :
            #     while l_i_secs > max_offset and not self.searching :
            #         l_i_secs = self.player.SynchPlayers([myKodi])
            #         # l_i_secs = VlcMove.GetOffset(myKodi, myVlc, max_offset)
            #     self.m_synchroDone = True
            # else :
            #     # Searching for 1.5 offset
            #     l_i_secs = self.player.SynchPlayers([myKodi])
            #     # l_i_secs = VlcMove.GetOffset(myKodi, myVlc, max_offset)
            #     if l_i_secs > max_offset or l_i_secs < -max_offset :
            #         self.player.Seek(-m_seek, False)
            #         # l_i_secs = VlcMove.GetOffset(myKodi, myVlc, max_offset)
            #
            #         l_i_secs = self.player.SynchPlayers([myKodi])
            l_i_secs = self.player.SynchPlayers([myKodi])
            return l_i_secs
        except Exception as e :
            print("SetSynchro" + str(e))
            return 0

    def GetPlaylist(self, pathVideo, dir_vid, index = -1) :
        """ Get current videos list (via socket) """
        try :
            path_video = []
            if pathVideo != "" :
                self.socket.CallPlaylist(pathVideo, "", dir_vid, 0, 0, index)
            # else :
            #     print("/media/xud/DL/ENCOURS|2/1")
            #     self.socket.CallPlaylist("/media/xud/DL/ENCOURS|2/1", "", dir_vid, 0, 0, index)

            # print(self.socket.LIST)
            if "error" in self.socket.LIST :
                print("EE : " + str(self.socket.LIST["error"]))
                return []
            for l_s_eps in self.socket.LIST :
                path_video.append(getRealPath(l_s_eps).replace(PATH2REMOVEROOT, PATH2REPLACE).replace("`", "\\`").replace("$", "\\$"))

            return path_video
        except Exception as e:
            print("GetPlaylist" + str(e))
            return []

    def PrevNextSample(self, direction, goToEnd = True) :
        pos = self.player.Position()
        if len(self.dico_eps) > 0 :
            goTo = self.GoToSample(direction, pos, goToEnd)
            self.cur_smp = goTo

            begin = goTo[0]
            end = goTo[1]
            if self.MASTER or not self.kodi.Exists() :
                # self.player.Seek(-(self.player.Position() - begin))
                self.player.GoTo(begin)
            else :
                self.kodi.Seek(begin)

            self.target = end
            self.m_synchroDone = True
        else :
            # Seek kodi
            seekTo = pos + self.duration * direction * 0.1
            self.kodi.Seek(seekTo)
            self.player.GoTo(seekTo, self.player.GetTrackPath())
            # self.player.SeekTo(seekTo)

    def SetPrefix(self) :
        str_replace = " "
        PlayTags.PREFIX_MSG = "{} KDI "
        if self.MASTER :
            str_replace = "(M)"

        if self.ADD_SAMPLE :
            PlayTags.PREFIX_MSG = "{} ADD "
        if self.NEW_SAMPLE > 0 :
            PlayTags.PREFIX_MSG = "{} NEW "
        if self.DEL_SAMPLE > 0 :
            PlayTags.PREFIX_MSG = "{} DEL "

        PlayTags.PREFIX_MSG = PlayTags.PREFIX_MSG.format(str_replace)

    def SetNextSample(self) :
        """ Executed after new playing video """
        try :
            # Saving new sample with end time as duration
            if self.eps not in Player.track_list :
                self.player.SetTrackInfo(self.eps)

            new_tag = self.SaveSample(Player.track_list[self.eps].duration)
            if len(new_tag) > 0 :
                self.socket.ReloadGenerix(self.socket.DIRS)

            # Now we're gonna take care of new track
            self.eps = self.player.track
            self.duration = self.player.track_duration
            # Get dico sample
            self.dico_eps = self.GetDicoEps(not self.TV_MODE, -1)
            # We will filter
            if self.eps not in self.dicoDone :
                self.dicoDone[self.eps] = 0

            if self.eps not in self.count_vid :
                self.count_vid[self.eps] = 0

            self.SetRoot()

            if len(self.dico_eps) > 0 :
                cnt_smp = self.count_vid[self.eps]
                if cnt_smp >= len(self.dico_eps) :
                    raise Exception("WEIRD !!!" + self.eps + " :: {}/{}".format(cnt_smp, len(self.dico_eps)) )

                begin = self.dico_eps[cnt_smp][0]
                end = self.dico_eps[cnt_smp][1]
                self.target = begin

                self.dicoDone[self.eps] = self.player.Position()
                if end <= begin :
                    print(os.path.basename(self.eps) + " has bad timestamps :" + str(begin) + " = " + str(end) + " > " + str(end + 2))
            self.ADD_SAMPLE = False
        except Exception as e :
            raise Exception("SetNextSample : " + str(e))

    def CheckNewEps(self) :
        try :
            """ Check if new episode is playing on kodi, synchronise otherwise """
            vidRemote = self.kodi.GetTrackPath()
            # volkodi = self.kodi.Volume()
            if vidRemote != "" :
                track = os.path.realpath(self.player.track)
                if self.player.IsStopped() :
                    # On va sortir de la boucle
                    self.player.track = ""
                elif getRealPath(vidRemote) != track :
                    # VLC master mode

                    if self.MASTER :
                        while track != getRealPath(vidRemote) :
                            # Wait for kodi to play next video
                            while vidRemote == self.kodi.GetTrackPath() or self.kodi.GetTrackPath() == "" :
                                if track == getRealPath(self.kodi.GetTrackPath()) :
                                    break

                                if not self.player.Exists() :
                                    print("break {}".format(self.proc.is_alive()))
                                    break

                                if getRealPath(vidRemote) in Player.track_list :
                                    idx_cur = list(Player.track_list.keys()).index(getRealPath(vidRemote))
                                    idx_new = list(Player.track_list.keys()).index(track)
                                    if Player.track_list[getRealPath(vidRemote)].duration <= 0 :
                                        Player.track_list[getRealPath(vidRemote)].duration = self.kodi.GetTrackDuration()
                                    if idx_new > idx_cur :
                                        self.kodi.Next()
                                    else :
                                        self.kodi.Previous()
                                    time.sleep(0.5)
                                else :
                                    print("ntm2")
                                    time.sleep(1)
                                    # Get current kodi playing video
                                    self.playlist = self.GetPlaylist(vidRemote, os.path.realpath(self.socket.DIRS[0]))
                                    self.player.Run("file://" + getRealPath(vidRemote))

                                    while getRealPath(vidRemote) not in Player.track_list :
                                        self.player.SetTrackList()

                                    t_id = Player.track_list[getRealPath(vidRemote)].track_id
                                    for f in self.playlist[1:] :
                                        self.player.AddTrack("file://" + f, t_id)
                                        self.player.Next()

                                        while f not in Player.track_list :
                                            self.player.SetTrackList()
                                        t_id = Player.track_list[f].track_id
                                    self.player.SetPosition(Player.track_list[getRealPath(vidRemote)].track_id)

                            vidRemote = self.kodi.GetTrackPath()
                            if not self.player.Exists() :
                                break

                    else :
                        if getRealPath(vidRemote) in Player.track_list :
                            t_id = Player.track_list[getRealPath(vidRemote)].track_id
                            self.player.Interface('TrackList').GoTo(t_id)
                            time.sleep(.5)
                        else :
                            time.sleep(1)
                            # Get current kodi playing video
                            while self.socket is None :
                                print("Wait...1")
                                time.sleep(.2)
                            self.playlist = self.GetPlaylist(vidRemote, os.path.realpath(self.socket.DIRS[0]))
                            self.player.Run("file://" + getRealPath(vidRemote))

                            while getRealPath(vidRemote) not in Player.track_list :
                                self.player.SetTrackList()

                            t_id = Player.track_list[getRealPath(vidRemote)].track_id
                            for f in self.playlist[1:] :
                                self.player.AddTrack("file://" + f, t_id)
                                self.player.Next()

                                while f not in Player.track_list :
                                    self.player.SetTrackList()
                                t_id = Player.track_list[f].track_id

                            self.player.SetPosition(Player.track_list[getRealPath(vidRemote)].track_id)

                            time.sleep(1)

                else :
                    pos = self.player.Position()
                    # self.SetCurrentSample(pos)

                    if self.NEW_SAMPLE > 0 :
                        # If we're already on a sample, then we move to nearly end
                        if len(self.cur_smp) > 0 :
                            # print(self.cur_smp)
                            on_tag = self.cur_smp[1]
                            if on_tag > 0 and on_tag - self.kodi.Position() > 2:
                                # print("GOTO : {}".format(txt.HRTime(on_tag - 1)))
                                self.overwrite += 1
                                if self.MASTER :
                                    self.player.Seek((on_tag - pos) - 1)
                                else :
                                    self.kodi.Seek(on_tag - 1)
                            if self.cur_smp[0] < self.NEW_SAMPLE :
                                self.NEW_SAMPLE = self.cur_smp[0]

                    if self.READ_MODE :
                        if len(self.dico_eps) > 0 :
                            if len(self.cur_smp) > 0 and self.cur_smp in self.dico_eps and self.dico_eps.index(self.cur_smp) == len(self.dico_eps) - 1 and pos > self.cur_smp[1] - 1 :
                                while pos > self.cur_smp[1] :
                                    time.sleep(.1)
                                print("ioj {}".format(Player.track_list[getRealPath(vidRemote)].duration))
                                if self.MASTER :
                                    self.player.Seek(Player.track_list[getRealPath(vidRemote)].duration - 0.5)
                                else :
                                    self.kodi.Seek(Player.track_list[getRealPath(vidRemote)].duration - 0.5)
                                time.sleep(1)
                            else :
                                if not len(self.cur_smp) > 0 or self.target > self.cur_smp[1] :
                                    self.PrevNextSample(1, self.ADD_SAMPLE)
                                self.m_synchroDone = True

                    if self.MASTER :
                        idx = 0
                        # Get position changes
                        new_pos_kodi = self.player.Position()
                        pos_kodi = self.kodi.Position()

                        if self.player.Exists() and new_pos_kodi > 0 :
                            # If delta position is more than 2s, ...
                            if pos_kodi - new_pos_kodi > 2 :
                                # Check for repetitive action for 0.5s
                                while self.player.Position() - new_pos_kodi < 1 :
                                    print("INF :: {}".format(self.player.Position() - new_pos_kodi))
                                    new_pos_kodi = self.player.Position()
                                    time.sleep(.1)
                                    idx += 1
                                    if idx == 10 :
                                        break
                                self.kodi.Seek(new_pos_kodi)
                                while self.kodi.Position() < new_pos_kodi :
                                    time.sleep(.1)
                            elif pos_kodi - new_pos_kodi < -2 :
                                # Check for repetitive action for 0.5s
                                while self.player.Position() - new_pos_kodi > -1 :
                                    # print(self.player.Position() - new_pos_kodi)
                                    print("SUP :: {}".format(self.player.Position() - new_pos_kodi))
                                    new_pos_kodi = self.player.Position()
                                    time.sleep(.1)
                                    idx += 1
                                    if idx == 10 :
                                        break
                                self.kodi.Seek(new_pos_kodi)
                                while self.kodi.Position() > new_pos_kodi :
                                    time.sleep(.1)
        except Exception as e :
            raise Exception("CheckNewEps " + str(e))

            # # Synchronize volumes between both instances
            # if AUDIO_MASTER :
            #     volvlc = int(self.player.Volume() * 100)
            #
            #     if volvlc <= 100 :
            #         if volkodi - volvlc != 0 :
            #             self.kodi.Volume(volvlc / 100)
            # else :
            #     # volkodi = self.kodi.Volume()
            #     self.player.Volume(volkodi / 100)

# BEGIN Events
    def endLoop(self) :
        # if we want to stop kodi, we put self.eps as empty
        try :
            if self.STOPPED :
                self.player.track = ""
                return False

            self.CheckNewEps()
            if self.kodi.Exists() and self.t_synchro is None  :
                # print("Synch relaunched !")
                self.t_synchro = Thread(target=self.ThreadSynchro)
                self.t_synchro.start()
        except Exception as e :
            raise Exception("endLoop " + str(e))

    def afterLoop(self) :
        """ Stream has change, ... """
        if self.eps != "" :
            # Get kodi running video
            self.vidRemote = self.kodi.GetTrackPath()
            # And playlist from socket
            while self.socket is None :
                print("Wait...2")
                time.sleep(.2)
            self.playlist = self.GetPlaylist(self.vidRemote, os.path.realpath(self.socket.DIRS[0]))

            if getRealPath(self.vidRemote) in Player.track_list :
                if Player.track_list[getRealPath(self.vidRemote)].duration <= 0 :
                    Player.track_list[getRealPath(self.vidRemote)].duration = self.kodi.GetTrackDuration()

        if self.player.Exists() :
            self.SetNextSample()
            self.PrintRunTime(self.TV_MODE)
            self.m_synchroDone = False
            self.SetRoot()
            # while self.t_synchro is not None :
            #     time.sleep(.05)
            if self.t_synchro is None :
                # Wait for last synchro to stop
                t = Thread(target=self.ThreadRunTime)
                t.start()
                self.t_synchro = Thread(target=self.ThreadSynchro)
                self.t_synchro.start()

            # print(self.player.t_rate)
            # print(self.player.m_rate)
            self.overwrite = 0

    def onFullScreenChange(self) :
        nb_click = self.player.FullScreened(0, 4, False)
        if nb_click == 1 :
            self.player.fullscreen = not self.player.fullscreen
            self.player.FullScreen(self.player.fullscreen)
        elif nb_click == 2 :
            self.STOPPED = True
            self.eps = ""

    def onRateChange(self) :
        """
        Rate has change many times, we're gonna do something...
        """
        if not self.searching :
            # print("onRateChange")
        # else :
            if not self.player.Rate() == 1.0 :
                nb_click = self.player.Rated(0, 9, False)
                self.player.Rate(1)
                if self.PrevNextMode :
                    if nb_click in [ -1, 1 ] :
                        self.PrevNextSample(nb_click, False)
                    elif nb_click in [ -2, 2 ] :
                        # Seek kodi 10%
                        seekTo = self.player.Position() + self.duration * nb_click/2 * 0.1
                        self.kodi.Seek(seekTo)
                        # self.player.GoTo(seekTo, self.player.GetTrackPath())
                        self.player.SeekTo(seekTo)
                else :
                    # Seek kodi
                    ope = 1
                    if nb_click < 0 :
                        ope = -1
                    if nb_click * ope <= len(VlcMove.JUMPS) :
                        seekTo = self.player.Position() + ope * VlcMove.JUMPS[nb_click * ope - 1]
                        # print(seekTo)
                        self.kodi.Seek(seekTo)
                        if seekTo < self.player.track_duration :
                            # self.player.GoTo(seekTo, self.player.GetTrackPath())
                            self.player.SeekTo(seekTo)
                    else :
                        if nb_click < 0 :
                            # print("prev")
                            self.kodi.Seek(0)
                            self.kodi.Previous()
                        else :
                            # print("next")
                            self.kodi.Seek(self.player.track_duration)
                self.m_synchroDone = False

    def onShuffleChanged(self) :
        try :
            # Counting clicks
            nb_click = self.player.Shuffled(0, 4, False)
            # Reinit random mode to false
            if nb_click == 1 :
                # print(nb_click)
                if self.kodi.IsPaused() :
                    self.kodi.Play()
                else :
                    # Seek kodi
                    self.kodi.Pause()
                    while not self.kodi.IsPaused() :
                        time.sleep(.2)

                    self.player.paused = True
                    seekTo = self.FramePerfectMode()
                    if seekTo > 0 :
                        self.kodi.Seek(seekTo)
                        self.player.SeekTo(seekTo)
                    self.kodi.Play()
            elif nb_click in [ -2, 2 ] :
                self.PrevNextMode = not self.PrevNextMode
            elif nb_click == 3 :
                print("reading tag mode : {}".format(not self.READ_MODE))
                self.READ_MODE = not self.READ_MODE
                if not self.READ_MODE :
                    self.m_synchroDone = True
            elif nb_click == 4 :
                # Deleting sample
                # TODO : Finish !!!
                if self.DEL_SAMPLE > 0 :
                    p = self.player.Position()
                    self.DeleteSample(p)
                elif self.ADD_SAMPLE :
                    self.ADD_SAMPLE = False
                else :
                    self.DEL_SAMPLE = self.player.Position()
                    print("DEL WTF @{}".format(self.DEL_SAMPLE))
            self.player.Shuffle(False)
        except Exception as e:
            raise Exception("TVMODE : " + str(e))

    def onLoopChanged(self) :
        nb_click = self.player.Looped(0, 4, False)
        # print(nb_click)
        self.player.LoopStatus(self.player.loop)

    def onQuadruplePause(self) :
        self.MASTER = not self.MASTER
        self.SetPrefix()
        self.player.Play()

    def onTriplePause(self) :
        try :
            if self.NEW_SAMPLE >= 0 :
                print("CANCELLED : {}".format(self.NEW_SAMPLE))
                self.NEW_SAMPLE = -1
                self.overwrite = 0
                PlayTags.PREFIX_MSG = PlayTags.PREFIX_MSG_DEFAULT
            else :
                #
                if self.jump > 0 :
                    self.kodi.PlayPause()
                    self.player.Pause()
                    pos = self.player.Position()
                    new_pos_kodi = self.FramePerfectMode(prefix = "AUTO")
                    if new_pos_kodi > pos :
                        self.NEW_SAMPLE = new_pos_kodi - self.jump
                    else :
                        self.NEW_SAMPLE = new_pos_kodi
                    new_tag = self.SaveSample(self.NEW_SAMPLE + self.jump)
                    self.kodi.PlayPause()
                    self.kodi.Seek(self.NEW_SAMPLE + self.jump)
                    time.sleep(1)
                    self.player.Seek(self.NEW_SAMPLE + self.jump)
                    self.NEW_SAMPLE = -1
        except Exception as e:
            raise Exception("onTriplePause : " + str(e))

    def onDoublePause(self) :
        pos = round(self.player.Position(), 3)
        if pos <= 0.9 :
            pos = 0.0

        self.searching = True
        if self.NEW_SAMPLE >= 0 :
            if pos == -1 :
                pos = self.duration
            else :
                self.kodi.PlayPause()
                pos = self.FramePerfectMode(prefix = "END")
            if pos > 0 :
                new_tag = self.SaveSample(pos)
                self.kodi.Seek(pos)
                self.socket.ReloadGenerix(self.socket.DIRS)
            else :
                self.NEW_SAMPLE = -1
            self.kodi.PlayPause()

        elif self.DEL_SAMPLE >= 0 :
            if pos == -1 :
                pos = self.duration
            else :
                self.kodi.PlayPause()
                pos = self.FramePerfectMode(prefix = "END")
            if pos > 0 :
                new_tag = self.DeleteSample(pos)
                # print("{} {} will be deleted".format(self.DEL_SAMPLE, pos))
                self.kodi.Seek(pos)
                self.socket.ReloadGenerix(self.socket.DIRS)
            else :
                self.DEL_SAMPLE = -1
            self.kodi.PlayPause()
        elif len(self.catList) > 0 :
            # Gonna create new sample
            self.kodi.PlayPause()
            self.player.Pause()
            # if len(self.dico_eps) > 0 :
            #     on_tag = self.FindIfJump(self.eps, pos, self.catSearch)
            #     if on_tag > 0 and on_tag - pos > 2:
            #         self.player.Seek((on_tag - pos) - 1)
            #         self.kodi.Seek(on_tag - 1)
            # self.FramePerfectMode(prefix = "BEG")
            # Then we seek kodi to selected frame
            kodi_seek = self.FramePerfectMode(prefix = "BEG")
            if kodi_seek >= 0 :
                self.kodi.Seek(kodi_seek)
            # If FramePerfectMode has been switch to delete mode, then we reinitialize NEW_SAMPLE and init DEL_SAMPLE
            if self.DEL_SAMPLE > 0 :
                self.DEL_SAMPLE = kodi_seek
                self.NEW_SAMPLE = -1
            else :
                self.NEW_SAMPLE = kodi_seek

            self.m_synchroDone = False

            self.kodi.PlayPause()
        else :
            print("No cat")

        # print("END DBL PATSI")
        self.player.paused = False
        self.SetPrefix()

    def onPause(self) :
        self.player.Pause()
        kodi_exist = self.kodi.Exists() or (self.kodi.Exists() and self.kodi.IsPlaying())
        self.m_synchroDone = False
        # Saving new sample
        if self.NEW_SAMPLE >= 0 :
            # print("ONE PATSI")
            pos = self.player.Position()
            if pos == -1 :
                pos = self.duration
            self.player.Play()
            new_tag = self.SaveSample(pos)
        elif self.DEL_SAMPLE >= 0 :
            pos = self.player.Position()
            if pos == -1 :
                pos = self.duration
            self.player.Play()
            new_tag = self.DeleteSample(pos)
        else :
            while self.player.GetStatus() == "Paused" :
                # To relaunch after a standby
                if not kodi_exist and self.kodi.Exists() and self.kodi.IsPlaying() :
                    self.player.Play()
                    break
                self.CheckNewEps()

                nb_click = self.player.Paused(0, 4, False)
                if nb_click == 1 :
                    self.player.Play()
                elif nb_click == 2 :
                    # Search category
                    self.kodi.Pause()
                    if len(self.catEdit) > 1 :
                        self.catList = self.SetNewCat(self.catSearch)
                    else :
                        self.catList = self.SetNewCat(self.catSearch.split("/")[0])
                    print(self.catList)
                    self.ADD_SAMPLE = True
                    self.SetNextSample()
                    self.catSearch = self.catList[0]
                    self.kodi.Play()

                elif nb_click == 3 :
                    self.MASTER = not self.MASTER
                    print(self.player.name + " : Master : {}".format(self.MASTER))
                    self.player.Play()
                elif nb_click == 4 :
                    print(self.player.name + " : Master : {}".format(self.MASTER))

    def Stopped(self) :
        """ Waiting for player to go to next title for 1 second """
        pass
        # i = 0
        # print("Stopped")
        # while self.player.IsStopped() :
        #     time.sleep(.1)
        #     i += 1
        #     if i > 10 :
        #         break
        # if self.player.IsStopped() :
        #     self.player.track = ""
        #     self.STOPPED = True

    def SetFunc(self) :
        while self.player.track == "" :
            self.player.track = self.player.GetTrackPath("file://", "")

        # Synchronize volumes between both instances
        # volvlc = int(self.player.Volume() * 100)
        volkodi = self.kodi.Volume()
        self.player.Volume(volkodi / 100)
        self.player.m_paused = [ self.onPause, self.onDoublePause, self.onTriplePause, self.onQuadruplePause ]
        self.player.m_loopTrack = [self.onLoopChanged]
        # self.player.m_shuffle = []
        # self.player.m_beg_loop = []
        self.player.endLoop = self.endLoop
        self.player.afterLoop = self.afterLoop
        self.player.Stopped = self.Stopped

        self.player.LoopStatus("Playlist")
        self.player.Shuffle(False)
        self.player.loop = self.player.LoopStatus()
        self.player.m_rate = [ self.onRateChange ]
        self.player.m_fullscreen = [ self.onFullScreenChange ]
        self.player.m_shuffle = [self.onShuffleChanged]

    def SetSocket(self, tag_new, tag_read, idx_host = 0, player = None) :
        a_host = self.hosts[idx_host].split(":")
        HOSTS = a_host[0]
        MAIN_SOURCE_PORT = a_host[1]

        PATH2REMOVE = PREFIX_SHARE + HOSTS + PATH2REMOVEROOT
        # We will try 3 times to contact socket
        attempts = 3

        print("Trying to contact socket at : {}:{} ...".format(HOSTS, MAIN_SOURCE_PORT))
        print("Type Ctrl + C to quit", end = "", flush=True)
        # self.socket = VlcMove.GetSocket(HOSTS, MAIN_SOURCE_PORT)
        socket = None
        while socket is None :
            attempts -= 1
            socket = VlcMove.GetSocket(HOSTS, MAIN_SOURCE_PORT)
            if player != None and (player.IsStopped() or not player.Exists()) :
                return None
            # print(attempts)
            if attempts < 0 :
                if len(self.hosts) > 1 :
                    if idx_host + 1 >= len(self.hosts) :
                        idx_host = 0
                    else :
                        idx_host += 1
                    a_host = self.hosts[idx_host].split(":")
                    HOSTS = a_host[0]
                    MAIN_SOURCE_PORT = a_host[1]
                    attempts = 3
                    sys.stdout.write('\033[2K\033[1G')
                    print("Trying to contact socket at : {}:{} ...".format(HOSTS, MAIN_SOURCE_PORT))
                    print("Type Ctrl + C to quit", end = "", flush=True)

            # attempts -= 1
        if self.catSearch is None :
            sys.stdout.write('\033[2K\033[1G')
            dirs = []
            for d in socket.DIRS :
                # print(PATH2REMOVEROOT)
                dirs.append(os.path.realpath(d.replace(PATH2REMOVEROOT, PATH2REPLACE)))
            super().__init__(dirs, tag_new, tag_read)

            self.catSearch = self.catList[0]
        # Get authentication information from socket
        ws = socket.Query("webserver")
        while "error" in ws :
            ws = socket.Query("webserver")
            # print(str(ws) + " " + str(self.socket.ADDR))
        if len(ws) > 0 :
            kodi = KodiPlayer(HOSTS, *ws)

        self.kodi = kodi
        self.socket = socket
        sys.stdout.write('\033[2K\033[1G')
        print("Using kodi at : {}".format(self.kodi.GetAddr()))

        # return self.socket

# END Events

    def __init__(self, hosts, tag_new, player = None, tag_read = "") :
        """ Run the remote kodi video on local vlc. """
        try :
            global TAG

            self.eps = ""
            self.duration = 0
            self.target = 0
            self.playlist = []
            self.NEW_SAMPLE = -1
            self.counter_save = [0, 0]
            self.MASTER = False
            # Vlc instance
            self.player = player
            self.STOPPED = False
            self.ADD_SAMPLE = False          # Indicate that we will edit a video
            self.DEL_SAMPLE = -1            # Position from which to delete
            self.count_vid = {}
            self.dicoDone = {}
            self.dico_eps = []
            self.cur_smp = []               # Current timeStamps array
            self.TV_MODE = True
            self.READ_MODE = False
            self.PrevNextMode = False
            self.catSearch = None
            self.socket = None
            self.t_synchro = None

            PlayTags.PREFIX_MSG_DEFAULT = "  KDI "

            self.hosts = []
            # Manage multiple hosts
            if "," in hosts :
                a_hosts = hosts.split(",")
            else :
                a_hosts = [ hosts ]

            if len(a_hosts) > 0 :
                for h in a_hosts :
                    if ":" in h :
                        # p = h[1]
                        self.hosts.append(h)
                    else :
                        self.hosts.append(h + ":" + str(SOCKET))

            self.SetSocket(tag_new, tag_read, 0, player)
            t = Thread(target=self.ThreadMainSocket)
            t.start()
                # self.t_synchro = Thread(target=self.ThreadSynchro)
                # self.t_synchro.start()

            if self.kodi.GetTrackPath() == "" :
                print("KDI {} stopped or not running".format(self.socket.GetAddr()), end = "...", flush = True)
                self.kodi.ExecQry( 'Addons.ExecuteAddon', {'addonid' : "plugin.video.kodi2tv", 'params': { "addr" : self.socket.GetAddr(), "play" : "True" }, "wait" : True}, 'ExecAddon')
                # print(self.kodi.ExecQry( 'Input.Down', {}, 'ActionDown'))
                # print(self.kodi.ExecQry( 'Input.Up', {}, 'ActionUp'))
                # # print(self.kodi.ExecQry( 'Input.Down', {}, 'ActionDown'))
                time.sleep(5)
                self.kodi.ExecQry( 'Input.Select', {}, 'ActionSelect')
                time.sleep(1)
                self.kodi.ExecQry( 'Input.Select', {}, 'ActionSelect')
                try_count = 0
                while self.kodi.GetTrackPath() == "" and try_count < 15:
                    time.sleep(1)
                    try_count += 1

                if try_count < 15 :
                    print("OK")
                else :
                    print("FAILED")

            if player != None :
                self.player.Shuffle(False)
                self.SetFunc()
                self.player.Play()

            PlayTags.PREFIX_MSG = "  KDI "
            # self.vol = self.kodi.Volume() / 100


            # VlcMove.MAX_OFFSET = 0.8

            while True :
                if self.player != None and not self.player.Exists() :
                    if not DAEMON_MODE :
                        print("process die...")
                    # else :
                    #     self.player = None

                if self.player == None :
                    # Récupère la video en cours de lecture depuis l'API de kodi
                    self.vidRemote = self.kodi.GetTrackPath()
                    pos = self.kodi.Position()
                    self.playlist = self.GetPlaylist(self.vidRemote, os.path.realpath(self.socket.DIRS[0]))
                    # La video distante n'existe pas en local, on va attendre 5 seconde
                    if len(self.playlist) > 0 :
                        if not os.path.exists(self.playlist[0]) :
                            print("WAIT, " + self.playlist[0] + " DOES NOT EXISTS ON LOCAL SYSTEM !")
                            time.sleep(5)
                            continue
                    else :
                        print("Playlist is empty !")
                        sys.exit(3)

                    self.player = self.runInstance(self.playlist, [VLC_OPT], pos)
                    self.SetFunc()
                    # Launching volume synch
                    t = Thread(target=self.ThreadVolume)
                    t.start()
                elif not self.player.Exists() :
                    break

                if self.STOPPED :
                    break
                if not self.player.LoopPlayer(self.eps) :
                    break

        except KeyboardInterrupt as e:
            print("Interruption")
        except Exception as e:
            print("Error : "  + str(e))
        finally :
            if self.player is not None and self.player.Exists() :
                self.player.Stop()
            self.STOPPED = True
            self.eps = ""
            if len(self.counter_save) > 0 and self.counter_save[0] > 0 :
                print("Total : +{} samples saved for {}".format(self.counter_save[0], txt.HRTime(self.counter_save[1])))
            print("End Synchro")

def getRealPath(original_path):
    """ Get "transcrypted" path of a video """
    l_path = original_path

    if l_path.startswith(PATH2REMOVE):
        l_path = l_path.replace(PATH2REMOVE,PATH2REPLACE)
    elif l_path.startswith(PATH2REMOVEROOT) :
        l_path = l_path.replace(PATH2REMOVEROOT,PATH2REPLACE)

    return os.path.realpath(l_path)

def SetConfig(argv):
    """ Args treatment """
    try:
        opts, args = getopt.getopt(argv, usage.GetOpts(), [*usage.GetOptsValue(), "help"])
    except Exception as ex:
        print(ex)
        usage.usage()
        sys.exit(2)
    try:
        global VLC_OPT, TAG
        for opt, arg in opts:
            if opt == "--help" :
                usage.usage()
                sys.exit()
            if opt in ("-h", "--host"):
                global HOSTS
                HOSTS = arg
            elif opt in ("-S", "--socket"):
                global SOCKET
                SOCKET = int(arg)
            elif opt in ("-N", "--no-synchro") :
                global SYNCHRO
                SYNCHRO = False
            elif opt in ("-n", "--tag") :
                global TAG
                TAG = arg
            elif opt in ("-f", "--fullscreen") :
                VLC_OPT += " -f"
            elif opt in ("--crop") :
                VLC_OPT += " --crop " + arg
            elif opt in ("-v", "--verbose") :
                global OUTPUT_CMD
                OUTPUT_CMD = ""
            elif opt in ("--no-exit") :
                VLC_OPT = VLC_OPT.replace("--play-and-exit", "")
            elif opt == "--cvlc" :
                global VLC_CMD
                VLC_CMD = "cvlc"
            elif opt == "--sample" :
                global SAMPLE
                SAMPLE = True
                VLC_OPT += " -Z --no-osd"
            elif opt in ("-A", "--audio") :
                global AUDIO_MASTER
                AUDIO_MASTER = True
            elif opt in ("-r", "--replace") :
                global PATH2REPLACE, PATH2REMOVEROOT, PREFIX_SHARE
                l_arg = arg.split(",")
                PATH2REPLACE = l_arg[0]
                PATH2REMOVEROOT = l_arg[1]

                a_prefix = PATH2REMOVEROOT.split(":")
                if len(a_prefix) > 1 :
                    PREFIX_SHARE = a_prefix[1] + "://"
                    PATH2REMOVEROOT = a_prefix[0]
            else :
                VLC_OPT += " " + opt + " " + arg
        global VLC_ARG
        for arg in args :
            VLC_ARG += " \"" + os.path.realpath(arg) + "\""
        return usage.SetDico(opts, args)
    except Exception as ex:
        print(ex)
        usage.usage()
        sys.exit(2)

if __name__ == "__main__":
    try :
        config = SetConfig(sys.argv[1:])
        # for opt in config :
        #     if opt == "path" :
        #         # VLC_OPT.append(opt)
        #         # if arg != "" :
        #         for p in arg :
        #             # print(p)
        #             if os.path.exists(p) :
        #                 VLC_ARG.append(p)
        VlcMove(HOSTS, TAG, tag_read = "" )
    except Exception as e:
        print("FAIL !\n" + str(e))

