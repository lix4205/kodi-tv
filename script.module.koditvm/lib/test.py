#!/usr/bin/env python
# -*- coding: utf-8 -*-
 
import os
import re
import sys


dico = {
    "1" : 1,
    "1aa" : 1,
    "1a" : 1,
    "100" : 1,
    "111" : 1,
    "11" : 1,
    "10" : 1,
    }

print(dico[0:2])

def f(x) :
    print("ok : " + str(x))
    pass
contains = filter(f(x),  [ 12, 455,65,46854,354,54,5,454,224,545,45,136,543,454 ] )
print(contains(45))

sys.exit(0)

class FilesMng() :

    def __init__ (self, source_dir, encours_name) :
        self.MAIN_SOURCE_DIR = source_dir
        self.DIR_ENCOURS_NAME = encours_name
        #self.MAIN_SOURCE_DIR = args["source_dir"]
        self.DIR_ENCOURS = source_dir + "/" + encours_name
        self.NEW = False
        self.serie = {}

# BEGIN link management
    """
    Creer les liens dans le repertoire ENCOURS,
    Ou renvoi la liste des derniers épisodes joués
    l_l_idx : la liste des fichiers du répertoire de la série
    l_idx : l'index du fichier en cours de lecture
    l_relPath : Le chemin relatif (../)
    l_name : Le nom de la série
    """
    def CreateNextLinks(self, l_l_idx, l_idx, l_relPath, l_name, l_i_keepNumber = 0, p_b_save = True) :
        l_idx_eps = l_idx
        l_a_result = []

        # On se déplace vers le répertoire DIR_ENCOURS
        os.chdir(self.DIR_ENCOURS)
        if l_name != "" :
            # On créer le répertoire
            if not os.path.exists(l_name) :
                self.NEW = True
                os.mkdir(l_name)
            # Et on se déplace dedans
            os.chdir(l_name)

        # On parcourt les fichiers trouvés
        for l_s_val in l_l_idx :
            # On génère le chemins et l'index du lien
            paths_link = self.CreatePathLink(l_idx_eps, l_s_val, l_relPath) + [ l_name]
            #print(paths_link)
            # On créer le lien
            if p_b_save :
                self.CreateLink(*paths_link)
            #os.path.getsize(paths_link[0])
            l_idx_eps += 1

            l_a_result.append([l_s_val, os.path.getsize(l_s_val)])
        #print("Link done")
        # On fait un nettoyage
        if p_b_save :
            self.CleanLink(l_l_idx, l_idx, l_name)
        # TODO : Le retour est tronqué ?!
        return l_a_result[0:10]

    """
    Cherche les prochains fichiers a jouer dans le repertoire et les suivant
    l_l_idx : la liste des fichiers du répertoire de la série
    l_idx : l'index du fichier en cours de lecture
    l_relPath : Le chemin relatif (../)
    l_name : Le nom de la série
    """
    def SearchNextLinks(self, l_l_idx, l_idx, l_relPath, l_name, l_i_keepNumber = 0) :
        l_a_result = []

        # On continue que si le repertoire DIR_ENCOURS existe
        if os.path.exists(self.DIR_ENCOURS) :
            # Le dico qui va contenir les fichiers a créer
            array_files = {}
            # Va contenir le nom du répertoire d'un épisode
            f_dir = ""
            #
            l_idx_eps = l_idx - l_i_keepNumber

            if l_idx_eps < 0 :
                l_i_keepNumber = 0
                l_idx_eps = 0
            # On tourne sur les NB_TOLINK fichiers a partir du courant
            for onef in l_l_idx[l_idx_eps:l_idx_eps + NB_TOSHOW + l_i_keepNumber] :
                # Si on change de saison, on ajoute une nouvelle clé dans le dico
                if f_dir != os.path.dirname(onef) :
                    f_dir = os.path.dirname(onef)
                    array_files[os.path.dirname(onef)] = []

                # On ajoute l'épisode dans le dico
                array_files[os.path.dirname(onef)].append(os.path.basename(onef))
            # On ordonne le tableau de retour
            l_a_file = self.findIndexSaison(array_files)
            #print("F: " + str(l_a_file))
            # Si on a pas 13 fichiers, on va chercher dans les dossiers suivant
            if len(l_a_file) < NB_TOSHOW + l_i_keepNumber :
                # On recupere l'index du dernier élément + 1 dans l_a_file
                l_i_autre = l_l_idx.index(sorted(l_a_file)[len(l_a_file)-1]) + 1
                # Si on est pas a la fin de la série...
                if not l_i_autre > len(l_l_idx) - 1 :
                    # On créer une nouvelle entrée dans le tableau avec le nom du répertoire du fichier
                    array_files[os.path.dirname(l_l_idx[l_i_autre])] = []
                    # On ajoute le fichier du nouveau dossier
                    array_files[os.path.dirname(l_l_idx[l_i_autre])].append(os.path.basename(l_l_idx[l_i_autre]))
                    # Puis on recherche une nouvelle fois avec les x saisons
                    l_a_file = self.findIndexSaison(array_files)
                #return [ '/nfs/data/videos/ANIMES/Final Space/S01/009 - Final Space.mp4', '/nfs/data/videos/ANIMES/Final Space/S01/010.mp4', '/nfs/data/videos/ANIMES/Final Space/S02/FS_S02E01_FR.mp4', str(len(l_a_file)) ]
            if l_i_keepNumber >= len(l_a_file) or l_a_file[l_i_keepNumber] != l_l_idx[l_idx_eps] :
                #print(l_a_file[l_i_keepNumber] + " != " + l_l_idx[l_idx_eps])
                return l_l_idx[l_idx_eps:l_idx_eps + NB_TOSHOW + l_i_keepNumber]
            ##print(l_a_file)
            #l_a_result = []
            ## On parcourt les fichiers trouvés
            #for l_s_val in l_a_file :
                ## On génère le chemins et l'index du lien
                #paths_link = self.CreatePathLink(l_idx_eps, l_s_val, l_relPath) + [ l_name]
                #print(paths_link)
                ## On créer le lien
                #self.CreateLink(*paths_link)
                ##os.path.getsize(paths_link[0])
                #l_idx_eps += 1

                #l_a_result.append([l_s_val, os.path.getsize(l_s_val)])
            ##print("Link done")
            ## On fait un nettoyage
            #self.CleanLink(l_a_file, l_idx - l_i_keepNumber, l_name)

            print("WAR :  ->" + str(NB_TOSHOW) + " don't exists !")
        else :
            print("ERR :  ->" + self.DIR_ENCOURS + " don't exists !")
        # TODO : Le retour est tronqué ?!
        return l_a_file

    """
    Retourne un tableau contenant le chemin relatif du fichier et le nom indexé du lien a créer
    l_idx : l'index du fichier dans la totalité des fichiers du répertoire parent
    real_path : le chemin complet du fichier a lier
    l_relPath : le chemin relatif a insérer dans le chemin calculé (../)
    """
    def CreatePathLink(self, l_idx, real_path, l_relPath) :
        # Remplace le chemin de base dans le chemin complet du fichier
        l_path = real_path.replace(self.MAIN_SOURCE_DIR + "/", "")
        # On ajoute le chemin relatif (../)
        l_s_relpath = l_relPath + l_path
        # On donne le nom du lien indexé
        l_nameLink = Right(FILE_PREFIX + str(l_idx), 4) + "_" + os.path.basename(l_path)
        # Et on retourne le tout
        return [l_s_relpath, l_nameLink]

    """
    Création du lien
    source : source du lien
    target : cible du lien
    new_dir : Le répertoire a créer
    force_creation : Si on veut forcer la création... (PAS UTILISÉ 20200220)
    """
    def CreateLink(self, source, target, new_dir = "", force_creation = False) :
        l_source = str(source)
        l_target = str(target)
        l_targetTmp = l_target + ".tmp"


        # Si la source est bien trouvé
        if os.path.exists(source) :
            # Si la cible existe déjà,
            if os.path.exists(target) :
                # Et qu'on force la création
                if force_creation :
                    # On link
                    os.symlink(l_source, l_targetTmp)
                    os.rename(l_targetTmp, l_target)
                print("ERA :   -> " + os.path.basename(l_source) + " " + target)
            else :
                # On créer le lien
                print("ADD :   -> " + os.path.dirname(l_source) + " " + target)
                os.symlink(l_source, str(target + ".tmp"))
                os.rename(l_targetTmp, l_target)
        else :
            # Pas de source, on affiche l'erreur dans la log
            print("ERR : " + l_source + " don't exists !")

    """
    Nettoie les liens dans le répertoire DIR_ENCOURS
    """
    def CleanLink(self, l_l_idx, l_idx, new_dir) :
        # On se déplace vers le répertoire DIR_ENCOURS
        os.chdir(self.DIR_ENCOURS)

        # On remplit la liste des fichiers dans le réperoire
        list_file_present = self.SearchFileInLinks(new_dir)

        print("Begin Clean")
        # On cherche si on a des liens qui n'ont rien a foutre la...
        for i in range(len(l_l_idx)) :
            # On reconstruit le nom du fichier
            l_nameLink = self.DIR_ENCOURS + "/" + new_dir + "/" + Right(FILE_PREFIX + str(l_idx+i), 4) + "_" + os.path.basename(l_l_idx[i])
            #l_nameLink = l_nameLink.encode("utf8")
            #print(l_nameLink + " " + str(l_idx))
            # Si le fichier est trouvé,
            if os.path.basename(l_nameLink) in list_file_present :
                # On le supprime de la liste
                list_file_present.remove(os.path.basename(l_nameLink))

        #print("PRESENT : " + str(list_file_present))
        #print("COPIED : " + str(l_l_idx))
        # Si on a encore des fichiers
        if len(list_file_present) > 0 :
            # On les supprime.
            for file_present in sorted(list_file_present) :
                os.remove(self.DIR_ENCOURS + "/" + new_dir + "/" + file_present)
                print(str(file_present) + " removed" )

        print("End Clean")

    def CleanDir(self, l_l_idx, l_idx, new_dir) :
        # Dernier épisode de la série, on nettoie
        if l_idx == len(l_l_idx) - 1 :
            print("Cleaning directory")
            l_nameLink = self.DIR_ENCOURS + "/" + new_dir + "/" + Right(FILE_PREFIX + str(l_idx), 4) + "_" + os.path.basename(l_l_idx[l_idx])
            #os.remove(l_nameLink)
            if new_dir != "" :
                # Delete folder and sub. ...
                #shutil.rmtree('/folder_name')
                # Delete empty directory
                 os.rmdir(self.DIR_ENCOURS + "/" + new_dir)
                 #os.remove(DIR_ENCOURS + "/" + DIR_ENCOURS_NAME + "_" + new_dir + FILENAME_AUTOREAD_EXT)

# END link management


# BEGIN File listing/searching...
    """
    Cherche le pattern correspondant dans la chaine
    Liste des patterns en parametre
    Retourne le pattern trouvé
    """
    def searchPattern(self, p_s_chaine, p_s_pattern) :
        for l_s_pattern in p_s_pattern :
            l_found = re.findall(l_s_pattern, p_s_chaine)
            if l_found :
                return l_found

    """
    Return la liste trié des fichiers par les pattern de numérotation (2x05, S01E11, 01- ...)
    """
    def findIndexSaison(self, array_files, p_b_dirSearch = True) :
        l_a_file = {}

        l_i_count = 0
        l_idx = 0
        l_idx_saison = 0
        l_idx_newSaison = 0

        for l_s_key in sorted(self.serie.keys()) :
            l_a_fileSearch = self.serie[l_s_key]
            #print(array_files)
            if p_b_dirSearch :
                #print("[ORDON]")
                # On récupère le premier élément
                l_s_first = str(l_s_key) + "/" + l_a_fileSearch[0]
                # On cherche les épisodes dans tout le répertoire
                l_a_fileDir = { l_s_key : self.ListNextFiles(l_s_first) }
                # On les ordonne
                l_a_fileDir = self.findIndexSaison(l_a_fileDir, False)

                if l_s_first not in l_a_fileDir :
                    l_a_fileDir.insert(0, l_s_first)
                # On cherche le numéro du premier épisode
                l_idx = l_a_fileDir.index(l_s_first)

                l_a_fileSearch = l_a_fileDir

            # En début de nouvelle saison, on cherche l'épisode 0 de la suivante
            if l_idx_newSaison != l_idx_saison :
                l_idx = 0
                l_idx_saison = os.path.basename(l_s_key)

            for l_file in l_a_fileSearch[l_idx:len(l_a_fileSearch)] :
                if l_i_count > NB_TOLINK and p_b_dirSearch :
                    break

                # Recherche l'un des pattern
                l_found = self.searchPattern(l_file.lower(), [ "(\D\d\d[A-Za-z]\d\d\D)", "(\D\d[A-Za-z]\d\d\D)", "(\D\d[A-Za-z]\d\d\D)", "(\D\d\d\d\D)", "(\D\d\d\D)", "(\D\d\D)" ])
                if l_found :
                    print(l_found)
                    #try :
                        #print(str(l_found) + " " + os.path.basename(l_file).encode())
                    #except :
                        #print(str(l_found))
                    l_idx_eps = l_found[0][-2:]
                    l_idx_saison = os.path.basename(l_s_key)
                    #l_s_newKey = Right("00" + str(l_idx_eps),2)

                    l_s_newKey = str(l_idx_saison) + Right(FILE_PREFIX + str(l_idx_eps), 4)
                    #print("CLE : " + l_s_newKey )
                    if not l_s_newKey in l_a_file :
                        l_a_file[l_s_newKey] = []

                    l_a_file[l_s_newKey].append(l_file)
                    l_i_count +=1
            #l_i_max = l_i_min + len(array_files[l_s_key])
            #print("### " + str(l_i_min) + " : " + str(l_i_max))
            l_idx_newSaison += 1
        l_a_final = []

        #print(l_a_file)

        for l_i_key in sorted(l_a_file.keys()) :
        #for l_s_key in l_a_file :
            for l_s_eps in l_a_file[l_i_key] :
                #print(str(l_iidx) + " " + l_s_eps)
                #l_iidx += 1
                l_a_final.append(l_s_eps)
        #print("### Files : " + str(l_a_final) + "\n" + str(sorted(l_a_file.keys())))
        return l_a_final

    """
    Retourne le chemin du fichier sans les parametres du serveur
    """
    def GetRealPath(self, original_path) :
        l_path = original_path

        for l_invalid_path in PATH2REMOVE :
            if l_path.startswith(l_invalid_path) :
                l_path = l_path.replace(l_invalid_path ,"")
                break
        return l_path

    """
    Return file list in links directory
    """
    def SearchFileInLinks(self, l_dirname = "") :
        files_in_dir = list()
        if l_dirname != "" :
            for l_file in os.listdir(self.DIR_ENCOURS + "/" + l_dirname):
                l_path_file = os.path.basename(l_file)
                files_in_dir.append(l_path_file)

        return files_in_dir

    """
    Retourne un dico avec les infos de la video
    """
    def GetTypeVideo(original_path, path_dir, jump_dir = 0) :
        l_vid = { "path" : original_path, "type" : "", "name" : "" }
        l_path = original_path
        l_a_path = []
        l_type = ""
        l_name = ""

        if l_path.startswith(path_dir) :
            l_path = l_path.replace(path_dir + "/", "")
            l_a_path = l_path.split("/")
            l_type = l_a_path[jump_dir]
            l_name = l_a_path[jump_dir+1]
            if len(l_a_path) > 2 :
                l_type = "SERIE"
            else :
                l_type = "FILM"
                l_name = ""
            l_vid["type"] = l_type
            l_vid["name"] = l_name
        return l_vid

# BEGIN NEW FUNCTIONS
    """
    Get index of a file in directories list
    path_file : chemin d'un fichier,
    root_only = (False) Ne cherche pas dans les sous dossier,
    p_saison = Saison en cours
    """
    def SetSerie(self, path_file, root_only = False, p_saison = -1) :
        l_idx_file = 0
        l_path = path_file
        l_listNextFiles = {}
        # Deleting prefix file to get SERIE/SAGA dir
        if l_path.startswith(self.MAIN_SOURCE_DIR) :
            l_path = l_path.replace(self.MAIN_SOURCE_DIR + "/", "")
        l_a_path = l_path.split("/")
        if os.path.isfile(path_file) :
            if len(l_a_path) > 2 :
                if len(l_a_path) > 3 :
                    l_dir_name = os.path.dirname(os.path.dirname(path_file))
                else :
                    l_dir_name = os.path.dirname(path_file)
            else :
                l_dir_name = os.path.dirname(path_file)
        else :
            l_dir_name = path_file
        #l_listFile = sorted(os.walk(l_dir_name))
        l_listFile = sorted(os.listdir(str(l_dir_name)))
        #print(l_dir_name)
        #l_saison = 1

        #print("LIST : " + str(l_listFile))
        for l_file in l_listFile :
            self.saison = p_saison
            if self.IgnoreFile(l_file) :
                continue

            #print(l_file)
            if not root_only and os.path.isdir(l_dir_name + "/" + l_file) :
                l_found = self.searchPattern(l_file, ["(\d\d\d\d)", "(\d\d\d)","(\d\d)","(\d)" ])
                #if l_found and int(l_found[0]) == l_saison :
                #print(str(l_found) + " " + l_file)

                if l_found :
                    self.saison = self.GetNewIndex(float(l_found[0]), self.serie.keys())
                    #print("NEW S : " + str(self.saison) + " | " + l_file)
                    self.serie[self.saison] = []
                else :
                    self.saison = l_file
                    self.serie[l_file] = []
                self.SetSerie(l_dir_name + "/" + l_file, root_only, self.saison)

                #print(self.findIndexSaison(self.serie))

            elif os.path.isfile(l_dir_name + "/" + l_file)  :
                if os.path.exists(os.path.realpath(l_dir_name + "/" + l_file)) :
                    if not self.saison in self.serie :
                        self.serie[self.saison] = []
                    #print(str(l_file) + " " + str(self.saison))
                    self.serie[self.saison].append(os.path.join(l_dir_name, l_file))


    def FindPattern(self, p_chaine, patterns_search) :
        idx_pattern = -1
        for pattern in patterns_search :
            l_found = re.findall(pattern, p_chaine)
            if l_found :
                idx_pattern +=1
                break
            idx_pattern +=1
        if not len(l_found) > 0 :
            idx_pattern = -1
        return [ idx_pattern, l_found ]


    def GetSerie(self, try_order = True) :
        l_a_fileDir = {}
        l_a_epsDir = {}
        l_a_returnPattern = []
        #patterns_search = [ "(\d\d\d)", "(\d\d)","(\d)" ]
        patterns_search = [ "(\D\d\d[A-Za-z]\d\d\D)", "(\D\d[A-Za-z]\d\d\D)", "(\D\d\d\d\D)", "(\D\d\d\D)", "(\D\d\D)", "(^\d\d\D)"  ]
        l_count = 0
        l_i_saison = -1
        #print(self.serie.keys())
        #return True

        #if -1 in self.serie.keys() :
        if try_order :
            for l_saison in self.serie.keys() :
                for l_episode in self.serie[l_saison] :
                    #print(str('SAISON :') + ' ' + str(l_saison) + " " + str(l_episode))
                    l_count += 1
                    #l_found = []
                    #for pattern in patterns_search :
                        #l_found = re.findall(pattern, os.path.basename(l_episode))
                        #if l_found :
                            #break
                        #idx_pattern +=1
                    l_a_returnPattern = self.FindPattern(os.path.basename(l_episode), patterns_search)
                    #print(l_episode + " " + str(l_a_returnPattern))
                    if l_a_returnPattern[0] == 0 :
                        # Pattern classique S01E02
                        #print(l_a_returnPattern[1][0][1:len(l_a_returnPattern[1][0])-1])
                        # On recupere la chaine a analyser
                        l_a_returnPattern[1] = l_a_returnPattern[1][0][1:len(l_a_returnPattern[1][0])-1]
                        l_i_saison = l_saison # float(l_a_returnPattern[1][0:2])
                        l_i_eps = float(l_a_returnPattern[1][3:])

                        #l_a_epsDir[l_i_eps] = l_episode
                        if l_i_saison not in l_a_fileDir :
                            l_a_fileDir[l_i_saison] = {}
                        #if l_i_eps not in l_a_fileDir[l_i_saison] :
                            #l_a_fileDir[l_i_saison] = {}
                        #l_a_fileDir[l_i_saison] = []
                        #print("NEW : " + str(self.GetNewIndex(l_i_eps, l_a_fileDir[l_i_saison].keys())) + " " +l_episode)
                        l_a_fileDir[l_i_saison][self.GetNewIndex(l_i_eps, l_a_fileDir[l_i_saison].keys())] = l_episode #l_a_epsDir
                    elif l_a_returnPattern[0] == 1 :
                        # Autre pattern classique 1x02
                        l_a_returnPattern[1] = l_a_returnPattern[1][0][1:len(l_a_returnPattern[1][0])-1]
                        l_i_saison = float(l_a_returnPattern[1][0:1])
                        l_i_eps = l_a_returnPattern[1][2:]

                        if l_i_saison not in l_a_fileDir :
                            l_a_fileDir[l_i_saison] = {}
                        l_a_fileDir[l_i_saison][l_i_eps] = l_episode
                    elif l_a_returnPattern[0] == 2 :
                        # En ligne : 102
                        l_a_returnPattern[1] = l_a_returnPattern[1][0][1:len(l_a_returnPattern[1][0])-1]
                        l_i_saison = float(l_a_returnPattern[1][0:1])
                        l_i_eps = float(l_a_returnPattern[1][1:])

                        if l_i_saison not in l_a_fileDir :
                            l_a_fileDir[l_i_saison] = {}
                        l_a_fileDir[l_i_saison][self.GetNewIndex(l_i_eps, l_a_fileDir[l_i_saison].keys())] = l_episode
                    elif l_a_returnPattern[0] == 3 or l_a_returnPattern[0] == 5 :
                        # Autre cas, 01[...]03
                        if len(l_a_returnPattern[1]) > 1 :
                            l_a_returnPattern[1][0] = l_a_returnPattern[1][0][1:len(l_a_returnPattern[1][0])-1]
                            l_i_saison = float(l_a_returnPattern[1][0][0:2])
                            l_a_returnPattern[1][1] = l_a_returnPattern[1][1][1:len(l_a_returnPattern[1][1])-1]
                            l_i_eps = float(l_a_returnPattern[1][1])
                            #print(l_i_eps)
                            if l_i_saison not in l_a_fileDir :
                                l_a_fileDir[l_i_saison] = {}
                            l_a_fileDir[l_i_saison][l_i_eps] = l_episode
                        else :
                            # Juste l'épisode 02
                            l_a_returnPattern[1][0] = l_a_returnPattern[1][0][1:len(l_a_returnPattern[1][0])-1]
                            #print(l_a_returnPattern[0])

                            l_i_saison = float(l_saison)
                            #l_a_returnPattern[1][1] = l_a_returnPattern[1][1][1:len(l_a_returnPattern[1][1])-1]
                            l_i_eps = float(l_a_returnPattern[1][0])
                            #print(l_i_eps)
                            if l_i_saison not in l_a_fileDir :
                                l_a_fileDir[l_i_saison] = {}
                            l_a_fileDir[l_i_saison][l_i_eps] = l_episode
                    elif l_a_returnPattern[0] == 4 :
                        # Juste l'épisode 2
                        #print(str(l_a_returnPattern[0]) + " " +l_episode)
                        l_a_returnPattern[1][0] = l_a_returnPattern[1][0][1:len(l_a_returnPattern[1][0])-1]

                        if isinstance(l_saison, str) :
                            l_i_saison = l_saison
                        else :
                            l_i_saison = self.GetNewIndex(float(l_saison), l_a_fileDir.keys())
                        #l_i_saison = float(l_saison)
                        #l_a_returnPattern[1][1] = l_a_returnPattern[1][1][1:len(l_a_returnPattern[1][1])-1]
                        l_i_eps = float(l_a_returnPattern[1][0])
                        #print(l_i_eps)
                        if l_i_saison not in l_a_fileDir :
                            l_a_fileDir[l_i_saison] = {}



                        #print("NEW : " + str(self.GetNewIndex(l_i_eps, l_a_fileDir[l_i_saison].keys())))
                        if l_i_eps in l_a_fileDir[l_i_saison] :
                            l_i_eps_tmp = l_i_eps + 0.1
                            while l_i_eps_tmp in l_a_fileDir[l_i_saison] :
                                l_i_eps_tmp += + 0.1

                            #print(l_i_eps_tmp)
                            l_a_fileDir[l_i_saison][l_i_eps_tmp] = l_episode
                        else :
                            l_a_fileDir[l_i_saison][l_i_eps] = l_episode

                    else :
                        print("Pattern not found : " + str(l_saison) + " " + str(l_episode))
                        l_i_eps = float(-1)


                        #print("NEW S : " + str(self.GetNewIndex(l_i_saison, l_a_fileDir.keys(), -1)))
                        #print("NEW : " + str(self.GetNewIndex(l_i_eps, l_a_fileDir[l_i_saison].keys())))
                        if not isinstance(l_saison, str) and l_i_saison > 0 and l_i_saison not in l_a_fileDir :
                            l_a_fileDir[l_i_saison] = {}
                        else :
                            if isinstance(l_saison, str) :
                                l_i_saison = l_saison
                            else :
                                l_i_saison = self.GetNewIndex(float(l_saison), l_a_fileDir.keys())
                            if l_i_saison not in l_a_fileDir :
                                l_a_fileDir[l_i_saison] = {}
                            #print(self.GetNewIndex(l_i_eps, l_a_fileDir[l_i_saison].keys()))
                        l_a_fileDir[l_i_saison][l_i_eps] = l_episode
                #print(str('SAISON :') + ' ' + str(l_i_saison) + " " + str(l_a_fileDir))
                    #else :
                        ## Aucun pattern, on ajoute a la liste
                        #l_a_epsDir[0] = l_episode
                        #l_a_fileDir[l_saison] = l_a_epsDir

                        #l_a_returnPattern[1] = l_a_returnPattern[1][0][1:len(l_a_returnPattern[1][0])-1]
                        #l_i_saison = int(l_a_returnPattern[1][0:1])
                        #l_i_eps = int(l_a_returnPattern[1][1:])

                        ##print(l_episode + " " + str(l_i_saison) + " " + str(l_i_eps))
                        #l_a_epsDir[l_i_eps] = l_episode

                        #l_a_fileDir[l_i_saison] = l_a_epsDir
        else :
            list_episode = []
            for l_saison in self.serie.keys() :
                #print(str(l_saison) + " " + str(len(self.serie[l_saison])))
                for l_episode in self.serie[l_saison] :
                    #print(str(l_saison) + "E" + str(l_episode) + " " + str(l_a_fileDir[l_saison][l_episode]))
                    list_episode.append(l_episode)

            return list_episode

        print("COUNT : " + str(l_count))
        #print("FILES : " + str(l_a_fileDir))
        #self.serie = {}
        #l_a_fileDir
        list_episode = []
        for l_saison in sorted(l_a_fileDir.keys()) :
            #print(str(l_saison) + " " + str(len(l_a_fileDir[l_saison])))
            for l_episode in sorted(l_a_fileDir[l_saison].keys()) :
                #print(str(l_saison) + "E" + str(l_episode) + " " + str(l_a_fileDir[l_saison][l_episode]))
                list_episode.append(l_a_fileDir[l_saison][l_episode])
                #self.serie[l_saison] = l_a_fileDir[l_saison][l_episode]
                #print(l_a_fileDir[l_saison][l_episode])
        #list_episode = []
        #for saison in sorted(self.serie.keys()) :
            #for episode in self.serie[saison] :
                ##print(episode)
                #list_episode.append(episode)

        return list_episode

    def GetNewIndex(self, p_idx, p_keys, p_order = 1) :
        l_index = p_idx
        #if l_i_eps in l_a_fileDir[l_i_saison] :
        #l_i_eps_tmp = p_idx + 0.1
        while l_index in p_keys :
            l_index += 0.1 * p_order
        return l_index

# END FUNCTIONS



    """
    Retourne la liste des fichiers d'un répertoire, ou la liste des fichiers contenus dans le répertoire parent et ses sous dossiers.
    """
    def ListNextFiles(self, path_file, file_before = False) :
        l_listNextFiles = list()
        l_dir_name = ""
        l_b_fileFound = False
        if os.path.exists(path_file) :
            if os.path.isdir(path_file) :
                l_dir_name = path_file
            else :
                if file_before :
                    l_dir_name = os.path.dirname(os.path.dirname(path_file))
                else :
                    l_dir_name = os.path.dirname(path_file)
            l_listFile = sorted(os.listdir(l_dir_name))
            for l_file in l_listFile :
                if self.IgnoreFile(l_file) :
                    continue
                if os.path.isdir(l_dir_name + "/" + l_file) :
                    l_listNextFiles += self.ListNextFiles(l_dir_name + "/" + l_file)
                if l_dir_name + "/" + l_file == path_file :
                    l_b_fileFound = True

                if l_b_fileFound or os.path.isfile(l_dir_name + "/" + l_file) :
                    l_listNextFiles.append(l_dir_name + "/" + l_file)
                    if file_before :
                        return l_listNextFiles

        return l_listNextFiles

    """
    Retourne vrai si le fichier n'est pas dans les valeurs de IGNORE_FILE
    qui contient les pattern de nom de fichier a ignorer
    """
    def IgnoreFile(self, l_s_path) :
        for l_s_ignore in IGNORE_FILE :
            if l_s_path.endswith(l_s_ignore) :

                return True
        #print(" ###### " + l_s_path)
        return False

# END File listing

def Right(s, amount):
    return s[-amount:]

IGNORE_FILE = [ ".directory", ".db", ".log", ".srt" , ".nfo" ]

NB_TOLINK = 2
NB_TOSHOW = 12

FILE_PREFIX = "0000"
FILE_PREFIX_LEN = len(FILE_PREFIX)



path_file_local = "/media/srv/data/videos/MANGAS/Code Geass/Code Geass R2 Saison 2/[S`n`N]Code_Geass_R2_01_Vostfr.avi"
path_file_local = "/media/srv/data/videos/SERIES/Breaking Bad/S02/Breaking.Bad.S02E01.FRENCH.DVDRip.XviD-JMT.avi"
#path_file_local = "/media/srv/data/videos/SERIES/The IT Crowd/S01/The.IT.Crowd.S01E01.FRENCH.LD.DVDRiP.XviD-JMT.avi"
#path_file_local = "/media/srv/data/videos/MANGAS/Gintama/S06 2012-2/Gintama - 255 - Les boules dorées de Kin-san.avi"
#path_file_local = "/media/pix/data/videos/SERIES/Doctor Who/S03/Doctor.Who.S03E02.FRENCH.Xvid.zone-telechargement.com.avi"
#path_file_local = "/media/srv/data/videos/ANIMES/Les Simpson/S25 2013/The.Simpsons.S25E13.TRUEFRENCH.HDTV.XViD-ZT-www.Zone-Telechargement.com.avi"
#path_file_local = "/media/srv/data/videos/FILMS_720/Quentin Dupieux/2010.Rubber.CD1.VOSTFR.BRRip.XviD.AC3-SolO.avi"
#path_file_local = "/media/srv/data/videos/FILMS_720/Coco.2017.MULTi.TRUEFRENCH.1080p.BluRay.Light.x264.AC3.mkv"
#path_file_local = "/media/pix/data/videos/FILMS_720/American Nightmare/2016.The.Purge.Election.Year.Truefrench.1080p.HDLight.DTS.H264.mkv"

MAIN_SOURCE_DIR = "/media/srv/data/videos"
DIR_ENCOURS_NAME = "ENCOURS"
info_files = FilesMng(MAIN_SOURCE_DIR, DIR_ENCOURS_NAME)
l_countLinks = 0
l_infoFile = FilesMng.GetTypeVideo(path_file_local, MAIN_SOURCE_DIR, 0)
            #logXbmc("count : " + str(l_idx))

l_type = l_infoFile["type"]
l_name = l_infoFile["name"]
print(l_infoFile)
l_relPath = "../"

l_relPathEnCours = l_relPath

#logXbmc(l_name + " == " + os.path.basename(path_file_local))
#logXbmc(l_type)
#self.MAIN_SOURCE_DIR = args["source_dir"]
#self.DIR_ENCOURS = self.MAIN_SOURCE_DIR + "/" + DIR_ENCOURS_NAME
if l_name ==  os.path.basename(path_file_local) :
    l_name = l_type

if l_type == "SERIE" : # or l_type == "SAGA" :
    l_relPath += "../"
#self.SAGA = l_name
#logXbmc("count : " + str(l_name))


    info_files.SetSerie(path_file_local)
    print(info_files.serie)
    #listeIndex = info_files.findIndexSaison(info_files.serie)

    list_file = info_files.GetSerie()
#print(list_file)

    for fi in list_file :
        print(fi)
    print(list_file.index(path_file_local))


#print(listeIndex)
#self.findIndexSaison(self.serie)
#print(listeIndex)
##if len(self.listeIndex) == 0 or ( len(self.listeIndex) > 0 and str(path_file_local) in l_l_idx) :
##self.listeIndex = l_l_idx
#l_idx = listeIndex.index(str(path_file_local))
##print("SEL : " + str(self.listeIndex) + " " + path_file_local)
#l_countLinks = info_files.SearchNextLinks(listeIndex, l_idx, l_relPath, l_name, 0)
#print(l_countLinks)
##l_countLinks = info_files.CreateNextLinks(l_countLinks, l_idx, l_relPath, l_name, l_i_keep_number, False)
