# Copyright (c) Elie Coutaud 2024
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!/usr/bin/python
# -*- coding: latin-1 -*-
import os, json, sys
from koditvm import kodiUtils
from koditvm.TagsMng import TagsMng, Md5Mng, TagsList
from koditvm.QueriesSocket import QuerySocket
import time
from koditvm.dbus_mpris import Player
# from koditvm.KodiMove import KodiMove
from koditvm.pythcomm.helper_help import usage
import koditvm.pythcomm.txt_utils as txt
import getopt
from random import uniform, shuffle
import hashlib
from threading import Thread
import allinone
from koditvm.play_tags import PlayTags
from koditvm.ffmpeg_tools import FfmpegTools
from koditvm.Constants import CmdSocket

SYNCHRO = True
NOEXIT = False
OUTPUT_CMD = "2> /dev/null"
SAMPLE = True
DEFAULT_TIME = 5
TIME = -1
VLC_ARG = []
VLC_OPT = [] # "--play-and-exit"
VLC_CMD = "vlc"
SOCKET = 9876

TV_MODE = True
TAG_EDIT = ""
TAG_READ = ""
TAG_NEW = ""
TAG_READ_RANDOM = ""
# FOLDERS = ""

KODI_FOLLOW = False
KODI_SOCKET_DEFAULT = 9876
KODI_SOCKET = "localhost:" + str(KODI_SOCKET_DEFAULT)
AUDIO_MASTER = False
SHOW_STAT = False
QUALITY = None
DEBUG = True


# BEGIN Help

usage.OPTIONS_ALIAS = {
    "A" : "audio",
    "v" : "verbose",
    "t" : "time",

    "e" : "tag-edit",
    "r" : "tag-read",
    "n" : "tag-new",

    "Z" : "random",
    "K" : "kodi",
    "S" : "socket",
    "j" : "jump",
    "q" : "quality"
}
usage.OPTIONS_VALUES = {
    "j" : "integer (optional)",
    "e" : "tag",
    "r" : "tag",
    "rr" : "tag",
    "n" : "tag",
    "t" : "time",
    "q" : "quality",
    "video-x" : "width",
    "video-y" : "height",
    "crop" : "(4:3,16:9,...)",
    "S" : "socket port",
}

usage.OPTIONS_HELP = {
    "notv": "Tv Mode (default)",
    "e" : "Edit tags",
    "r" : "Read tags",
    "rr" : "Read random tags",
    "n" : "New tags",
    "f": "Fullscreen",
    "t" : "time to wait before switching",
    "j" : "number of jump on a video (not implemented)",
    "K" : "Show kodi instead of playing samples",
    "S" : "Use kodi/socket instead of playing samples",
    "no-exit": "Do not exit after end of playlist",
    "cvlc" : "Do not use vlc Interface",
    "q" : "video minimum quality",
    "video-x" : "Window width",
    "video-y" : "Window height",
    "crop" : "crop image",
    "stat": "Show stats and exit",
    "Z": "Random mode",
    "v" : "Verbose mode (vlc)",
    "path" : "local files to add to vlc playlist"
}

usage.OPTIONS_MANDATORY = ["path"]
usage.OPTIONS_ARGS = ["path"]
usage.OPTIONS_DEFAULT = {
    "j" : 10
    }
# usage.OPTIONS_MANDATORY = []
# TODO : Use Text as first line and use help.CMD instead
usage.TEXT = "python " + sys.argv[0]
usage.DESC = "Play files randomly for X second \nCredentials are required if kodi is involved\nDefault socket port is 9876"
# END Help

# # import sys
# import select
# import koditvm.pythcomm.futil
# from threading import Thread
# class VlcCLI() :
#     dicoCmd = {}
#     def __init__(self, player) :
#         self.CONTINUE = True
#         self.RUNNING = False
#         VlcCLI.dicoCmd = {
#             " " : [ player.PlayPause ],
#             "s" : [ player.Stop ],
#             "n" : [ player.Next ],
#             "p" : [ player.Previous ],
#
#             "q" : [ player.Quit ],
#             "f" : [ player.SetFullScreen ],
#             "r" : [ player.SetShuffle ],
#             "-" : [ player.Rate, 1.50 ],
#             "+" : [ player.Rate, 0.5 ],
#             "l" : [ player.Looped ],
#             # chr(24): player.Pause
#             # chr(27).encode() : player.Pause
#         }
#         # print("Follow vlc")
#         # print(rid)
#
#     def Run(self) :
#         # self.TestSys()
#         # print(chr(27).encode())
#         # print("[B".encode())
#         K_RIGHT = b'\x1b[C'
#         K_LEFT  = b'\x1b[D'
#         K_UP = b'\x1b[A'
#         K_DOWN  = b'\x1b[B'
#         ui = koditvm.pythcomm.futil.rid()
#         self.CONTINUE = False
#         while self.RUNNING :
#             time.sleep(.1)
#         print("no process")
#
#         self.CONTINUE = True
#         self.RUNNING = True
#         while self.CONTINUE :
#             if select.select([sys.stdin,],[],[], 0.5)[0] :
#                 print("wait")
#                 # line = sys.stdin.readline()
#                 line = ui.ask1char("w")
#                 # line = sys.stdin.read(1)
#                 # line = koditvm.pythcomm.futil.getChar()
#                 # print("ANSWER : " + line)
#                 if line:
#                     if line in VlcCLI.dicoCmd :
#                         try :
#                             VlcCLI.dicoCmd[line][0](*VlcCLI.dicoCmd[line][1:])
#                         except Exception as e :
#                             print(e)
#                         # break
#                 else:
#                     print('EOF!')
#             # else:
#             #     print('No data for 2 secs')
#             # test = ui.ask1char() #.encode()
#             # if test == K_DOWN :
#             # print(test)
#             # if test == chr(27).encode() :
#             #     print("escape")
#             #     break
#         self.RUNNING = False
#         print("done")
#
#     def TestSys(self) :
#         while True:
#             if select.select([sys.stdin,],[],[],2.0)[0]:
#                 # line = sys.stdin.readline()
#                 line = sys.stdin.read(1)
#                 if line:
#                     print('Got:', line, end='')
#                 else:
#                     print('EOF!')
#                 # break
#             # else:
#             #     print('No data for 2 secs')

class VlcSample(PlayTags) :

    def set_new_vid(self, seekToRandom = False, cat_keep = "", done_vid = 0) :
        pos_tmp = -1
        if not self.player == None :
            while not self.player.IsPlaying() :
                time.sleep(.5)
                # print(str(self.duration))

            # # On recupere le chemin de la video qui tourne sur le lecteur local.
            # duration = self.player.GetTrackDuration()
            # while duration <= 0 :
            #     time.sleep(.006)
            #     # print("du")
            #     duration = self.player.GetTrackDuration()

            # self.duration = self.player.track_duration
            self.eps = self.player.GetTrackPath("file://", "")
            l_md5 = self.md5.GetMd5(self.eps)
            if len(self.dico_eps) > 0 :
                done_vid = self.tags.FindIfJump(l_md5, done_vid, cat_keep, CmdSocket.MAIN_CAT)

            if self.eps not in self.dicoDone :
                self.dicoDone[self.eps] = done_vid

            # print(self.eps + " __ " + str(self.dicoDone[self.eps]) + " > " + str(done_vid))

            if seekToRandom :
                if done_vid > 0 :
                    new_time = done_vid
                    # if new_time > self.dicoDone[self.eps] :
                    self.dicoDone[self.eps] = done_vid
                    # else :
                    #     # self.dicoDone[self.eps] += self.SAMPLE_TIME
                    #     new_time = self.dicoDone[self.eps]
                else :
                    # print("!done_vid > 0 " + str(done_vid))
                    if self.dicoDone[self.eps] + (1/10) <= 1 :
                        new_time = self.dicoDone[self.eps] * self.player.track_duration
                        # uniform(self.dicoDone[self.eps] * self.player.track_duration, (self.dicoDone[self.eps] + (1/10) * self.player.track_duration))
                    else :
                        new_time = self.player.track_duration * 95
                        # new_time = uniform(self.dicoDone[self.eps] * self.player.track_duration, self.player.track_duration - self.SAMPLE_TIME)
                        self.dicoDone[self.eps] = 0
                    if new_time < 0 :
                        while new_time < self.SAMPLE_TIME + 1 :
                            new_time = uniform(self.SAMPLE_TIME + 1, self.player.track_duration - self.SAMPLE_TIME)
                    self.dicoDone[self.eps] += 1 / 10
                print(" {} < {}".format( pos_tmp , new_time ))
                while pos_tmp < new_time :
                    if self.player.GetTrackPath("file://", "") == self.eps :
                        self.player.Seek(new_time)
                        pos_tmp = self.player.Position()
                        self.target = new_time + self.SAMPLE_TIME
                    else :
                        print("CHANGE " + self.eps + " > " + self.player.GetTrackPath("file://", ""))
                        if not self.player.GetTrackPath("file://", "") in self.dicoDone :
                            self.dicoDone[self.player.GetTrackPath("file://", "")] = 0
                        return self.set_new_vid(seekToRandom, cat_keep, self.dicoDone[self.player.GetTrackPath("file://", "")])
                    time.sleep(.08)

                print(" {} < {}".format( pos_tmp , self.target ))
                if pos_tmp > self.target :
                    print("SEEK :: #" + str(new_time))
                    self.player.Seek(new_time)

            return self.target

    def ThreadRunTime(self) :
        try :
            # First played track is not the same in kodi
            while self.eps != self.player.track :
                time.sleep(.3)
            while self.player.Exists() and not KODI_FOLLOW :
                # pos = self.player.Position()
                # self.SetCurrentSample(self.player.Position(), True)
                self.printRunTime()
            print("End print runtime thread")
            return True
        except Exception as e :
            print("Error ThreadRunTime : " + str(e))
            return False
        finally :
            self.t_runtime = None

    def Switch2Kodi(self) :
        shuffle_mode = self.player.Shuffle()
        pos = self.player.Position()
        track_path = self.eps
        kodi = allinone.VlcMove(KODI_SOCKET, self.catSearch, self.player)
        # while not kodi.STOPPED :
        #     time.sleep(0.2)
        # TODO : Do not destroy player...
        # self.player = None
        print("Stopped kofi")
        # self.player.Play()
        self.SetFunc()
        self.player.Run(track_path)
        self.SetRoot()
        # self.player.Shuffle()
        self.player.Shuffle(shuffle_mode)
        self.SetPrefix()
        self.KFLOW = False

    def PrevNextSample(self, direction, goToEnd = True) :
        pos = self.player.Position()
        goTo = self.GoToSample(direction, pos, goToEnd)
        # self.cur_smp = goTo

        if len(goTo) > 0 :
            begin = goTo[0]
            end = goTo[1]

            # self.player.SeekTo(begin)
            # self.SetCurrentSample(begin)
            # self.target = self.player.GetTrackDuration()
            self.player.GoTo(begin, self.player.GetTrackPath())
            # self.player.Seek(goTo)
            # print("Will reach : {} > {}".format(txt.HRTime(begin, False), txt.HRTime(end)))
            # Fill timeline dict
            self.dicoDone[self.eps] = self.player.Position()
            self.local_target = end
            # self.printRunTime(self.TV_MODE)

    def SetSampleTime(self, time_smp) :
        if not self.TV_MODE :
            # self.SetSampleTime(self.SAMPLE_TIME)
            if not time_smp > 0 :
                self.SAMPLE_TIME = DEFAULT_TIME
            else :
                self.SAMPLE_TIME = time_smp
            return self.SAMPLE_TIME

        return -1

    def SetPrefix(self) :
        str_replace = " "
        PlayTags.PREFIX_MSG = "{} SMP "
        if self.TV_MODE :
            PlayTags.PREFIX_MSG = "{} TVSMP "

        if self.ADD_SAMPLE :
            PlayTags.PREFIX_MSG = "{} ADD "
        if self.NEW_SAMPLE > 0 :
            PlayTags.PREFIX_MSG = "{} NEW "
        if self.DEL_SAMPLE > 0 :
            PlayTags.PREFIX_MSG = "{} DEL "

        PlayTags.PREFIX_MSG = PlayTags.PREFIX_MSG.format(str_replace)

# BEGIN Events
    def Stopped(self) :
        if self.player.Exists() and self.player.IsStopped() :

        # # Waiting for player to go to next title for 1 second
            i = 0
            while self.player.IsStopped() :
                time.sleep(.1)
                i += 1
                if i > 10 :
                    print("{} {} {} -- {}".format(self.player.IsStopped(), self.player.IsPlaying(), self.player.Position(), self.player.Exists()))

                    self.player.FullScreen()
                    self.player.Seek(1)
                    self.player.Next()
                    self.player.FullScreen()
                    break
        # if self.player.IsStopped() :
        #     print("Stopped")
        #     self.player.Quit()
        pass

    def endLoop(self) :
        """
        Executed while video is playing
        """
        try :
            if self.t_runtime is None :
                self.t_runtime = Thread(target=self.ThreadRunTime)
                self.t_runtime.start()

            # while self.target <= 0 :
            #     time.sleep(.1)
            # self.target = self.local_target

            self.overwrite = self.target

            pos = self.player.Position()
            # print(self.eps)
            if self.ADD_SAMPLE or self.TV_MODE :
                self.SetCurrentSample(pos)
            # dico_eps = self.GetDicoEps()
            # if self.TV_MODE :
            if pos > 0 :
                self.dicoDone[self.eps] = pos

            if self.NEW_SAMPLE > 0 :
                # If we're already on a sample, then we move to nearly end
                on_tag = self.FindIfJump(self.eps, pos, self.catSearch)
                if on_tag > 0 and on_tag - self.player.Position() > 2 :
                    print("GOTO ADD : {}".format(txt.HRTime(on_tag - 1)))
                    self.player.GoTo(on_tag - 1, self.player.GetTrackPath())
                # self.SetCurrentSample(pos)

            elif self.player.GetTrackPath("file://", "") == self.eps :
                # if self.ADD_SAMPLE :
                # self.SetCurrentSample(pos)

                if pos >= self.target and not self.player.GetStatus() == "Paused" and not self.ADD_SAMPLE :
                    # if self.count_vid[self.eps] + 1 >= len(self.dico_eps) or not self.TV_MODE :
                    if not self.TV_MODE or (len(self.cur_smp) > 0 and self.cur_smp in self.dico_eps and self.dico_eps.index(self.cur_smp) == len(self.dico_eps) - 1 and pos >= self.cur_smp[1] - 1) :
                        i = 0
                        self.vol = self.player.Volume()
                        self.player.Next()
                        if self.player.IsStopped() :
                            while self.player.IsStopped() :
                                time.sleep(.05)
                                i += 1
                                if i % 5 == 0 :
                                    print("Wait IsStopped !")
                                    self.player.Next()
                                    break
                        self.player.Volume(self.vol)
                        i = 0
                        while self.player.GetTrackPath("file://", "") == self.eps :
                            # print("wait for new file")
                            time.sleep(.1)
                            i += 1
                            if i % 5 == 0 :
                                # print("Seems to be the same... (" + self.eps + "::" + str(self.count_vid[self.eps]   ) + ")")
                                print("[DBL] ", end = " ", flush = True)
                                print()
                                # self.printRunTime()
                                if len(self.dico_eps) > 0 :
                                    if self.count_vid[self.eps] + 1 >= len(self.dico_eps) :
                                        self.count_vid[self.eps] = 0
                                    else :
                                        self.count_vid[self.eps] += 1
                                    new_target = self.dico_eps[self.count_vid[self.eps]][1]
                                    new_time = self.dico_eps[self.count_vid[self.eps]][0]
                                else :
                                    new_target = self.dicoDone[self.eps] + self.SAMPLE_TIME
                                    new_time = self.dicoDone[self.eps]

                                self.target = new_target
                                self.player.Seek(new_time)
                                self.start = time.time()
                                # print("GOTO :: {} => {}".format(new_time, new_target))
                                break
                    else :
                        self.PrevNextSample(1, self.ADD_SAMPLE)
                # elif len(self.cur_smp) > 0 and pos + 1 < self.cur_smp[0] :
                #     t_old = self.cur_smp
                #     self.SetCurrentSample(pos)
                #     # if self.target
                #     self.cur_smp = t_old
                #     self.target = t_old[1]


            if self.KFLOW :
                if not allinone.VlcMove.GetSocket(*KODI_SOCKET.split(":")) is None :
                    self.Switch2Kodi()
                    # if self.TV_MODE :
                    #     self.afterLoopTv()
                    # else:
                    time.sleep(1)
                    self.eps = os.path.realpath(self.player.GetTrackPath("file://", ""))
                    # self.afterLoop()
                    # # self.target = 0
                    # self.PrevNextSample(1, self.ADD_SAMPLE)


            # self.printRunTime(self.TV_MODE)

            if self.player.GetStatus() == "Paused" :
                self.start = time.time()
        except Exception as e :
            raise Exception("endLoop : " + str(e))

    def afterLoop(self) :
        """ Executed after new playing video """
        try :
            # Saving new sample with end time as duration
            if self.eps not in Player.track_list :
                self.player.SetTrackInfo(self.eps)

            new_tag = self.SaveSample(Player.track_list[self.eps].duration)
            # Now we're gonna take care of new track
            self.eps = os.path.realpath(self.player.GetTrackPath("file://", ""))
            self.player.Volume(self.vol)

            # Get dico sample
            self.dico_eps = self.GetDicoEps(not self.TV_MODE, self.SetSampleTime(self.SAMPLE_TIME))
            if self.eps not in self.count_vid :
                self.count_vid[self.eps] = 0
            # We will filter
            if self.eps not in self.dicoDone :
                self.dicoDone[self.eps] = 0
            # print(self.player.GetTrackPath("file://", ""))

            cnt_smp = self.count_vid[self.eps]
            self.SetRoot()
            if self.player.IsStopped() :
                print("Wait IsStopped !")
                while self.player.IsStopped() :
                    time.sleep(.05)


#             if self.DEL_SAMPLE > 0 :
#                 p = self.vidTagged[self.eps][self.count_vid[self.eps]][1]
#                 self.DeleteSample(p)
            # print("ok 11 {}".format(self.dico_eps))
            if len(self.dico_eps) > 0 :

                if cnt_smp >= len(self.dico_eps) :
                    # raise Exception("WEIRD !!!" + self.eps + " :: {}/{}".format(cnt_smp, len(self.dico_eps)) )
                    print("WEIRD !!!" + self.eps + " :: {}/{}".format(cnt_smp, len(self.dico_eps)) )
                    self.SetCurrentSample(pos)
                    cnt_smp = self.count_vid[self.eps]
                # print(self.eps in self.vidTagged)
                begin = self.dico_eps[cnt_smp][0]
                end = self.dico_eps[cnt_smp][1]
                self.player.Seek(begin)
                self.cur_smp = self.dico_eps[cnt_smp]
                # print("CNT {} {} {}".format(cnt_smp, dico_eps, begin, dico_eps[-1]))

                self.dicoDone[self.eps] = self.player.Position()
                if end <= begin :
                    print(os.path.basename(self.eps) + " has bad timestamps :" + str(begin) + " = " + str(end) + " > " + str(end + 2))

                self.target = end

                if self.player.Position() < self.target - self.SAMPLE_TIME :
                    time.sleep(.1)
                if cnt_smp + 1 >= len(self.dico_eps) :
                    self.count_vid[self.eps] = 0
                else :
                    self.count_vid[self.eps] += 1
            else :
                self.dicoDone[self.eps] += self.SAMPLE_TIME
                # self.player.Seek(self.dicoDone[self.eps])
                self.set_new_vid(True, self.catSearch, done_vid=self.dicoDone[self.eps])
                self.target = self.dicoDone[self.eps] + self.SAMPLE_TIME
                # new_target = self.dicoDone[self.eps] + self.SAMPLE_TIME
                # new_time = self.dicoDone[self.eps]
            # print(self.eps + " " + self.player.track)
            # if self.eps in self.dicoDone :
            self.ADD_SAMPLE = False
            self.PrintRunTime(self.TV_MODE)
            self.start = time.time()
        except Exception as e :
            raise Exception("afterLoop : " + str(e))

    def onDoubleRate(self) :
        print("DOUBM")

    def onFullScreenChange(self) :
        # self.player.FullScreen
        nb_click = self.player.FullScreened(0, 4, False)
        # print(str(self.player.shuffle) + " " + str(nb_click))
        # self.player.FullScreen(self.player.fullscreen)
        # print(nb_click)
        if nb_click == 1 :
            # print("Fullscreen changed !")
            self.player.fullscreen = not self.player.fullscreen
            self.player.FullScreen(self.player.fullscreen)
        elif nb_click == 2 :
            self.KFLOW = not self.KFLOW
        #     # print(TagsMng.LIST_INDEX_FILES.keys())
        #     shuffle_mode = self.player.Shuffle()
        #     pos = self.player.Position()
        #     track_path = self.eps
        #     kodi = allinone.VlcMove(*KODI_SOCKET.split(":"), self.player, self.catSearch)
        #     # while not kodi.STOPPED :
        #     #     time.sleep(0.2)
        #     # TODO : Do not destroy player...
        #     # self.player = None
        #     print("Stopped kofi")
        #     # self.player.Play()
        #     self.player.Run(track_path)
        #     self.SetRoot()
        #     self.SetFunc()
        #     self.player.Shuffle()
        #     self.SetPrefix()
        #     # self.player.Next()
        #     # print(self.player.GetTrackId())
        #     # print(TagsMng.LIST_INDEX_FILES)
        #     # time_smp = self.SetSampleTime(self.SAMPLE_TIME)
        #     # for f in self.dirs :
        #     #     dicoTaggedList = self.GetList(f, self.catList, not self.TV_MODE, time_smp)
        #     #     dicoTaggedRead = self.GetList(f, self.catRead, not self.TV_MODE, time_smp)
        # # elif nb_click == 2 :

    def onLoopChanged(self) :
        """ 1 :  """
        nb_click = self.player.Looped(0, 4, False)
        # print(self.player.loop + " " + str(nb_click))
        self.player.LoopStatus(self.player.loop)
        if nb_click == 1 :
            if self.player.track_id == self.player.GetTrackId():
                self.player.GetControl().Next()
                i = 0
                while self.player.GetTrackPath("file://", "") == self.eps :
                    # print("wait for new file")
                    time.sleep(.05)
                    i += 1
                    if i == 6 :
                        self.player.GetControl().Next()
                        i = 0
                        time.sleep(.05)
                        break
                if self.md5.GetMd5(self.eps) in self.dicoDone :
                    # percent = round(self.dicoDone[self.md5.GetMd5(self.eps)] * 100 / self.player.track_duration, 2)
                    percent = txt.Percent(self.dicoDone[self.md5.GetMd5(self.eps)], self.player.track_duration)
                    print("DELETED " + str(percent) + "%  : " + self.eps + " " + self.player.track_id)
                self.player.Interface("TrackList").RemoveTrack(self.player.track_id)

    def onRateChange(self) :
        """
        Rate has change many times,
        We're gonna do something
        """
        if self.DEL_SAMPLE > 0 :
            return False

        # Player has to be at normal rate
        if not self.player.Rate() == 1.0 :
            nb_click = self.player.Rated(0, 9, False)
            self.player.Rate(1)
            if nb_click in [ -1, 1 ] :
                if self.NEW_SAMPLE >= 0 :
                    pos = self.player.Position()
                    if nb_click > 0 :
                        if len(self.cur_smp) > 0 :
                            self.player.Seek(pos - self.cur_smp[1])
                    else :
                        self.player.Seek(pos - self.NEW_SAMPLE)
                else :
                    self.PrevNextSample(nb_click, self.ADD_SAMPLE)
            elif nb_click == 2 :
                if self.DEL_SAMPLE > 0 :
                    print("CANCELLED DEL @{}".format(self.DEL_SAMPLE))
                    self.DEL_SAMPLE = -1
                else :
                    self.ADD_SAMPLE = True
                    # print("ADD SMP MODE {}".format(self.ADD_SAMPLE))
            elif nb_click == -2 :
            #     # print("DEL @{}".format(self.DEL_SAMPLE), end="")
            #     # print(nb_click)
            #     if self.DEL_SAMPLE > 0 :
            #         p = self.player.Position()
            #         self.DeleteSample(p)
                # elif self.ADD_SAMPLE :
                self.ADD_SAMPLE = False
                # print("ADD SMP MODE {}".format(self.ADD_SAMPLE))
            #     else :
            #         self.DEL_SAMPLE = self.player.Position()
            #         print("DEL WTF @{}".format(self.DEL_SAMPLE))
            #         # print("DEL @{}".format(self.DEL_SAMPLE), end=" ")
            #
            #
            #
            elif nb_click in [ -3, 3 ] :
                if self.DEL_SAMPLE > 0 :
                    self.DEL_SAMPLE = -1
                    return False
                # If we're already on a sample, then we move to nearly end
                pos = self.player.Position()
                self.DEL_SAMPLE = pos
                on_tag = self.FindIfJump(self.eps, pos, self.catRead[0])

                if not on_tag > 0 :
                #     print("ON TAG")
                # else :
                    idx = self.FindIndexNextJump(self.dico_eps, pos)
                    # if idx == -1 :
                    idx += 1
                    # else :
                    #     idx
                    new_pos = self.dico_eps[idx][0]
                    # print(dico_eps[idx])
                    print("GOTO DEL : {}".format(txt.HRTime(new_pos)))
                    self.player.Seek((new_pos - pos))
                    self.DEL_SAMPLE = new_pos

                    self.count_vid[self.eps] = idx

                else :
                    # If we're on tag, we're gonna wait for mark beginning !!!
                    # while self.player.IsPlaying() :
                    #     time.sleep(.1)
                    idx = self.count_vid[self.eps] - 1
                    if idx == -1 :
                        idx = 0
                    self.count_vid[self.eps] = idx

                    self.DEL_SAMPLE = self.FramePerfectMode(self.dico_eps[self.count_vid[self.eps]], pos)
                    self.player.Play()

                end_sample = self.FramePerfectMode(self.dico_eps[self.count_vid[self.eps]], self.DEL_SAMPLE, True)
                if end_sample > 0 :
                    self.DeleteSample(end_sample)
                #
                # print("Gonna delete : {} -> {}".format(self.DEL_SAMPLE, end_sample))
                # self.player.Rate(1)
                # self.player.Play()
                #     # print(idx)
                self.DEL_SAMPLE = -1
        self.SetPrefix()

    def onShuffleChanged(self) :
        try :
            nb_click = self.player.Shuffled(0, 4, False)
            # print(str(self.player.shuffle) + " " + str(nb_click))
            self.player.Shuffle(self.player.shuffle)
            if nb_click == 1 :
                self.player.shuffle = not self.player.shuffle
                self.player.Shuffle(self.player.shuffle)
            elif nb_click == 2 :
                root_file = self.root_file
                if self.TV_MODE :
                    self.TV_MODE = False
                    self.SetSampleTime(self.SAMPLE_TIME)
                    self.dico_eps = self.GetDicoEps(not self.TV_MODE, self.SAMPLE_TIME)
                    # On redéfini la target
                    self.target = self.player.Position() + self.SAMPLE_TIME
                else :
                    self.TV_MODE = True
                    # On redéfini la target
                    self.dico_eps = self.GetDicoEps()
                    if len(self.dico_eps) > 0 :
                        cur_sample = self.FindIndexNextJump(self.dico_eps, self.player.Position())
                        if cur_sample == -1 :
                            cur_sample = 0
                        self.count_vid[self.eps] = cur_sample
                        self.target = self.dico_eps[self.count_vid[self.eps]][1]
                    else :
                        self.target = Player.track_list[self.eps].duration
                print()
                self.SetRoot()
            self.SetPrefix()
        except Exception as e:
            raise Exception("TVMODE : " + str(e))

    def onQuadruplePause(self) :
        pass

    def onTriplePause(self) :
        if self.NEW_SAMPLE >= 0 :
            print("CANCELLED : {}".format(self.NEW_SAMPLE))
            self.NEW_SAMPLE = -1

    def onDoublePause(self) :
        pos = self.player.Position()
        # if self.player.GetTrackPath().replace("file://", "") == self.eps :
        pos = round(pos, 2)
        if pos <= 0.9 :
            pos = 0.0

        if self.NEW_SAMPLE > 0 :
            if pos == -1 :
                pos = self.duration
            else :
                pos = self.FramePerfectMode(prefix="END")
            new_tag = self.SaveSample(pos)
        else :
            if len(self.catList) :
                if self.TV_MODE :
                    if len(self.cur_smp) > 0 :
                        if pos >= self.cur_smp[0] and pos <= self.cur_smp[1] :
                            # pos = self.cur_smp[0]
                            print("SEEK BEG :: {} <- {} - ({})".format(self.cur_smp[1], pos, self.cur_smp[0]))
                            self.player.GoTo(self.cur_smp[0], self.player.GetTrackPath())
                    self.NEW_SAMPLE = self.FramePerfectMode(prefix="BEG")
                    self.ADD_SAMPLE = True
                    pos = self.NEW_SAMPLE

                    if len(self.cur_smp) and pos >= self.cur_smp[0] and pos <= self.cur_smp[1] :
                        # pos = self.cur_smp[0]
                        print("SEEK END :: {}".format(self.cur_smp[0] - pos))
                        self.player.GoTo(self.cur_smp[1], self.player.GetTrackPath())

                else :
                    self.NEW_SAMPLE = pos
                    new_tag = self.SaveSample(pos + self.SAMPLE_TIME + self.SAMPLE_TIME * (1 / 3))
                    self.ADD_SAMPLE = False

                    if len(self.player.GetTracks()) > 1 :
                        self.player.GetControl().Next()

                        i = 0
                        while self.player.GetTrackPath("file://", "") == self.eps :
                            # print("wait for new file")
                            time.sleep(.1)
                            i += 1
                            if i == 5 :
                                print("[DBL]", end=" ")
                                self.set_new_vid(True, self.catSearch, done_vid=self.dicoDone[self.eps])
                                break
            else :
                print("No cat")
        self.SetPrefix()
        self.player.paused = False

    def onPause(self) :
        # print("Paused on " + self.eps)
        if self.NEW_SAMPLE >= 0 :
            pos = self.player.Position()
            self.player.Play()

            new_tag = self.SaveSample(pos)
        elif self.DEL_SAMPLE >= 0 :
            print("WTF {}".format(self.DEL_SAMPLE))

            pos = self.player.Position()
            self.player.Play()
            self.player.Rate(1)

            new_tag = self.DeleteSample(pos)
        else :
            self.player.Interface().Pause()

            while self.player.GetStatus() == "Paused" :
                # if not self.player.Rate() == 1.0 :
                #     nb_click = self.player.Rated(0, 9, False)
                #     self.player.Rate(1)
                #     print(nb_click)
                nb_click = self.player.Paused(0, 10, False)

                if nb_click == 1 :
                    self.player.Play()
                elif nb_click == 2 :
                    # Search category
                    if len(self.catEdit) > 1 :
                        self.catList = self.SetNewCat(self.catSearch)
                    else :
                        self.catList = self.SetNewCat(self.catSearch.split("/")[0])
                    print(self.catList)
                    self.ADD_SAMPLE = True

                elif nb_click == 3 :
                    # print(TagsMng.LIST_INDEX_FILES.keys())
                    shuffle_mode = self.player.Shuffle()
                    kodi = allinone.VlcMove(*KODI_SOCKET.split(":"), self.player, self.catSearch)
                    # while not kodi.STOPPED :
                    #     time.sleep(0.2)
                    # TODO : Do not destroy player...
                    # self.player = None
                    print("Stopped kofi")
                    self.SetFunc()
                    self.player.Shuffle(shuffle_mode)
                    self.player.Next()
                elif nb_click == 4 :
                    # Changing tag category
                    THROW_TAG = CmdSocket.MAIN_CAT
                    if self.catSearch == THROW_TAG :
                        self.catSearch = self.cat
                    else :
                        self.catSearch = THROW_TAG
                    print("CAT : " + self.catSearch)
                    if self.TV_MODE :
                        self.player.Play()
                    print("8")
                elif nb_click == 8 :
                    print("8")
            self.start = time.time()

# END Events

    def SetFunc(self) :
        l_meth_endLoop = self.endLoop
        # if self.TV_MODE :
        #     l_meth_afterLoop = self.afterLoopTv
        # else :
        l_meth_afterLoop = self.afterLoop

        while self.player.track == "" :
            self.player.track = self.player.GetTrackPath("file://", "")
        self.start = time.time()

        self.player.m_paused = [ self.onPause, self.onDoublePause, self.onTriplePause ]

        self.player.m_loopTrack = [self.onLoopChanged]
        self.player.m_shuffle = [self.onShuffleChanged]

        # self.player.m_loopTrack = [ self.onLoopChange ]
        # self.player.m_rate = [ self.onssRateChange, self.onDoubleRate ]
        self.player.m_fullscreen = [ self.onFullScreenChange ]
        self.player.m_rate = [ self.onRateChange ]
        self.player.Stopped = self.Stopped
        # self.player.m_beg_loop = []
        self.player.endLoop = l_meth_endLoop
        self.player.afterLoop = l_meth_afterLoop
        # print(self.player.m_after_loop)
        # print(self.player.track)

        # self.player.LoopStatus("Playlist")
        self.player.loop = self.player.LoopStatus()
        self.player.shuffle = self.player.Shuffle()

    def __init__(self, VLC_ARG, tag_new, player = None, tag_read = "") :
        """
        Run the remote kodi video on local vlc.
        """
        try :
            global SHOW_STAT, KODI_FOLLOW, KODI_SOCKET, VLC_OPT, QUALITY

            self.eps = ""                   # Main player episode path
            self.player = None              # Main player
            self.vol = 0.5                  # Main player volume

            self.dicoDone = {}              # Time where we position has stopped
            self.TV_MODE = TV_MODE          # Playing full/sampled video
            # Sample management
            self.SAMPLE_TIME = TIME         # Time to play then next
            self.dico_eps = []              # TimeStamps array
            self.cur_smp = []               # Current timeStamps array
            self.KFLOW = KODI_FOLLOW
            self.t_runtime = None

            self.local_target = -1
            RANDOM = False
            TAG = ""

            time_smp = self.SetSampleTime(self.SAMPLE_TIME)

            self.player = player
            args_cmd = []
            for p in VLC_ARG :
                args_cmd.append(os.path.realpath(p))

            super().__init__(args_cmd, tag_new, tag_read, QUALITY)

            VLC_ARG = list(self.vidTagged.keys())

            # if FfmpegTools.check_call("command -v ffprobe >> /dev/null") != 0 :
            #     print("ffmpeg has not been found on this system !")

            # for k in VLC_ARG :
            #     if os.path.exists(k) :
            #         # print("{} {} {} {}".format(k, FfmpegTools.GetFPS(k), FfmpegTools.GetQuality(k), FfmpegTools.GetTime(k)))
            #         self.eps = k
            #         self.SetInfoFile(FfmpegTools.GetQuality(k), FfmpegTools.GetFPS(k), FfmpegTools.GetTime(k))
            #         # if "duration" not in
            # self.tags.Save()
            # sys.exit(0)

            self.catSearch = self.catList[0]

            if SHOW_STAT :
                self.Stats()
                sys.exit(0)

            if not len(VLC_ARG) > 0 :
                print("No {} sample found in '{}'".format(self.catRead, self.dirs))
                sys.exit(0)

            while True :
                try :
                    # If process has exited, we break
                    if len(proc_list) > 0 :
                        if not proc_list[0].is_alive() :
                            print("process die...")
                            break

                    if self.player == None :
                        # print("wtf")
                        self.player = self.runInstance(list(VLC_ARG), VLC_OPT)
                        fullcreeen_mode = self.player.FullScreen()
                        self.SetFunc()
                        self.player.FullScreen(fullcreeen_mode)

                        # self.cli = VlcCLI(self.player)
                        # t = Thread(target=self.cli.Run)
                        # t.start()

                    elif not self.player.Exists() :
                        print("TF2")
                        self.cli.CONTINUE = False
                        break
                    else :
                        self.SetFunc()

                    # if not self.player.LoopPlayer(self.eps, [self.PrintRunTime]) :
                    if not self.player.LoopPlayer(self.eps) :
                        print("TF3")
                        self.cli.CONTINUE = False
                        self.player.Quit()
                        break
                except KeyboardInterrupt as e:
                    print("Interruption")
                    self.cli.CONTINUE = False
                    if len(proc_list) > 0 :
                        if proc_list[0].is_alive() :
                            proc_list[0].join(1)
                        proc_list.pop(0)
                    # return False

        except Exception as e:
            print("Error : "  + str(e))
            # return False
        finally :
            self.md5.Save()

            if len(self.dicoDone) > 0 :
                with open(FILE_TIME, 'w') as fp:
                    json.dump(self.dicoDone, fp, indent=4)

            if self.counter_save[0] > 0 :
                print("Total : {} samples saved for {}".format("+" + str(self.counter_save[0]), str(self.counter_save[1]) +"s"))


            if len(proc_list) > 0 :
                if proc_list[0].is_alive() :
                    proc_list[0].join(1)
                proc_list.pop(0)
            # return True

def SetConfig(argv):
    """
    Args treatment
    """
    try:
        opts, args = getopt.getopt(argv, usage.GetOpts(), [*usage.GetOptsValue(), "help"])
    except Exception as ex:
        # print(argv)
        if "-j" in argv :
            argv.pop(argv.index("-j"))
            opts, args = getopt.getopt(argv, usage.GetOpts(), [*usage.GetOptsValue(), "help"])
            FindOptValue("--jump")
        elif "--jump" in argv :
            argv.pop(argv.index("--jump"))
            opts, args = getopt.getopt(argv, usage.GetOpts(), [*usage.GetOptsValue(), "help"])
            FindOptValue("--jump")

        else :
            print(ex)
            usage.usage()
            sys.exit(2)
        global JUMP_MODE
        print("STR !:: " + str(JUMP_MODE))
           # argv.pop(argv.index("-j"))
            # opts, args = getopt.getopt(argv, usage.GetOpts(), [*usage.GetOptsValue(), "help"])
    try:
        return usage.SetDico(opts, args)
    except Exception as ex:
        # usage(argv)
        print(ex)
        usage.usage()
        sys.exit(2)

# import time, sys
#
# lorem = "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
#
# for x in lorem:
#     print("\b"+x, end=" ", flush=True)
#     time.sleep(0.03)
#     # sys.stdout.flush()
#
# sys.exit(1)
proc_list = []
if __name__ == "__main__":
    try :

        JUMP_MODE = -1
        config = SetConfig(sys.argv[1:])
        for opt in config :
            arg = config[opt]
            # print(opt + " = " + str(arg))
            if opt == "t" :
                if arg.startswith("-") :
                    raise Exception("-t needs float value !")
                TIME = float(arg)
            elif opt == "j" :
                JUMP_MODE = 10
                if arg.startswith("--") or arg.startswith("-"):
                    FindOptValue(arg)
                elif arg.startswith("/"):
                    VLC_ARG.append(os.path.realpath(arg))
                elif not arg == "" :
                    JUMP_MODE = arg
            elif opt == "v" :
                OUTPUT_CMD = ""
            elif opt == "e" :
                TAG_NEW = arg
            elif opt == "r" :
                TAG_READ = arg
            elif opt == "rr" :
                TAG_READ_RANDOM = arg
                TAG_READ = arg
            elif opt == "n":
                TAG_NEW = arg
            # elif opt == "--":
            #     FOLDERS = arg
            elif opt == "stat" :
                SHOW_STAT = True
            elif opt == "K" :
                KODI_FOLLOW = True
            elif opt == "S" :
                if len(arg.split(":")) > 1 :
                    KODI_SOCKET = arg
                else :
                    KODI_SOCKET = arg + ":" + str(KODI_SOCKET_DEFAULT)
            elif opt == "notv" :
                TV_MODE = False
            elif opt == "q" :
                QUALITY = arg
            elif opt == "cvlc" :
                VLC_CMD = "cvlc"
            elif opt == "path" :
                # VLC_OPT.append(opt)
                # if arg != "" :
                for p in arg :
                    # print(p)
                    if os.path.exists(p) :
                        VLC_ARG.append(p)
            else :
                if len(opt) > 1 :
                    VLC_OPT.append("--" + opt)
                else :
                    VLC_OPT.append("-" + opt)
                if arg != "" :
                    VLC_OPT.append(arg)
        if len(VLC_ARG) == 0 :
            print("Error ! Please specify valid path")
            usage.usage()
            sys.exit(2)

        # print(FOLDERS)
        # sys.exit(2)
        VLC_OPT.append("--no-osd")
        # print(VLC_OPT)
        # print(VLC_ARG)
        if TAG_NEW == "" :
            TAG_NEW = TAG_READ

        if TAG_NEW == "" :
            print("No category specified !" + str(TAG_READ))
            usage.usage()
            sys.exit(2)
        # print("{} != {}".format(TAG_NEW, TAG_READ))

        VlcSample(VLC_ARG, TAG_NEW, None, TAG_READ)

    except Exception as e:
        print(str(e))
        # usage.usage()
        # if DEBUG :
        #     if e.errno == os.errno.EsNOENT:
        #         sys.exit(e.errno)

    finally :
        print("End Samples")
        if len(proc_list) > 0 :
            if proc_list[0].is_alive() :
                proc_list[0].join(1)
            proc_list.pop(0)
