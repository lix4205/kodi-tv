# Copyright (c) Elie Coutaud 2023
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!/usr/bin/python
# -*- coding: latin-1 -*-
import os
import json
import sys
import time
from koditvm.pythcomm.helper_help import usage
import koditvm.pythcomm.txt_utils as txt
from koditvm.pythcmd.spinner import spinner
import getopt
from random import uniform, choices, sample
from koditvm.ffmpeg_tools import FfmpegTools, FfmpegConcat, FfmpegSplit
from koditvm.TagsMng import TagsList, TagsMng
import subprocess
from koditvm.Constants import CmdSocket

# ffprobe ~/DL/20220715/Bori_is_back_hu095.mp4
# ffmpeg -ss 0 -i ~/DL/20220715/Bori_is_back_hu095.mp4 -ss:a 0 -i ~/Musique/Noisia/2016\ -\ Outer\ Edges/03\ -\ Noisia\ -\ Collider.mp3 -t 286 -t:a 286 -c:v libx265 -map 0:v -map 1:a /tmp/test.mp4
# ffmpeg  -ss:a 0 -i ~/Musique/Noisia/2016\ -\ Outer\ Edges/03\ -\ Noisia\ -\ Collider.mp3 -i ~/DL/20220715/Bori_is_back_hu095.mp4 -i ~/DL/20220715/HuCow_38_Sybian_and_goat_milker_hu287.mp4 -i ~/DL/20220715/Katz_Cow_exercise_hu048.mp4 -filter_complex "[1:v:0][2:v:0][3:v:0]concat=n=3:v=1:a=0[outv]" -map "[outv]" -map 0:a -c:v libx265 /tmp/output.mkv

AUDIO_FILE = ""

OUTPUT_CMD_FILE = "/tmp/test_fvm.log"
OUTPUT_CMD = "2>> " + OUTPUT_CMD_FILE
OUTPUT_CMD_SAMPLE = "2>> " + OUTPUT_CMD_FILE
OUTPUT_CMD_INTERMEDIATE = "2>> " + OUTPUT_CMD_FILE

TIME = 5
VLC_ARG = []
# VLC_CMD = "cvlc"
VLC_CMD = "vlc"

W_DIR = "/media/xud/.dux/ffmpeg-sample/"
SAFE_FILE = "/media/xud/.dux/ffmpeg-sample/.safe.json"
FINAL_FILE = ""
RANDOM = False
TARGET = ""
TAGS = ""

# BEGIN Help

# Deleting log file
if os.path.exists(OUTPUT_CMD_FILE) :
    if os.stat(OUTPUT_CMD_FILE).st_size > 5 * 1024 * 1024 :
        os.remove(OUTPUT_CMD_FILE)

# TODO : Recuperer usage depuis DZ /!\
# TODO : Add vlc options !!!
# --video-x 0 --video-y 1024 -f --crop 16:9  --play-and-exit --no-random
usage.OPTIONS_ALIAS = {
    "v" : "verbose",
    "t" : "time",
    "Z" : "random",
    "a" : "audio",
    "w" : "workdir",
}
usage.OPTIONS_VALUES = {
    "w" : "directory",
    "v" : "verbose",
    "t" : "time",
    "ts" : ".ts directory to compile",
    "target" : "Target directory",
    "final" : "final ts file",
    "a" : "audio file",
    "video-x" : "width",
    "video-y" : "height",
    "crop" : "(4:3,16:9,...)",
    "tags" : "tag",
}
# usage.OPTION_LOCAL = [
#     "N" , "no-random", "f", "cvlc", "play-and-exit", "video-x", "video-y" #: "run without synchro",
# ]
usage.OPTIONS_HELP = {
    "a" : "Audio file",
    "f": "Fullscreen",
    "t" : "time to wait before switching",
    "ts" : "use directory containing .ts files",
    "target" : "Target directory",
    "final" : "final ts file to compile with music",
    "fps" : "Frames per second (default : 30)",
    # "sample" : "Sample mode (play 10s then next video) ",
    "tags" : "Use tags",
    "no-exit": "Do not exit after end of playlist",
    "cvlc" : "Do not use vlc Interface",
    "video-x" : "Window width",
    "video-y" : "Window height",
    "crop" : "crop image",
    # "play-and-exit" : "Exit when playlist has finish",
    "Z": "Random mode",
    "w" : "Working directory",
    "v" : "Verbose mode (ffmpeg final video creation) | -vv (ffmpeg intermediate video creation) | -vvv (ffmpeg all video creation)",
    "path" : "local files to add to vlc playlist"
}

usage.OPTIONS_DEFAULT = {
    "-v" : "",
    "--tags" : CmdSocket.MAIN_CAT }
usage.OPTIONS_MANDATORY = ["path"]
usage.OPTIONS_ARGS = ["path"]
# usage.OPTIONS_MANDATORY = []
# TODO : Use Text as first line and use help.CMD instead
usage.TEXT = "python " + sys.argv[0]
usage.DESC = "Compile samples of movies and mix them with audio file"
# END Help


LOADING = True

class Clip() :
    CMD = {
        # "ffprobe_frames" : "ffprobe -v error -select_streams v:0 -count_frames -show_entries stream=nb_read_frames  -print_format default=nokey=1:noprint_wrappers=1 \"{}\"",
        # "duration" : "ffprobe \"{}\" 2>/dev/stdout | grep Duration",
        # "ffmpeg_sample" : "ffmpeg -y -r {} -ss {} -i \"{}\" -t {} -c:v {} -map 0:v -map 0:a -vf scale=\"1920:1080\" -c:a aac -af \"apad\" \"{}\" {}",
        # "ffmpeg_concat" : "ffmpeg -y -r 30 {} -c:v {} -c:a aac -af \"apad,volume=0.5\" -shortest -avoid_negative_ts make_zero -fflags +genpts \"{}\" {}",
        # "ffmpeg_wav" : "ffmpeg -y -i \"{}\" -i \"{}\" -filter_complex amix=inputs=2:duration=first \"{}\" {}",
        "ffmpeg_end" : "ffmpeg -y -r {} -ss 0 -i \"{}\" -ss:a 0 -i \"{}\" -t {} -t:a {} -c:v {} -map 0:v -map 1:a -vf fps={} -vf scale=\"1920:1080\" -c:a aac \"{}\" {}",

        # "ffmpeg_end" : "ffmpeg -y -i \"{}\" -t {} \"{}\" {}"

        # "ffmpeg_sample" : "ffmpeg -r 30 -y -ss {} -i \"{}\" -t {} -c:v {} -map 0:v -vf crop=16:9  -vf scale=\"1920:1080\" \"{}\" {}",
        # "ffmpeg_concat" : "ffmpeg -r 30 -y {} -c:v {} \"{}\" {}",
        # "ffmpeg_end" : "ffmpeg -r 30 -y -ss 0 -i \"{}\" -ss:a 0 -i \"{}\" -t {} -t:a {} -c:v {} -map 0:v -map 1:a -vf crop=16:9 -vf scale=\"1920:1080\" \"{}\" {}",
    }

    def GetRandomTime(self, duration, begin = -1, end = -1) :
        """
        Get random time (in every way...)
        """
        if begin == -1 :
            begin = duration * 0.05
        if end == -1 :
            end = duration * 0.95 - self.SAMPLE_TIME
        # l_files.append(new_dir)
        new_time = uniform(begin, end)
        # print(str(begin) + " >= " + str(end) + " - " + str(new_time))
        if new_time < 0 :
            while new_time < duration * 0.05 :
                new_time = uniform(begin, end)
        return new_time


    def SearchFilesInDir(self, arg_path, idx = 1, tot = 0) :
        """
        Scanning for existing files
        """
        l_files = []
        proc_list = []
        for path in arg_path :
            if os.path.exists(path) :
                if idx != 0 :
                    # sys.stdout.write('\033[2K\033[1G')
                    print("\r[{}/{}] Scanning : '{}'".format(idx, tot, path), end="")
                if os.path.islink(path) :
                    tot -= 1
                    continue
                elif os.path.isdir(path) :
                    # if len(proc_list) <= self.PARALLEL_PROC_NUMBER :
                    #     durThread = runner.start(method=self.ScanDir, args_method=[path, idx, tot])
                    res = self.ScanDir(path, idx, tot)
                    idx = res[0]
                    tot = res[1]
                elif os.path.isfile(path) :
                    res = self.ScanFile(path, idx, tot)
                    idx = res[0]
                    tot = res[1]

        return [ idx, tot ]

    def ScanDir(self, path, idx = 1, tot = 0) :
        """
        Scan a directory recursivly or try to scan a file
        """
        tot += len(os.listdir(path))
        for l_dir in os.listdir(path) :
            new_dir = os.path.join(path, l_dir)

            if os.path.islink(new_dir) :
                tot -= 1
                continue
            elif os.path.isdir(new_dir) :
                # tot -= 1
                res = self.SearchFilesInDir([new_dir], idx, tot)
                idx = res[0]
                tot = res[1] - 1
            elif os.path.isfile(new_dir) :
                if new_dir.lower().endswith(".xspf") or new_dir.lower().endswith(".jpg") or new_dir.lower().endswith(".png") or new_dir.lower().endswith(".part") or new_dir.lower().endswith(".xcf") or new_dir.lower().endswith(".gif") or new_dir.lower().endswith(".jpeg") or new_dir.lower().endswith(".zip") or new_dir.lower().endswith(".rar") or new_dir.lower().endswith(".pdf")  :
                    tot -= 1
                    continue

                res = self.ScanFile(new_dir, idx, tot)
                idx = res[0]
                tot = res[1]

        return [ idx, tot ]

    def ScanFile(self, path, idx = 1, tot = 0) :
        """
        Scan a file and add it to dict (if file is valid video file)
        """
        if path in self.dicoTime :
            duration = self.dicoTime[path]
        else :
            duration = FfmpegTools.GetTime(path.strip())

        if duration > self.SAMPLE_TIME :
            # print("PATH :: " + path)
            self.dicoTime[path] = duration
            self.dicoSample[path] = []
            self.SamplesFromTag(path)
            # print("SS FIN" + )
            if not path in self.dicoSample :
                # print("SS FIN" + path)
                self.dicoSample[path] = []

            if len(self.dicoSample[path]) == 0 :
                print("[PRISE RDM]", end = "")
                while len(self.dicoSample[path]) <= self.FRAME_FILE :
                    new_time = self.GetRandomTime(duration)
                    self.dicoSample[path].append(new_time)
                print()
            idx += 1
            # tot += 1
            return [ idx, tot ]
        else    :
            tot -= 1
            # idx -= 1
            return [ idx, tot ]

    def SamplesFromTag(self, path = "") :
        length = 0
        dicoTagged = {}
        # print(" [NEW SAMPLE]", end=" ")
        if path != "" :
            if path in self.vidTagged :
                 # If no sample in self.vidTagged[path], then we refresh we  self.vidTagged[path]
                if len(self.vidTagged[path]) == 0 :
                    # print("| RAZ : ", end = " ")
                    self.vidTagged[path] = [ *self.origVidTagged[path]]
                    # self.origVidTagged[v] = [ *self.vidTagged[v] ]
                dicoTagged[path] = self.vidTagged[path]
                # print(TagsMng.TotalTimeEps()
            else :
                print(" not in vidTagged")
                # return -1
        else :
            dicoTagged = self.vidTagged
            # print(": {}".format(self.tags.TotalTime(self.cat)), end="")


        for vid in dicoTagged :
            if len(dicoTagged[vid]) == 0 :
                continue
            # print(len(dicoTagged[vid]))

            # print("INIT " + vid)
            self.dicoSample[vid] = []
            length += len(dicoTagged[vid]) * self.SAMPLE_TIME
            # print(dicoTagged[vid])
            for t in dicoTagged[vid] :
                # print("APPEND " + vid + " " +str(t))
                self.dicoSample[vid].append(t[0])
                self.vidTagged[vid].pop(self.vidTagged[vid].index(t))
                if len(self.dicoSample[vid]) >= self.FRAME_FILE :
                    break

        # print(str(length), end=" ")
        return length

    def GetFFmpegInfo(self, l_dir) :

        if not os.path.exists(l_dir) :
            print()
            print("{} does not exists".format(l_dir))
            if l_dir in self.dicoSample :
                self.dicoSample.pop(l_dir)
            # l_count_file -= 1
            return None

        fftools = FfmpegSplit(l_dir, self.FPS, 1, False)
        if fftools.FPS_VID == - 1 :
            print()
            print("Error while searching fps for: " + l_dir)
            if l_dir in self.dicoSample :
                self.dicoSample.pop(l_dir)
            # l_count_file -= 1
            return None
        if not fftools.SetSample(self.SAMPLE_TIME) :
            print()
            print("Unable to set sample for {} (division by 0 ?)".format(l_dir))
            if l_dir in self.dicoSample :
                self.dicoSample.pop(l_dir)
            # l_count_file -= 1
            return None

        return [ l_dir, fftools ]

    def LoopForSample(self) :
        l_sample = list(self.dicoSample.keys())
        l_idx = 0
        l_count_file = len(l_sample)
        l_dico = {}
        proc_list = []

        for l_dir in sample(l_sample, l_count_file) :
            l_idx += 1

            sys.stdout.write('\033[2K\033[1G')
            print("\r[{}/{}] Calculating {}/{} : '{}'".format(round(self.FRAME_COUNT/self.FPS,2), round(self.FRAME_TOTAL/self.FPS,2), l_idx, l_count_file, l_dir), end="")

            proc_cmd  = FfmpegTools.runner.start(method=self.GetFFmpegInfo, args_method=[l_dir])
            proc_list.append(proc_cmd)

            if len(proc_list) >= self.PARALLEL_PROC_NUMBER :
                for proc in proc_list :
                    if not proc.is_alive() :
                        a_fftools = proc.returnMethod
                        if a_fftools != None :
                            fftools = a_fftools[1]
                            self.time_new += fftools.SAMPLE_TIME
                            self.FRAME_COUNT += (fftools.SAMPLE_TIME * (fftools.FPS_VID * fftools.RAPPORT_FPS))
                            l_dico[a_fftools[0]] = fftools
                        else :
                            l_count_file -= 1
                            l_idx -= 1
                        proc_list.pop(proc_list.index(proc))

            if self.FRAME_COUNT >= self.FRAME_TOTAL  :
                break

        while len(proc_list) > 0 :
            for proc in proc_list :
                if not proc.is_alive() :
                    a_fftools = proc.returnMethod
                    if a_fftools != None :
                        fftools = a_fftools[1]
                        self.time_new += fftools.SAMPLE_TIME
                        self.FRAME_COUNT += (fftools.SAMPLE_TIME * (fftools.FPS_VID * fftools.RAPPORT_FPS))
                        l_dico[a_fftools[0]] = fftools
                    proc_list.pop(proc_list.index(proc))
        # print(len(l_dico))
        # sys.exit(0)
        return l_dico

    def Sampling(self, l_dir, target, fftools, l_idx, work_dir) :
        # l_sample = list(self.dicoSample.keys())
        # l_idx = 0
        l_count_file = len(list(self.dicoSample.keys()))
        # l_dico = {}
        #
        # print(l_dico)

        adj_0 = FfmpegSplit.SetFileName(int(self.FRAME_TOTAL/self.FPS * 1000))
        # for l_dir in l_dico :
            # l_idx += 1
        # fftools = l_dico[l_dir]

        sys.stdout.write('\033[2K\033[1G')
        print("\r[{}/{}] Sampling {}/{} : '{}'".format(round(self.FRAME_COUNT/self.FPS,2), round(self.FRAME_TOTAL/self.FPS,2), l_idx, l_count_file, l_dir), end="")

        # If whole scenes has been treated, we're gonna generate new samples
        work_file = os.path.join(work_dir, os.path.basename(l_dir))
        if l_dir not in self.dicoSample or (l_dir in self.dicoSample and len(self.dicoSample[l_dir]) == 0) :
            self.ScanFile(l_dir, 0)

        if l_dir not in self.dicoSample :
            print("\nUnable to get sample from {}\n".format(l_dir))
            # continue

        path_sample = os.path.join(W_DIR, str(self.SAMPLE_TIME), os.path.basename(l_dir))
        smp_list = []
        # l_dico = self.dicoSample[l_dir]

        # For each time in dict
        for new_time in self.dicoSample[l_dir] :
            l_smp = fftools.LoopForSample(path_sample, round(new_time, 2), round(new_time + self.SAMPLE_TIME * 2, 2), OUTPUT_CMD_SAMPLE)
            if len(l_smp) > 0 :
            # print(str(round(new_time, 2)) + "/" + str(round(new_time+ self.SAMPLE_TIME * 2, 2) ), end = "")
                smp_list.append(l_smp[0])
                # fftools.LAST_CMD = ""

            else :
                print("\nFailed {} : {} -> {}".format(os.path.basename(path_sample), round(new_time, 2), round(new_time + self.SAMPLE_TIME * 2, 2)))
                print(fftools.LAST_CMD)
                fftools.LAST_CMD = ""

        for output_file in smp_list :
            if self.FRAME_COUNT >= self.FRAME_TOTAL  :
                break
            if os.path.exists(output_file) :
                if os.stat(output_file).st_size > 0 :
                    if os.stat(output_file).st_size > 12 * 1024 :
                        # target = os.path.join(self.FINAL_WDIR, Right(adj_0 + str(int(round(self.time_new,2) * 1000)), len(adj_0)) + ".ts")
                        if os.path.exists(target) :
                            os.remove(target)
                        os.symlink(output_file, target)
                        self.FRAME_COUNT += (fftools.SAMPLE_TIME * (fftools.FPS_VID * fftools.RAPPORT_FPS))
                        # self.time_new += fftools.SAMPLE_TIME #* fftools.RAPPORT_FPS
                else :
                    os.remove(output_file)
            if self.FRAME_COUNT >= self.FRAME_TOTAL  :
                break

        self.dicoSample[l_dir] = []
        return self.time_new

    def CreateWavFile(self, audio_file_1, audio_file_2, output_file_wave) :
        return FfmpegTools.MixAudioFile(audio_file_1, audio_file_2, output_file_wave)

    def endsin_array(val, arr) :
        for v in arr :
            if v.endswith(val) :
                return True
        return False

    # def SplitedTimeStamp(self, a_tsp) :
    #     a_res = []
    #     for t in a_tsp :
    #         idx = 0
    #         for i in range(int(t[0]), int(t[1]) + 2) :
    #             a_res.append([t[0] + idx * self.SAMPLE_TIME, t[0] + idx * self.SAMPLE_TIME + self.SAMPLE_TIME])
    #             idx += 1
    #     return a_res


    def Create(self) :
        """
        Create a music video with mixed video samples
        """
        try :
            global RANDOM, TAGS, W_DIR, AUDIO_FILE, VLC_ARG

            instance = None
            set_done = False
            input_cmd = ""
            filter_cmd = ""
            l_vid2launch = ""
            instance_name = ""
            l_vid2localSample = ""
            l_i_secs = 0
            idx = 0
            new_time = 0
            self.time_new = 0
            DURATION_DONE = 0
            films = []
            CUR_PLS = []
            LIST = []
            proc_list = []
            count_vid = {}

            TO_IGNORE = [ ]
            DIR_VIDS = "/media/xud/.dux/DL"
            FILE_MD5 = os.path.join( DIR_VIDS, "ENCOURS", ".md5.json")
            FILE_INDEX = os.path.join( DIR_VIDS, "ENCOURS", ".index.json")
            IGNORE_FILE_PATH = W_DIR + ".ignoremov" + ".json"

            # kodi_sec = .3
            self.eps = ""
            self.duration = 0
            self.target = 0
            self.dicoTime = {}
            self.vidTagged = {}
            self.origVidTagged = {}
            self.dicoSample = {}
            self.PARALLEL_PROC_NUMBER = 20       # Number of process in parallel to run to create samples
            self.FRAME_COUNT = 0
            self.FRAME_FILE = 10

            self.FPS = 60

            self.SAMPLE_TIME = TIME
            runner = spinner()
            time_v = FfmpegTools.GetTime(AUDIO_FILE.strip())
            # try :
            # global W_DIR, AUDIO_FILE, VLC_ARG

            self.FRAME_TOTAL = time_v * self.FPS


            print(AUDIO_FILE + " : " + str(time_v))

            work_dir = os.path.join(W_DIR, str(self.SAMPLE_TIME))
            final_work_dir = os.path.join(W_DIR, os.path.basename(AUDIO_FILE), str(self.SAMPLE_TIME))

            self.FINAL_WDIR = final_work_dir

            if os.path.exists(IGNORE_FILE_PATH) :
                with open(IGNORE_FILE_PATH, 'rb') as f:
                    TO_IGNORE = json.load(f)
            # print(TO_IGNORE)
            # sys.exit(0)
            if self.SAMPLE_TIME > 2 :
                self.PARALLEL_PROC_NUMBER = 2
            elif self.SAMPLE_TIME > 0.9 :
                self.PARALLEL_PROC_NUMBER = 5

            tt = TagsList(VLC_ARG)

            if TAGS == "" :
                TAGS = CmdSocket.MAIN_CAT

            # for f in TagsMng.LIST_INDEX_FILES :
            #     print(TagsMng.LIST_INDEX_FILES[f].SearchForMultipleTags(TAGS.split(",")))

            # indexed_files = TagsMng.LIST_INDEX_FILES
            if not len(TagsMng.LIST_INDEX_FILES) > 0 :
                raise Exception("No index file in {}".format(",".join(VLC_ARG)))


            # self.cat = TAGS

            # for f in indexed_files :
            #     FILE_INDEX = f
            #     # FILE_MD5 = os.path.join(FILE_INDEX, os.path.join(FOLDER_VIDS, INDEX_VIDS_MD5))
            #     FILE_TIME = os.path.join(FILE_INDEX, ".tags_tv.json")
            #
            #     TagsMng.LIST_INDEX_FILES[f] = TagsMng(os.path.join(FILE_INDEX, os.path.join(FOLDER_VIDS, INDEX_VIDS_JSON)), FILE_INDEX)
            #     # TagsMng.LIST_MD5_FILES[f] = Md5Mng(FILE_MD5)
            #
            # # self.tags = TagsMng(FILE_INDEX, VLC_ARG[0])
            # # self.dicoTimeStamps = self.tags.dicoTimeStamps
            # # self.md5 = Md5Mng(FILE_MD5, True)
            # # self.dicoMd5 = self.md5.dicoMd5
            # print(indexed_files)

            dicoSampleTmp = {}
            if len(TagsMng.LIST_INDEX_FILES) > 0 :
                for k in TagsMng.LIST_INDEX_FILES :
                    # print(k)
                    dicoSampleTmp = tt.GetList(k, TAGS.split(","))
                    # print(self.vidTagged)
                    # print("kk")
                    # FILTER_ON = ""
                    # if not k == indexed_files[k][0] :
                    #     FILTER_ON = indexed_files[k][0].replace(os.path.join(k, ""), "")
                    # else :
                    #     indexed_files[k] = []

                    # dicoSampleTmp = TagsMng.LIST_INDEX_FILES[k].SearchForTags(TAGS, True, FILTER_ON, self.SAMPLE_TIME)

                    # print("{} : {}".format(k, len(dicoSampleTmp)))
                    for k in dicoSampleTmp :
                        self.vidTagged[k] = dicoSampleTmp[k]

                print(len(self.vidTagged))
                # sys.exit(0)
                # global VLC_ARG
                # self.TIME_TAGGED = 0
                # self.vidTagged = self.tags.SearchForTags(TAGS, self.SAMPLE_TIME, random_sample = True)
                # self.origVidTagged = self.vidTagged
                VLC_ARG = self.vidTagged.keys()
                for v in self.vidTagged :
                    self.origVidTagged[v] = [ *self.vidTagged[v] ]

            # BEGIN
            # if FINAL_FILE != "" :
            #     if os.path.exists(FINAL_FILE) :
            #         # time.sleep(1)
            #         if TARGET != "" :
            #             W_DIR = TARGET
            #         print("TSF :: " + str(FfmpegTools.GetTime(FINAL_FILE)) + str(W_DIR))
            #         final_file = os.path.join(W_DIR, os.path.basename(AUDIO_FILE + "_" + str(self.SAMPLE_TIME) +".mp4"))
            #         print()
            #         final_file_frames = FfmpegTools.GetFrames(FINAL_FILE)
            #         print("FRAME/FICHIERS : " + str(final_file_frames)+ "/" + str(self.FRAME_TOTAL))
            #         if final_file_frames < self.FRAME_TOTAL :
            #             print("Gonna generate new sample !")
            #
            #             if os.path.exists(SAFE_FILE) :
            #                 with open(SAFE_FILE, 'rb') as f:
            #                     self.dicoSample = json.load(f)
            #             else :
            #                 self.SearchFilesInDir(VLC_ARG)
            #                 # fileThread = runner.start(method=self.SearchFilesInDir, args_method=[VLC_ARG], in_thread=False)
            #                 with open(SAFE_FILE, 'w') as fp:
            #                     json.dump(self.dicoSample, fp, indent=4)
            #             # print(VLC_ARG)
            #
            #             if len(self.dicoSample) == 0 :
            #                 print("ERROR : No files available in : " + str(VLC_ARG))
            #                 return False
            #
            #             for l_ignore in TO_IGNORE :
            #                 if l_ignore in self.dicoSample :
            #                     self.dicoSample.remove(l_ignore)
            #             # print(self.dicoSample)
            #             self.FRAME_FILE = self.FRAME_TOTAL/len(self.dicoSample) / 30
            #             print("FRAME/FICHIERS : " + str(self.FRAME_FILE))
            #             self.FRAME_COUNT = final_file_frames
            #             time_new = self.FRAME_COUNT / 30
            #             # sys.exit(0)
            #             while self.FRAME_COUNT <= self.FRAME_TOTAL :
            #                 self.LoopForSample(time_new, work_dir, runner, proc_list)
            #                 # continue
            #
            #             output_file = os.path.join(os.path.join(W_DIR, os.path.basename(AUDIO_FILE), "output-{}.ts"))
            #             output_file_wave = os.path.join(os.path.join(W_DIR, os.path.basename(AUDIO_FILE), "output.wav"))
            #             # output_file = os.path.join(os.path.join(W_DIR, os.path.basename(AUDIO_FILE), "output-{}.ts"))
            #             self.ConcatFiles(output_file, final_work_dir, runner)
            #
            #             if os.path.exists(output_file.format("end")) :
            #                 # time.sleep(1)
            #                 # print("FILE TS :: " + str(self.GetTime(output_file.format("end"))))
            #                 # final_cmd = Clip.CMD["ffmpeg_wav"].format(output_file.format("end"), AUDIO_FILE, output_file_wave, OUTPUT_CMD)
            #                 # # print(final_cmd)
            #                 # final_res = runner.start("Creating : {} from : {}", [output_file_wave, output_file.format("end")], Clip.check_call, [final_cmd], False)
            #                 #
            #                 if not self.CreateWavFile(output_file.format("end"), AUDIO_FILE, output_file_wave) :
            #                     print("Wave file creation has failed...")
            #                 AUDIO_FILE = output_file_wave
            #         # else :
            #         final_cmd = Clip.CMD["ffmpeg_end"].format(FINAL_FILE, AUDIO_FILE, time_v, time_v, "libx265", final_file, OUTPUT_CMD)
            #
            #         # print(final_cmd)
            #         # "ffmpeg -r 30 -y -ss 0 -i \"{}\" -ss:a 0 -i \"{}\" -t {} -t:a {} -c:v {} -map 0:v -map 1:a -vf crop=16:9 -vf scale=\"1920:1080\" \"{}\" {}".format(output_file.format("end"), AUDIO_FILE, time_v, time_v, "libx265", final_file, OUTPUT_CMD)
            #
            #         final_res = runner.start("Creating : {} from : {}", [final_file, FINAL_FILE], FfmpegTools.check_call, [final_cmd], False)
            #
            #         if final_res.returnMethod != 0 :
            #             print("Error on : " + final_cmd)
            #             sys.exit(2)
            #         print("Video available : {}".format(final_file))
            #
            #         os.system("vlc \"" + final_file + "\"")
            #
            #     sys.exit(0)
            # END

            if not os.path.exists(final_work_dir) :
                if not os.path.exists(os.path.dirname(final_work_dir)) :
                    os.mkdir(os.path.dirname(final_work_dir))
                os.mkdir(final_work_dir)
            else :
                DURATION_DONE = len(os.listdir(final_work_dir)) * self.SAMPLE_TIME
                self.FRAME_COUNT = DURATION_DONE * self.FPS

            if not os.path.exists(work_dir) :
                # Work dir does not exists, we create it
                if not os.path.exists(W_DIR) :
                    os.mkdir(W_DIR)
                os.mkdir(work_dir)

                print("Sample time : " + str(self.SAMPLE_TIME)) #+ " " + str(duration))
                print("Total done : " + str(DURATION_DONE))

            self.time_new = DURATION_DONE
            # Recherche des fichiers a créer...
            # TODO : Intégrer le traitement directement dans la recherche...

            print("Tot done : " + str(DURATION_DONE))
            if DURATION_DONE <= time_v :
                """
                Sample creation
                """
                print("files to be created : " + str((time_v - DURATION_DONE) / self.SAMPLE_TIME) + " | time to do : " + str(time_v - DURATION_DONE) + ", time done(-) : " + str(DURATION_DONE) + " | FRAMES :" + str(self.FRAME_TOTAL) )

                if os.path.exists(SAFE_FILE) :
                    with open(SAFE_FILE, 'rb') as f:
                        self.dicoSampleFile = json.load(f)
                else :
                    # fileThread = runner.start(method=self.SearchFilesInDir, args_method=[VLC_ARG], in_thread=False)
                    self.SearchFilesInDir(VLC_ARG)
                    if len(self.dicoTime) > 0 :
                        with open(SAFE_FILE, 'w') as fp:
                            json.dump(self.dicoTime, fp, indent=4)
                # else :vidTagged
                    # fileThread = runner.start(method=self.SearchFilesInDir, args_method=[VLC_ARG], in_thread=False)
                # self.SearchFilesInDir(VLC_ARG)
                # if len(self.dicoSample) > 0 :
                #     with open(SAFE_FILE, 'w') as fp:
                #         json.dump(self.dicoSample, fp, indent=4)

                # Une seconde de video samplé et switch...
                self.FRAME_FILE = self.FRAME_TOTAL / self.FPS / len(self.vidTagged)

                self.FRAME_FILE = 1
                print("FRAME/FICHIERS : " + str(self.FRAME_FILE))

                self.SamplesFromTag()

                if len(self.dicoSample) == 0 :
                    print("ERROR : No files available in : " + str(VLC_ARG))
                    return False
                # print(self.dicoSample)

                for l_ignore in TO_IGNORE :
                    if l_ignore in self.dicoSample :
                        self.dicoSample.remove(l_ignore)

                adj_0 = FfmpegSplit.SetFileName(int(self.FRAME_TOTAL/self.FPS * 1000))
                l_dico = self.LoopForSample()
                # print(l_dico)
                self.FRAME_COUNT = DURATION_DONE * self.FPS
                self.time_new = DURATION_DONE

                print()
                idx = 0
                while self.FRAME_COUNT <= self.FRAME_TOTAL :
                    for l_dir in l_dico :
                        fftools = l_dico[l_dir]
                        target = os.path.join(self.FINAL_WDIR, txt.Right(adj_0 + str(int(round(self.time_new,2) * 1000)), len(adj_0)) + ".ts")
                        proc_cmd  = FfmpegTools.runner.start(method=self.Sampling, args_method=[l_dir, target, fftools, idx, work_dir])
                        proc_list.append(proc_cmd)
                        idx += 1
                        self.time_new += fftools.SAMPLE_TIME

                        if len(proc_list) >= self.PARALLEL_PROC_NUMBER :
                            while len(proc_list) > 0 :
                                for proc in proc_list :
                                    if not proc.is_alive() :
                                        proc_list.pop(proc_list.index(proc))

                        if self.FRAME_COUNT >= self.FRAME_TOTAL  :
                            for proc in proc_list :
                                while proc.is_alive() :
                                    time.sleep(.1)
                            print()
                            sys.stdout.write('\033[2K\033[1G')
                            print("[{}/{}] Samples ready !".format(round(self.FRAME_COUNT, 2), str(self.FRAME_TOTAL)))
                            break

            output_file = os.path.join(os.path.join(W_DIR, os.path.basename(AUDIO_FILE), "output-{}.ts"))
            output_file_wave = os.path.join(os.path.join(W_DIR, os.path.basename(AUDIO_FILE), "output.wav"))

            ffmpeg_compile = FfmpegConcat(os.path.join(W_DIR, os.path.basename(AUDIO_FILE), str(self.SAMPLE_TIME)), self.FPS)
            ffmpeg_compile.SAMPLE_TIME = self.SAMPLE_TIME
            ffmpeg_compile.output_file = output_file
            ffmpeg_compile.output_file_final = output_file.format("end")
            if ffmpeg_compile.ConcatFiles(RANDOM, OUTPUT_CMD_INTERMEDIATE) :

                print()
                if os.path.exists(output_file.format("end")) :
                    # print("Cheking FRAME/FICHIERS : ", end="")
                    # final_file_frames = 0
                    # sample_files_path = os.path.join(os.path.join(W_DIR, os.path.basename(AUDIO_FILE), self.SAMPLE_TIME)
                    # for FfmpegTools.GetFrames(output_file.format("end"))
                    # print(str(final_file_frames)+ "/" + str(self.FRAME_TOTAL))
                    # if final_file_frames < self.FRAME_TOTAL :
                    #     while final_file_frames < self.FRAME_TOTAL :
                    #         print("Gonna generate new sample !")
                    #         if len(self.dicoSample) == 0 :
                    #             if os.path.exists(SAFE_FILE) :
                    #                 with open(SAFE_FILE, 'rb') as f:
                    #                     self.dicoSample = json.load(f)
                    #             else :
                    #                 fileThread = runner.start(method=self.SearchFilesInDir, args_method=[VLC_ARG], in_thread=False)
                    #                 with open(SAFE_FILE, 'w') as fp:
                    #                     json.dump(self.dicoSample, fp, indent=4)
                    #
                    #         for l_ignore in TO_IGNORE :
                    #             if l_ignore in self.dicoSample :
                    #                 self.dicoSample.remove(l_ignore)
                    #
                    #         self.FRAME_FILE = self.FRAME_TOTAL/len(self.dicoSample)
                    #         print("FRAME/FICHIERS : " + str(self.FRAME_FILE))
                    #         self.FRAME_COUNT = final_file_frames
                    #         time_new = self.FRAME_COUNT / 30
                    #         # sys.exit(0)
                    #         while self.FRAME_COUNT <= self.FRAME_TOTAL :
                    #             self.LoopForSample(time_new, work_dir, runner, proc_list)
                    #
                    #         self.ConcatFiles(output_file, final_work_dir, runner)
                    #         final_file_frames = FfmpegTools.GetFrames(output_file.format("end"))

                    print("Creating wave file ...")
                    # output_file_wave = output_file.format("wav")
                    if not self.CreateWavFile(AUDIO_FILE, output_file.format("end"), output_file_wave) :
                        print("Wave file creation has failed...")

                    if os.path.exists(output_file.format("end")) and os.path.exists(output_file_wave) :
                        # time.sleep(1)
                        # print("FILE TS :: " + str(self.GetTime(output_file.format("end"))))
                        final_file = os.path.join(W_DIR, os.path.basename(AUDIO_FILE + "_" + str(self.SAMPLE_TIME) +".mp4"))
                        final_cmd = Clip.CMD["ffmpeg_end"].format(self.FPS, output_file.format("end"), output_file_wave, time_v, time_v, "libx265", self.FPS, final_file, OUTPUT_CMD)

                        # "ffmpeg -r 30 -y -ss 0 -i \"{}\" -ss:a 0 -i \"{}\" -t {} -t:a {} -c:v {} -map 0:v -map 1:a -vf crop=16:9 -vf scale=\"1920:1080\" \"{}\" {}".format(output_file.format("end"), AUDIO_FILE, time_v, time_v, "libx265", final_file, OUTPUT_CMD)
                        # print(final_cmd)
                        final_res = runner.start("Creating : {} from : {}", [final_file, output_file.format("end")], FfmpegTools.check_call, [final_cmd], False)

                        if final_res.returnMethod != 0 :
                            print("Error on : " + final_cmd)
                            sys.exit(2)
                        print("Video available : {}".format(final_file))

                        os.system(VLC_CMD + " \"" + final_file + "\"")

        except KeyboardInterrupt as e:
            print("Interruption")
        except Exception as e:
            print("Error : "  + str(e))
            return False
        finally :
            print("End Compil")

def SetConfig(argv):
    """
    Args treatment
    """
    try:
        # print(argv)
        # print(usage.GetOpts())
        # print([*usage.GetOptsValue(), "help"])
        truc = usage.DicoArgs(argv)
        # print(truc)
        opts = truc[0]
        args = []
        if len(truc) > 1 :
            args = truc[1]
    except Exception as ex:
        # usage(argv)
        print(ex)
        usage.usage()
        sys.exit(2)
    try:
        global OUTPUT_CMD, VLC_ARG, OUTPUT_CMD_SAMPLE, OUTPUT_CMD_INTERMEDIATE
        for opt in opts:
            # print(opt)
            arg = opts[opt]
            if opt == "--help" :
                usage.usage()
                sys.exit()
            if opt in ("-a", "--audio"):
                global AUDIO_FILE
                if os.path.exists(arg) and os.path.isfile(arg) :
                    AUDIO_FILE = arg
                else :
                    raise Exception(str(arg) + " does not exists !")
                    # sys.exit(1)
            elif opt == "--tags":
                global TAGS
                TAGS = arg
            elif opt in ("--final"):
                global FINAL_FILE
                if os.path.exists(arg) and os.path.isfile(arg) :
                    FINAL_FILE = arg
                else :
                    raise Exception("Final file : '" + str(arg) + "' does not exists !")
                    # sys.exit(1)
            elif opt == "--target" :
                global TARGET
                if os.path.exists(arg) and os.path.isdir(arg) :
                    TARGET = arg
                else :
                    raise Exception("Defined target : '" + str(arg) + "' does not exists !")
                    # sys.exit(1)
            elif opt in ("-t", "--time"):
                global TIME
                TIME = float(arg)
                if TIME <= 0 :
                    raise Exception("--time value must be superior to 0  !")
                    # sys.exit(23) # ref : number 23...

            elif opt in ("-Z", "--random"):
                global RANDOM
                RANDOM = True
            elif opt in ("-v", "--verbose") :
                OUTPUT_CMD = ""
                print(arg)
                if arg.startswith("v") :
                    OUTPUT_CMD_INTERMEDIATE = ""
                if arg == "vv" :
                    OUTPUT_CMD_SAMPLE = ""
            # elif opt in ("-vv") :
            # elif opt in ("-vvv") :
            elif opt == "--cvlc" :
                global VLC_CMD
                VLC_CMD = "cvlc"

        if not len(args) > 0 :
            raise Exception("No path specified !")
        for arg in args :
            # path =
            # if os.path.exists(arg) :
            #     print(os.path.realpath(arg))
            VLC_ARG.append(os.path.realpath(arg))
    except Exception as ex:
        # usage(argv)
        print(ex)
        usage.usage()
        sys.exit(2)

def test_loading(t, wft = "wft") :
    time.sleep(t)
    # print(wft)
    return t + 1

try :

    SetConfig(sys.argv[1:])

    print("""
###################################
### Welcome to sample factory ! ###
###################################""")
    Clip().Create()

except Exception as e:
    print("FAIL !\n" + str(e))
    if DEBUG :
        if e.errno == os.errno.EsNOENT:
            sys.exit(e.errno)

# finally :
#     if len(proc_list) > 0 :
#         if proc_list[0].is_alive() :
#             proc_list[0].join(1)
#         proc_list.pop(0)
