# Copyright (c) Elie Coutaud 2024
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!/usr/bin/python
# -*- coding: latin-1 -*-
import os
import json
import sys
from koditvm import kodiUtils
from koditvm.TagsMng import TagsMng
from koditvm.TagsMng import Md5Mng
from koditvm.QueriesSocket import QuerySocket
import time
from koditvm.dbus_mpris import Player
# from threading import Thread
# from koditvm.KodiMove import KodiMove
from koditvm.helper_help import usage
import getopt
from random import uniform
from random import shuffle
import subprocess
import hashlib
from threading import Thread
import allinone
import vlc_sample
from koditvm.play_tags import PlayTags
from koditvm.kodi_mpris import KodiPlayer

SYNCHRO = True
NOEXIT = False
OUTPUT_CMD = "2> /dev/null"
SAMPLE = True
TIME = 5
VLC_ARG = []
VLC_OPT = "--play-and-exit" #"--video-x 0 --video-y 1024 -f --crop 16:9  --play-and-exit --no-random"
# VLC_CMD = "cvlc"
VLC_CMD = "vlc"
SOCKET = 9876
# PATH2REPLACE = "/media/xud/.dux"
# PATH2REMOVE = "/media/xud"

HOST = "localhost"
# USER = "kodi"
PATH2REPLACE = "/media/pix"
PATH2REMOVEROOT = "/nfs"
PATH2REMOVE = "nfs://" + HOST + PATH2REMOVEROOT

TV_MODE = True
TAG_EDIT = ""
TAG_READ = ""
TAG_NEW = ""
TAG_READ_RANDOM = ""

# BEGIN Help
# "NhH:u:p:P:S:", ["help", "no-synchro", "host=", "user=", "password=", "port=", "socket="]

# TODO : Recuperer usage depuis DZ /!\
# TODO : Add vlc options !!!
# --video-x 0 --video-y 1024 -f --crop 16:9  --play-and-exit --no-random

usage.OPTIONS_ALIAS = {
    "h" : "host",
    "N" : "no-synchro",
    "u" : "user",
    "p" : "port",
    "P" : "password",
    "S" : "socket",
    "v" : "verbose",
    "n" : "tag",
    "A" : "audio",
    "f" : "fullscreen",

}
usage.OPTIONS_VALUES = {
    "h" : "host",
    "u" : "kodi user",
    "p" : "kodi port",
    "P" : "kodi password",
    "S" : "socket port",
    "n" : "categorie/tag",
    "video-x" : "width",
    "video-y" : "height",
    "crop" : "(4:3,16:9,...)",
    "--" : "vlc options"
}

usage.OPTIONS_HELP = {
    "h" : "host",
    "u" : "kodi user",
    "p" : "kodi port",
    "P" : "kodi password",
    "N" : "run without synchro",
    "S" : "socket port",
    "f": "Fullscreen",
    "n" : "Tag reccording",
    "A" : "Audio master",
    "sample" : "Sample mode (play 10s then next video) ",
    "no-exit": "Do not exit after end of playlist",
    "cvlc" : "Do not use vlc Interface",
    "video-x" : "Window width",
    "video-y" : "Window height",
    "--" : "vlc options",
    "crop" : "crop image",
    "v" : "Verbose mode (vlc)",
    "path" : "Add local files to playlist (sample mode)"
}
usage.OPTIONS_MANDATORY = ["path"]
usage.OPTIONS_ARGS = ["path"]
usage.OPTIONS_DEFAULT = {
    "j" : 10
    }
# usage.OPTIONS_MANDATORY = []
# TODO : Use Text as first line and use help.CMD instead
usage.TEXT = "python " + sys.argv[0]
usage.DESC = "Play files randomly for X second \nCredentials are required if kodi is involved\nDefault socket port is 9876"
# END Help


# PATH2REMOVE = "nfs://192.168.0.30" + PATH2REMOVEROOT

class DbusMonitor(PlayTags) :

    def GetSocket(HOST, MAIN_SOURCE_PORT) :
        try :
            socket = QuerySocket(HOST, MAIN_SOURCE_PORT)
            if not len(socket.Init()) > 0 :
                return None
            return socket

        except KeyboardInterrupt as e:
            raise KeyboardInterrupt(e)

    def GetPlaylist(self, pathVideo, dir_vid, index = -1) :
        """ Get current videos list (via socket) """
        try :
            pathVideoDux = []
            if pathVideo != "" :
                self.socket.CallPlaylist(pathVideo, "", dir_vid, 0, 0, index)
            else :
                self.socket.CallPlaylist("/media/xud/DL/ENCOURS|2/1", "", dir_vid, 0, 0, index)

            if "error" in self.socket.LIST :
                print("EE : " + str(self.socket.LIST["error"]))
                return []
            for l_s_eps in self.socket.LIST :
                pathVideoDux.append(getRealPath(l_s_eps).replace(PATH2REMOVEROOT, PATH2REPLACE).replace("`", "\\`").replace("$", "\\$"))

            return pathVideoDux
        except Exception as e:
            print(e)
            return []

    """
    return output done by command line to be treated
    """
    def check_output(self, cmd) :
        try :
            return subprocess.check_output([cmd], shell=True)
        except :
            return ""

    # def runVlc(self, cmd) :
    #     """
    #     run command in a thread and add it to the main list
    #     """
    #     t = Player.RunPlayer(cmd)
    #     # print(cmd)
    #     proc_list.append(t[0])
    #     return t[1]

    def set_new_vid(self, seekToRandom = False, cat_keep = "", done_vid = 0) :
        pos_tmp = -1
        if not self.player == None :
            while not self.player.IsPlaying() :
                time.sleep(.5)
                # print(str(self.duration))

            # # On recupere le chemin de la video qui tourne sur le lecteur local.
            # duration = self.player.GetTrackDuration()
            # while duration <= 0 :
            #     time.sleep(.006)
            #     # print("du")
            #     duration = self.player.GetTrackDuration()

            # self.duration = self.player.track_duration
            self.eps = self.player.GetTrackPath("file://", "")
            l_md5 = self.md5.GetMd5(self.eps)

            done_vid = self.tags.FindIfJump(l_md5, done_vid, cat_keep)

            if self.eps not in self.dicoDone :
            #     self.dicoTime[self.eps] = {}
                self.dicoDone[self.eps] = done_vid

            # print(self.eps + " __ " + str(self.dicoDone[self.eps]) + " > " + str(done_vid))

            if seekToRandom :
                if done_vid > 0 :
                    new_time = done_vid
                    # if new_time > self.dicoDone[self.eps] :
                    self.dicoDone[self.eps] = done_vid
                    # else :
                    #     # self.dicoDone[self.eps] += self.SAMPLE_TIME
                    #     new_time = self.dicoDone[self.eps]
                else :
                    # print("!done_vid > 0 " + str(done_vid))
                    if self.dicoDone[self.eps] + (1/10) <= 1 :
                        new_time = self.dicoDone[self.eps] * self.player.track_duration
                        # uniform(self.dicoDone[self.eps] * self.player.track_duration, (self.dicoDone[self.eps] + (1/10) * self.player.track_duration))
                    else :
                        new_time = self.player.track_duration * 95
                        # new_time = uniform(self.dicoDone[self.eps] * self.player.track_duration, self.player.track_duration - self.SAMPLE_TIME)
                        self.dicoDone[self.eps] = 0
                    if new_time < 0 :
                        while new_time < self.SAMPLE_TIME + 1 :
                            new_time = uniform(self.SAMPLE_TIME + 1, self.player.track_duration - self.SAMPLE_TIME)
                    self.dicoDone[self.eps] += 1 / 10

                while pos_tmp < new_time :
                    if self.player.GetTrackPath("file://", "") == self.eps :
                        self.player.Seek(new_time)
                        pos_tmp = self.player.Position()
                        self.target = new_time + self.SAMPLE_TIME
                    else :
                        print("CHANGE " + self.eps + " > " + self.player.GetTrackPath("file://", ""))
                        if not self.player.GetTrackPath("file://", "") in self.dicoDone :
                            self.dicoDone[self.player.GetTrackPath("file://", "")] = 0
                        return self.set_new_vid(seekToRandom, cat_keep, cat_avoid, self.dicoDone[self.player.GetTrackPath("file://", "")])
                    time.sleep(.08)

                if pos_tmp > self.target :
                    print("SEEK :: #" + str(new_time))
                    self.player.Seek(new_time)

            return self.target

    # def CheckTimeStamps(self, vidTimeStamps, tag = "", avoid = "usage/throw") :
    #     """
    #     Return video time stamps to extract
    #     """
    #     dicoConvert = { }
    #     listTimeStamp = []
    #     for t in vidTimeStamps :
    #         if not avoid in vidTimeStamps[t][1] :
    #             continue
    #         if tag != "" and tag not in vidTimeStamps[t][1] :
    #             continue
    #         listTimeStamp.append(float(t))
    #
    #     idx = 0
    #     listTimeStamp = sorted(listTimeStamp)
    #     print(listTimeStamp)
    #     for t in listTimeStamp :
    #         begin_jump = float(t)
    #         end_jump = vidTimeStamps[str(t)][0]
    #
    #         if idx == 0 :
    #             if len(listTimeStamp) > 1 :
    #                 dicoConvert[end_jump] = listTimeStamp[idx + 1]
    #             else :
    #                 dicoConvert[end_jump] = end
    #         else :
    #             if idx == len(listTimeStamp) - 1:
    #                 dicoConvert[end_jump] = end
    #                 # break
    #             else :
    #                 dicoConvert[end_jump] = listTimeStamp[idx + 1]
    #         idx += 1
    #
    #     return dicoConvert


# BEGIN Events
#     def onLoopChange(self) :
#
# # #     "None" if the playback will stop when there are no more tracks to play
# # #     "Track" if the current track will start again from the begining once it has finished playing
# # #     "Playlist" if the playback loops through a list of tracks
#
#         self.player.Prop("LoopStatus", "Playlist")
#         # print(self.player.Get("LoopStatus"))
#         if self.player.track_id == self.player.GetTrackId():
#             self.player.GetControl().Next()
#             i = 0
#             while self.player.GetTrackPath("file://", "") == self.eps :
#                 # print("wait for new file")
#                 time.sleep(.05)
#                 i += 1
#                 if i == 6 :
#                     self.player.GetControl().Next()
#                     i = 0
#                     time.sleep(.05)
#                     break
#             if self.md5.GetMd5(self.eps) in self.dicoDone :
#                 percent = round(self.dicoDone[self.md5.GetMd5(self.eps)] * 100 / self.player.track_duration, 2)
#                 print("DELETED " + str(percent) + "%  : " + self.eps + " " + self.player.track_id)
#             self.player.Interface("TrackList").RemoveTrack(self.player.track_id)
#
#     def endLoop(self) :
#         end = time.time()
#         if end - self.start > self.SAMPLE_TIME and not self.player.GetStatus() == "Paused" :
#         # if self.target - self.player.Position() < 0 :
#             # print(str(self.target) + " reached for " +  self.eps + "! (" + str(self.player.Position()) +")")
#             self.vol = self.player.Volume()
#             self.player.GetControl().Next()
#             self.player.Volume(self.vol)
#
#             i = 0
#             while self.player.GetTrackPath().replace("file://", "") == self.eps :
#                 # print("wait for new file")
#                 time.sleep(.1)
#                 i += 1
#                 if i == 5 :
#                     if len(self.vidTagged) > 0 :
#                         # self.count_vid[self.eps] += 1
#                         print("Seems to be the same... (" + self.eps + "::" + str(self.count_vid[self.eps]   ) + ")")
#
#                         if self.count_vid[self.eps] + 1 >= len(self.vidTagged[self.eps]) :
#                             self.count_vid[self.eps] = 0
#                         else :
#                             self.count_vid[self.eps] += 1
#                         new_target = self.vidTagged[self.eps][self.count_vid[self.eps]][1]
#                         new_time = self.vidTagged[self.eps][self.count_vid[self.eps]][0]
#                     else :
#                         if self.eps not in self.dicoDone :
#                             # if not os.path.isdir(self.player.track) :
#                             self.dicoDone[self.eps] = 0
#                         new_target = self.dicoDone[self.eps] + self.SAMPLE_TIME
#                         new_time = self.dicoDone[self.eps]
#
#                     self.target = new_target
#                     self.player.Seek(new_target)
#                     self.start = time.time()
#
#
#         if self.player.GetStatus() == "Paused" :
#             self.start = time.time()
#
#     def afterLoop(self) :
#         self.eps = self.player.GetTrackPath().replace("file://", "")
#         self.player.Volume(self.vol)
#         # We will filter
#         if len(self.vidTagged) > 0 :
#             if self.eps not in self.count_vid :
#                 self.count_vid[self.eps] = 0
#
#             begin = self.vidTagged[self.eps][self.count_vid[self.eps]][0]
#             end = self.vidTagged[self.eps][self.count_vid[self.eps]][1]
#             self.player.Seek(begin)
#
#             if end == begin :
#                 # end = end + self.SAMPLE_TIME
#                 print(os.path.basename(self.eps) + " has bad timestamps :" + str(begin) + " = " + str(end) + " > " + str(end + 2))
#
#             self.target = end
#             # print(self.target)
#
#             if self.player.Position() < self.target - self.SAMPLE_TIME :
#                 time.sleep(.1)
#             if self.count_vid[self.eps] + 1 >= len(self.vidTagged[self.eps]) :
#                 self.count_vid[self.eps] = 0
#             else :
#                 self.count_vid[self.eps] += 1
#         else :
#             if self.eps not in self.dicoDone :
#                 # if not os.path.isdir(self.player.track) :
#                 self.dicoDone[self.eps] = 0
#             self.dicoDone[self.eps] += self.SAMPLE_TIME
#             # self.player.Seek(self.dicoDone[self.eps])
#             self.set_new_vid(True, self.catSearch, done_vid=self.dicoDone[self.eps])
#             self.target = self.dicoDone[self.eps] + self.SAMPLE_TIME
#             # new_target = self.dicoDone[self.eps] + self.SAMPLE_TIME
#             # new_time = self.dicoDone[self.eps]
#
#
#
#         self.start = time.time()
#
#     def endLoopTv(self) :
#         self.end = time.time()
#         pos = self.player.Position()
#         if pos > 0 :
#             self.dicoDone[self.eps] = pos
#         self.target = pos + self.SAMPLE_TIME
#         if self.end - self.start > self.SAMPLE_TIME :
#             self.start = time.time()
#         self.MANU_NEXT = True
#
#     #     instance.Set(["Volume", vol])
#     #     self.set_new_vid(instance)
#     def afterLoopTv(self) :
#         # We were trying to catch sample :
#         if self.NEW_SAMPLE >= 0 :
#             # Saving new sample with end time as duration
#             new_tag = self.SaveSample(self.NEW_SAMPLE, self.duration)
#             self.NEW_SAMPLE = -1
#             if len(new_tag) > 0 :
#                 print("[{}] : Saved {}@{} for {}".format(self.catSearch, str(round(new_tag[2], 2)), str(new_tag[0]), os.path.basename(self.eps)))
#                 self.counter_save = [self.counter_save[0] + 1, self.counter_save[1] + new_tag[2]]
#
#         # self.dicoPlayers[self.player.name]
#         if self.eps == "" :
#             self.eps = self.player.GetTrackPath("file://", "")
#         else :
#             if self.eps not in Player.track_list :
#                 Player.track_list[self.eps].cur_time = self.dicoDone[self.player.track]
#             percent = round(self.dicoDone[self.eps] * 100 / Player.track_list[self.eps].duration, 2)
#             # if Player.track_list[self.eps].cur_time + self.SAMPLE_TIME > Player.track_list[self.eps].duration:
#             if percent >= 99.5 :
#                 print("RAZ " + str(percent) + "% | [" + str(round(self.dicoDone[self.eps], 2)) + "/" + str(round(Player.track_list[self.eps].duration, 2)) + ") TO " + str(round(self.target, 2)) + " " + self.eps + " (" +self.player.track_id+ ")")
#                 # print("TRK DEL " + self.eps + " " + str(Player.track_list[self.eps].track_id))
#                 self.dicoDone[self.eps] = 0
#                 Player.track_list[self.eps].cur_time = 0
#             # print("TV on " + self.eps + " at " + str(Player.track_list[self.eps].cur_time))
#         while os.path.isdir(self.player.track) :
#             time.sleep(.1)
#             self.player.track = self.player.GetTrackPath("file://", "")
#
#             if os.path.isdir(self.player.track) :
#                 print(self.player.track)
#             continue
#
#         if self.player.track != "" :
#             # self.eps = self.player.track
#
#             if self.player.track not in self.dicoDone :
#                 if not os.path.isdir(self.player.track) :
#                     self.dicoDone[self.player.track] = 0
#
#             # print("self.set_new_vid(True, " + self.catSearch + ", " + str(self.dicoDone[self.player.track]))
#             self.set_new_vid(True, self.catSearch, done_vid=self.dicoDone[self.player.track])
#             # self.set_new_vid(self.player, self.JUMP_MODE, self.catSearch, done_vid=self.dicoDone[self.player.track])
#             # self.eps = self.player.GetTrackPath("file://", "")
#             if self.eps not in Player.track_list :
#                 Player.track_list[self.eps].cur_time = self.dicoDone[self.player.track]
#
#             # if self.dicoDone[self.eps] >= 0 :
#                 # self.player.Seek(self.dicoDone[self.eps])
#             percent = round(self.dicoDone[self.player.track] * 100 / Player.track_list[self.eps].duration, 2)
#             print("RUN TV " + str(percent) + "% | [" + str(round(self.dicoDone[self.eps], 2)) + "/" + str(round(Player.track_list[self.eps].duration, 2)) + ") TO " + str(round(self.target, 2)) + " " + self.eps + " (" + self.player.track_id + ")")
#             self.start = time.time()


    # def SaveSample(self, beg, end) :
    #     new_tag = []
    #     new_time = [ beg, end, end - beg ]
    #     l_s_md5 = self.md5.GetMd5(self.eps)
    #
    #     new_tag = self.tags.NewTag(l_s_md5, self.eps, self.catSearch, new_time)
    #
    #     return new_tag
    #
    # def onRateChange(self) :
    #     print("RATE changed 1 :: {} {}".format(type(self.player.Rate()), type(1)))
    #     if self.player.track == self.player.GetTrackPath("file://", "") :
    #         print(self.player.track + " " + self.player.GetTrackPath("file://", ""))
    #         self.player.Rate(1.0)
    #         print("RATE changed 2 :: {}".format(float(self.player.Rate())))
    #
    # def onTriplePause(self) :
    #     if self.NEW_SAMPLE >= 0 :
    #         print("CANCELLED : {}".format(self.NEW_SAMPLE))
    #
    #         self.NEW_SAMPLE = -1
    #
    # def onDoublePause(self) :
    #     pos = self.player.Position()
    #     # if self.player.GetTrackPath().replace("file://", "") == self.eps :
    #     pos = round(pos, 2)
    #     if pos <= 0.9 :
    #         pos = 0.0
    #
    #     if self.NEW_SAMPLE >= 0 :
    #         new_tag = self.SaveSample(self.NEW_SAMPLE, pos)
    #         self.NEW_SAMPLE = -1
    #         if len(new_tag) > 0 :
    #             print("[{}] : Saved {}@{} for {}".format(self.catSearch, str(round(new_tag[2], 2)), str(new_tag[0]), os.path.basename(self.eps)))
    #             self.counter_save = [self.counter_save[0] + 1, self.counter_save[1] + new_tag[2]]
    #     else :
    #
    #         # else :
    #         if self.TV_MODE :
    #             self.NEW_SAMPLE = pos
    #             # print("WAIT : new sample begin at " + str(pos))
    #             print("[{}] : NEW @{} for {}".format(self.catSearch, str(round(pos, 2)),  os.path.basename(self.eps)))
    #         else :
    #             new_tag = self.SaveSample(pos, pos + self.SAMPLE_TIME + self.SAMPLE_TIME * (1 / 3))
    #             print("[{}] : Saved {}@{} for {}".format(self.catSearch, str(round(new_tag[2], 2)), str(new_tag[0]), os.path.basename(self.eps)))
    #             self.counter_save = [self.counter_save[0] + 1, self.counter_save[1] + new_tag[2]]
    #
    #             if len(self.player.GetTracks("Tracks")) > 1 :
    #                 self.player.GetControl().Next()
    #
    #                 i = 0
    #                 while self.player.GetTrackPath("file://", "") == self.eps :
    #                     # print("wait for new file")
    #                     time.sleep(.1)
    #                     i += 1
    #                     if i == 5 :
    #                         print("Seems to be the same...")
    #                         self.set_new_vid(True, self.catSearch, done_vid=self.dicoDone[self.eps])
    #                         # self.player.Play()
    #                         self.start = time.time()
    #                         break
    #                     else :
    #                         # self.player.Play()
    #                         self.start = time.time()
    #
    #
    #     self.player.paused = False
    #
    # def onPause(self) :
    #     # print("Paused on " + self.eps)
    #     if self.NEW_SAMPLE >= 0 :
    #         pos = self.player.Position()
    #         self.player.Play()
    #
    #         new_tag = self.SaveSample(self.NEW_SAMPLE, pos)
    #         self.NEW_SAMPLE = -1
    #         if len(new_tag) > 0 :
    #             print("[{}] : Saved {}@{} for {}".format(self.catSearch, str(round(new_tag[2], 2)), str(new_tag[0]), os.path.basename(self.eps)))
    #             self.counter_save = [self.counter_save[0] + 1, self.counter_save[1] + new_tag[2]]
    #     else :
    #         self.player.Interface().Pause()
    #
    #         while self.player.GetStatus() == "Paused" :
    #             nb_click = self.player.TripeClickPause(0, 3, False)
    #
    #             if nb_click == 1 :
    #                 self.player.Play()
    #
    #                 # # Saving new sample
    #                 # if self.NEW_SAMPLE >= 0 :
    #                 #     pos = self.player.Position()
    #                 #     self.player.Play()
    #                 #
    #                 #     new_tag = self.SaveSample(self.NEW_SAMPLE, pos)
    #                 #     self.NEW_SAMPLE = -1
    #                 #     if len(new_tag) > 0 :
    #                 #         print("[{}] : Saved {}@{} for {}".format(self.catSearch, str(round(new_tag[2], 2)), str(new_tag[0]), os.path.basename(self.eps)))
    #                 #         self.counter_save = [self.counter_save[0] + 1, self.counter_save[1] + new_tag[2]]
    #             elif nb_click == 2 :
    #                 if self.TV_MODE :
    #                     self.TV_MODE = False
    #                     self.player.m_end_loop = [ self.endLoop ]
    #                     self.player.m_after_loop = [ self.afterLoop ]
    #                 else :
    #                     self.TV_MODE = True
    #                     self.player.m_end_loop = [ self.endLoopTv ]
    #                     self.player.m_after_loop = [ self.afterLoopTv ]
    #                     self.player.Play()
    #                 print("TV_MODE : " + str(self.TV_MODE))
    #             elif nb_click == 3 :
    #                 # Changing tag category
    #                 THROW_TAG = "usage/throw"
    #                 if self.catSearch == THROW_TAG :
    #                     self.catSearch = self.cat
    #                 else :
    #                     self.catSearch = THROW_TAG
    #                 print("CAT : " + self.catSearch)
    #                 self.player.Play()

# END Events

    """
    Use kodi/socket information
    """
    def check_set_vid(self, jump_mode = -1) :
        """
        Run the remote kodi video on local vlc.
        """
        try :
            self.SAMPLE_TIME = TIME
            # SAMPLE_TIME = self.SAMPLE_TIME
            new_time = 0
            self.dicoPlayers = {}
            self.eps = ""
            self.target = 0
            self.vol = 0.5
            counter_jump = 0
            self.dicoTime = {}
            self.dicoNewTime = {}
            self.dicoDone = {}
            self.dicoMd5 = {}
            self.player = None
            self.TV_MODE = TV_MODE
            self.counter_save = [0, 0]
            self.NEW_SAMPLE = -1

            self.vidTagged = {}
            RANDOM = False

            DIR_VIDS = "/media/xud/.dux/DL"
            DIR_FILE_TAG = "/media/xud/.dux/ffmpeg-sample"
            FILE_TAG = os.path.join(DIR_FILE_TAG, ".tags.json")
            FILE_INDEX = os.path.join( DIR_VIDS, "ENCOURS", ".index.json")
            FILE_MD5 = os.path.join( "/media/xud/.dux/DL/ENCOURS", ".md5.json")
            TAG = ""

            self.catSearch = ""
            self.catThrow = "usage/throw"

            # if os.path.exists(FILE_INDEX) :
            #     with open(FILE_INDEX, 'rb') as f:
            #         self.dicoTime = json.load(f)

            global VLC_ARG
            self.tags = TagsMng(FILE_INDEX, DIR_VIDS)
            self.dicoTime = self.tags.dicoTimeStamps
            self.md5 = Md5Mng(FILE_MD5)



            FILE_TIME = os.path.join(DIR_FILE_TAG, ".tags_tv.json")
            if os.path.exists(FILE_TIME) :
                with open(FILE_TIME, 'rb') as f:
                    self.dicoDone = json.load(f)

            if TAG_EDIT != "" or  TAG_READ != "" or TAG_NEW != "" or TAG_READ_RANDOM != "" :
                if TAG_EDIT != "" :
                    TAG = TAG_EDIT
                elif TAG_READ != "" :
                    TAG = TAG_READ
                elif TAG_NEW != "" :
                    TAG = TAG_NEW
                elif TAG_READ_RANDOM != "" :
                    # TAG_READ = TAG_READ_RANDOM
                    TAG = TAG_READ_RANDOM
                    RANDOM = True
                # print(TAG)



                # self.Migrate()
                # sys.exit(0)
                if TAG == "" :
                    print("No tag specified !")
                    return False
                TAG = self.tags.SearchForCat(TAG)

                self.cat = TAG
                self.catSearch = TAG
                self.count_vid = {}
                if (TAG_READ != "" or TAG_EDIT != "" or TAG_READ_RANDOM != "") and len(self.dicoTime) > 0 :
                    # self.TIME_TAGGED = 0
                    # self.PATH_MEDIA = VLC_ARG[0]
                    self.vidTagged = self.tags.SearchForTags(TAG, self.SAMPLE_TIME, random_sample=RANDOM)
                    if len(self.vidTagged) > 0 :
                        VLC_ARG = self.vidTagged.keys()
                    else :
                        print("No sample found for '{}'".format(TAG))
                        sys.exit(2)

            # if self.TV_MODE :
            #     l_meth_endLoop = self.endLoopTv
            #     l_meth_afterLoop = self.afterLoopTv
            # else :
            #     l_meth_endLoop = self.endLoop
            #     l_meth_afterLoop = self.afterLoop
            # BEGIN Monitoring dbus.mpris instances
            # self.player = Player()
            main_process = Player.Monitor()
            # print("i")
            self.playerList = []
            self.playerProcesses = []
            # t.start()
            count_old = 0
            # while True :
                # break
            # END
            while True :
                try :
                    # If process has exited, we break
                    if not main_process.is_alive() :
                        main_process = Player.Monitor()
                        print("process die...")
                        break

                    if len(Player.player_list) <= len(self.playerList) :
                        time.sleep(.5)
                        # We're gonna watch players and relaunch loop if available
                        idx = 0
                        for p in self.playerList :
                            # p = Player.plist[p_name]
                            if p.Exists() :
                                t = self.playerProcesses[idx]
                                print(p.name + " " + p.GetTrackPath())
                                if not t.is_alive() :
                                    process = p.LoopThread(self.eps)
                                    self.playerList[idx] = p
                                    print("HERE LOOP stop : @{} for {}".format(self.eps, p.name))
                                    self.playerProcesses[idx] = process
                            else :
                                print("HERE VLC stop : " + p.name)
                                self.playerList.pop(idx)
                                self.playerProcesses.pop(idx)
                                count_old = len(Player.player_list)
                                # p = self.playerList[idx]
                            # else :
                            #     print("EXIST :: {}".format(self.Exists()))

                            # print(p_name)
                            idx += 1
                        continue
                    # Waiting for new player running a track
                    print("proc checked " + str(len(self.playerList)) + " " +str(len(Player.player_list)) + " <= " + str(count_old))
                    count_old = len(Player.player_list)
                    # Player.player_list.pop(Player.player_list.index(k_todel))
                    new_player = Player.player_list[len(Player.player_list) - 1]

                    # vlc_sample.TAG_EDIT = TAG
                    self.player = Player.plist[new_player]
                    print("FOUND ! " + str(new_player))
                    while self.player.track == "" :
                        self.player.track = self.player.GetTrackPath("file://", "")
                    # self.start = time.time()
                    # self.player.m_paused_1 = [ self.onPause ]
                    # self.player.m_paused_2 = [ self.onDoublePause ]
                    # self.player.m_paused_3 = [ self.onTriplePause ]
                    # self.player.m_loopTrack = [ self.onLoopChange ]
                    # self.player.m_rate = [ self.onRateChange ]
                    # self.player.m_shuffle = []
                    # self.player.m_beg_loop = []
                    # self.player.m_end_loop = [ l_meth_endLoop ]
                    # self.player.endLoop = l_meth_endLoop
                    # self.player.afterLoop = l_meth_afterLoop
                    # self.player.m_after_loop = [ l_meth_afterLoop ]


                    if not self.player.Exists() :
                        continue

                    process = self.player.LoopThread()
                    # print(self.player)
                    print("proc launched for p:" + self.player.name)
                    # self.playerList = Player.player_list
                    self.playerList.append(self.player) # Player.player_list
                    self.playerProcesses.append(process)
                    time.sleep(.5)
                    self.dicoPlayers[self.player.name] = { "p" : self.player, "t" : process, "eps" : self.player.track }

                    # print("PLIST : " + str(self.playerList))
                    # print("TLIST : " + str(self.playerProcesses))
                    # pos = self.player.Position()
                    # self.player.track = self.eps
                    # if not self.player.LoopPlayer(self.eps) :
                    #     break
                except KeyboardInterrupt as e:
                    print("Interruption")
                    # if len(proc_list) > 0 :
                    #     if proc_list[0].is_alive() :
                    #         proc_list[0].join(1)
                    #     proc_list.pop(0)
                    # return False

        except Exception as e:
            print("Error : "  + str(e))
            return False
        finally :
            Player.RUNNING = False
            # if len(self.md5.dicoMd5) > 0 :
            self.md5.Save()
                # with open(FILE_MD5, 'w') as fp:
                #     json.dump(self.dicoMd5, fp)

            if len(self.dicoDone) > 0 :
                with open(FILE_TIME, 'w') as fp:
                    json.dump(self.dicoDone, fp, indent=4)

            # self.tags.dicoTimeStamps = self.dicoTime
            # self.tags.Save()
            # if len(self.dicoTime) > 0 :
            #     with open(FILE_INDEX, 'w') as fp:
            #         json.dump(self.dicoTime, fp, indent=4)

            if self.counter_save[0] > 0 :
                print("Total : {} samples saved for {}".format("+" + str(self.counter_save[0]), str(self.counter_save[1]) +"s"))

            print("End Samples")
            # if instance.Exists() :
            #     instance.Quit()
            if len(proc_list) > 0 :
                if proc_list[0].is_alive() :
                    proc_list[0].join(1)
                proc_list.pop(0)
            return True

    def __init__(self, HOST, MAIN_SOURCE_PORT = 9876, player = None, tag_new = "usage/throw", tag_read = "") :
        """ Run the remote kodi video on local vlc. """
        try :
            global TAG

            self.eps = ""
            self.duration = 0
            self.target = 0
            self.playlist = []
            self.NEW_SAMPLE = -1
            self.counter_save = [0, 0]
            self.MASTER = False
            # Vlc instance
            self.player = player
            self.STOPPED = False
            self.ADD_SAMPLE = False          # Indicate that we will edit a video
            self.DEL_SAMPLE = -1            # Position from which to delete
            self.count_vid = {}
            self.dicoDone = {}
            self.dico_eps = []
            self.cur_smp = []               # Current timeStamps array
            self.TV_MODE = True
            self.READ_MODE = False
            self.PrevNextMode = False
            self.kodi = None

            PlayTags.PREFIX_MSG_DEFAULT = "  KDI "

            PATH2REMOVE = "nfs://" + HOST + PATH2REMOVEROOT

            self.socket = DbusMonitor.GetSocket(HOST, MAIN_SOURCE_PORT)
            print("Trying to contact socket at : {}:{} ...".format(HOST, MAIN_SOURCE_PORT))
            print("Type Ctrl + C to quit")
            while self.socket is None :
                self.socket = DbusMonitor.GetSocket(HOST, MAIN_SOURCE_PORT)
                if player != None and (player.IsStopped() or not player.Exists()) :
                    return None

            dirs = []
            for d in self.socket.DIRS :
                dirs.append(os.path.realpath(d.replace(PATH2REMOVEROOT, PATH2REPLACE)))
            super().__init__(dirs, tag_new, tag_read)

            self.catSearch = self.catList[0]
            ws = self.socket.Query("webserver")
            if len(ws) > 0 :
                self.kodi = KodiPlayer(HOST, *ws)

            self.players = []
            master = None
            p_list = 0
            # Player.MAX_OFFSET = 0.2
            while True :
                if self.kodi != None and self.kodi.GetVideoPath() != "" :
                    # print(Player.plist)
                    Player.init()
                    if p_list != len(Player.plist) :
                        Player.init()
                        p_list = len(Player.plist)
                        self.players = []
                        # master = None
                        time.sleep(3)

                    # print(p_list)
                        for p_dbus in Player.plist :
                            p = Player.plist[p_dbus]
                            if os.path.basename(p.GetTrackPath()) == os.path.basename(self.kodi.GetVideoPath()) :
                                print(p.name)
                                if master is None :
                                    master = p
                                    continue
                                elif master.name not in Player.plist :
                                    print("NEW master")
                                    master = p
                                    continue

                                if p not in self.players :
                                    self.players.append(p)

                if len(self.players) > 0 :
                    # if master is None :
                    #     master = self.players[0]
                    #     # master = self.players[len(self.players) - 1]
                    #     self.players.remove(master)
                    # print(master)
                        # print(master.Position())
                    if len(self.players) > 0 :
                        # print("1 " + str(master) + " " + str(master.Position()))
                        # for p in self.players[0:1] :
                        master.SynchPlayers(self.players, True)


            # if self.kodi.GetVideoPath() == "" :
            #     print("KDI {} stopped or not running".format(self.socket.GetAddr()), end = "...", flush = True)
            #     self.kodi.ExecQry( 'Addons.ExecuteAddon', {'addonid' : "plugin.video.kodi2tv", 'params': { "addr" : self.socket.GetAddr(), "play" : "True" }, "wait" : True}, 'ExecAddon')
            #     # print(self.kodi.ExecQry( 'Input.Down', {}, 'ActionDown'))
            #     # print(self.kodi.ExecQry( 'Input.Up', {}, 'ActionUp'))
            #     # # print(self.kodi.ExecQry( 'Input.Down', {}, 'ActionDown'))
            #     time.sleep(5)
            #     self.kodi.ExecQry( 'Input.Select', {}, 'ActionSelect')
            #     time.sleep(1)
            #     self.kodi.ExecQry( 'Input.Select', {}, 'ActionSelect')
            #     try_count = 0
            #     while self.kodi.GetTrackPath() == "" and try_count < 15:
            #         time.sleep(1)
            #         try_count += 1
            #
            #     if try_count < 15 :
            #         print("OK")
            #     else :
            #         print("FAILED")
            #
            # if player != None :
            #     self.player.Shuffle(False)
            #     self.SetFunc()
            #     self.player.Play()
            #
            # PlayTags.PREFIX_MSG = "  KDI "
            # self.vol = self.kodi.Volume() / 100
            #
            # # DbusMonitor.MAX_OFFSET = 0.8
            #
            # while True :
            #     if self.player != None and not self.player.Exists() :
            #         if not DAEMON_MODE :
            #             print("process die...")
            #         else :
            #             self.player = None
            #
            #     if self.player == None :
            #         # Récupère la video en cours de lecture depuis l'API de kodi
            #         self.vidRemote = self.kodi.GetTrackPath()
            #         pos = self.kodi.Position()
            #         self.playlist = self.GetPlaylist(self.vidRemote, os.path.realpath(self.socket.DIRS[0]))
            #         # La video distante n'existe pas en local, on va attendre 5 seconde
            #         if len(self.playlist) > 0 :
            #             if not os.path.exists(self.playlist[0]) :
            #                 print("WAIT, " + self.playlist[0] + " DOES NOT EXISTS ON LOCAL SYSTEM !")
            #                 time.sleep(5)
            #                 continue
            #         else :
            #             print("Playlist is empty !")
            #             sys.exit(3)
            #
            #         self.player = self.runInstance(self.playlist, [VLC_OPT], pos)
            #         self.SetFunc()
            #     elif not self.player.Exists() :
            #         break
            #
            #     if self.STOPPED :
            #         break
            #     if not self.player.LoopPlayer(self.eps) :
            #         break

        except KeyboardInterrupt as e:
            print("Interruption")
        except Exception as e:
            print("Error : "  + str(e))
        finally :
            if self.player.Exists() :
                self.player.Stop()
            self.STOPPED = True
            self.eps = ""
            print("End Synchro")

def getRealPath(original_path):
    """ Get "transcrypted" path of a video """
    l_path = original_path

    if l_path.startswith(PATH2REMOVE):
        l_path = l_path.replace(PATH2REMOVE,PATH2REPLACE)
    elif l_path.startswith(PATH2REMOVEROOT) :
        l_path = l_path.replace(PATH2REMOVEROOT,PATH2REPLACE)

    return os.path.realpath(l_path)

# """
# Calcule la somme md5 d'un fichier
# """
# def md5(fname) :
#     hash_md5 = hashlib.md5()
#     with open(fname, "rb") as f:
#         for chunk in iter(lambda: f.read(4096), b""):
#             hash_md5.update(chunk)
#
#     return hash_md5.hexdigest()


def FindOptValue(opt, arg = "") :
    global VLC_OPT
    if opt == "--help" :
        usage.usage()
        sys.exit()
    if opt in ("-t", "--time"):
        global TIME
        TIME = float(arg)
    elif opt in ("-j", "--jump"):
        global JUMP_MODE
        JUMP_MODE = 10
        if arg.startswith("--") or arg.startswith("-"):
            FindOptValue(arg)
        elif arg.startswith("/"):
            global VLC_ARG
            VLC_ARG.append(os.path.realpath(arg))
        elif not arg == "" :
            JUMP_MODE = arg
    elif opt in ("-v", "--verbose") :
        global OUTPUT_CMD
        OUTPUT_CMD = ""
    elif opt in ("-e", "--tag-edit"):
        global TAG_EDIT
        TAG_EDIT = arg
    elif opt in ("-r", "--tag-read"):
        global TAG_READ
        TAG_READ = arg
    elif opt in ("--rr", "--tag-read-random"):
        global TAG_READ_RANDOM
        TAG_READ_RANDOM = arg
    elif opt in ("-n", "--tag-new"):
        global TAG_NEW
        TAG_NEW = arg
    elif opt in ("--no-tv"):
        global TV_MODE
        TV_MODE = False
        # TAG_EDIT = 10
        # if arg.startswith("--") or arg.startswith("-"):
        #     FindOptValue(arg)
        # elif arg.startswith("/"):
        #     global VLC_ARG
        #     VLC_ARG.append(os.path.realpath(arg))
        # elif not arg == "" :
        #     JUMP_MODE = arg
        #

    elif opt in ("--no-exit") :
        # global NOEXIT
        # NOEXIT = ""
        VLC_OPT = VLC_OPT.replace("--play-and-exit", "")
    elif opt == "--cvlc" :
        global VLC_CMD
        VLC_CMD = "cvlc"
    else :
        VLC_OPT += " " + opt + " " + arg

def SetConfig(argv):
    """ Args treatment """
    try:
        opts, args = getopt.getopt(argv, usage.GetOpts(), [*usage.GetOptsValue(), "help"])
    except Exception as ex:
        print(ex)
        usage.usage()
        sys.exit(2)
    try:
        global VLC_OPT, TAG
        for opt, arg in opts:
            if opt == "--help" :
                usage.usage()
                sys.exit()
            if opt in ("-h", "--host"):
                global HOST
                HOST = arg
            elif opt in ("-S", "--socket"):
                global SOCKET
                SOCKET = int(arg)
            elif opt in ("-N", "--no-synchro") :
                global SYNCHRO
                SYNCHRO = False
            elif opt in ("-n", "--tag") :
                global TAG
                TAG = arg
            elif opt in ("-f", "--fullscreen") :
                VLC_OPT += " -f"
            elif opt in ("--crop") :
                VLC_OPT += " --crop " + arg
            elif opt in ("-v", "--verbose") :
                global OUTPUT_CMD
                OUTPUT_CMD = ""
            elif opt in ("--no-exit") :
                VLC_OPT = VLC_OPT.replace("--play-and-exit", "")
            elif opt == "--cvlc" :
                global VLC_CMD
                VLC_CMD = "cvlc"
            elif opt == "--sample" :
                global SAMPLE
                SAMPLE = True
                VLC_OPT += " -Z --no-osd"
            elif opt in ("-A", "--audio") :
                global AUDIO_MASTER
                AUDIO_MASTER = True
            else :
                VLC_OPT += " " + opt + " " + arg
        global VLC_ARG
        for arg in args :
            VLC_ARG += " \"" + os.path.realpath(arg) + "\""
    except Exception as ex:
        print(ex)
        usage.usage()
        sys.exit(2)


if __name__ == "__main__":
    try :
        SetConfig(sys.argv[1:])
        DbusMonitor(HOST, SOCKET, tag_new = TAG, tag_read = "" )
    except Exception as e:
        print("FAIL !\n" + str(e))

# try :
#     proc_list = []
#
#     JUMP_MODE = -1
#     # SetConfig(sys.argv[1:])
#     VLC_OPT += " --no-osd"
#
#     # URL = "http://" + HOST + ":" + PORT + "/jsonrpc"
#     # PATH2REMOVE = "nfs://" + HOST + PATH2REMOVEROOT
#     # myKodi = kodiUtils.KodiJson(HOST, USER , PASS, PORT)
#     #
#     #
#     # if HOST != myKodi.ADDR :
#     #     socket = QuerySocket(HOST, SOCKET)
#     # else :
#     #     socket = QuerySocket(myKodi.ADDR, SOCKET)
#     # print(JUMP_MODE)
#     DbusMonitor().check_set_vid(int(JUMP_MODE))
#
# except Exception as e:
#     print("FAIL !\n" + str(e))
#     if DEBUG :
#         if e.errno == os.errno.EsNOENT:
#             sys.exit(e.errno)
#
# finally :
#     if len(proc_list) > 0 :
#         if proc_list[0].is_alive() :
#             proc_list[0].join(1)
#         proc_list.pop(0)
